# Wiki

The purpose of this file is to add some additional documentation of how
to use this API client.

* [Test examples](#test-examples)
* [How to upload files](#file-uploads)
* [Asynchronous functions](#asynchronous-functions)


## Test examples

Here some test examples to follow:

* [Get API instance](own_test/rs_login_helper_location_test.dart)
* [Login](own_test/rs_login_helper_location_test.dart): with your location
  account id and password. 
* [Test unauthenticated method](own_test/req_json_api_server_version_test.dart):
  this test points to `/jsonApiServer/version`, accessible without auth.
* [Add autorized token](own_test/add_autorized_token_test.dart)
* [Test authenticated method](own_test/req_rs_gxs_channels_get_channels_summaries_test.dart):
  points to `/rsGxsChannels/getChannelsSummaries`, needed auth.
* Create channel 
  [with deprecated method](own_test/req_rs_gxs_channels_create_channel_test.dart)(not supported anymore)
  and [v2 method](own_test/req_rs_gxs_channels_create_channel_v2_test.dart).
* [Get channels info](own_test/req_rs_gxs_channels_get_channels_info_test.dart).
* [Add shared directory](own_test/req_rs_files_add_shared_directory_test.dart).
* [Create a post with files]:
  this have also an example of how to loop through channel summaries.
* [Create an identity](own_test/req_rs_identity_create_identity_test.dart) or
  [update it].
* [Modify a post and add a thumbnail].
  Here we can see the process to add a base64 image.
  
#### Testing return types

This tests are used to test that the return types values are correct:

* [Test map<key, value>]
* [Test pair<first, second>]
  
## File uploads

For file uploads you have two ways:

1. [Add shared directory](own_test/req_rs_files_add_shared_directory_test.dart)
   and then play with `/rsFiles/getSharedDirectories`,
   `/rsFiles/setSharedDirectories` and `/rsFiles/addSharedDirectory` to
   get the SHA1 generated by RetroShare. Otherwise you can generate your
   own SHA1 following the example on the test
   [create a post with files]
   
2. Add an independent file playing with `/rsFiles/ExtraFileHash` and
   `/rsFiles/ExtraFileRemove`.
   
## Asynchronous functions

Not yet supported, see
https://github.com/OpenAPITools/openapi-generator/issues/3696 .