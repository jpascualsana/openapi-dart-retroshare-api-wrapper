part of openapi.api;

class QueryParam {
  String name;
  String value;

  QueryParam(this.name, this.value);
}

class ApiClient {

  String basePath;
  var client = Client();

  Map<String, String> _defaultHeaderMap = {};
  Map<String, Authentication> _authentications = {};

  final _regList = RegExp(r'^List<(.*)>$');
  final _regMap = RegExp(r'^Map<String,(.*)>$');

  ApiClient({this.basePath = "http://127.0.0.1:9092"}) {
    // Setup authentications (key: authentication name, value: authentication).
    _authentications['BasicAuth'] = HttpBasicAuth();
  }

  void addDefaultHeader(String key, String value) {
     _defaultHeaderMap[key] = value;
  }

  dynamic _deserialize(dynamic value, String targetType) {
    try {
      switch (targetType) {
        case 'String':
          return '$value';
        case 'int':
          return value is int ? value : int.parse('$value');
        case 'bool':
          return value is bool ? value : '$value'.toLowerCase() == 'true';
        case 'double':
          return value is double ? value : double.parse('$value');
        case 'BannedFileEntry':
          return BannedFileEntry.fromJson(value);
        case 'ChatId':
          return ChatId.fromJson(value);
        case 'ChatIdType':
          return new ChatIdTypeTypeTransformer().decode(value);
        case 'ChatLobbyId':
          return ChatLobbyId.fromJson(value);
        case 'ChatLobbyInfo':
          return ChatLobbyInfo.fromJson(value);
        case 'ChatLobbyInfoGxsIds':
          return ChatLobbyInfoGxsIds.fromJson(value);
        case 'ChatLobbyInvite':
          return ChatLobbyInvite.fromJson(value);
        case 'ChunkState':
          return new ChunkStateTypeTransformer().decode(value);
        case 'CompressedChunkMap':
          return CompressedChunkMap.fromJson(value);
        case 'DirDetails':
          return DirDetails.fromJson(value);
        case 'DirStub':
          return DirStub.fromJson(value);
        case 'DistantChatPeerInfo':
          return DistantChatPeerInfo.fromJson(value);
        case 'DwlSpeed':
          return new DwlSpeedTypeTransformer().decode(value);
        case 'FileChunksInfo':
          return FileChunksInfo.fromJson(value);
        case 'FileChunksInfoActiveChunks':
          return FileChunksInfoActiveChunks.fromJson(value);
        case 'FileChunksInfoChunkStrategy':
          return new FileChunksInfoChunkStrategyTypeTransformer().decode(value);
        case 'FileChunksInfoCompressedPeerAvailabilityMaps':
          return FileChunksInfoCompressedPeerAvailabilityMaps.fromJson(value);
        case 'FileChunksInfoPendingSlices':
          return FileChunksInfoPendingSlices.fromJson(value);
        case 'FileChunksInfoSliceInfo':
          return FileChunksInfoSliceInfo.fromJson(value);
        case 'FileInfo':
          return FileInfo.fromJson(value);
        case 'FileRequestFlags':
          return new FileRequestFlagsTypeTransformer().decode(value);
        case 'GxsGroupStatistic':
          return GxsGroupStatistic.fromJson(value);
        case 'GxsReputation':
          return GxsReputation.fromJson(value);
        case 'GxsServiceStatistic':
          return GxsServiceStatistic.fromJson(value);
        case 'RSTrafficClue':
          return RSTrafficClue.fromJson(value);
        case 'ReqAcceptInvite':
          return ReqAcceptInvite.fromJson(value);
        case 'ReqAcceptLobbyInvite':
          return ReqAcceptLobbyInvite.fromJson(value);
        case 'ReqAddFriend':
          return ReqAddFriend.fromJson(value);
        case 'ReqAddGroup':
          return ReqAddGroup.fromJson(value);
        case 'ReqAddPeerLocator':
          return ReqAddPeerLocator.fromJson(value);
        case 'ReqAddSharedDirectory':
          return ReqAddSharedDirectory.fromJson(value);
        case 'ReqAddSslOnlyFriend':
          return ReqAddSslOnlyFriend.fromJson(value);
        case 'ReqAlreadyHaveFile':
          return ReqAlreadyHaveFile.fromJson(value);
        case 'ReqAssignPeerToGroup':
          return ReqAssignPeerToGroup.fromJson(value);
        case 'ReqAssignPeersToGroup':
          return ReqAssignPeersToGroup.fromJson(value);
        case 'ReqAttemptLogin':
          return ReqAttemptLogin.fromJson(value);
        case 'ReqAuthorizeUser':
          return ReqAuthorizeUser.fromJson(value);
        case 'ReqBanFile':
          return ReqBanFile.fromJson(value);
        case 'ReqBanFileFileSize':
          return ReqBanFileFileSize.fromJson(value);
        case 'ReqBanNode':
          return ReqBanNode.fromJson(value);
        case 'ReqCancelCircleMembership':
          return ReqCancelCircleMembership.fromJson(value);
        case 'ReqClearChatLobby':
          return ReqClearChatLobby.fromJson(value);
        case 'ReqCloseDistantChatConnexion':
          return ReqCloseDistantChatConnexion.fromJson(value);
        case 'ReqCollectEntropy':
          return ReqCollectEntropy.fromJson(value);
        case 'ReqConnectAttempt':
          return ReqConnectAttempt.fromJson(value);
        case 'ReqCreateChannel':
          return ReqCreateChannel.fromJson(value);
        case 'ReqCreateChannelV2':
          return ReqCreateChannelV2.fromJson(value);
        case 'ReqCreateChatLobby':
          return ReqCreateChatLobby.fromJson(value);
        case 'ReqCreateCircle':
          return ReqCreateCircle.fromJson(value);
        case 'ReqCreateComment':
          return ReqCreateComment.fromJson(value);
        case 'ReqCreateCommentV2':
          return ReqCreateCommentV2.fromJson(value);
        case 'ReqCreateForum':
          return ReqCreateForum.fromJson(value);
        case 'ReqCreateForumV2':
          return ReqCreateForumV2.fromJson(value);
        case 'ReqCreateIdentity':
          return ReqCreateIdentity.fromJson(value);
        case 'ReqCreateLocation':
          return ReqCreateLocation.fromJson(value);
        case 'ReqCreateMessage':
          return ReqCreateMessage.fromJson(value);
        case 'ReqCreatePost':
          return ReqCreatePost.fromJson(value);
        case 'ReqCreatePostV2':
          return ReqCreatePostV2.fromJson(value);
        case 'ReqCreateVote':
          return ReqCreateVote.fromJson(value);
        case 'ReqCreateVoteV2':
          return ReqCreateVoteV2.fromJson(value);
        case 'ReqDeleteIdentity':
          return ReqDeleteIdentity.fromJson(value);
        case 'ReqDenyLobbyInvite':
          return ReqDenyLobbyInvite.fromJson(value);
        case 'ReqEditChannel':
          return ReqEditChannel.fromJson(value);
        case 'ReqEditCircle':
          return ReqEditCircle.fromJson(value);
        case 'ReqEditForum':
          return ReqEditForum.fromJson(value);
        case 'ReqEditGroup':
          return ReqEditGroup.fromJson(value);
        case 'ReqEnableIPFiltering':
          return ReqEnableIPFiltering.fromJson(value);
        case 'ReqExportChannelLink':
          return ReqExportChannelLink.fromJson(value);
        case 'ReqExportCircleLink':
          return ReqExportCircleLink.fromJson(value);
        case 'ReqExportCollectionLink':
          return ReqExportCollectionLink.fromJson(value);
        case 'ReqExportFileLink':
          return ReqExportFileLink.fromJson(value);
        case 'ReqExportForumLink':
          return ReqExportForumLink.fromJson(value);
        case 'ReqExportIdentity':
          return ReqExportIdentity.fromJson(value);
        case 'ReqExportIdentityLink':
          return ReqExportIdentityLink.fromJson(value);
        case 'ReqExportIdentityToString':
          return ReqExportIdentityToString.fromJson(value);
        case 'ReqExtraFileHash':
          return ReqExtraFileHash.fromJson(value);
        case 'ReqExtraFileRemove':
          return ReqExtraFileRemove.fromJson(value);
        case 'ReqExtraFileStatus':
          return ReqExtraFileStatus.fromJson(value);
        case 'ReqFileCancel':
          return ReqFileCancel.fromJson(value);
        case 'ReqFileControl':
          return ReqFileControl.fromJson(value);
        case 'ReqFileDetails':
          return ReqFileDetails.fromJson(value);
        case 'ReqFileDownloadChunksDetails':
          return ReqFileDownloadChunksDetails.fromJson(value);
        case 'ReqFileRequest':
          return ReqFileRequest.fromJson(value);
        case 'ReqFileUploadChunksDetails':
          return ReqFileUploadChunksDetails.fromJson(value);
        case 'ReqForceDirectoryCheck':
          return ReqForceDirectoryCheck.fromJson(value);
        case 'ReqGetChannelAllContent':
          return ReqGetChannelAllContent.fromJson(value);
        case 'ReqGetChannelAutoDownload':
          return ReqGetChannelAutoDownload.fromJson(value);
        case 'ReqGetChannelComments':
          return ReqGetChannelComments.fromJson(value);
        case 'ReqGetChannelContent':
          return ReqGetChannelContent.fromJson(value);
        case 'ReqGetChannelDownloadDirectory':
          return ReqGetChannelDownloadDirectory.fromJson(value);
        case 'ReqGetChannelStatistics':
          return ReqGetChannelStatistics.fromJson(value);
        case 'ReqGetChannelsInfo':
          return ReqGetChannelsInfo.fromJson(value);
        case 'ReqGetChatLobbyInfo':
          return ReqGetChatLobbyInfo.fromJson(value);
        case 'ReqGetCircleDetails':
          return ReqGetCircleDetails.fromJson(value);
        case 'ReqGetCircleExternalIdList':
          return ReqGetCircleExternalIdList.fromJson(value);
        case 'ReqGetCircleRequest':
          return ReqGetCircleRequest.fromJson(value);
        case 'ReqGetCircleRequests':
          return ReqGetCircleRequests.fromJson(value);
        case 'ReqGetCirclesInfo':
          return ReqGetCirclesInfo.fromJson(value);
        case 'ReqGetContentSummaries':
          return ReqGetContentSummaries.fromJson(value);
        case 'ReqGetCustomStateString':
          return ReqGetCustomStateString.fromJson(value);
        case 'ReqGetDiscFriends':
          return ReqGetDiscFriends.fromJson(value);
        case 'ReqGetDiscPgpFriends':
          return ReqGetDiscPgpFriends.fromJson(value);
        case 'ReqGetDistantChatStatus':
          return ReqGetDistantChatStatus.fromJson(value);
        case 'ReqGetFileData':
          return ReqGetFileData.fromJson(value);
        case 'ReqGetForumContent':
          return ReqGetForumContent.fromJson(value);
        case 'ReqGetForumMsgMetaData':
          return ReqGetForumMsgMetaData.fromJson(value);
        case 'ReqGetForumStatistics':
          return ReqGetForumStatistics.fromJson(value);
        case 'ReqGetForumsInfo':
          return ReqGetForumsInfo.fromJson(value);
        case 'ReqGetGPGId':
          return ReqGetGPGId.fromJson(value);
        case 'ReqGetGroupInfo':
          return ReqGetGroupInfo.fromJson(value);
        case 'ReqGetGroupInfoByName':
          return ReqGetGroupInfoByName.fromJson(value);
        case 'ReqGetIdDetails':
          return ReqGetIdDetails.fromJson(value);
        case 'ReqGetIdentitiesInfo':
          return ReqGetIdentitiesInfo.fromJson(value);
        case 'ReqGetIdentityForChatLobby':
          return ReqGetIdentityForChatLobby.fromJson(value);
        case 'ReqGetLastUsageTS':
          return ReqGetLastUsageTS.fromJson(value);
        case 'ReqGetLobbyAutoSubscribe':
          return ReqGetLobbyAutoSubscribe.fromJson(value);
        case 'ReqGetMaxMessageSecuritySize':
          return ReqGetMaxMessageSecuritySize.fromJson(value);
        case 'ReqGetMessage':
          return ReqGetMessage.fromJson(value);
        case 'ReqGetMessageTag':
          return ReqGetMessageTag.fromJson(value);
        case 'ReqGetMsgParentId':
          return ReqGetMsgParentId.fromJson(value);
        case 'ReqGetOwnOpinion':
          return ReqGetOwnOpinion.fromJson(value);
        case 'ReqGetPeerDetails':
          return ReqGetPeerDetails.fromJson(value);
        case 'ReqGetPeerVersion':
          return ReqGetPeerVersion.fromJson(value);
        case 'ReqGetPeersConnected':
          return ReqGetPeersConnected.fromJson(value);
        case 'ReqGetPeersCount':
          return ReqGetPeersCount.fromJson(value);
        case 'ReqGetReputationInfo':
          return ReqGetReputationInfo.fromJson(value);
        case 'ReqGetRetroshareInvite':
          return ReqGetRetroshareInvite.fromJson(value);
        case 'ReqGetServiceItemNames':
          return ReqGetServiceItemNames.fromJson(value);
        case 'ReqGetServiceName':
          return ReqGetServiceName.fromJson(value);
        case 'ReqGetServicePermissions':
          return ReqGetServicePermissions.fromJson(value);
        case 'ReqGetServicesAllowed':
          return ReqGetServicesAllowed.fromJson(value);
        case 'ReqGetServicesProvided':
          return ReqGetServicesProvided.fromJson(value);
        case 'ReqGetShortInvite':
          return ReqGetShortInvite.fromJson(value);
        case 'ReqImportIdentity':
          return ReqImportIdentity.fromJson(value);
        case 'ReqImportIdentityFromString':
          return ReqImportIdentityFromString.fromJson(value);
        case 'ReqInitiateDistantChatConnexion':
          return ReqInitiateDistantChatConnexion.fromJson(value);
        case 'ReqInviteIdsToCircle':
          return ReqInviteIdsToCircle.fromJson(value);
        case 'ReqInvitePeerToLobby':
          return ReqInvitePeerToLobby.fromJson(value);
        case 'ReqIsARegularContact':
          return ReqIsARegularContact.fromJson(value);
        case 'ReqIsAuthTokenValid':
          return ReqIsAuthTokenValid.fromJson(value);
        case 'ReqIsFriend':
          return ReqIsFriend.fromJson(value);
        case 'ReqIsHashBanned':
          return ReqIsHashBanned.fromJson(value);
        case 'ReqIsIdentityBanned':
          return ReqIsIdentityBanned.fromJson(value);
        case 'ReqIsKnownId':
          return ReqIsKnownId.fromJson(value);
        case 'ReqIsNodeBanned':
          return ReqIsNodeBanned.fromJson(value);
        case 'ReqIsOnline':
          return ReqIsOnline.fromJson(value);
        case 'ReqIsOwnId':
          return ReqIsOwnId.fromJson(value);
        case 'ReqIsPgpFriend':
          return ReqIsPgpFriend.fromJson(value);
        case 'ReqIsSslOnlyFriend':
          return ReqIsSslOnlyFriend.fromJson(value);
        case 'ReqJoinVisibleChatLobby':
          return ReqJoinVisibleChatLobby.fromJson(value);
        case 'ReqLoadCertificateFromString':
          return ReqLoadCertificateFromString.fromJson(value);
        case 'ReqLoadDetailsFromStringCert':
          return ReqLoadDetailsFromStringCert.fromJson(value);
        case 'ReqLocalSearchRequest':
          return ReqLocalSearchRequest.fromJson(value);
        case 'ReqMarkRead':
          return ReqMarkRead.fromJson(value);
        case 'ReqMessageDelete':
          return ReqMessageDelete.fromJson(value);
        case 'ReqMessageForwarded':
          return ReqMessageForwarded.fromJson(value);
        case 'ReqMessageJunk':
          return ReqMessageJunk.fromJson(value);
        case 'ReqMessageLoadEmbeddedImages':
          return ReqMessageLoadEmbeddedImages.fromJson(value);
        case 'ReqMessageRead':
          return ReqMessageRead.fromJson(value);
        case 'ReqMessageReplied':
          return ReqMessageReplied.fromJson(value);
        case 'ReqMessageSend':
          return ReqMessageSend.fromJson(value);
        case 'ReqMessageStar':
          return ReqMessageStar.fromJson(value);
        case 'ReqMessageToDraft':
          return ReqMessageToDraft.fromJson(value);
        case 'ReqMessageToTrash':
          return ReqMessageToTrash.fromJson(value);
        case 'ReqOverallReputationLevel':
          return ReqOverallReputationLevel.fromJson(value);
        case 'ReqParseFilesLink':
          return ReqParseFilesLink.fromJson(value);
        case 'ReqParseShortInvite':
          return ReqParseShortInvite.fromJson(value);
        case 'ReqPgpIdFromFingerprint':
          return ReqPgpIdFromFingerprint.fromJson(value);
        case 'ReqRemoveFriend':
          return ReqRemoveFriend.fromJson(value);
        case 'ReqRemoveFriendLocation':
          return ReqRemoveFriendLocation.fromJson(value);
        case 'ReqRemoveGroup':
          return ReqRemoveGroup.fromJson(value);
        case 'ReqRemoveMessageTagType':
          return ReqRemoveMessageTagType.fromJson(value);
        case 'ReqRemoveSharedDirectory':
          return ReqRemoveSharedDirectory.fromJson(value);
        case 'ReqRequestCircleMembership':
          return ReqRequestCircleMembership.fromJson(value);
        case 'ReqRequestDirDetails':
          return ReqRequestDirDetails.fromJson(value);
        case 'ReqRequestFiles':
          return ReqRequestFiles.fromJson(value);
        case 'ReqRequestIdentity':
          return ReqRequestIdentity.fromJson(value);
        case 'ReqRequestNewTokenAutorization':
          return ReqRequestNewTokenAutorization.fromJson(value);
        case 'ReqRequestStatus':
          return ReqRequestStatus.fromJson(value);
        case 'ReqRevokeAuthToken':
          return ReqRevokeAuthToken.fromJson(value);
        case 'ReqRevokeIdsFromCircle':
          return ReqRevokeIdsFromCircle.fromJson(value);
        case 'ReqSendChat':
          return ReqSendChat.fromJson(value);
        case 'ReqSendLobbyStatusPeerLeaving':
          return ReqSendLobbyStatusPeerLeaving.fromJson(value);
        case 'ReqSendMail':
          return ReqSendMail.fromJson(value);
        case 'ReqSendStatusString':
          return ReqSendStatusString.fromJson(value);
        case 'ReqSetAsRegularContact':
          return ReqSetAsRegularContact.fromJson(value);
        case 'ReqSetAutoAddFriendIdsAsContact':
          return ReqSetAutoAddFriendIdsAsContact.fromJson(value);
        case 'ReqSetAutoPositiveOpinionForContacts':
          return ReqSetAutoPositiveOpinionForContacts.fromJson(value);
        case 'ReqSetBindingAddress':
          return ReqSetBindingAddress.fromJson(value);
        case 'ReqSetChannelAutoDownload':
          return ReqSetChannelAutoDownload.fromJson(value);
        case 'ReqSetChannelDownloadDirectory':
          return ReqSetChannelDownloadDirectory.fromJson(value);
        case 'ReqSetChunkStrategy':
          return ReqSetChunkStrategy.fromJson(value);
        case 'ReqSetCustomStateString':
          return ReqSetCustomStateString.fromJson(value);
        case 'ReqSetDefaultChunkStrategy':
          return ReqSetDefaultChunkStrategy.fromJson(value);
        case 'ReqSetDefaultIdentityForChatLobby':
          return ReqSetDefaultIdentityForChatLobby.fromJson(value);
        case 'ReqSetDeleteBannedNodesThreshold':
          return ReqSetDeleteBannedNodesThreshold.fromJson(value);
        case 'ReqSetDestinationDirectory':
          return ReqSetDestinationDirectory.fromJson(value);
        case 'ReqSetDestinationName':
          return ReqSetDestinationName.fromJson(value);
        case 'ReqSetDownloadDirectory':
          return ReqSetDownloadDirectory.fromJson(value);
        case 'ReqSetDynDNS':
          return ReqSetDynDNS.fromJson(value);
        case 'ReqSetExtAddress':
          return ReqSetExtAddress.fromJson(value);
        case 'ReqSetFreeDiskSpaceLimit':
          return ReqSetFreeDiskSpaceLimit.fromJson(value);
        case 'ReqSetIdentityForChatLobby':
          return ReqSetIdentityForChatLobby.fromJson(value);
        case 'ReqSetListeningPort':
          return ReqSetListeningPort.fromJson(value);
        case 'ReqSetLobbyAutoSubscribe':
          return ReqSetLobbyAutoSubscribe.fromJson(value);
        case 'ReqSetLocalAddress':
          return ReqSetLocalAddress.fromJson(value);
        case 'ReqSetMaxDataRates':
          return ReqSetMaxDataRates.fromJson(value);
        case 'ReqSetMessageTag':
          return ReqSetMessageTag.fromJson(value);
        case 'ReqSetMessageTagType':
          return ReqSetMessageTagType.fromJson(value);
        case 'ReqSetNetworkMode':
          return ReqSetNetworkMode.fromJson(value);
        case 'ReqSetOperatingMode':
          return ReqSetOperatingMode.fromJson(value);
        case 'ReqSetOwnOpinion':
          return ReqSetOwnOpinion.fromJson(value);
        case 'ReqSetPartialsDirectory':
          return ReqSetPartialsDirectory.fromJson(value);
        case 'ReqSetRememberBannedIdThreshold':
          return ReqSetRememberBannedIdThreshold.fromJson(value);
        case 'ReqSetSharedDirectories':
          return ReqSetSharedDirectories.fromJson(value);
        case 'ReqSetThresholdForRemotelyNegativeReputation':
          return ReqSetThresholdForRemotelyNegativeReputation.fromJson(value);
        case 'ReqSetThresholdForRemotelyPositiveReputation':
          return ReqSetThresholdForRemotelyPositiveReputation.fromJson(value);
        case 'ReqSetVisState':
          return ReqSetVisState.fromJson(value);
        case 'ReqShareChannelKeys':
          return ReqShareChannelKeys.fromJson(value);
        case 'ReqSubscribeToChannel':
          return ReqSubscribeToChannel.fromJson(value);
        case 'ReqSubscribeToForum':
          return ReqSubscribeToForum.fromJson(value);
        case 'ReqSystemMessage':
          return ReqSystemMessage.fromJson(value);
        case 'ReqTurtleChannelRequest':
          return ReqTurtleChannelRequest.fromJson(value);
        case 'ReqTurtleSearchRequest':
          return ReqTurtleSearchRequest.fromJson(value);
        case 'ReqUnbanFile':
          return ReqUnbanFile.fromJson(value);
        case 'ReqUnsubscribeChatLobby':
          return ReqUnsubscribeChatLobby.fromJson(value);
        case 'ReqUpdateIdentity':
          return ReqUpdateIdentity.fromJson(value);
        case 'ReqUpdateServicePermissions':
          return ReqUpdateServicePermissions.fromJson(value);
        case 'ReqUpdateShareFlags':
          return ReqUpdateShareFlags.fromJson(value);
        case 'ResAcceptInvite':
          return ResAcceptInvite.fromJson(value);
        case 'ResAcceptLobbyInvite':
          return ResAcceptLobbyInvite.fromJson(value);
        case 'ResAddFriend':
          return ResAddFriend.fromJson(value);
        case 'ResAddGroup':
          return ResAddGroup.fromJson(value);
        case 'ResAddPeerLocator':
          return ResAddPeerLocator.fromJson(value);
        case 'ResAddSharedDirectory':
          return ResAddSharedDirectory.fromJson(value);
        case 'ResAddSslOnlyFriend':
          return ResAddSslOnlyFriend.fromJson(value);
        case 'ResAlreadyHaveFile':
          return ResAlreadyHaveFile.fromJson(value);
        case 'ResAssignPeerToGroup':
          return ResAssignPeerToGroup.fromJson(value);
        case 'ResAssignPeersToGroup':
          return ResAssignPeersToGroup.fromJson(value);
        case 'ResAttemptLogin':
          return ResAttemptLogin.fromJson(value);
        case 'ResAuthorizeUser':
          return ResAuthorizeUser.fromJson(value);
        case 'ResAutoAddFriendIdsAsContact':
          return ResAutoAddFriendIdsAsContact.fromJson(value);
        case 'ResAutoPositiveOpinionForContacts':
          return ResAutoPositiveOpinionForContacts.fromJson(value);
        case 'ResBanFile':
          return ResBanFile.fromJson(value);
        case 'ResCancelCircleMembership':
          return ResCancelCircleMembership.fromJson(value);
        case 'ResCloseDistantChatConnexion':
          return ResCloseDistantChatConnexion.fromJson(value);
        case 'ResCollectEntropy':
          return ResCollectEntropy.fromJson(value);
        case 'ResConnectAttempt':
          return ResConnectAttempt.fromJson(value);
        case 'ResCreateChannel':
          return ResCreateChannel.fromJson(value);
        case 'ResCreateChannelV2':
          return ResCreateChannelV2.fromJson(value);
        case 'ResCreateChatLobby':
          return ResCreateChatLobby.fromJson(value);
        case 'ResCreateCircle':
          return ResCreateCircle.fromJson(value);
        case 'ResCreateComment':
          return ResCreateComment.fromJson(value);
        case 'ResCreateCommentV2':
          return ResCreateCommentV2.fromJson(value);
        case 'ResCreateForum':
          return ResCreateForum.fromJson(value);
        case 'ResCreateForumV2':
          return ResCreateForumV2.fromJson(value);
        case 'ResCreateIdentity':
          return ResCreateIdentity.fromJson(value);
        case 'ResCreateLocation':
          return ResCreateLocation.fromJson(value);
        case 'ResCreateMessage':
          return ResCreateMessage.fromJson(value);
        case 'ResCreatePost':
          return ResCreatePost.fromJson(value);
        case 'ResCreatePostV2':
          return ResCreatePostV2.fromJson(value);
        case 'ResCreateVote':
          return ResCreateVote.fromJson(value);
        case 'ResCreateVoteV2':
          return ResCreateVoteV2.fromJson(value);
        case 'ResDefaultChunkStrategy':
          return ResDefaultChunkStrategy.fromJson(value);
        case 'ResDeleteBannedNodesThreshold':
          return ResDeleteBannedNodesThreshold.fromJson(value);
        case 'ResDeleteIdentity':
          return ResDeleteIdentity.fromJson(value);
        case 'ResDisableMulticastListening':
          return ResDisableMulticastListening.fromJson(value);
        case 'ResEditChannel':
          return ResEditChannel.fromJson(value);
        case 'ResEditCircle':
          return ResEditCircle.fromJson(value);
        case 'ResEditForum':
          return ResEditForum.fromJson(value);
        case 'ResEditGroup':
          return ResEditGroup.fromJson(value);
        case 'ResEnableMulticastListening':
          return ResEnableMulticastListening.fromJson(value);
        case 'ResExportChannelLink':
          return ResExportChannelLink.fromJson(value);
        case 'ResExportCircleLink':
          return ResExportCircleLink.fromJson(value);
        case 'ResExportCollectionLink':
          return ResExportCollectionLink.fromJson(value);
        case 'ResExportCollectionLinkRetval':
          return ResExportCollectionLinkRetval.fromJson(value);
        case 'ResExportFileLink':
          return ResExportFileLink.fromJson(value);
        case 'ResExportForumLink':
          return ResExportForumLink.fromJson(value);
        case 'ResExportIdentity':
          return ResExportIdentity.fromJson(value);
        case 'ResExportIdentityLink':
          return ResExportIdentityLink.fromJson(value);
        case 'ResExportIdentityToString':
          return ResExportIdentityToString.fromJson(value);
        case 'ResExtraFileHash':
          return ResExtraFileHash.fromJson(value);
        case 'ResExtraFileRemove':
          return ResExtraFileRemove.fromJson(value);
        case 'ResExtraFileStatus':
          return ResExtraFileStatus.fromJson(value);
        case 'ResFileCancel':
          return ResFileCancel.fromJson(value);
        case 'ResFileClearCompleted':
          return ResFileClearCompleted.fromJson(value);
        case 'ResFileControl':
          return ResFileControl.fromJson(value);
        case 'ResFileDetails':
          return ResFileDetails.fromJson(value);
        case 'ResFileDownloadChunksDetails':
          return ResFileDownloadChunksDetails.fromJson(value);
        case 'ResFileDownloads':
          return ResFileDownloads.fromJson(value);
        case 'ResFileRequest':
          return ResFileRequest.fromJson(value);
        case 'ResFileUploadChunksDetails':
          return ResFileUploadChunksDetails.fromJson(value);
        case 'ResFileUploads':
          return ResFileUploads.fromJson(value);
        case 'ResFreeDiskSpaceLimit':
          return ResFreeDiskSpaceLimit.fromJson(value);
        case 'ResGetAllBandwidthRates':
          return ResGetAllBandwidthRates.fromJson(value);
        case 'ResGetAllBandwidthRatesRatemap':
          return ResGetAllBandwidthRatesRatemap.fromJson(value);
        case 'ResGetAuthorizedTokens':
          return ResGetAuthorizedTokens.fromJson(value);
        case 'ResGetAuthorizedTokensRetval':
          return ResGetAuthorizedTokensRetval.fromJson(value);
        case 'ResGetBindingAddress':
          return ResGetBindingAddress.fromJson(value);
        case 'ResGetChannelAllContent':
          return ResGetChannelAllContent.fromJson(value);
        case 'ResGetChannelAutoDownload':
          return ResGetChannelAutoDownload.fromJson(value);
        case 'ResGetChannelComments':
          return ResGetChannelComments.fromJson(value);
        case 'ResGetChannelContent':
          return ResGetChannelContent.fromJson(value);
        case 'ResGetChannelDownloadDirectory':
          return ResGetChannelDownloadDirectory.fromJson(value);
        case 'ResGetChannelServiceStatistics':
          return ResGetChannelServiceStatistics.fromJson(value);
        case 'ResGetChannelStatistics':
          return ResGetChannelStatistics.fromJson(value);
        case 'ResGetChannelsInfo':
          return ResGetChannelsInfo.fromJson(value);
        case 'ResGetChannelsSummaries':
          return ResGetChannelsSummaries.fromJson(value);
        case 'ResGetChatLobbyInfo':
          return ResGetChatLobbyInfo.fromJson(value);
        case 'ResGetChatLobbyList':
          return ResGetChatLobbyList.fromJson(value);
        case 'ResGetCircleDetails':
          return ResGetCircleDetails.fromJson(value);
        case 'ResGetCircleExternalIdList':
          return ResGetCircleExternalIdList.fromJson(value);
        case 'ResGetCircleRequest':
          return ResGetCircleRequest.fromJson(value);
        case 'ResGetCircleRequests':
          return ResGetCircleRequests.fromJson(value);
        case 'ResGetCirclesInfo':
          return ResGetCirclesInfo.fromJson(value);
        case 'ResGetCirclesSummaries':
          return ResGetCirclesSummaries.fromJson(value);
        case 'ResGetConfigNetStatus':
          return ResGetConfigNetStatus.fromJson(value);
        case 'ResGetContentSummaries':
          return ResGetContentSummaries.fromJson(value);
        case 'ResGetCurrentAccountId':
          return ResGetCurrentAccountId.fromJson(value);
        case 'ResGetCurrentDataRates':
          return ResGetCurrentDataRates.fromJson(value);
        case 'ResGetCustomStateString':
          return ResGetCustomStateString.fromJson(value);
        case 'ResGetDefaultIdentityForChatLobby':
          return ResGetDefaultIdentityForChatLobby.fromJson(value);
        case 'ResGetDiscFriends':
          return ResGetDiscFriends.fromJson(value);
        case 'ResGetDiscPgpFriends':
          return ResGetDiscPgpFriends.fromJson(value);
        case 'ResGetDiscoveredPeers':
          return ResGetDiscoveredPeers.fromJson(value);
        case 'ResGetDistantChatStatus':
          return ResGetDistantChatStatus.fromJson(value);
        case 'ResGetDownloadDirectory':
          return ResGetDownloadDirectory.fromJson(value);
        case 'ResGetFileData':
          return ResGetFileData.fromJson(value);
        case 'ResGetForumContent':
          return ResGetForumContent.fromJson(value);
        case 'ResGetForumMsgMetaData':
          return ResGetForumMsgMetaData.fromJson(value);
        case 'ResGetForumServiceStatistics':
          return ResGetForumServiceStatistics.fromJson(value);
        case 'ResGetForumStatistics':
          return ResGetForumStatistics.fromJson(value);
        case 'ResGetForumsInfo':
          return ResGetForumsInfo.fromJson(value);
        case 'ResGetForumsSummaries':
          return ResGetForumsSummaries.fromJson(value);
        case 'ResGetFriendList':
          return ResGetFriendList.fromJson(value);
        case 'ResGetGPGId':
          return ResGetGPGId.fromJson(value);
        case 'ResGetGroupInfo':
          return ResGetGroupInfo.fromJson(value);
        case 'ResGetGroupInfoByName':
          return ResGetGroupInfoByName.fromJson(value);
        case 'ResGetGroupInfoList':
          return ResGetGroupInfoList.fromJson(value);
        case 'ResGetIdDetails':
          return ResGetIdDetails.fromJson(value);
        case 'ResGetIdentitiesInfo':
          return ResGetIdentitiesInfo.fromJson(value);
        case 'ResGetIdentitiesSummaries':
          return ResGetIdentitiesSummaries.fromJson(value);
        case 'ResGetIdentityForChatLobby':
          return ResGetIdentityForChatLobby.fromJson(value);
        case 'ResGetLastUsageTS':
          return ResGetLastUsageTS.fromJson(value);
        case 'ResGetListOfNearbyChatLobbies':
          return ResGetListOfNearbyChatLobbies.fromJson(value);
        case 'ResGetLobbyAutoSubscribe':
          return ResGetLobbyAutoSubscribe.fromJson(value);
        case 'ResGetLocations':
          return ResGetLocations.fromJson(value);
        case 'ResGetMaxDataRates':
          return ResGetMaxDataRates.fromJson(value);
        case 'ResGetMaxMessageSecuritySize':
          return ResGetMaxMessageSecuritySize.fromJson(value);
        case 'ResGetMessage':
          return ResGetMessage.fromJson(value);
        case 'ResGetMessageCount':
          return ResGetMessageCount.fromJson(value);
        case 'ResGetMessageSummaries':
          return ResGetMessageSummaries.fromJson(value);
        case 'ResGetMessageTag':
          return ResGetMessageTag.fromJson(value);
        case 'ResGetMessageTagTypes':
          return ResGetMessageTagTypes.fromJson(value);
        case 'ResGetMsgParentId':
          return ResGetMsgParentId.fromJson(value);
        case 'ResGetOnlineList':
          return ResGetOnlineList.fromJson(value);
        case 'ResGetOperatingMode':
          return ResGetOperatingMode.fromJson(value);
        case 'ResGetOwnOpinion':
          return ResGetOwnOpinion.fromJson(value);
        case 'ResGetOwnPseudonimousIds':
          return ResGetOwnPseudonimousIds.fromJson(value);
        case 'ResGetOwnServices':
          return ResGetOwnServices.fromJson(value);
        case 'ResGetOwnSignedIds':
          return ResGetOwnSignedIds.fromJson(value);
        case 'ResGetPGPLogins':
          return ResGetPGPLogins.fromJson(value);
        case 'ResGetPartialsDirectory':
          return ResGetPartialsDirectory.fromJson(value);
        case 'ResGetPeerDetails':
          return ResGetPeerDetails.fromJson(value);
        case 'ResGetPeerVersion':
          return ResGetPeerVersion.fromJson(value);
        case 'ResGetPeersConnected':
          return ResGetPeersConnected.fromJson(value);
        case 'ResGetPeersCount':
          return ResGetPeersCount.fromJson(value);
        case 'ResGetPendingChatLobbyInvites':
          return ResGetPendingChatLobbyInvites.fromJson(value);
        case 'ResGetPgpFriendList':
          return ResGetPgpFriendList.fromJson(value);
        case 'ResGetPrimaryBannedFilesList':
          return ResGetPrimaryBannedFilesList.fromJson(value);
        case 'ResGetPrimaryBannedFilesListBannedFiles':
          return ResGetPrimaryBannedFilesListBannedFiles.fromJson(value);
        case 'ResGetReputationInfo':
          return ResGetReputationInfo.fromJson(value);
        case 'ResGetRetroshareInvite':
          return ResGetRetroshareInvite.fromJson(value);
        case 'ResGetServiceItemNames':
          return ResGetServiceItemNames.fromJson(value);
        case 'ResGetServiceItemNamesNames':
          return ResGetServiceItemNamesNames.fromJson(value);
        case 'ResGetServiceName':
          return ResGetServiceName.fromJson(value);
        case 'ResGetServicePermissions':
          return ResGetServicePermissions.fromJson(value);
        case 'ResGetServicesAllowed':
          return ResGetServicesAllowed.fromJson(value);
        case 'ResGetServicesProvided':
          return ResGetServicesProvided.fromJson(value);
        case 'ResGetSharedDirectories':
          return ResGetSharedDirectories.fromJson(value);
        case 'ResGetShortInvite':
          return ResGetShortInvite.fromJson(value);
        case 'ResGetTotalBandwidthRates':
          return ResGetTotalBandwidthRates.fromJson(value);
        case 'ResGetTrafficInfo':
          return ResGetTrafficInfo.fromJson(value);
        case 'ResGetWaitingDiscCount':
          return ResGetWaitingDiscCount.fromJson(value);
        case 'ResImportIdentity':
          return ResImportIdentity.fromJson(value);
        case 'ResImportIdentityFromString':
          return ResImportIdentityFromString.fromJson(value);
        case 'ResInitiateDistantChatConnexion':
          return ResInitiateDistantChatConnexion.fromJson(value);
        case 'ResInviteIdsToCircle':
          return ResInviteIdsToCircle.fromJson(value);
        case 'ResIpFilteringEnabled':
          return ResIpFilteringEnabled.fromJson(value);
        case 'ResIsARegularContact':
          return ResIsARegularContact.fromJson(value);
        case 'ResIsAuthTokenValid':
          return ResIsAuthTokenValid.fromJson(value);
        case 'ResIsFriend':
          return ResIsFriend.fromJson(value);
        case 'ResIsHashBanned':
          return ResIsHashBanned.fromJson(value);
        case 'ResIsIdentityBanned':
          return ResIsIdentityBanned.fromJson(value);
        case 'ResIsKnownId':
          return ResIsKnownId.fromJson(value);
        case 'ResIsLoggedIn':
          return ResIsLoggedIn.fromJson(value);
        case 'ResIsMulticastListeningEnabled':
          return ResIsMulticastListeningEnabled.fromJson(value);
        case 'ResIsNodeBanned':
          return ResIsNodeBanned.fromJson(value);
        case 'ResIsOnline':
          return ResIsOnline.fromJson(value);
        case 'ResIsOwnId':
          return ResIsOwnId.fromJson(value);
        case 'ResIsPgpFriend':
          return ResIsPgpFriend.fromJson(value);
        case 'ResIsReady':
          return ResIsReady.fromJson(value);
        case 'ResIsSslOnlyFriend':
          return ResIsSslOnlyFriend.fromJson(value);
        case 'ResJoinVisibleChatLobby':
          return ResJoinVisibleChatLobby.fromJson(value);
        case 'ResListeningPort':
          return ResListeningPort.fromJson(value);
        case 'ResLoadCertificateFromString':
          return ResLoadCertificateFromString.fromJson(value);
        case 'ResLoadDetailsFromStringCert':
          return ResLoadDetailsFromStringCert.fromJson(value);
        case 'ResLocalSearchRequest':
          return ResLocalSearchRequest.fromJson(value);
        case 'ResMarkRead':
          return ResMarkRead.fromJson(value);
        case 'ResMessageDelete':
          return ResMessageDelete.fromJson(value);
        case 'ResMessageForwarded':
          return ResMessageForwarded.fromJson(value);
        case 'ResMessageJunk':
          return ResMessageJunk.fromJson(value);
        case 'ResMessageLoadEmbeddedImages':
          return ResMessageLoadEmbeddedImages.fromJson(value);
        case 'ResMessageRead':
          return ResMessageRead.fromJson(value);
        case 'ResMessageReplied':
          return ResMessageReplied.fromJson(value);
        case 'ResMessageSend':
          return ResMessageSend.fromJson(value);
        case 'ResMessageStar':
          return ResMessageStar.fromJson(value);
        case 'ResMessageToDraft':
          return ResMessageToDraft.fromJson(value);
        case 'ResMessageToTrash':
          return ResMessageToTrash.fromJson(value);
        case 'ResOverallReputationLevel':
          return ResOverallReputationLevel.fromJson(value);
        case 'ResParseFilesLink':
          return ResParseFilesLink.fromJson(value);
        case 'ResParseShortInvite':
          return ResParseShortInvite.fromJson(value);
        case 'ResPgpIdFromFingerprint':
          return ResPgpIdFromFingerprint.fromJson(value);
        case 'ResRememberBannedIdThreshold':
          return ResRememberBannedIdThreshold.fromJson(value);
        case 'ResRemoveFriend':
          return ResRemoveFriend.fromJson(value);
        case 'ResRemoveFriendLocation':
          return ResRemoveFriendLocation.fromJson(value);
        case 'ResRemoveGroup':
          return ResRemoveGroup.fromJson(value);
        case 'ResRemoveMessageTagType':
          return ResRemoveMessageTagType.fromJson(value);
        case 'ResRemoveSharedDirectory':
          return ResRemoveSharedDirectory.fromJson(value);
        case 'ResRequestCircleMembership':
          return ResRequestCircleMembership.fromJson(value);
        case 'ResRequestDirDetails':
          return ResRequestDirDetails.fromJson(value);
        case 'ResRequestFiles':
          return ResRequestFiles.fromJson(value);
        case 'ResRequestIdentity':
          return ResRequestIdentity.fromJson(value);
        case 'ResRequestNewTokenAutorization':
          return ResRequestNewTokenAutorization.fromJson(value);
        case 'ResRequestStatus':
          return ResRequestStatus.fromJson(value);
        case 'ResResetMessageStandardTagTypes':
          return ResResetMessageStandardTagTypes.fromJson(value);
        case 'ResRestart':
          return ResRestart.fromJson(value);
        case 'ResRevokeAuthToken':
          return ResRevokeAuthToken.fromJson(value);
        case 'ResRevokeIdsFromCircle':
          return ResRevokeIdsFromCircle.fromJson(value);
        case 'ResSendChat':
          return ResSendChat.fromJson(value);
        case 'ResSendMail':
          return ResSendMail.fromJson(value);
        case 'ResSetAsRegularContact':
          return ResSetAsRegularContact.fromJson(value);
        case 'ResSetChannelAutoDownload':
          return ResSetChannelAutoDownload.fromJson(value);
        case 'ResSetChannelDownloadDirectory':
          return ResSetChannelDownloadDirectory.fromJson(value);
        case 'ResSetChunkStrategy':
          return ResSetChunkStrategy.fromJson(value);
        case 'ResSetDefaultIdentityForChatLobby':
          return ResSetDefaultIdentityForChatLobby.fromJson(value);
        case 'ResSetDestinationDirectory':
          return ResSetDestinationDirectory.fromJson(value);
        case 'ResSetDestinationName':
          return ResSetDestinationName.fromJson(value);
        case 'ResSetDownloadDirectory':
          return ResSetDownloadDirectory.fromJson(value);
        case 'ResSetDynDNS':
          return ResSetDynDNS.fromJson(value);
        case 'ResSetExtAddress':
          return ResSetExtAddress.fromJson(value);
        case 'ResSetIdentityForChatLobby':
          return ResSetIdentityForChatLobby.fromJson(value);
        case 'ResSetLocalAddress':
          return ResSetLocalAddress.fromJson(value);
        case 'ResSetMaxDataRates':
          return ResSetMaxDataRates.fromJson(value);
        case 'ResSetMessageTag':
          return ResSetMessageTag.fromJson(value);
        case 'ResSetMessageTagType':
          return ResSetMessageTagType.fromJson(value);
        case 'ResSetNetworkMode':
          return ResSetNetworkMode.fromJson(value);
        case 'ResSetOperatingMode':
          return ResSetOperatingMode.fromJson(value);
        case 'ResSetOwnOpinion':
          return ResSetOwnOpinion.fromJson(value);
        case 'ResSetPartialsDirectory':
          return ResSetPartialsDirectory.fromJson(value);
        case 'ResSetSharedDirectories':
          return ResSetSharedDirectories.fromJson(value);
        case 'ResSetVisState':
          return ResSetVisState.fromJson(value);
        case 'ResShareChannelKeys':
          return ResShareChannelKeys.fromJson(value);
        case 'ResSubscribeToChannel':
          return ResSubscribeToChannel.fromJson(value);
        case 'ResSubscribeToForum':
          return ResSubscribeToForum.fromJson(value);
        case 'ResSystemMessage':
          return ResSystemMessage.fromJson(value);
        case 'ResThresholdForRemotelyNegativeReputation':
          return ResThresholdForRemotelyNegativeReputation.fromJson(value);
        case 'ResThresholdForRemotelyPositiveReputation':
          return ResThresholdForRemotelyPositiveReputation.fromJson(value);
        case 'ResTurtleChannelRequest':
          return ResTurtleChannelRequest.fromJson(value);
        case 'ResTurtleSearchRequest':
          return ResTurtleSearchRequest.fromJson(value);
        case 'ResUnbanFile':
          return ResUnbanFile.fromJson(value);
        case 'ResUpdateIdentity':
          return ResUpdateIdentity.fromJson(value);
        case 'ResUpdateServicePermissions':
          return ResUpdateServicePermissions.fromJson(value);
        case 'ResUpdateShareFlags':
          return ResUpdateShareFlags.fromJson(value);
        case 'ResVersion':
          return ResVersion.fromJson(value);
        case 'RsBroadcastDiscoveryResult':
          return RsBroadcastDiscoveryResult.fromJson(value);
        case 'RsConfigDataRates':
          return RsConfigDataRates.fromJson(value);
        case 'RsConfigNetStatus':
          return RsConfigNetStatus.fromJson(value);
        case 'RsFileTree':
          return RsFileTree.fromJson(value);
        case 'RsFileTreeDirData':
          return RsFileTreeDirData.fromJson(value);
        case 'RsFileTreeFileData':
          return RsFileTreeFileData.fromJson(value);
        case 'RsGroupInfo':
          return RsGroupInfo.fromJson(value);
        case 'RsGroupMetaData':
          return RsGroupMetaData.fromJson(value);
        case 'RsGxsChannelGroup':
          return RsGxsChannelGroup.fromJson(value);
        case 'RsGxsChannelPost':
          return RsGxsChannelPost.fromJson(value);
        case 'RsGxsCircleDetails':
          return RsGxsCircleDetails.fromJson(value);
        case 'RsGxsCircleDetailsMSubscriptionFlags':
          return RsGxsCircleDetailsMSubscriptionFlags.fromJson(value);
        case 'RsGxsCircleGroup':
          return RsGxsCircleGroup.fromJson(value);
        case 'RsGxsCircleMsg':
          return RsGxsCircleMsg.fromJson(value);
        case 'RsGxsCircleSubscriptionType':
          return new RsGxsCircleSubscriptionTypeTypeTransformer().decode(value);
        case 'RsGxsCircleType':
          return new RsGxsCircleTypeTypeTransformer().decode(value);
        case 'RsGxsComment':
          return RsGxsComment.fromJson(value);
        case 'RsGxsFile':
          return RsGxsFile.fromJson(value);
        case 'RsGxsForumGroup':
          return RsGxsForumGroup.fromJson(value);
        case 'RsGxsForumMsg':
          return RsGxsForumMsg.fromJson(value);
        case 'RsGxsGroupSummary':
          return RsGxsGroupSummary.fromJson(value);
        case 'RsGxsGrpMsgIdPair':
          return RsGxsGrpMsgIdPair.fromJson(value);
        case 'RsGxsIdGroup':
          return RsGxsIdGroup.fromJson(value);
        case 'RsGxsImage':
          return RsGxsImage.fromJson(value);
        case 'RsGxsImageMData':
          return RsGxsImageMData.fromJson(value);
        case 'RsGxsVote':
          return RsGxsVote.fromJson(value);
        case 'RsGxsVoteType':
          return new RsGxsVoteTypeTypeTransformer().decode(value);
        case 'RsIdentityDetails':
          return RsIdentityDetails.fromJson(value);
        case 'RsIdentityDetailsMUseCases':
          return RsIdentityDetailsMUseCases.fromJson(value);
        case 'RsIdentityUsage':
          return RsIdentityUsage.fromJson(value);
        case 'RsInitLoadCertificateStatus':
          return new RsInitLoadCertificateStatusTypeTransformer().decode(value);
        case 'RsLoginHelperLocation':
          return RsLoginHelperLocation.fromJson(value);
        case 'RsMailIdRecipientIdPair':
          return RsMailIdRecipientIdPair.fromJson(value);
        case 'RsMsgMetaData':
          return RsMsgMetaData.fromJson(value);
        case 'RsMsgsMessageInfo':
          return RsMsgsMessageInfo.fromJson(value);
        case 'RsMsgsMsgInfoSummary':
          return RsMsgsMsgInfoSummary.fromJson(value);
        case 'RsMsgsMsgTagInfo':
          return RsMsgsMsgTagInfo.fromJson(value);
        case 'RsMsgsMsgTagType':
          return RsMsgsMsgTagType.fromJson(value);
        case 'RsMsgsMsgTagTypeTypes':
          return RsMsgsMsgTagTypeTypes.fromJson(value);
        case 'RsMsgsMsgTagTypeValue':
          return RsMsgsMsgTagTypeValue.fromJson(value);
        case 'RsOpinion':
          return new RsOpinionTypeTransformer().decode(value);
        case 'RsPeerDetails':
          return RsPeerDetails.fromJson(value);
        case 'RsPeerServiceInfo':
          return RsPeerServiceInfo.fromJson(value);
        case 'RsPeerServiceInfoMServiceList':
          return RsPeerServiceInfoMServiceList.fromJson(value);
        case 'RsRecognTag':
          return RsRecognTag.fromJson(value);
        case 'RsReputationInfo':
          return RsReputationInfo.fromJson(value);
        case 'RsReputationLevel':
          return new RsReputationLevelTypeTransformer().decode(value);
        case 'RsServiceInfo':
          return RsServiceInfo.fromJson(value);
        case 'RsServicePermissions':
          return RsServicePermissions.fromJson(value);
        case 'RsServiceType':
          return new RsServiceTypeTypeTransformer().decode(value);
        case 'RsTokenServiceGxsRequestStatus':
          return new RsTokenServiceGxsRequestStatusTypeTransformer().decode(value);
        case 'RsUrl':
          return RsUrl.fromJson(value);
        case 'RstimeT':
          return RstimeT.fromJson(value);
        case 'SharedDirInfo':
          return SharedDirInfo.fromJson(value);
        case 'TransferInfo':
          return TransferInfo.fromJson(value);
        case 'TurtleFileInfoV2':
          return TurtleFileInfoV2.fromJson(value);
        case 'UsageCode':
          return new UsageCodeTypeTransformer().decode(value);
        case 'VisibleChatLobbyRecord':
          return VisibleChatLobbyRecord.fromJson(value);
        default:
          {
            Match match;
            if (value is List &&
                (match = _regList.firstMatch(targetType)) != null) {
              var newTargetType = match[1];
              return value.map((v) => _deserialize(v, newTargetType)).toList();
            } else if (value is Map &&
                (match = _regMap.firstMatch(targetType)) != null) {
              var newTargetType = match[1];
              return Map.fromIterables(value.keys,
                  value.values.map((v) => _deserialize(v, newTargetType)));
            }
          }
      }
    } on Exception catch (e, stack) {
      throw ApiException.withInner(500, 'Exception during deserialization.', e, stack);
    }
    throw ApiException(500, 'Could not find a suitable class for deserialization');
  }

  dynamic deserialize(String json, String targetType) {
    // Remove all spaces.  Necessary for reg expressions as well.
    targetType = targetType.replaceAll(' ', '');

    if (targetType == 'String') return json;

    var decodedJson = jsonDecode(json);
    return _deserialize(decodedJson, targetType);
  }

  String serialize(Object obj) {
    String serialized = '';
    if (obj == null) {
      serialized = '';
    } else {
      serialized = json.encode(obj);
    }
    return serialized;
  }

  // We don't use a Map<String, String> for queryParams.
  // If collectionFormat is 'multi' a key might appear multiple times.
  Future<Response> invokeAPI(String path,
                             String method,
                             Iterable<QueryParam> queryParams,
                             Object body,
                             Map<String, String> headerParams,
                             Map<String, String> formParams,
                             String nullableContentType,
                             List<String> authNames) async {

    _updateParamsForAuth(authNames, queryParams, headerParams);

    var ps = queryParams
      .where((p) => p.value != null)
      .map((p) => '${p.name}=${Uri.encodeQueryComponent(p.value)}');

    String queryString = ps.isNotEmpty ?
                         '?' + ps.join('&') :
                         '';

    String url = basePath + path + queryString;

    headerParams.addAll(_defaultHeaderMap);
    if (nullableContentType != null) {
      final contentType = nullableContentType;
      headerParams['Content-Type'] = contentType;
    }

    if(body is MultipartRequest) {
      var request = MultipartRequest(method, Uri.parse(url));
      request.fields.addAll(body.fields);
      request.files.addAll(body.files);
      request.headers.addAll(body.headers);
      request.headers.addAll(headerParams);
      var response = await client.send(request);
      return Response.fromStream(response);
    } else {
      var msgBody = nullableContentType == "application/x-www-form-urlencoded" ? formParams : serialize(body);
      final nullableHeaderParams = (headerParams.isEmpty)? null: headerParams;
      switch(method) {
        case "POST":
          return client.post(url, headers: nullableHeaderParams, body: msgBody);
        case "PUT":
          return client.put(url, headers: nullableHeaderParams, body: msgBody);
        case "DELETE":
          return client.delete(url, headers: nullableHeaderParams);
        case "PATCH":
          return client.patch(url, headers: nullableHeaderParams, body: msgBody);
        case "HEAD":
          return client.head(url, headers: nullableHeaderParams);
        default:
          return client.get(url, headers: nullableHeaderParams);
      }
    }
  }

  /// Update query and header parameters based on authentication settings.
  /// @param authNames The authentications to apply
  void _updateParamsForAuth(List<String> authNames, List<QueryParam> queryParams, Map<String, String> headerParams) {
    authNames.forEach((authName) {
      Authentication auth = _authentications[authName];
      if (auth == null) throw ArgumentError("Authentication undefined: " + authName);
      auth.applyToParams(queryParams, headerParams);
    });
  }

  T getAuthentication<T extends Authentication>(String name) {
    var authentication = _authentications[name];

    return authentication is T ? authentication : null;
  }
}
