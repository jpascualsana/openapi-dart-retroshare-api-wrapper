part of openapi.api;

class ResResetMessageStandardTagTypes {
  
  bool retval = null;
  
  RsMsgsMsgTagType tags = null;
  ResResetMessageStandardTagTypes();

  @override
  String toString() {
    return 'ResResetMessageStandardTagTypes[retval=$retval, tags=$tags, ]';
  }

  ResResetMessageStandardTagTypes.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    tags = (json['tags'] == null) ?
      null :
      RsMsgsMsgTagType.fromJson(json['tags']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (tags != null)
      json['tags'] = tags;
    return json;
  }

  static List<ResResetMessageStandardTagTypes> listFromJson(List<dynamic> json) {
    return json == null ? List<ResResetMessageStandardTagTypes>() : json.map((value) => ResResetMessageStandardTagTypes.fromJson(value)).toList();
  }

  static Map<String, ResResetMessageStandardTagTypes> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResResetMessageStandardTagTypes>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResResetMessageStandardTagTypes.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResResetMessageStandardTagTypes-objects as value to a dart map
  static Map<String, List<ResResetMessageStandardTagTypes>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResResetMessageStandardTagTypes>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResResetMessageStandardTagTypes.listFromJson(value);
       });
     }
     return map;
  }
}

