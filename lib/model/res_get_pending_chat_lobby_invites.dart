part of openapi.api;

class ResGetPendingChatLobbyInvites {
  
  List<ChatLobbyInvite> invites = [];
  ResGetPendingChatLobbyInvites();

  @override
  String toString() {
    return 'ResGetPendingChatLobbyInvites[invites=$invites, ]';
  }

  ResGetPendingChatLobbyInvites.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    invites = (json['invites'] == null) ?
      null :
      ChatLobbyInvite.listFromJson(json['invites']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (invites != null)
      json['invites'] = invites;
    return json;
  }

  static List<ResGetPendingChatLobbyInvites> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetPendingChatLobbyInvites>() : json.map((value) => ResGetPendingChatLobbyInvites.fromJson(value)).toList();
  }

  static Map<String, ResGetPendingChatLobbyInvites> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetPendingChatLobbyInvites>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetPendingChatLobbyInvites.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetPendingChatLobbyInvites-objects as value to a dart map
  static Map<String, List<ResGetPendingChatLobbyInvites>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetPendingChatLobbyInvites>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetPendingChatLobbyInvites.listFromJson(value);
       });
     }
     return map;
  }
}

