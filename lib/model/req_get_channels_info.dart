part of openapi.api;

class ReqGetChannelsInfo {
  
  List<String> chanIds = [];
  ReqGetChannelsInfo();

  @override
  String toString() {
    return 'ReqGetChannelsInfo[chanIds=$chanIds, ]';
  }

  ReqGetChannelsInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    chanIds = (json['chanIds'] == null) ?
      null :
      (json['chanIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (chanIds != null)
      json['chanIds'] = chanIds;
    return json;
  }

  static List<ReqGetChannelsInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetChannelsInfo>() : json.map((value) => ReqGetChannelsInfo.fromJson(value)).toList();
  }

  static Map<String, ReqGetChannelsInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetChannelsInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetChannelsInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetChannelsInfo-objects as value to a dart map
  static Map<String, List<ReqGetChannelsInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetChannelsInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetChannelsInfo.listFromJson(value);
       });
     }
     return map;
  }
}

