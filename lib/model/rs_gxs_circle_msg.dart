part of openapi.api;

class RsGxsCircleMsg {
  
  RsMsgMetaData mMeta = null;
  
  RsGxsCircleSubscriptionType mSubscriptionType = null;
  //enum mSubscriptionTypeEnum {  0,  1,  2,  };{
  RsGxsCircleMsg();

  @override
  String toString() {
    return 'RsGxsCircleMsg[mMeta=$mMeta, mSubscriptionType=$mSubscriptionType, ]';
  }

  RsGxsCircleMsg.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mMeta = (json['mMeta'] == null) ?
      null :
      RsMsgMetaData.fromJson(json['mMeta']);
    mSubscriptionType = (json['mSubscriptionType'] == null) ?
      null :
      RsGxsCircleSubscriptionType.fromJson(json['mSubscriptionType']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mMeta != null)
      json['mMeta'] = mMeta;
    if (mSubscriptionType != null)
      json['mSubscriptionType'] = mSubscriptionType;
    return json;
  }

  static List<RsGxsCircleMsg> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsCircleMsg>() : json.map((value) => RsGxsCircleMsg.fromJson(value)).toList();
  }

  static Map<String, RsGxsCircleMsg> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsCircleMsg>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsCircleMsg.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsCircleMsg-objects as value to a dart map
  static Map<String, List<RsGxsCircleMsg>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsCircleMsg>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsCircleMsg.listFromJson(value);
       });
     }
     return map;
  }
}

