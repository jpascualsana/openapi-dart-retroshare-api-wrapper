part of openapi.api;

class ReqGetIdentitiesInfo {
  
  List<String> ids = [];
  ReqGetIdentitiesInfo();

  @override
  String toString() {
    return 'ReqGetIdentitiesInfo[ids=$ids, ]';
  }

  ReqGetIdentitiesInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    ids = (json['ids'] == null) ?
      null :
      (json['ids'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (ids != null)
      json['ids'] = ids;
    return json;
  }

  static List<ReqGetIdentitiesInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetIdentitiesInfo>() : json.map((value) => ReqGetIdentitiesInfo.fromJson(value)).toList();
  }

  static Map<String, ReqGetIdentitiesInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetIdentitiesInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetIdentitiesInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetIdentitiesInfo-objects as value to a dart map
  static Map<String, List<ReqGetIdentitiesInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetIdentitiesInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetIdentitiesInfo.listFromJson(value);
       });
     }
     return map;
  }
}

