part of openapi.api;

class ResCloseDistantChatConnexion {
  
  bool retval = null;
  ResCloseDistantChatConnexion();

  @override
  String toString() {
    return 'ResCloseDistantChatConnexion[retval=$retval, ]';
  }

  ResCloseDistantChatConnexion.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResCloseDistantChatConnexion> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCloseDistantChatConnexion>() : json.map((value) => ResCloseDistantChatConnexion.fromJson(value)).toList();
  }

  static Map<String, ResCloseDistantChatConnexion> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCloseDistantChatConnexion>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCloseDistantChatConnexion.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCloseDistantChatConnexion-objects as value to a dart map
  static Map<String, List<ResCloseDistantChatConnexion>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCloseDistantChatConnexion>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCloseDistantChatConnexion.listFromJson(value);
       });
     }
     return map;
  }
}

