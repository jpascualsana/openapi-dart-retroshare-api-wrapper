part of openapi.api;

class ResAddSslOnlyFriend {
  
  bool retval = null;
  ResAddSslOnlyFriend();

  @override
  String toString() {
    return 'ResAddSslOnlyFriend[retval=$retval, ]';
  }

  ResAddSslOnlyFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResAddSslOnlyFriend> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAddSslOnlyFriend>() : json.map((value) => ResAddSslOnlyFriend.fromJson(value)).toList();
  }

  static Map<String, ResAddSslOnlyFriend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAddSslOnlyFriend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAddSslOnlyFriend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAddSslOnlyFriend-objects as value to a dart map
  static Map<String, List<ResAddSslOnlyFriend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAddSslOnlyFriend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAddSslOnlyFriend.listFromJson(value);
       });
     }
     return map;
  }
}

