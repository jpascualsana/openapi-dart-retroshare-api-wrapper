part of openapi.api;

class ResGetPeersConnected {
  
  List<String> peerSet = [];
  ResGetPeersConnected();

  @override
  String toString() {
    return 'ResGetPeersConnected[peerSet=$peerSet, ]';
  }

  ResGetPeersConnected.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    peerSet = (json['peerSet'] == null) ?
      null :
      (json['peerSet'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (peerSet != null)
      json['peerSet'] = peerSet;
    return json;
  }

  static List<ResGetPeersConnected> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetPeersConnected>() : json.map((value) => ResGetPeersConnected.fromJson(value)).toList();
  }

  static Map<String, ResGetPeersConnected> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetPeersConnected>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetPeersConnected.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetPeersConnected-objects as value to a dart map
  static Map<String, List<ResGetPeersConnected>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetPeersConnected>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetPeersConnected.listFromJson(value);
       });
     }
     return map;
  }
}

