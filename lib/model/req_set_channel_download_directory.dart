part of openapi.api;

class ReqSetChannelDownloadDirectory {
  
  String channelId = null;
  
  String directory = null;
  ReqSetChannelDownloadDirectory();

  @override
  String toString() {
    return 'ReqSetChannelDownloadDirectory[channelId=$channelId, directory=$directory, ]';
  }

  ReqSetChannelDownloadDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
    directory = json['directory'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    if (directory != null)
      json['directory'] = directory;
    return json;
  }

  static List<ReqSetChannelDownloadDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetChannelDownloadDirectory>() : json.map((value) => ReqSetChannelDownloadDirectory.fromJson(value)).toList();
  }

  static Map<String, ReqSetChannelDownloadDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetChannelDownloadDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetChannelDownloadDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetChannelDownloadDirectory-objects as value to a dart map
  static Map<String, List<ReqSetChannelDownloadDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetChannelDownloadDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetChannelDownloadDirectory.listFromJson(value);
       });
     }
     return map;
  }
}

