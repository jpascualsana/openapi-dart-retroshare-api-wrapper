part of openapi.api;

class ReqMessageRead {
  
  String msgId = null;
  
  bool unreadByUser = null;
  ReqMessageRead();

  @override
  String toString() {
    return 'ReqMessageRead[msgId=$msgId, unreadByUser=$unreadByUser, ]';
  }

  ReqMessageRead.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
    unreadByUser = json['unreadByUser'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    if (unreadByUser != null)
      json['unreadByUser'] = unreadByUser;
    return json;
  }

  static List<ReqMessageRead> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqMessageRead>() : json.map((value) => ReqMessageRead.fromJson(value)).toList();
  }

  static Map<String, ReqMessageRead> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqMessageRead>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqMessageRead.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqMessageRead-objects as value to a dart map
  static Map<String, List<ReqMessageRead>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqMessageRead>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqMessageRead.listFromJson(value);
       });
     }
     return map;
  }
}

