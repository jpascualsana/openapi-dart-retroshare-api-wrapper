part of openapi.api;

class ResAttemptLogin {
  
  RsInitLoadCertificateStatus retval = null;
  //enum retvalEnum {  0,  1,  2,  3,  };{
  ResAttemptLogin();

  @override
  String toString() {
    return 'ResAttemptLogin[retval=$retval, ]';
  }

  ResAttemptLogin.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      RsInitLoadCertificateStatus.fromJson(json['retval']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResAttemptLogin> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAttemptLogin>() : json.map((value) => ResAttemptLogin.fromJson(value)).toList();
  }

  static Map<String, ResAttemptLogin> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAttemptLogin>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAttemptLogin.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAttemptLogin-objects as value to a dart map
  static Map<String, List<ResAttemptLogin>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAttemptLogin>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAttemptLogin.listFromJson(value);
       });
     }
     return map;
  }
}

