part of openapi.api;

class RsGxsChannelGroup {
  
  String mDescription = null;
  
  RsGxsImage mImage = null;
  
  bool mAutoDownload = null;
  RsGxsChannelGroup();

  @override
  String toString() {
    return 'RsGxsChannelGroup[mDescription=$mDescription, mImage=$mImage, mAutoDownload=$mAutoDownload, ]';
  }

  RsGxsChannelGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mDescription = json['mDescription'];
    mImage = (json['mImage'] == null) ?
      null :
      RsGxsImage.fromJson(json['mImage']);
    mAutoDownload = json['mAutoDownload'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mDescription != null)
      json['mDescription'] = mDescription;
    if (mImage != null)
      json['mImage'] = mImage;
    if (mAutoDownload != null)
      json['mAutoDownload'] = mAutoDownload;
    return json;
  }

  static List<RsGxsChannelGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsChannelGroup>() : json.map((value) => RsGxsChannelGroup.fromJson(value)).toList();
  }

  static Map<String, RsGxsChannelGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsChannelGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsChannelGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsChannelGroup-objects as value to a dart map
  static Map<String, List<RsGxsChannelGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsChannelGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsChannelGroup.listFromJson(value);
       });
     }
     return map;
  }
}

