part of openapi.api;

class ReqGetLobbyAutoSubscribe {
  
  ChatLobbyId lobbyId = null;
  ReqGetLobbyAutoSubscribe();

  @override
  String toString() {
    return 'ReqGetLobbyAutoSubscribe[lobbyId=$lobbyId, ]';
  }

  ReqGetLobbyAutoSubscribe.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    lobbyId = (json['lobby_id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['lobby_id']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (lobbyId != null)
      json['lobby_id'] = lobbyId;
    return json;
  }

  static List<ReqGetLobbyAutoSubscribe> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetLobbyAutoSubscribe>() : json.map((value) => ReqGetLobbyAutoSubscribe.fromJson(value)).toList();
  }

  static Map<String, ReqGetLobbyAutoSubscribe> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetLobbyAutoSubscribe>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetLobbyAutoSubscribe.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetLobbyAutoSubscribe-objects as value to a dart map
  static Map<String, List<ReqGetLobbyAutoSubscribe>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetLobbyAutoSubscribe>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetLobbyAutoSubscribe.listFromJson(value);
       });
     }
     return map;
  }
}

