part of openapi.api;

class ResMessageRead {
  
  bool retval = null;
  ResMessageRead();

  @override
  String toString() {
    return 'ResMessageRead[retval=$retval, ]';
  }

  ResMessageRead.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResMessageRead> listFromJson(List<dynamic> json) {
    return json == null ? List<ResMessageRead>() : json.map((value) => ResMessageRead.fromJson(value)).toList();
  }

  static Map<String, ResMessageRead> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResMessageRead>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResMessageRead.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResMessageRead-objects as value to a dart map
  static Map<String, List<ResMessageRead>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResMessageRead>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResMessageRead.listFromJson(value);
       });
     }
     return map;
  }
}

