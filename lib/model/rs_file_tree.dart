part of openapi.api;

class RsFileTree {
  
  List<RsFileTreeFileData> mFiles = [];
  
  List<RsFileTreeDirData> mDirs = [];
  
  int mTotalFiles = null;
  
  ReqBanFileFileSize mTotalSize = null;
  RsFileTree();

  @override
  String toString() {
    return 'RsFileTree[mFiles=$mFiles, mDirs=$mDirs, mTotalFiles=$mTotalFiles, mTotalSize=$mTotalSize, ]';
  }

  RsFileTree.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mFiles = (json['mFiles'] == null) ?
      null :
      RsFileTreeFileData.listFromJson(json['mFiles']);
    mDirs = (json['mDirs'] == null) ?
      null :
      RsFileTreeDirData.listFromJson(json['mDirs']);
    mTotalFiles = json['mTotalFiles'];
    mTotalSize = (json['mTotalSize'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['mTotalSize']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mFiles != null)
      json['mFiles'] = mFiles;
    if (mDirs != null)
      json['mDirs'] = mDirs;
    if (mTotalFiles != null)
      json['mTotalFiles'] = mTotalFiles;
    if (mTotalSize != null)
      json['mTotalSize'] = mTotalSize;
    return json;
  }

  static List<RsFileTree> listFromJson(List<dynamic> json) {
    return json == null ? List<RsFileTree>() : json.map((value) => RsFileTree.fromJson(value)).toList();
  }

  static Map<String, RsFileTree> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsFileTree>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsFileTree.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsFileTree-objects as value to a dart map
  static Map<String, List<RsFileTree>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsFileTree>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsFileTree.listFromJson(value);
       });
     }
     return map;
  }
}

