part of openapi.api;

class ResSetDownloadDirectory {
  
  bool retval = null;
  ResSetDownloadDirectory();

  @override
  String toString() {
    return 'ResSetDownloadDirectory[retval=$retval, ]';
  }

  ResSetDownloadDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetDownloadDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetDownloadDirectory>() : json.map((value) => ResSetDownloadDirectory.fromJson(value)).toList();
  }

  static Map<String, ResSetDownloadDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetDownloadDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetDownloadDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetDownloadDirectory-objects as value to a dart map
  static Map<String, List<ResSetDownloadDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetDownloadDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetDownloadDirectory.listFromJson(value);
       });
     }
     return map;
  }
}

