part of openapi.api;

class ResIsLoggedIn {
  
  bool retval = null;
  ResIsLoggedIn();

  @override
  String toString() {
    return 'ResIsLoggedIn[retval=$retval, ]';
  }

  ResIsLoggedIn.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsLoggedIn> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsLoggedIn>() : json.map((value) => ResIsLoggedIn.fromJson(value)).toList();
  }

  static Map<String, ResIsLoggedIn> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsLoggedIn>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsLoggedIn.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsLoggedIn-objects as value to a dart map
  static Map<String, List<ResIsLoggedIn>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsLoggedIn>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsLoggedIn.listFromJson(value);
       });
     }
     return map;
  }
}

