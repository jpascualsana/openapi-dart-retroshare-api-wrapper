part of openapi.api;

class ResSetMessageTag {
  
  bool retval = null;
  ResSetMessageTag();

  @override
  String toString() {
    return 'ResSetMessageTag[retval=$retval, ]';
  }

  ResSetMessageTag.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetMessageTag> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetMessageTag>() : json.map((value) => ResSetMessageTag.fromJson(value)).toList();
  }

  static Map<String, ResSetMessageTag> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetMessageTag>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetMessageTag.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetMessageTag-objects as value to a dart map
  static Map<String, List<ResSetMessageTag>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetMessageTag>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetMessageTag.listFromJson(value);
       });
     }
     return map;
  }
}

