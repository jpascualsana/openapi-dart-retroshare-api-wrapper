part of openapi.api;

class ResConnectAttempt {
  
  bool retval = null;
  ResConnectAttempt();

  @override
  String toString() {
    return 'ResConnectAttempt[retval=$retval, ]';
  }

  ResConnectAttempt.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResConnectAttempt> listFromJson(List<dynamic> json) {
    return json == null ? List<ResConnectAttempt>() : json.map((value) => ResConnectAttempt.fromJson(value)).toList();
  }

  static Map<String, ResConnectAttempt> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResConnectAttempt>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResConnectAttempt.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResConnectAttempt-objects as value to a dart map
  static Map<String, List<ResConnectAttempt>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResConnectAttempt>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResConnectAttempt.listFromJson(value);
       });
     }
     return map;
  }
}

