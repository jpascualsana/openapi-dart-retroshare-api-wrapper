part of openapi.api;

class ResRevokeAuthToken {
  
  bool retval = null;
  ResRevokeAuthToken();

  @override
  String toString() {
    return 'ResRevokeAuthToken[retval=$retval, ]';
  }

  ResRevokeAuthToken.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRevokeAuthToken> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRevokeAuthToken>() : json.map((value) => ResRevokeAuthToken.fromJson(value)).toList();
  }

  static Map<String, ResRevokeAuthToken> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRevokeAuthToken>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRevokeAuthToken.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRevokeAuthToken-objects as value to a dart map
  static Map<String, List<ResRevokeAuthToken>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRevokeAuthToken>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRevokeAuthToken.listFromJson(value);
       });
     }
     return map;
  }
}

