part of openapi.api;

class ResCreateCommentV2 {
  
  bool retval = null;
  
  String commentMessageId = null;
  
  String errorMessage = null;
  ResCreateCommentV2();

  @override
  String toString() {
    return 'ResCreateCommentV2[retval=$retval, commentMessageId=$commentMessageId, errorMessage=$errorMessage, ]';
  }

  ResCreateCommentV2.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    commentMessageId = json['commentMessageId'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (commentMessageId != null)
      json['commentMessageId'] = commentMessageId;
    if (errorMessage != null)
      json['errorMessage'] = errorMessage;
    return json;
  }

  static List<ResCreateCommentV2> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateCommentV2>() : json.map((value) => ResCreateCommentV2.fromJson(value)).toList();
  }

  static Map<String, ResCreateCommentV2> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateCommentV2>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateCommentV2.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateCommentV2-objects as value to a dart map
  static Map<String, List<ResCreateCommentV2>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateCommentV2>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateCommentV2.listFromJson(value);
       });
     }
     return map;
  }
}

