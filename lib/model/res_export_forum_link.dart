part of openapi.api;

class ResExportForumLink {
  
  bool retval = null;
  
  String link = null;
  
  String errMsg = null;
  ResExportForumLink();

  @override
  String toString() {
    return 'ResExportForumLink[retval=$retval, link=$link, errMsg=$errMsg, ]';
  }

  ResExportForumLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    link = json['link'];
    errMsg = json['errMsg'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (link != null)
      json['link'] = link;
    if (errMsg != null)
      json['errMsg'] = errMsg;
    return json;
  }

  static List<ResExportForumLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ResExportForumLink>() : json.map((value) => ResExportForumLink.fromJson(value)).toList();
  }

  static Map<String, ResExportForumLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResExportForumLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResExportForumLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResExportForumLink-objects as value to a dart map
  static Map<String, List<ResExportForumLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResExportForumLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResExportForumLink.listFromJson(value);
       });
     }
     return map;
  }
}

