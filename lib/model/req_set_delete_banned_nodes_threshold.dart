part of openapi.api;

class ReqSetDeleteBannedNodesThreshold {
  
  int days = null;
  ReqSetDeleteBannedNodesThreshold();

  @override
  String toString() {
    return 'ReqSetDeleteBannedNodesThreshold[days=$days, ]';
  }

  ReqSetDeleteBannedNodesThreshold.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    days = json['days'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (days != null)
      json['days'] = days;
    return json;
  }

  static List<ReqSetDeleteBannedNodesThreshold> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetDeleteBannedNodesThreshold>() : json.map((value) => ReqSetDeleteBannedNodesThreshold.fromJson(value)).toList();
  }

  static Map<String, ReqSetDeleteBannedNodesThreshold> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetDeleteBannedNodesThreshold>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetDeleteBannedNodesThreshold.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetDeleteBannedNodesThreshold-objects as value to a dart map
  static Map<String, List<ReqSetDeleteBannedNodesThreshold>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetDeleteBannedNodesThreshold>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetDeleteBannedNodesThreshold.listFromJson(value);
       });
     }
     return map;
  }
}

