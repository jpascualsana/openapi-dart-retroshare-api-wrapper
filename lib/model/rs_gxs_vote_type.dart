part of openapi.api;

class RsGxsVoteType {
  /// The underlying value of this enum member.
  final int value;

  const RsGxsVoteType._internal(this.value);

  static const RsGxsVoteType number0_ = const RsGxsVoteType._internal(0);
  static const RsGxsVoteType number1_ = const RsGxsVoteType._internal(1);
  static const RsGxsVoteType number2_ = const RsGxsVoteType._internal(2);
  
  int toJson (){
    return this.value;
  }

  static RsGxsVoteType fromJson(int value) {
    return new RsGxsVoteTypeTypeTransformer().decode(value);
  }
  
  static List<RsGxsVoteType> listFromJson(List<dynamic> json) {
    return json == null ? new List<RsGxsVoteType>() : json.map((value) => RsGxsVoteType.fromJson(value)).toList();
  }
}

class RsGxsVoteTypeTypeTransformer {

  dynamic encode(RsGxsVoteType data) {
    return data.value;
  }

  RsGxsVoteType decode(dynamic data) {
    switch (data) {
      case 0: return RsGxsVoteType.number0_;
      case 1: return RsGxsVoteType.number1_;
      case 2: return RsGxsVoteType.number2_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}

