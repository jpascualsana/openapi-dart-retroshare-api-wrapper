part of openapi.api;

class ReqGetChannelComments {
  
  String channelId = null;
  
  List<String> contentIds = [];
  ReqGetChannelComments();

  @override
  String toString() {
    return 'ReqGetChannelComments[channelId=$channelId, contentIds=$contentIds, ]';
  }

  ReqGetChannelComments.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
    contentIds = (json['contentIds'] == null) ?
      null :
      (json['contentIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    if (contentIds != null)
      json['contentIds'] = contentIds;
    return json;
  }

  static List<ReqGetChannelComments> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetChannelComments>() : json.map((value) => ReqGetChannelComments.fromJson(value)).toList();
  }

  static Map<String, ReqGetChannelComments> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetChannelComments>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetChannelComments.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetChannelComments-objects as value to a dart map
  static Map<String, List<ReqGetChannelComments>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetChannelComments>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetChannelComments.listFromJson(value);
       });
     }
     return map;
  }
}

