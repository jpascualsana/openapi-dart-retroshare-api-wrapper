part of openapi.api;

class ReqCancelCircleMembership {
  
  String ownGxsId = null;
  
  String circleId = null;
  ReqCancelCircleMembership();

  @override
  String toString() {
    return 'ReqCancelCircleMembership[ownGxsId=$ownGxsId, circleId=$circleId, ]';
  }

  ReqCancelCircleMembership.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    ownGxsId = json['ownGxsId'];
    circleId = json['circleId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (ownGxsId != null)
      json['ownGxsId'] = ownGxsId;
    if (circleId != null)
      json['circleId'] = circleId;
    return json;
  }

  static List<ReqCancelCircleMembership> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCancelCircleMembership>() : json.map((value) => ReqCancelCircleMembership.fromJson(value)).toList();
  }

  static Map<String, ReqCancelCircleMembership> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCancelCircleMembership>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCancelCircleMembership.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCancelCircleMembership-objects as value to a dart map
  static Map<String, List<ReqCancelCircleMembership>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCancelCircleMembership>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCancelCircleMembership.listFromJson(value);
       });
     }
     return map;
  }
}

