part of openapi.api;

class RsBroadcastDiscoveryResult {
  
  String mPgpFingerprint = null;
  
  String mSslId = null;
  
  String mProfileName = null;
  
  RsUrl mLocator = null;
  RsBroadcastDiscoveryResult();

  @override
  String toString() {
    return 'RsBroadcastDiscoveryResult[mPgpFingerprint=$mPgpFingerprint, mSslId=$mSslId, mProfileName=$mProfileName, mLocator=$mLocator, ]';
  }

  RsBroadcastDiscoveryResult.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mPgpFingerprint = json['mPgpFingerprint'];
    mSslId = json['mSslId'];
    mProfileName = json['mProfileName'];
    mLocator = (json['mLocator'] == null) ?
      null :
      RsUrl.fromJson(json['mLocator']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mPgpFingerprint != null)
      json['mPgpFingerprint'] = mPgpFingerprint;
    if (mSslId != null)
      json['mSslId'] = mSslId;
    if (mProfileName != null)
      json['mProfileName'] = mProfileName;
    if (mLocator != null)
      json['mLocator'] = mLocator;
    return json;
  }

  static List<RsBroadcastDiscoveryResult> listFromJson(List<dynamic> json) {
    return json == null ? List<RsBroadcastDiscoveryResult>() : json.map((value) => RsBroadcastDiscoveryResult.fromJson(value)).toList();
  }

  static Map<String, RsBroadcastDiscoveryResult> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsBroadcastDiscoveryResult>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsBroadcastDiscoveryResult.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsBroadcastDiscoveryResult-objects as value to a dart map
  static Map<String, List<RsBroadcastDiscoveryResult>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsBroadcastDiscoveryResult>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsBroadcastDiscoveryResult.listFromJson(value);
       });
     }
     return map;
  }
}

