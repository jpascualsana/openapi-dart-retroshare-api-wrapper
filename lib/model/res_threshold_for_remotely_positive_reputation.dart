part of openapi.api;

class ResThresholdForRemotelyPositiveReputation {
  
  int retval = null;
  ResThresholdForRemotelyPositiveReputation();

  @override
  String toString() {
    return 'ResThresholdForRemotelyPositiveReputation[retval=$retval, ]';
  }

  ResThresholdForRemotelyPositiveReputation.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResThresholdForRemotelyPositiveReputation> listFromJson(List<dynamic> json) {
    return json == null ? List<ResThresholdForRemotelyPositiveReputation>() : json.map((value) => ResThresholdForRemotelyPositiveReputation.fromJson(value)).toList();
  }

  static Map<String, ResThresholdForRemotelyPositiveReputation> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResThresholdForRemotelyPositiveReputation>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResThresholdForRemotelyPositiveReputation.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResThresholdForRemotelyPositiveReputation-objects as value to a dart map
  static Map<String, List<ResThresholdForRemotelyPositiveReputation>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResThresholdForRemotelyPositiveReputation>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResThresholdForRemotelyPositiveReputation.listFromJson(value);
       });
     }
     return map;
  }
}

