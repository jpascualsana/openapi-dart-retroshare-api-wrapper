part of openapi.api;

class ResExportIdentityToString {
  
  bool retval = null;
  
  String data = null;
  
  String errorMsg = null;
  ResExportIdentityToString();

  @override
  String toString() {
    return 'ResExportIdentityToString[retval=$retval, data=$data, errorMsg=$errorMsg, ]';
  }

  ResExportIdentityToString.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    data = json['data'];
    errorMsg = json['errorMsg'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (data != null)
      json['data'] = data;
    if (errorMsg != null)
      json['errorMsg'] = errorMsg;
    return json;
  }

  static List<ResExportIdentityToString> listFromJson(List<dynamic> json) {
    return json == null ? List<ResExportIdentityToString>() : json.map((value) => ResExportIdentityToString.fromJson(value)).toList();
  }

  static Map<String, ResExportIdentityToString> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResExportIdentityToString>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResExportIdentityToString.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResExportIdentityToString-objects as value to a dart map
  static Map<String, List<ResExportIdentityToString>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResExportIdentityToString>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResExportIdentityToString.listFromJson(value);
       });
     }
     return map;
  }
}

