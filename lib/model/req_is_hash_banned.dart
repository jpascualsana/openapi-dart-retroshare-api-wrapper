part of openapi.api;

class ReqIsHashBanned {
  
  String hash = null;
  ReqIsHashBanned();

  @override
  String toString() {
    return 'ReqIsHashBanned[hash=$hash, ]';
  }

  ReqIsHashBanned.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hash = json['hash'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hash != null)
      json['hash'] = hash;
    return json;
  }

  static List<ReqIsHashBanned> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqIsHashBanned>() : json.map((value) => ReqIsHashBanned.fromJson(value)).toList();
  }

  static Map<String, ReqIsHashBanned> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqIsHashBanned>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqIsHashBanned.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqIsHashBanned-objects as value to a dart map
  static Map<String, List<ReqIsHashBanned>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqIsHashBanned>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqIsHashBanned.listFromJson(value);
       });
     }
     return map;
  }
}

