part of openapi.api;

class ResGetOwnServices {
  
  bool retval = null;
  
  RsPeerServiceInfo info = null;
  ResGetOwnServices();

  @override
  String toString() {
    return 'ResGetOwnServices[retval=$retval, info=$info, ]';
  }

  ResGetOwnServices.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    info = (json['info'] == null) ?
      null :
      RsPeerServiceInfo.fromJson(json['info']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (info != null)
      json['info'] = info;
    return json;
  }

  static List<ResGetOwnServices> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetOwnServices>() : json.map((value) => ResGetOwnServices.fromJson(value)).toList();
  }

  static Map<String, ResGetOwnServices> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetOwnServices>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetOwnServices.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetOwnServices-objects as value to a dart map
  static Map<String, List<ResGetOwnServices>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetOwnServices>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetOwnServices.listFromJson(value);
       });
     }
     return map;
  }
}

