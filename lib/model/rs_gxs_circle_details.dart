part of openapi.api;

class RsGxsCircleDetails {
  
  String mCircleId = null;
  
  String mCircleName = null;
  
  RsGxsCircleType mCircleType = null;
  //enum mCircleTypeEnum {  0,  1,  2,  3,  4,  5,  6,  };{
  
  String mRestrictedCircleId = null;
  
  bool mAmIAllowed = null;
  
  bool mAmIAdmin = null;
  
  List<String> mAllowedGxsIds = [];
  
  List<String> mAllowedNodes = [];
  
  List<RsGxsCircleDetailsMSubscriptionFlags> mSubscriptionFlags = [];
  RsGxsCircleDetails();

  @override
  String toString() {
    return 'RsGxsCircleDetails[mCircleId=$mCircleId, mCircleName=$mCircleName, mCircleType=$mCircleType, mRestrictedCircleId=$mRestrictedCircleId, mAmIAllowed=$mAmIAllowed, mAmIAdmin=$mAmIAdmin, mAllowedGxsIds=$mAllowedGxsIds, mAllowedNodes=$mAllowedNodes, mSubscriptionFlags=$mSubscriptionFlags, ]';
  }

  RsGxsCircleDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mCircleId = json['mCircleId'];
    mCircleName = json['mCircleName'];
    mCircleType = (json['mCircleType'] == null) ?
      null :
      RsGxsCircleType.fromJson(json['mCircleType']);
    mRestrictedCircleId = json['mRestrictedCircleId'];
    mAmIAllowed = json['mAmIAllowed'];
    mAmIAdmin = json['mAmIAdmin'];
    mAllowedGxsIds = (json['mAllowedGxsIds'] == null) ?
      null :
      (json['mAllowedGxsIds'] as List).cast<String>();
    mAllowedNodes = (json['mAllowedNodes'] == null) ?
      null :
      (json['mAllowedNodes'] as List).cast<String>();
    mSubscriptionFlags = (json['mSubscriptionFlags'] == null) ?
      null :
      RsGxsCircleDetailsMSubscriptionFlags.listFromJson(json['mSubscriptionFlags']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mCircleId != null)
      json['mCircleId'] = mCircleId;
    if (mCircleName != null)
      json['mCircleName'] = mCircleName;
    if (mCircleType != null)
      json['mCircleType'] = mCircleType;
    if (mRestrictedCircleId != null)
      json['mRestrictedCircleId'] = mRestrictedCircleId;
    if (mAmIAllowed != null)
      json['mAmIAllowed'] = mAmIAllowed;
    if (mAmIAdmin != null)
      json['mAmIAdmin'] = mAmIAdmin;
    if (mAllowedGxsIds != null)
      json['mAllowedGxsIds'] = mAllowedGxsIds;
    if (mAllowedNodes != null)
      json['mAllowedNodes'] = mAllowedNodes;
    if (mSubscriptionFlags != null)
      json['mSubscriptionFlags'] = mSubscriptionFlags;
    return json;
  }

  static List<RsGxsCircleDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsCircleDetails>() : json.map((value) => RsGxsCircleDetails.fromJson(value)).toList();
  }

  static Map<String, RsGxsCircleDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsCircleDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsCircleDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsCircleDetails-objects as value to a dart map
  static Map<String, List<RsGxsCircleDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsCircleDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsCircleDetails.listFromJson(value);
       });
     }
     return map;
  }
}

