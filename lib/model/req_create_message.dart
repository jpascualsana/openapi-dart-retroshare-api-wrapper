part of openapi.api;

class ReqCreateMessage {
  
  RsGxsForumMsg message = null;
  ReqCreateMessage();

  @override
  String toString() {
    return 'ReqCreateMessage[message=$message, ]';
  }

  ReqCreateMessage.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    message = (json['message'] == null) ?
      null :
      RsGxsForumMsg.fromJson(json['message']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (message != null)
      json['message'] = message;
    return json;
  }

  static List<ReqCreateMessage> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateMessage>() : json.map((value) => ReqCreateMessage.fromJson(value)).toList();
  }

  static Map<String, ReqCreateMessage> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateMessage>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateMessage.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateMessage-objects as value to a dart map
  static Map<String, List<ReqCreateMessage>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateMessage>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateMessage.listFromJson(value);
       });
     }
     return map;
  }
}

