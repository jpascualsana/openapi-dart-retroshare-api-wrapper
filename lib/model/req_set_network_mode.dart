part of openapi.api;

class ReqSetNetworkMode {
  
  String sslId = null;
  
  int netMode = null;
  ReqSetNetworkMode();

  @override
  String toString() {
    return 'ReqSetNetworkMode[sslId=$sslId, netMode=$netMode, ]';
  }

  ReqSetNetworkMode.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
    netMode = json['netMode'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    if (netMode != null)
      json['netMode'] = netMode;
    return json;
  }

  static List<ReqSetNetworkMode> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetNetworkMode>() : json.map((value) => ReqSetNetworkMode.fromJson(value)).toList();
  }

  static Map<String, ReqSetNetworkMode> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetNetworkMode>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetNetworkMode.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetNetworkMode-objects as value to a dart map
  static Map<String, List<ReqSetNetworkMode>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetNetworkMode>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetNetworkMode.listFromJson(value);
       });
     }
     return map;
  }
}

