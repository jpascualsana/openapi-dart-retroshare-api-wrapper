part of openapi.api;

class ResDisableMulticastListening {
  
  bool retval = null;
  ResDisableMulticastListening();

  @override
  String toString() {
    return 'ResDisableMulticastListening[retval=$retval, ]';
  }

  ResDisableMulticastListening.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResDisableMulticastListening> listFromJson(List<dynamic> json) {
    return json == null ? List<ResDisableMulticastListening>() : json.map((value) => ResDisableMulticastListening.fromJson(value)).toList();
  }

  static Map<String, ResDisableMulticastListening> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResDisableMulticastListening>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResDisableMulticastListening.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResDisableMulticastListening-objects as value to a dart map
  static Map<String, List<ResDisableMulticastListening>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResDisableMulticastListening>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResDisableMulticastListening.listFromJson(value);
       });
     }
     return map;
  }
}

