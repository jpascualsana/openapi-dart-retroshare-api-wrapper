part of openapi.api;

class FileInfo {
  
  int storagePermissionFlags = null;
  
  int transferInfoFlags = null;
  
  int mId = null;
  
  int searchId = null;
  
  String path = null;
  
  String fname = null;
  
  String hash = null;
  
  String ext = null;
  
  ReqBanFileFileSize size = null;
  
  ReqBanFileFileSize avail = null;
  
  num rank = null;
  
  int age = null;
  
  int queuePosition = null;
  
  ReqBanFileFileSize transfered = null;
  
  num tfRate = null;
  
  int downloadStatus = null;
  
  List<TransferInfo> peers = [];
  
  DwlSpeed priority = null;
  //enum priorityEnum {  0,  1,  2,  };{
  
  RstimeT lastTS = null;
  
  List<String> parentGroups = [];
  FileInfo();

  @override
  String toString() {
    return 'FileInfo[storagePermissionFlags=$storagePermissionFlags, transferInfoFlags=$transferInfoFlags, mId=$mId, searchId=$searchId, path=$path, fname=$fname, hash=$hash, ext=$ext, size=$size, avail=$avail, rank=$rank, age=$age, queuePosition=$queuePosition, transfered=$transfered, tfRate=$tfRate, downloadStatus=$downloadStatus, peers=$peers, priority=$priority, lastTS=$lastTS, parentGroups=$parentGroups, ]';
  }

  FileInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    storagePermissionFlags = json['storage_permission_flags'];
    transferInfoFlags = json['transfer_info_flags'];
    mId = json['mId'];
    searchId = json['searchId'];
    path = json['path'];
    fname = json['fname'];
    hash = json['hash'];
    ext = json['ext'];
    size = (json['size'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['size']);
    avail = (json['avail'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['avail']);
    rank = json['rank'];
    age = json['age'];
    queuePosition = json['queue_position'];
    transfered = (json['transfered'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['transfered']);
    tfRate = json['tfRate'];
    downloadStatus = json['downloadStatus'];
    peers = (json['peers'] == null) ?
      null :
      TransferInfo.listFromJson(json['peers']);
    priority = (json['priority'] == null) ?
      null :
      DwlSpeed.fromJson(json['priority']);
    lastTS = (json['lastTS'] == null) ?
      null :
      RstimeT.fromJson(json['lastTS']);
    parentGroups = (json['parent_groups'] == null) ?
      null :
      (json['parent_groups'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (storagePermissionFlags != null)
      json['storage_permission_flags'] = storagePermissionFlags;
    if (transferInfoFlags != null)
      json['transfer_info_flags'] = transferInfoFlags;
    if (mId != null)
      json['mId'] = mId;
    if (searchId != null)
      json['searchId'] = searchId;
    if (path != null)
      json['path'] = path;
    if (fname != null)
      json['fname'] = fname;
    if (hash != null)
      json['hash'] = hash;
    if (ext != null)
      json['ext'] = ext;
    if (size != null)
      json['size'] = size;
    if (avail != null)
      json['avail'] = avail;
    if (rank != null)
      json['rank'] = rank;
    if (age != null)
      json['age'] = age;
    if (queuePosition != null)
      json['queue_position'] = queuePosition;
    if (transfered != null)
      json['transfered'] = transfered;
    if (tfRate != null)
      json['tfRate'] = tfRate;
    if (downloadStatus != null)
      json['downloadStatus'] = downloadStatus;
    if (peers != null)
      json['peers'] = peers;
    if (priority != null)
      json['priority'] = priority;
    if (lastTS != null)
      json['lastTS'] = lastTS;
    if (parentGroups != null)
      json['parent_groups'] = parentGroups;
    return json;
  }

  static List<FileInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<FileInfo>() : json.map((value) => FileInfo.fromJson(value)).toList();
  }

  static Map<String, FileInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, FileInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = FileInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of FileInfo-objects as value to a dart map
  static Map<String, List<FileInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<FileInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = FileInfo.listFromJson(value);
       });
     }
     return map;
  }
}

