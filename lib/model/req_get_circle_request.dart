part of openapi.api;

class ReqGetCircleRequest {
  
  String circleId = null;
  
  String msgId = null;
  ReqGetCircleRequest();

  @override
  String toString() {
    return 'ReqGetCircleRequest[circleId=$circleId, msgId=$msgId, ]';
  }

  ReqGetCircleRequest.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    circleId = json['circleId'];
    msgId = json['msgId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (circleId != null)
      json['circleId'] = circleId;
    if (msgId != null)
      json['msgId'] = msgId;
    return json;
  }

  static List<ReqGetCircleRequest> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetCircleRequest>() : json.map((value) => ReqGetCircleRequest.fromJson(value)).toList();
  }

  static Map<String, ReqGetCircleRequest> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetCircleRequest>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetCircleRequest.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetCircleRequest-objects as value to a dart map
  static Map<String, List<ReqGetCircleRequest>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetCircleRequest>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetCircleRequest.listFromJson(value);
       });
     }
     return map;
  }
}

