part of openapi.api;

class ResGetSharedDirectories {
  
  bool retval = null;
  
  List<SharedDirInfo> dirs = [];
  ResGetSharedDirectories();

  @override
  String toString() {
    return 'ResGetSharedDirectories[retval=$retval, dirs=$dirs, ]';
  }

  ResGetSharedDirectories.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    dirs = (json['dirs'] == null) ?
      null :
      SharedDirInfo.listFromJson(json['dirs']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (dirs != null)
      json['dirs'] = dirs;
    return json;
  }

  static List<ResGetSharedDirectories> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetSharedDirectories>() : json.map((value) => ResGetSharedDirectories.fromJson(value)).toList();
  }

  static Map<String, ResGetSharedDirectories> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetSharedDirectories>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetSharedDirectories.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetSharedDirectories-objects as value to a dart map
  static Map<String, List<ResGetSharedDirectories>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetSharedDirectories>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetSharedDirectories.listFromJson(value);
       });
     }
     return map;
  }
}

