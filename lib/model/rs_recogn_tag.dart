part of openapi.api;

class RsRecognTag {
  
  int tagClass = null;
  
  int tagType = null;
  
  bool valid = null;
  RsRecognTag();

  @override
  String toString() {
    return 'RsRecognTag[tagClass=$tagClass, tagType=$tagType, valid=$valid, ]';
  }

  RsRecognTag.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    tagClass = json['tag_class'];
    tagType = json['tag_type'];
    valid = json['valid'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (tagClass != null)
      json['tag_class'] = tagClass;
    if (tagType != null)
      json['tag_type'] = tagType;
    if (valid != null)
      json['valid'] = valid;
    return json;
  }

  static List<RsRecognTag> listFromJson(List<dynamic> json) {
    return json == null ? List<RsRecognTag>() : json.map((value) => RsRecognTag.fromJson(value)).toList();
  }

  static Map<String, RsRecognTag> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsRecognTag>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsRecognTag.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsRecognTag-objects as value to a dart map
  static Map<String, List<RsRecognTag>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsRecognTag>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsRecognTag.listFromJson(value);
       });
     }
     return map;
  }
}

