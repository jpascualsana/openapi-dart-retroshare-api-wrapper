part of openapi.api;

class CompressedChunkMap {
  
  List<int> map = [];
  CompressedChunkMap();

  @override
  String toString() {
    return 'CompressedChunkMap[map=$map, ]';
  }

  CompressedChunkMap.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    map = (json['_map'] == null) ?
      null :
      (json['_map'] as List).cast<int>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (map != null)
      json['_map'] = map;
    return json;
  }

  static List<CompressedChunkMap> listFromJson(List<dynamic> json) {
    return json == null ? List<CompressedChunkMap>() : json.map((value) => CompressedChunkMap.fromJson(value)).toList();
  }

  static Map<String, CompressedChunkMap> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, CompressedChunkMap>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = CompressedChunkMap.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of CompressedChunkMap-objects as value to a dart map
  static Map<String, List<CompressedChunkMap>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<CompressedChunkMap>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = CompressedChunkMap.listFromJson(value);
       });
     }
     return map;
  }
}

