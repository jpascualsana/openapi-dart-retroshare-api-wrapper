part of openapi.api;

class ResGetLastUsageTS {
  
  RstimeT retval = null;
  ResGetLastUsageTS();

  @override
  String toString() {
    return 'ResGetLastUsageTS[retval=$retval, ]';
  }

  ResGetLastUsageTS.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      RstimeT.fromJson(json['retval']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetLastUsageTS> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetLastUsageTS>() : json.map((value) => ResGetLastUsageTS.fromJson(value)).toList();
  }

  static Map<String, ResGetLastUsageTS> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetLastUsageTS>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetLastUsageTS.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetLastUsageTS-objects as value to a dart map
  static Map<String, List<ResGetLastUsageTS>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetLastUsageTS>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetLastUsageTS.listFromJson(value);
       });
     }
     return map;
  }
}

