part of openapi.api;

class ResSetMessageTagType {
  
  bool retval = null;
  ResSetMessageTagType();

  @override
  String toString() {
    return 'ResSetMessageTagType[retval=$retval, ]';
  }

  ResSetMessageTagType.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetMessageTagType> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetMessageTagType>() : json.map((value) => ResSetMessageTagType.fromJson(value)).toList();
  }

  static Map<String, ResSetMessageTagType> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetMessageTagType>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetMessageTagType.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetMessageTagType-objects as value to a dart map
  static Map<String, List<ResSetMessageTagType>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetMessageTagType>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetMessageTagType.listFromJson(value);
       });
     }
     return map;
  }
}

