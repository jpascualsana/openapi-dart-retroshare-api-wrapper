part of openapi.api;

class ReqGetIdentityForChatLobby {
  
  ChatLobbyId lobbyId = null;
  ReqGetIdentityForChatLobby();

  @override
  String toString() {
    return 'ReqGetIdentityForChatLobby[lobbyId=$lobbyId, ]';
  }

  ReqGetIdentityForChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    lobbyId = (json['lobby_id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['lobby_id']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (lobbyId != null)
      json['lobby_id'] = lobbyId;
    return json;
  }

  static List<ReqGetIdentityForChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetIdentityForChatLobby>() : json.map((value) => ReqGetIdentityForChatLobby.fromJson(value)).toList();
  }

  static Map<String, ReqGetIdentityForChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetIdentityForChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetIdentityForChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetIdentityForChatLobby-objects as value to a dart map
  static Map<String, List<ReqGetIdentityForChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetIdentityForChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetIdentityForChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}

