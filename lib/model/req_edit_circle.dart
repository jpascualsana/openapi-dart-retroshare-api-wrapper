part of openapi.api;

class ReqEditCircle {
  
  RsGxsCircleGroup cData = null;
  ReqEditCircle();

  @override
  String toString() {
    return 'ReqEditCircle[cData=$cData, ]';
  }

  ReqEditCircle.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    cData = (json['cData'] == null) ?
      null :
      RsGxsCircleGroup.fromJson(json['cData']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (cData != null)
      json['cData'] = cData;
    return json;
  }

  static List<ReqEditCircle> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqEditCircle>() : json.map((value) => ReqEditCircle.fromJson(value)).toList();
  }

  static Map<String, ReqEditCircle> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqEditCircle>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqEditCircle.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqEditCircle-objects as value to a dart map
  static Map<String, List<ReqEditCircle>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqEditCircle>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqEditCircle.listFromJson(value);
       });
     }
     return map;
  }
}

