part of openapi.api;

class ResExportCollectionLink {
  
  ResExportCollectionLinkRetval retval = null;
  
  String link = null;
  ResExportCollectionLink();

  @override
  String toString() {
    return 'ResExportCollectionLink[retval=$retval, link=$link, ]';
  }

  ResExportCollectionLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      ResExportCollectionLinkRetval.fromJson(json['retval']);
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (link != null)
      json['link'] = link;
    return json;
  }

  static List<ResExportCollectionLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ResExportCollectionLink>() : json.map((value) => ResExportCollectionLink.fromJson(value)).toList();
  }

  static Map<String, ResExportCollectionLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResExportCollectionLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResExportCollectionLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResExportCollectionLink-objects as value to a dart map
  static Map<String, List<ResExportCollectionLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResExportCollectionLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResExportCollectionLink.listFromJson(value);
       });
     }
     return map;
  }
}

