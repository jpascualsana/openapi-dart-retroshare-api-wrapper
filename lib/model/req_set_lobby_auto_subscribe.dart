part of openapi.api;

class ReqSetLobbyAutoSubscribe {
  
  ChatLobbyId lobbyId = null;
  
  bool autoSubscribe = null;
  ReqSetLobbyAutoSubscribe();

  @override
  String toString() {
    return 'ReqSetLobbyAutoSubscribe[lobbyId=$lobbyId, autoSubscribe=$autoSubscribe, ]';
  }

  ReqSetLobbyAutoSubscribe.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    lobbyId = (json['lobby_id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['lobby_id']);
    autoSubscribe = json['autoSubscribe'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (lobbyId != null)
      json['lobby_id'] = lobbyId;
    if (autoSubscribe != null)
      json['autoSubscribe'] = autoSubscribe;
    return json;
  }

  static List<ReqSetLobbyAutoSubscribe> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetLobbyAutoSubscribe>() : json.map((value) => ReqSetLobbyAutoSubscribe.fromJson(value)).toList();
  }

  static Map<String, ReqSetLobbyAutoSubscribe> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetLobbyAutoSubscribe>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetLobbyAutoSubscribe.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetLobbyAutoSubscribe-objects as value to a dart map
  static Map<String, List<ReqSetLobbyAutoSubscribe>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetLobbyAutoSubscribe>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetLobbyAutoSubscribe.listFromJson(value);
       });
     }
     return map;
  }
}

