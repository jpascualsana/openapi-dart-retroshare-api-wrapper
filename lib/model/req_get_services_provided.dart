part of openapi.api;

class ReqGetServicesProvided {
  
  String peerId = null;
  ReqGetServicesProvided();

  @override
  String toString() {
    return 'ReqGetServicesProvided[peerId=$peerId, ]';
  }

  ReqGetServicesProvided.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    peerId = json['peerId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (peerId != null)
      json['peerId'] = peerId;
    return json;
  }

  static List<ReqGetServicesProvided> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetServicesProvided>() : json.map((value) => ReqGetServicesProvided.fromJson(value)).toList();
  }

  static Map<String, ReqGetServicesProvided> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetServicesProvided>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetServicesProvided.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetServicesProvided-objects as value to a dart map
  static Map<String, List<ReqGetServicesProvided>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetServicesProvided>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetServicesProvided.listFromJson(value);
       });
     }
     return map;
  }
}

