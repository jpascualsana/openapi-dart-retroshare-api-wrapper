part of openapi.api;

class ResSetIdentityForChatLobby {
  
  bool retval = null;
  ResSetIdentityForChatLobby();

  @override
  String toString() {
    return 'ResSetIdentityForChatLobby[retval=$retval, ]';
  }

  ResSetIdentityForChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetIdentityForChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetIdentityForChatLobby>() : json.map((value) => ResSetIdentityForChatLobby.fromJson(value)).toList();
  }

  static Map<String, ResSetIdentityForChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetIdentityForChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetIdentityForChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetIdentityForChatLobby-objects as value to a dart map
  static Map<String, List<ResSetIdentityForChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetIdentityForChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetIdentityForChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}

