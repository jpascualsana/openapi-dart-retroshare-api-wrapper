part of openapi.api;

class ResGetPgpFriendList {
  
  bool retval = null;
  
  List<String> pgpIds = [];
  ResGetPgpFriendList();

  @override
  String toString() {
    return 'ResGetPgpFriendList[retval=$retval, pgpIds=$pgpIds, ]';
  }

  ResGetPgpFriendList.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    pgpIds = (json['pgpIds'] == null) ?
      null :
      (json['pgpIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (pgpIds != null)
      json['pgpIds'] = pgpIds;
    return json;
  }

  static List<ResGetPgpFriendList> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetPgpFriendList>() : json.map((value) => ResGetPgpFriendList.fromJson(value)).toList();
  }

  static Map<String, ResGetPgpFriendList> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetPgpFriendList>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetPgpFriendList.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetPgpFriendList-objects as value to a dart map
  static Map<String, List<ResGetPgpFriendList>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetPgpFriendList>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetPgpFriendList.listFromJson(value);
       });
     }
     return map;
  }
}

