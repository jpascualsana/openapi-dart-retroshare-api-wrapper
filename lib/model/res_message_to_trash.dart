part of openapi.api;

class ResMessageToTrash {
  
  bool retval = null;
  ResMessageToTrash();

  @override
  String toString() {
    return 'ResMessageToTrash[retval=$retval, ]';
  }

  ResMessageToTrash.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResMessageToTrash> listFromJson(List<dynamic> json) {
    return json == null ? List<ResMessageToTrash>() : json.map((value) => ResMessageToTrash.fromJson(value)).toList();
  }

  static Map<String, ResMessageToTrash> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResMessageToTrash>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResMessageToTrash.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResMessageToTrash-objects as value to a dart map
  static Map<String, List<ResMessageToTrash>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResMessageToTrash>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResMessageToTrash.listFromJson(value);
       });
     }
     return map;
  }
}

