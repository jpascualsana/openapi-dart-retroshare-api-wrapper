part of openapi.api;

class ResGetRetroshareInvite {
  
  String retval = null;
  ResGetRetroshareInvite();

  @override
  String toString() {
    return 'ResGetRetroshareInvite[retval=$retval, ]';
  }

  ResGetRetroshareInvite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetRetroshareInvite> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetRetroshareInvite>() : json.map((value) => ResGetRetroshareInvite.fromJson(value)).toList();
  }

  static Map<String, ResGetRetroshareInvite> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetRetroshareInvite>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetRetroshareInvite.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetRetroshareInvite-objects as value to a dart map
  static Map<String, List<ResGetRetroshareInvite>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetRetroshareInvite>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetRetroshareInvite.listFromJson(value);
       });
     }
     return map;
  }
}

