part of openapi.api;

class ReqFileDownloadChunksDetails {
  
  String hash = null;
  ReqFileDownloadChunksDetails();

  @override
  String toString() {
    return 'ReqFileDownloadChunksDetails[hash=$hash, ]';
  }

  ReqFileDownloadChunksDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hash = json['hash'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hash != null)
      json['hash'] = hash;
    return json;
  }

  static List<ReqFileDownloadChunksDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqFileDownloadChunksDetails>() : json.map((value) => ReqFileDownloadChunksDetails.fromJson(value)).toList();
  }

  static Map<String, ReqFileDownloadChunksDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqFileDownloadChunksDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqFileDownloadChunksDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqFileDownloadChunksDetails-objects as value to a dart map
  static Map<String, List<ReqFileDownloadChunksDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqFileDownloadChunksDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqFileDownloadChunksDetails.listFromJson(value);
       });
     }
     return map;
  }
}

