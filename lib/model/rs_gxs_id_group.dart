part of openapi.api;

class RsGxsIdGroup {
  
  RsGroupMetaData mMeta = null;
  
  String mPgpIdHash = null;
  
  String mPgpIdSign = null;
  
  List<String> mRecognTags = [];
  
  RsGxsImage mImage = null;
  
  RstimeT mLastUsageTS = null;
  
  bool mPgpKnown = null;
  
  bool mIsAContact = null;
  
  String mPgpId = null;
  
  GxsReputation mReputation = null;
  RsGxsIdGroup();

  @override
  String toString() {
    return 'RsGxsIdGroup[mMeta=$mMeta, mPgpIdHash=$mPgpIdHash, mPgpIdSign=$mPgpIdSign, mRecognTags=$mRecognTags, mImage=$mImage, mLastUsageTS=$mLastUsageTS, mPgpKnown=$mPgpKnown, mIsAContact=$mIsAContact, mPgpId=$mPgpId, mReputation=$mReputation, ]';
  }

  RsGxsIdGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mMeta = (json['mMeta'] == null) ?
      null :
      RsGroupMetaData.fromJson(json['mMeta']);
    mPgpIdHash = json['mPgpIdHash'];
    mPgpIdSign = json['mPgpIdSign'];
    mRecognTags = (json['mRecognTags'] == null) ?
      null :
      (json['mRecognTags'] as List).cast<String>();
    mImage = (json['mImage'] == null) ?
      null :
      RsGxsImage.fromJson(json['mImage']);
    mLastUsageTS = (json['mLastUsageTS'] == null) ?
      null :
      RstimeT.fromJson(json['mLastUsageTS']);
    mPgpKnown = json['mPgpKnown'];
    mIsAContact = json['mIsAContact'];
    mPgpId = json['mPgpId'];
    mReputation = (json['mReputation'] == null) ?
      null :
      GxsReputation.fromJson(json['mReputation']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mMeta != null)
      json['mMeta'] = mMeta;
    if (mPgpIdHash != null)
      json['mPgpIdHash'] = mPgpIdHash;
    if (mPgpIdSign != null)
      json['mPgpIdSign'] = mPgpIdSign;
    if (mRecognTags != null)
      json['mRecognTags'] = mRecognTags;
    if (mImage != null)
      json['mImage'] = mImage;
    if (mLastUsageTS != null)
      json['mLastUsageTS'] = mLastUsageTS;
    if (mPgpKnown != null)
      json['mPgpKnown'] = mPgpKnown;
    if (mIsAContact != null)
      json['mIsAContact'] = mIsAContact;
    if (mPgpId != null)
      json['mPgpId'] = mPgpId;
    if (mReputation != null)
      json['mReputation'] = mReputation;
    return json;
  }

  static List<RsGxsIdGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsIdGroup>() : json.map((value) => RsGxsIdGroup.fromJson(value)).toList();
  }

  static Map<String, RsGxsIdGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsIdGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsIdGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsIdGroup-objects as value to a dart map
  static Map<String, List<RsGxsIdGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsIdGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsIdGroup.listFromJson(value);
       });
     }
     return map;
  }
}

