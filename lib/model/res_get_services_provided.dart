part of openapi.api;

class ResGetServicesProvided {
  
  bool retval = null;
  
  RsPeerServiceInfo info = null;
  ResGetServicesProvided();

  @override
  String toString() {
    return 'ResGetServicesProvided[retval=$retval, info=$info, ]';
  }

  ResGetServicesProvided.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    info = (json['info'] == null) ?
      null :
      RsPeerServiceInfo.fromJson(json['info']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (info != null)
      json['info'] = info;
    return json;
  }

  static List<ResGetServicesProvided> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetServicesProvided>() : json.map((value) => ResGetServicesProvided.fromJson(value)).toList();
  }

  static Map<String, ResGetServicesProvided> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetServicesProvided>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetServicesProvided.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetServicesProvided-objects as value to a dart map
  static Map<String, List<ResGetServicesProvided>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetServicesProvided>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetServicesProvided.listFromJson(value);
       });
     }
     return map;
  }
}

