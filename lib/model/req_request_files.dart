part of openapi.api;

class ReqRequestFiles {
  
  RsFileTree collection = null;
  
  String destPath = null;
  
  List<String> srcIds = [];
  
  FileRequestFlags flags = null;
  //enum flagsEnum {  64,  128,  256,  8192,  33554432,  };{
  ReqRequestFiles();

  @override
  String toString() {
    return 'ReqRequestFiles[collection=$collection, destPath=$destPath, srcIds=$srcIds, flags=$flags, ]';
  }

  ReqRequestFiles.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    collection = (json['collection'] == null) ?
      null :
      RsFileTree.fromJson(json['collection']);
    destPath = json['destPath'];
    srcIds = (json['srcIds'] == null) ?
      null :
      (json['srcIds'] as List).cast<String>();
    flags = (json['flags'] == null) ?
      null :
      FileRequestFlags.fromJson(json['flags']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (collection != null)
      json['collection'] = collection;
    if (destPath != null)
      json['destPath'] = destPath;
    if (srcIds != null)
      json['srcIds'] = srcIds;
    if (flags != null)
      json['flags'] = flags;
    return json;
  }

  static List<ReqRequestFiles> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRequestFiles>() : json.map((value) => ReqRequestFiles.fromJson(value)).toList();
  }

  static Map<String, ReqRequestFiles> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRequestFiles>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRequestFiles.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRequestFiles-objects as value to a dart map
  static Map<String, List<ReqRequestFiles>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRequestFiles>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRequestFiles.listFromJson(value);
       });
     }
     return map;
  }
}

