part of openapi.api;

class ResGetPGPLogins {
  
  int retval = null;
  
  List<String> pgpIds = [];
  ResGetPGPLogins();

  @override
  String toString() {
    return 'ResGetPGPLogins[retval=$retval, pgpIds=$pgpIds, ]';
  }

  ResGetPGPLogins.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    pgpIds = (json['pgpIds'] == null) ?
      null :
      (json['pgpIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (pgpIds != null)
      json['pgpIds'] = pgpIds;
    return json;
  }

  static List<ResGetPGPLogins> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetPGPLogins>() : json.map((value) => ResGetPGPLogins.fromJson(value)).toList();
  }

  static Map<String, ResGetPGPLogins> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetPGPLogins>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetPGPLogins.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetPGPLogins-objects as value to a dart map
  static Map<String, List<ResGetPGPLogins>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetPGPLogins>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetPGPLogins.listFromJson(value);
       });
     }
     return map;
  }
}

