part of openapi.api;

class ReqJoinVisibleChatLobby {
  
  ChatLobbyId lobbyId = null;
  
  String ownId = null;
  ReqJoinVisibleChatLobby();

  @override
  String toString() {
    return 'ReqJoinVisibleChatLobby[lobbyId=$lobbyId, ownId=$ownId, ]';
  }

  ReqJoinVisibleChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    lobbyId = (json['lobby_id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['lobby_id']);
    ownId = json['own_id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (lobbyId != null)
      json['lobby_id'] = lobbyId;
    if (ownId != null)
      json['own_id'] = ownId;
    return json;
  }

  static List<ReqJoinVisibleChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqJoinVisibleChatLobby>() : json.map((value) => ReqJoinVisibleChatLobby.fromJson(value)).toList();
  }

  static Map<String, ReqJoinVisibleChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqJoinVisibleChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqJoinVisibleChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqJoinVisibleChatLobby-objects as value to a dart map
  static Map<String, List<ReqJoinVisibleChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqJoinVisibleChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqJoinVisibleChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}

