part of openapi.api;

class ResSetLocalAddress {
  
  bool retval = null;
  ResSetLocalAddress();

  @override
  String toString() {
    return 'ResSetLocalAddress[retval=$retval, ]';
  }

  ResSetLocalAddress.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetLocalAddress> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetLocalAddress>() : json.map((value) => ResSetLocalAddress.fromJson(value)).toList();
  }

  static Map<String, ResSetLocalAddress> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetLocalAddress>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetLocalAddress.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetLocalAddress-objects as value to a dart map
  static Map<String, List<ResSetLocalAddress>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetLocalAddress>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetLocalAddress.listFromJson(value);
       });
     }
     return map;
  }
}

