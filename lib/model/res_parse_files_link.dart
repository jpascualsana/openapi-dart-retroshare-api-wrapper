part of openapi.api;

class ResParseFilesLink {
  
  ResExportCollectionLinkRetval retval = null;
  
  RsFileTree collection = null;
  ResParseFilesLink();

  @override
  String toString() {
    return 'ResParseFilesLink[retval=$retval, collection=$collection, ]';
  }

  ResParseFilesLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      ResExportCollectionLinkRetval.fromJson(json['retval']);
    collection = (json['collection'] == null) ?
      null :
      RsFileTree.fromJson(json['collection']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (collection != null)
      json['collection'] = collection;
    return json;
  }

  static List<ResParseFilesLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ResParseFilesLink>() : json.map((value) => ResParseFilesLink.fromJson(value)).toList();
  }

  static Map<String, ResParseFilesLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResParseFilesLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResParseFilesLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResParseFilesLink-objects as value to a dart map
  static Map<String, List<ResParseFilesLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResParseFilesLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResParseFilesLink.listFromJson(value);
       });
     }
     return map;
  }
}

