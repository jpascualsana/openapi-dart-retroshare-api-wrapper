part of openapi.api;

class ReqPgpIdFromFingerprint {
  
  String fpr = null;
  ReqPgpIdFromFingerprint();

  @override
  String toString() {
    return 'ReqPgpIdFromFingerprint[fpr=$fpr, ]';
  }

  ReqPgpIdFromFingerprint.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    fpr = json['fpr'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (fpr != null)
      json['fpr'] = fpr;
    return json;
  }

  static List<ReqPgpIdFromFingerprint> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqPgpIdFromFingerprint>() : json.map((value) => ReqPgpIdFromFingerprint.fromJson(value)).toList();
  }

  static Map<String, ReqPgpIdFromFingerprint> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqPgpIdFromFingerprint>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqPgpIdFromFingerprint.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqPgpIdFromFingerprint-objects as value to a dart map
  static Map<String, List<ReqPgpIdFromFingerprint>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqPgpIdFromFingerprint>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqPgpIdFromFingerprint.listFromJson(value);
       });
     }
     return map;
  }
}

