part of openapi.api;

class ReqGetDistantChatStatus {
  
  String pid = null;
  ReqGetDistantChatStatus();

  @override
  String toString() {
    return 'ReqGetDistantChatStatus[pid=$pid, ]';
  }

  ReqGetDistantChatStatus.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    pid = json['pid'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (pid != null)
      json['pid'] = pid;
    return json;
  }

  static List<ReqGetDistantChatStatus> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetDistantChatStatus>() : json.map((value) => ReqGetDistantChatStatus.fromJson(value)).toList();
  }

  static Map<String, ReqGetDistantChatStatus> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetDistantChatStatus>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetDistantChatStatus.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetDistantChatStatus-objects as value to a dart map
  static Map<String, List<ReqGetDistantChatStatus>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetDistantChatStatus>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetDistantChatStatus.listFromJson(value);
       });
     }
     return map;
  }
}

