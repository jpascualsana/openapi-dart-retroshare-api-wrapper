part of openapi.api;

class ReqSetPartialsDirectory {
  
  String path = null;
  ReqSetPartialsDirectory();

  @override
  String toString() {
    return 'ReqSetPartialsDirectory[path=$path, ]';
  }

  ReqSetPartialsDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    path = json['path'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (path != null)
      json['path'] = path;
    return json;
  }

  static List<ReqSetPartialsDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetPartialsDirectory>() : json.map((value) => ReqSetPartialsDirectory.fromJson(value)).toList();
  }

  static Map<String, ReqSetPartialsDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetPartialsDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetPartialsDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetPartialsDirectory-objects as value to a dart map
  static Map<String, List<ReqSetPartialsDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetPartialsDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetPartialsDirectory.listFromJson(value);
       });
     }
     return map;
  }
}

