part of openapi.api;

class ResGetChannelAllContent {
  
  bool retval = null;
  
  List<RsGxsChannelPost> posts = [];
  
  List<RsGxsComment> comments = [];
  
  List<RsGxsVote> votes = [];
  ResGetChannelAllContent();

  @override
  String toString() {
    return 'ResGetChannelAllContent[retval=$retval, posts=$posts, comments=$comments, votes=$votes, ]';
  }

  ResGetChannelAllContent.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    posts = (json['posts'] == null) ?
      null :
      RsGxsChannelPost.listFromJson(json['posts']);
    comments = (json['comments'] == null) ?
      null :
      RsGxsComment.listFromJson(json['comments']);
    votes = (json['votes'] == null) ?
      null :
      RsGxsVote.listFromJson(json['votes']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (posts != null)
      json['posts'] = posts;
    if (comments != null)
      json['comments'] = comments;
    if (votes != null)
      json['votes'] = votes;
    return json;
  }

  static List<ResGetChannelAllContent> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetChannelAllContent>() : json.map((value) => ResGetChannelAllContent.fromJson(value)).toList();
  }

  static Map<String, ResGetChannelAllContent> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetChannelAllContent>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetChannelAllContent.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetChannelAllContent-objects as value to a dart map
  static Map<String, List<ResGetChannelAllContent>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetChannelAllContent>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetChannelAllContent.listFromJson(value);
       });
     }
     return map;
  }
}

