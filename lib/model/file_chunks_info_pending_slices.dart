part of openapi.api;

class FileChunksInfoPendingSlices {
  
  int key = null;
  
  List<FileChunksInfoSliceInfo> value = [];
  FileChunksInfoPendingSlices();

  @override
  String toString() {
    return 'FileChunksInfoPendingSlices[key=$key, value=$value, ]';
  }

  FileChunksInfoPendingSlices.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    key = json['key'];
    value = (json['value'] == null) ?
      null :
      FileChunksInfoSliceInfo.listFromJson(json['value']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (key != null)
      json['key'] = key;
    if (value != null)
      json['value'] = value;
    return json;
  }

  static List<FileChunksInfoPendingSlices> listFromJson(List<dynamic> json) {
    return json == null ? List<FileChunksInfoPendingSlices>() : json.map((value) => FileChunksInfoPendingSlices.fromJson(value)).toList();
  }

  static Map<String, FileChunksInfoPendingSlices> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, FileChunksInfoPendingSlices>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = FileChunksInfoPendingSlices.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of FileChunksInfoPendingSlices-objects as value to a dart map
  static Map<String, List<FileChunksInfoPendingSlices>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<FileChunksInfoPendingSlices>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = FileChunksInfoPendingSlices.listFromJson(value);
       });
     }
     return map;
  }
}

