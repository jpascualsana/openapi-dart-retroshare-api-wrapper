part of openapi.api;

class ResCreateChannel {
  
  bool retval = null;
  
  RsGxsChannelGroup channel = null;
  ResCreateChannel();

  @override
  String toString() {
    return 'ResCreateChannel[retval=$retval, channel=$channel, ]';
  }

  ResCreateChannel.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    channel = (json['channel'] == null) ?
      null :
      RsGxsChannelGroup.fromJson(json['channel']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (channel != null)
      json['channel'] = channel;
    return json;
  }

  static List<ResCreateChannel> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateChannel>() : json.map((value) => ResCreateChannel.fromJson(value)).toList();
  }

  static Map<String, ResCreateChannel> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateChannel>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateChannel.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateChannel-objects as value to a dart map
  static Map<String, List<ResCreateChannel>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateChannel>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateChannel.listFromJson(value);
       });
     }
     return map;
  }
}

