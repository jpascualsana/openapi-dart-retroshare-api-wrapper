part of openapi.api;

class ResGetConfigNetStatus {
  
  int retval = null;
  
  RsConfigNetStatus status = null;
  ResGetConfigNetStatus();

  @override
  String toString() {
    return 'ResGetConfigNetStatus[retval=$retval, status=$status, ]';
  }

  ResGetConfigNetStatus.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    status = (json['status'] == null) ?
      null :
      RsConfigNetStatus.fromJson(json['status']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (status != null)
      json['status'] = status;
    return json;
  }

  static List<ResGetConfigNetStatus> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetConfigNetStatus>() : json.map((value) => ResGetConfigNetStatus.fromJson(value)).toList();
  }

  static Map<String, ResGetConfigNetStatus> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetConfigNetStatus>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetConfigNetStatus.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetConfigNetStatus-objects as value to a dart map
  static Map<String, List<ResGetConfigNetStatus>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetConfigNetStatus>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetConfigNetStatus.listFromJson(value);
       });
     }
     return map;
  }
}

