part of openapi.api;

class ResIsARegularContact {
  
  bool retval = null;
  ResIsARegularContact();

  @override
  String toString() {
    return 'ResIsARegularContact[retval=$retval, ]';
  }

  ResIsARegularContact.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsARegularContact> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsARegularContact>() : json.map((value) => ResIsARegularContact.fromJson(value)).toList();
  }

  static Map<String, ResIsARegularContact> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsARegularContact>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsARegularContact.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsARegularContact-objects as value to a dart map
  static Map<String, List<ResIsARegularContact>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsARegularContact>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsARegularContact.listFromJson(value);
       });
     }
     return map;
  }
}

