part of openapi.api;

class ResSetChunkStrategy {
  
  bool retval = null;
  ResSetChunkStrategy();

  @override
  String toString() {
    return 'ResSetChunkStrategy[retval=$retval, ]';
  }

  ResSetChunkStrategy.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetChunkStrategy> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetChunkStrategy>() : json.map((value) => ResSetChunkStrategy.fromJson(value)).toList();
  }

  static Map<String, ResSetChunkStrategy> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetChunkStrategy>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetChunkStrategy.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetChunkStrategy-objects as value to a dart map
  static Map<String, List<ResSetChunkStrategy>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetChunkStrategy>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetChunkStrategy.listFromJson(value);
       });
     }
     return map;
  }
}

