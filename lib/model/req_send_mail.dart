part of openapi.api;

class ReqSendMail {
  
  String from = null;
  
  String subject = null;
  
  String mailBody = null;
  
  List<String> to = [];
  
  List<String> cc = [];
  
  List<String> bcc = [];
  
  List<FileInfo> attachments = [];
  ReqSendMail();

  @override
  String toString() {
    return 'ReqSendMail[from=$from, subject=$subject, mailBody=$mailBody, to=$to, cc=$cc, bcc=$bcc, attachments=$attachments, ]';
  }

  ReqSendMail.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    from = json['from'];
    subject = json['subject'];
    mailBody = json['mailBody'];
    to = (json['to'] == null) ?
      null :
      (json['to'] as List).cast<String>();
    cc = (json['cc'] == null) ?
      null :
      (json['cc'] as List).cast<String>();
    bcc = (json['bcc'] == null) ?
      null :
      (json['bcc'] as List).cast<String>();
    attachments = (json['attachments'] == null) ?
      null :
      FileInfo.listFromJson(json['attachments']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (from != null)
      json['from'] = from;
    if (subject != null)
      json['subject'] = subject;
    if (mailBody != null)
      json['mailBody'] = mailBody;
    if (to != null)
      json['to'] = to;
    if (cc != null)
      json['cc'] = cc;
    if (bcc != null)
      json['bcc'] = bcc;
    if (attachments != null)
      json['attachments'] = attachments;
    return json;
  }

  static List<ReqSendMail> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSendMail>() : json.map((value) => ReqSendMail.fromJson(value)).toList();
  }

  static Map<String, ReqSendMail> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSendMail>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSendMail.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSendMail-objects as value to a dart map
  static Map<String, List<ReqSendMail>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSendMail>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSendMail.listFromJson(value);
       });
     }
     return map;
  }
}

