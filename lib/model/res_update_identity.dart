part of openapi.api;

class ResUpdateIdentity {
  
  bool retval = null;
  ResUpdateIdentity();

  @override
  String toString() {
    return 'ResUpdateIdentity[retval=$retval, ]';
  }

  ResUpdateIdentity.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResUpdateIdentity> listFromJson(List<dynamic> json) {
    return json == null ? List<ResUpdateIdentity>() : json.map((value) => ResUpdateIdentity.fromJson(value)).toList();
  }

  static Map<String, ResUpdateIdentity> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResUpdateIdentity>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResUpdateIdentity.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResUpdateIdentity-objects as value to a dart map
  static Map<String, List<ResUpdateIdentity>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResUpdateIdentity>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResUpdateIdentity.listFromJson(value);
       });
     }
     return map;
  }
}

