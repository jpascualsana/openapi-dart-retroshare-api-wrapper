part of openapi.api;

class ChatLobbyInvite {
  
  ChatLobbyId lobbyId = null;
  
  String peerId = null;
  
  String lobbyName = null;
  
  String lobbyTopic = null;
  
  int lobbyFlags = null;
  ChatLobbyInvite();

  @override
  String toString() {
    return 'ChatLobbyInvite[lobbyId=$lobbyId, peerId=$peerId, lobbyName=$lobbyName, lobbyTopic=$lobbyTopic, lobbyFlags=$lobbyFlags, ]';
  }

  ChatLobbyInvite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    lobbyId = (json['lobby_id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['lobby_id']);
    peerId = json['peer_id'];
    lobbyName = json['lobby_name'];
    lobbyTopic = json['lobby_topic'];
    lobbyFlags = json['lobby_flags'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (lobbyId != null)
      json['lobby_id'] = lobbyId;
    if (peerId != null)
      json['peer_id'] = peerId;
    if (lobbyName != null)
      json['lobby_name'] = lobbyName;
    if (lobbyTopic != null)
      json['lobby_topic'] = lobbyTopic;
    if (lobbyFlags != null)
      json['lobby_flags'] = lobbyFlags;
    return json;
  }

  static List<ChatLobbyInvite> listFromJson(List<dynamic> json) {
    return json == null ? List<ChatLobbyInvite>() : json.map((value) => ChatLobbyInvite.fromJson(value)).toList();
  }

  static Map<String, ChatLobbyInvite> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ChatLobbyInvite>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ChatLobbyInvite.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ChatLobbyInvite-objects as value to a dart map
  static Map<String, List<ChatLobbyInvite>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ChatLobbyInvite>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ChatLobbyInvite.listFromJson(value);
       });
     }
     return map;
  }
}

