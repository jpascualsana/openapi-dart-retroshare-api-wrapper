part of openapi.api;

class ResRequestStatus {
  
  RsTokenServiceGxsRequestStatus retval = null;
  //enum retvalEnum {  0,  1,  2,  3,  4,  5,  };{
  ResRequestStatus();

  @override
  String toString() {
    return 'ResRequestStatus[retval=$retval, ]';
  }

  ResRequestStatus.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      RsTokenServiceGxsRequestStatus.fromJson(json['retval']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRequestStatus> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRequestStatus>() : json.map((value) => ResRequestStatus.fromJson(value)).toList();
  }

  static Map<String, ResRequestStatus> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRequestStatus>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRequestStatus.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRequestStatus-objects as value to a dart map
  static Map<String, List<ResRequestStatus>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRequestStatus>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRequestStatus.listFromJson(value);
       });
     }
     return map;
  }
}

