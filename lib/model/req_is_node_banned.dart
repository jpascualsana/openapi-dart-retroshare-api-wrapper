part of openapi.api;

class ReqIsNodeBanned {
  
  String id = null;
  ReqIsNodeBanned();

  @override
  String toString() {
    return 'ReqIsNodeBanned[id=$id, ]';
  }

  ReqIsNodeBanned.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqIsNodeBanned> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqIsNodeBanned>() : json.map((value) => ReqIsNodeBanned.fromJson(value)).toList();
  }

  static Map<String, ReqIsNodeBanned> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqIsNodeBanned>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqIsNodeBanned.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqIsNodeBanned-objects as value to a dart map
  static Map<String, List<ReqIsNodeBanned>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqIsNodeBanned>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqIsNodeBanned.listFromJson(value);
       });
     }
     return map;
  }
}

