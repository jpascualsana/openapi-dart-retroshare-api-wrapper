part of openapi.api;

class ReqIsIdentityBanned {
  
  String id = null;
  ReqIsIdentityBanned();

  @override
  String toString() {
    return 'ReqIsIdentityBanned[id=$id, ]';
  }

  ReqIsIdentityBanned.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqIsIdentityBanned> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqIsIdentityBanned>() : json.map((value) => ReqIsIdentityBanned.fromJson(value)).toList();
  }

  static Map<String, ReqIsIdentityBanned> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqIsIdentityBanned>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqIsIdentityBanned.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqIsIdentityBanned-objects as value to a dart map
  static Map<String, List<ReqIsIdentityBanned>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqIsIdentityBanned>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqIsIdentityBanned.listFromJson(value);
       });
     }
     return map;
  }
}

