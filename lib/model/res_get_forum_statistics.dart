part of openapi.api;

class ResGetForumStatistics {
  
  bool retval = null;
  
  GxsGroupStatistic stat = null;
  ResGetForumStatistics();

  @override
  String toString() {
    return 'ResGetForumStatistics[retval=$retval, stat=$stat, ]';
  }

  ResGetForumStatistics.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    stat = (json['stat'] == null) ?
      null :
      GxsGroupStatistic.fromJson(json['stat']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (stat != null)
      json['stat'] = stat;
    return json;
  }

  static List<ResGetForumStatistics> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetForumStatistics>() : json.map((value) => ResGetForumStatistics.fromJson(value)).toList();
  }

  static Map<String, ResGetForumStatistics> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetForumStatistics>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetForumStatistics.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetForumStatistics-objects as value to a dart map
  static Map<String, List<ResGetForumStatistics>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetForumStatistics>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetForumStatistics.listFromJson(value);
       });
     }
     return map;
  }
}

