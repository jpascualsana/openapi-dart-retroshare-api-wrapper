part of openapi.api;

class ReqSendChat {
  
  ChatId id = null;
  
  String msg = null;
  ReqSendChat();

  @override
  String toString() {
    return 'ReqSendChat[id=$id, msg=$msg, ]';
  }

  ReqSendChat.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = (json['id'] == null) ?
      null :
      ChatId.fromJson(json['id']);
    msg = json['msg'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (msg != null)
      json['msg'] = msg;
    return json;
  }

  static List<ReqSendChat> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSendChat>() : json.map((value) => ReqSendChat.fromJson(value)).toList();
  }

  static Map<String, ReqSendChat> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSendChat>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSendChat.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSendChat-objects as value to a dart map
  static Map<String, List<ReqSendChat>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSendChat>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSendChat.listFromJson(value);
       });
     }
     return map;
  }
}

