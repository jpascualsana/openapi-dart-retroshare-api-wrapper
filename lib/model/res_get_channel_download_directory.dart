part of openapi.api;

class ResGetChannelDownloadDirectory {
  
  bool retval = null;
  
  String directory = null;
  ResGetChannelDownloadDirectory();

  @override
  String toString() {
    return 'ResGetChannelDownloadDirectory[retval=$retval, directory=$directory, ]';
  }

  ResGetChannelDownloadDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    directory = json['directory'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (directory != null)
      json['directory'] = directory;
    return json;
  }

  static List<ResGetChannelDownloadDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetChannelDownloadDirectory>() : json.map((value) => ResGetChannelDownloadDirectory.fromJson(value)).toList();
  }

  static Map<String, ResGetChannelDownloadDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetChannelDownloadDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetChannelDownloadDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetChannelDownloadDirectory-objects as value to a dart map
  static Map<String, List<ResGetChannelDownloadDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetChannelDownloadDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetChannelDownloadDirectory.listFromJson(value);
       });
     }
     return map;
  }
}

