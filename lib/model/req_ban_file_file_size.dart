part of openapi.api;

class ReqBanFileFileSize {
  
  num xint64 = null;
  
  String xstr64 = null;
  ReqBanFileFileSize();

  @override
  String toString() {
    return 'ReqBanFileFileSize[xint64=$xint64, xstr64=$xstr64, ]';
  }

  ReqBanFileFileSize.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    xint64 = json['xint64'];
    xstr64 = json['xstr64'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (xint64 != null)
      json['xint64'] = xint64;
    if (xstr64 != null)
      json['xstr64'] = xstr64;
    return json;
  }

  static List<ReqBanFileFileSize> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqBanFileFileSize>() : json.map((value) => ReqBanFileFileSize.fromJson(value)).toList();
  }

  static Map<String, ReqBanFileFileSize> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqBanFileFileSize>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqBanFileFileSize.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqBanFileFileSize-objects as value to a dart map
  static Map<String, List<ReqBanFileFileSize>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqBanFileFileSize>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqBanFileFileSize.listFromJson(value);
       });
     }
     return map;
  }
}

