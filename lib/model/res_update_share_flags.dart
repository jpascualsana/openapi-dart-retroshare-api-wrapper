part of openapi.api;

class ResUpdateShareFlags {
  
  bool retval = null;
  ResUpdateShareFlags();

  @override
  String toString() {
    return 'ResUpdateShareFlags[retval=$retval, ]';
  }

  ResUpdateShareFlags.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResUpdateShareFlags> listFromJson(List<dynamic> json) {
    return json == null ? List<ResUpdateShareFlags>() : json.map((value) => ResUpdateShareFlags.fromJson(value)).toList();
  }

  static Map<String, ResUpdateShareFlags> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResUpdateShareFlags>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResUpdateShareFlags.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResUpdateShareFlags-objects as value to a dart map
  static Map<String, List<ResUpdateShareFlags>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResUpdateShareFlags>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResUpdateShareFlags.listFromJson(value);
       });
     }
     return map;
  }
}

