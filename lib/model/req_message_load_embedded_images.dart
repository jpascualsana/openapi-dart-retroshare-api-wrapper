part of openapi.api;

class ReqMessageLoadEmbeddedImages {
  
  String msgId = null;
  
  bool load = null;
  ReqMessageLoadEmbeddedImages();

  @override
  String toString() {
    return 'ReqMessageLoadEmbeddedImages[msgId=$msgId, load=$load, ]';
  }

  ReqMessageLoadEmbeddedImages.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
    load = json['load'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    if (load != null)
      json['load'] = load;
    return json;
  }

  static List<ReqMessageLoadEmbeddedImages> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqMessageLoadEmbeddedImages>() : json.map((value) => ReqMessageLoadEmbeddedImages.fromJson(value)).toList();
  }

  static Map<String, ReqMessageLoadEmbeddedImages> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqMessageLoadEmbeddedImages>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqMessageLoadEmbeddedImages.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqMessageLoadEmbeddedImages-objects as value to a dart map
  static Map<String, List<ReqMessageLoadEmbeddedImages>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqMessageLoadEmbeddedImages>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqMessageLoadEmbeddedImages.listFromJson(value);
       });
     }
     return map;
  }
}

