part of openapi.api;

class RsConfigNetStatus {
  
  String ownId = null;
  
  String ownName = null;
  
  String localAddr = null;
  
  int localPort = null;
  
  String extAddr = null;
  
  int extPort = null;
  
  String extDynDns = null;
  
  bool firewalled = null;
  
  bool forwardPort = null;
  
  bool dHTActive = null;
  
  bool uPnPActive = null;
  
  int uPnPState = null;
  
  bool netLocalOk = null;
  
  bool netUpnpOk = null;
  
  bool netDhtOk = null;
  
  bool netStunOk = null;
  
  bool netExtAddressOk = null;
  
  int netDhtNetSize = null;
  
  int netDhtRsNetSize = null;
  RsConfigNetStatus();

  @override
  String toString() {
    return 'RsConfigNetStatus[ownId=$ownId, ownName=$ownName, localAddr=$localAddr, localPort=$localPort, extAddr=$extAddr, extPort=$extPort, extDynDns=$extDynDns, firewalled=$firewalled, forwardPort=$forwardPort, dHTActive=$dHTActive, uPnPActive=$uPnPActive, uPnPState=$uPnPState, netLocalOk=$netLocalOk, netUpnpOk=$netUpnpOk, netDhtOk=$netDhtOk, netStunOk=$netStunOk, netExtAddressOk=$netExtAddressOk, netDhtNetSize=$netDhtNetSize, netDhtRsNetSize=$netDhtRsNetSize, ]';
  }

  RsConfigNetStatus.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    ownId = json['ownId'];
    ownName = json['ownName'];
    localAddr = json['localAddr'];
    localPort = json['localPort'];
    extAddr = json['extAddr'];
    extPort = json['extPort'];
    extDynDns = json['extDynDns'];
    firewalled = json['firewalled'];
    forwardPort = json['forwardPort'];
    dHTActive = json['DHTActive'];
    uPnPActive = json['uPnPActive'];
    uPnPState = json['uPnPState'];
    netLocalOk = json['netLocalOk'];
    netUpnpOk = json['netUpnpOk'];
    netDhtOk = json['netDhtOk'];
    netStunOk = json['netStunOk'];
    netExtAddressOk = json['netExtAddressOk'];
    netDhtNetSize = json['netDhtNetSize'];
    netDhtRsNetSize = json['netDhtRsNetSize'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (ownId != null)
      json['ownId'] = ownId;
    if (ownName != null)
      json['ownName'] = ownName;
    if (localAddr != null)
      json['localAddr'] = localAddr;
    if (localPort != null)
      json['localPort'] = localPort;
    if (extAddr != null)
      json['extAddr'] = extAddr;
    if (extPort != null)
      json['extPort'] = extPort;
    if (extDynDns != null)
      json['extDynDns'] = extDynDns;
    if (firewalled != null)
      json['firewalled'] = firewalled;
    if (forwardPort != null)
      json['forwardPort'] = forwardPort;
    if (dHTActive != null)
      json['DHTActive'] = dHTActive;
    if (uPnPActive != null)
      json['uPnPActive'] = uPnPActive;
    if (uPnPState != null)
      json['uPnPState'] = uPnPState;
    if (netLocalOk != null)
      json['netLocalOk'] = netLocalOk;
    if (netUpnpOk != null)
      json['netUpnpOk'] = netUpnpOk;
    if (netDhtOk != null)
      json['netDhtOk'] = netDhtOk;
    if (netStunOk != null)
      json['netStunOk'] = netStunOk;
    if (netExtAddressOk != null)
      json['netExtAddressOk'] = netExtAddressOk;
    if (netDhtNetSize != null)
      json['netDhtNetSize'] = netDhtNetSize;
    if (netDhtRsNetSize != null)
      json['netDhtRsNetSize'] = netDhtRsNetSize;
    return json;
  }

  static List<RsConfigNetStatus> listFromJson(List<dynamic> json) {
    return json == null ? List<RsConfigNetStatus>() : json.map((value) => RsConfigNetStatus.fromJson(value)).toList();
  }

  static Map<String, RsConfigNetStatus> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsConfigNetStatus>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsConfigNetStatus.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsConfigNetStatus-objects as value to a dart map
  static Map<String, List<RsConfigNetStatus>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsConfigNetStatus>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsConfigNetStatus.listFromJson(value);
       });
     }
     return map;
  }
}

