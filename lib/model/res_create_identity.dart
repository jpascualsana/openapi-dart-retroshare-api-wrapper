part of openapi.api;

class ResCreateIdentity {
  
  bool retval = null;
  
  String id = null;
  ResCreateIdentity();

  @override
  String toString() {
    return 'ResCreateIdentity[retval=$retval, id=$id, ]';
  }

  ResCreateIdentity.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ResCreateIdentity> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateIdentity>() : json.map((value) => ResCreateIdentity.fromJson(value)).toList();
  }

  static Map<String, ResCreateIdentity> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateIdentity>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateIdentity.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateIdentity-objects as value to a dart map
  static Map<String, List<ResCreateIdentity>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateIdentity>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateIdentity.listFromJson(value);
       });
     }
     return map;
  }
}

