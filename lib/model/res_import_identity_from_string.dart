part of openapi.api;

class ResImportIdentityFromString {
  
  bool retval = null;
  
  String pgpId = null;
  
  String errorMsg = null;
  ResImportIdentityFromString();

  @override
  String toString() {
    return 'ResImportIdentityFromString[retval=$retval, pgpId=$pgpId, errorMsg=$errorMsg, ]';
  }

  ResImportIdentityFromString.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    pgpId = json['pgpId'];
    errorMsg = json['errorMsg'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (pgpId != null)
      json['pgpId'] = pgpId;
    if (errorMsg != null)
      json['errorMsg'] = errorMsg;
    return json;
  }

  static List<ResImportIdentityFromString> listFromJson(List<dynamic> json) {
    return json == null ? List<ResImportIdentityFromString>() : json.map((value) => ResImportIdentityFromString.fromJson(value)).toList();
  }

  static Map<String, ResImportIdentityFromString> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResImportIdentityFromString>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResImportIdentityFromString.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResImportIdentityFromString-objects as value to a dart map
  static Map<String, List<ResImportIdentityFromString>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResImportIdentityFromString>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResImportIdentityFromString.listFromJson(value);
       });
     }
     return map;
  }
}

