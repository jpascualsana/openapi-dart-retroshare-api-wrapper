part of openapi.api;

class ReqSetMessageTag {
  
  String msgId = null;
  
  int tagId = null;
  
  bool set_ = null;
  ReqSetMessageTag();

  @override
  String toString() {
    return 'ReqSetMessageTag[msgId=$msgId, tagId=$tagId, set_=$set_, ]';
  }

  ReqSetMessageTag.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
    tagId = json['tagId'];
    set_ = json['set'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    if (tagId != null)
      json['tagId'] = tagId;
    if (set_ != null)
      json['set'] = set_;
    return json;
  }

  static List<ReqSetMessageTag> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetMessageTag>() : json.map((value) => ReqSetMessageTag.fromJson(value)).toList();
  }

  static Map<String, ReqSetMessageTag> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetMessageTag>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetMessageTag.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetMessageTag-objects as value to a dart map
  static Map<String, List<ReqSetMessageTag>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetMessageTag>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetMessageTag.listFromJson(value);
       });
     }
     return map;
  }
}

