part of openapi.api;

class ResIsOnline {
  
  bool retval = null;
  ResIsOnline();

  @override
  String toString() {
    return 'ResIsOnline[retval=$retval, ]';
  }

  ResIsOnline.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsOnline> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsOnline>() : json.map((value) => ResIsOnline.fromJson(value)).toList();
  }

  static Map<String, ResIsOnline> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsOnline>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsOnline.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsOnline-objects as value to a dart map
  static Map<String, List<ResIsOnline>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsOnline>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsOnline.listFromJson(value);
       });
     }
     return map;
  }
}

