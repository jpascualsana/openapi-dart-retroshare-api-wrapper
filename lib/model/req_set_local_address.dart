part of openapi.api;

class ReqSetLocalAddress {
  
  String sslId = null;
  
  String addr = null;
  
  int port = null;
  ReqSetLocalAddress();

  @override
  String toString() {
    return 'ReqSetLocalAddress[sslId=$sslId, addr=$addr, port=$port, ]';
  }

  ReqSetLocalAddress.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
    addr = json['addr'];
    port = json['port'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    if (addr != null)
      json['addr'] = addr;
    if (port != null)
      json['port'] = port;
    return json;
  }

  static List<ReqSetLocalAddress> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetLocalAddress>() : json.map((value) => ReqSetLocalAddress.fromJson(value)).toList();
  }

  static Map<String, ReqSetLocalAddress> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetLocalAddress>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetLocalAddress.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetLocalAddress-objects as value to a dart map
  static Map<String, List<ReqSetLocalAddress>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetLocalAddress>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetLocalAddress.listFromJson(value);
       });
     }
     return map;
  }
}

