part of openapi.api;

class ResAcceptInvite {
  
  bool retval = null;
  ResAcceptInvite();

  @override
  String toString() {
    return 'ResAcceptInvite[retval=$retval, ]';
  }

  ResAcceptInvite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResAcceptInvite> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAcceptInvite>() : json.map((value) => ResAcceptInvite.fromJson(value)).toList();
  }

  static Map<String, ResAcceptInvite> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAcceptInvite>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAcceptInvite.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAcceptInvite-objects as value to a dart map
  static Map<String, List<ResAcceptInvite>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAcceptInvite>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAcceptInvite.listFromJson(value);
       });
     }
     return map;
  }
}

