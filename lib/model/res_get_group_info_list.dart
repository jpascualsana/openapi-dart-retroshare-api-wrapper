part of openapi.api;

class ResGetGroupInfoList {
  
  bool retval = null;
  
  List<RsGroupInfo> groupInfoList = [];
  ResGetGroupInfoList();

  @override
  String toString() {
    return 'ResGetGroupInfoList[retval=$retval, groupInfoList=$groupInfoList, ]';
  }

  ResGetGroupInfoList.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    groupInfoList = (json['groupInfoList'] == null) ?
      null :
      RsGroupInfo.listFromJson(json['groupInfoList']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (groupInfoList != null)
      json['groupInfoList'] = groupInfoList;
    return json;
  }

  static List<ResGetGroupInfoList> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetGroupInfoList>() : json.map((value) => ResGetGroupInfoList.fromJson(value)).toList();
  }

  static Map<String, ResGetGroupInfoList> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetGroupInfoList>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetGroupInfoList.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetGroupInfoList-objects as value to a dart map
  static Map<String, List<ResGetGroupInfoList>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetGroupInfoList>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetGroupInfoList.listFromJson(value);
       });
     }
     return map;
  }
}

