part of openapi.api;

class ResSendMail {
  
  int retval = null;
  
  List<RsMailIdRecipientIdPair> trackingIds = [];
  
  String errorMsg = null;
  ResSendMail();

  @override
  String toString() {
    return 'ResSendMail[retval=$retval, trackingIds=$trackingIds, errorMsg=$errorMsg, ]';
  }

  ResSendMail.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    trackingIds = (json['trackingIds'] == null) ?
      null :
      RsMailIdRecipientIdPair.listFromJson(json['trackingIds']);
    errorMsg = json['errorMsg'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (trackingIds != null)
      json['trackingIds'] = trackingIds;
    if (errorMsg != null)
      json['errorMsg'] = errorMsg;
    return json;
  }

  static List<ResSendMail> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSendMail>() : json.map((value) => ResSendMail.fromJson(value)).toList();
  }

  static Map<String, ResSendMail> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSendMail>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSendMail.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSendMail-objects as value to a dart map
  static Map<String, List<ResSendMail>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSendMail>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSendMail.listFromJson(value);
       });
     }
     return map;
  }
}

