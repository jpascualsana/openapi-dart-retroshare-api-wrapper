part of openapi.api;

class ReqGetChannelAutoDownload {
  
  String channelId = null;
  ReqGetChannelAutoDownload();

  @override
  String toString() {
    return 'ReqGetChannelAutoDownload[channelId=$channelId, ]';
  }

  ReqGetChannelAutoDownload.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    return json;
  }

  static List<ReqGetChannelAutoDownload> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetChannelAutoDownload>() : json.map((value) => ReqGetChannelAutoDownload.fromJson(value)).toList();
  }

  static Map<String, ReqGetChannelAutoDownload> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetChannelAutoDownload>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetChannelAutoDownload.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetChannelAutoDownload-objects as value to a dart map
  static Map<String, List<ReqGetChannelAutoDownload>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetChannelAutoDownload>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetChannelAutoDownload.listFromJson(value);
       });
     }
     return map;
  }
}

