part of openapi.api;

class ReqGetMessageTag {
  
  String msgId = null;
  ReqGetMessageTag();

  @override
  String toString() {
    return 'ReqGetMessageTag[msgId=$msgId, ]';
  }

  ReqGetMessageTag.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    return json;
  }

  static List<ReqGetMessageTag> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetMessageTag>() : json.map((value) => ReqGetMessageTag.fromJson(value)).toList();
  }

  static Map<String, ReqGetMessageTag> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetMessageTag>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetMessageTag.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetMessageTag-objects as value to a dart map
  static Map<String, List<ReqGetMessageTag>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetMessageTag>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetMessageTag.listFromJson(value);
       });
     }
     return map;
  }
}

