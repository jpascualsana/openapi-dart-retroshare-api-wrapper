part of openapi.api;

class ReqForceDirectoryCheck {
  
  bool addSafeDelay = null;
  ReqForceDirectoryCheck();

  @override
  String toString() {
    return 'ReqForceDirectoryCheck[addSafeDelay=$addSafeDelay, ]';
  }

  ReqForceDirectoryCheck.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    addSafeDelay = json['add_safe_delay'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (addSafeDelay != null)
      json['add_safe_delay'] = addSafeDelay;
    return json;
  }

  static List<ReqForceDirectoryCheck> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqForceDirectoryCheck>() : json.map((value) => ReqForceDirectoryCheck.fromJson(value)).toList();
  }

  static Map<String, ReqForceDirectoryCheck> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqForceDirectoryCheck>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqForceDirectoryCheck.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqForceDirectoryCheck-objects as value to a dart map
  static Map<String, List<ReqForceDirectoryCheck>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqForceDirectoryCheck>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqForceDirectoryCheck.listFromJson(value);
       });
     }
     return map;
  }
}

