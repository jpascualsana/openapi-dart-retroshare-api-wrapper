part of openapi.api;

class ResMessageStar {
  
  bool retval = null;
  ResMessageStar();

  @override
  String toString() {
    return 'ResMessageStar[retval=$retval, ]';
  }

  ResMessageStar.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResMessageStar> listFromJson(List<dynamic> json) {
    return json == null ? List<ResMessageStar>() : json.map((value) => ResMessageStar.fromJson(value)).toList();
  }

  static Map<String, ResMessageStar> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResMessageStar>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResMessageStar.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResMessageStar-objects as value to a dart map
  static Map<String, List<ResMessageStar>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResMessageStar>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResMessageStar.listFromJson(value);
       });
     }
     return map;
  }
}

