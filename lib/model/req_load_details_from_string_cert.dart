part of openapi.api;

class ReqLoadDetailsFromStringCert {
  
  String cert = null;
  ReqLoadDetailsFromStringCert();

  @override
  String toString() {
    return 'ReqLoadDetailsFromStringCert[cert=$cert, ]';
  }

  ReqLoadDetailsFromStringCert.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    cert = json['cert'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (cert != null)
      json['cert'] = cert;
    return json;
  }

  static List<ReqLoadDetailsFromStringCert> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqLoadDetailsFromStringCert>() : json.map((value) => ReqLoadDetailsFromStringCert.fromJson(value)).toList();
  }

  static Map<String, ReqLoadDetailsFromStringCert> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqLoadDetailsFromStringCert>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqLoadDetailsFromStringCert.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqLoadDetailsFromStringCert-objects as value to a dart map
  static Map<String, List<ReqLoadDetailsFromStringCert>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqLoadDetailsFromStringCert>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqLoadDetailsFromStringCert.listFromJson(value);
       });
     }
     return map;
  }
}

