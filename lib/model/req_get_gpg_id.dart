part of openapi.api;

class ReqGetGPGId {
  
  String sslId = null;
  ReqGetGPGId();

  @override
  String toString() {
    return 'ReqGetGPGId[sslId=$sslId, ]';
  }

  ReqGetGPGId.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    return json;
  }

  static List<ReqGetGPGId> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetGPGId>() : json.map((value) => ReqGetGPGId.fromJson(value)).toList();
  }

  static Map<String, ReqGetGPGId> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetGPGId>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetGPGId.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetGPGId-objects as value to a dart map
  static Map<String, List<ReqGetGPGId>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetGPGId>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetGPGId.listFromJson(value);
       });
     }
     return map;
  }
}

