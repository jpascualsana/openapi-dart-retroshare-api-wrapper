part of openapi.api;

class RsPeerServiceInfo {
  
  String mPeerId = null;
  
  List<RsPeerServiceInfoMServiceList> mServiceList = [];
  RsPeerServiceInfo();

  @override
  String toString() {
    return 'RsPeerServiceInfo[mPeerId=$mPeerId, mServiceList=$mServiceList, ]';
  }

  RsPeerServiceInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mPeerId = json['mPeerId'];
    mServiceList = (json['mServiceList'] == null) ?
      null :
      RsPeerServiceInfoMServiceList.listFromJson(json['mServiceList']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mPeerId != null)
      json['mPeerId'] = mPeerId;
    if (mServiceList != null)
      json['mServiceList'] = mServiceList;
    return json;
  }

  static List<RsPeerServiceInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<RsPeerServiceInfo>() : json.map((value) => RsPeerServiceInfo.fromJson(value)).toList();
  }

  static Map<String, RsPeerServiceInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsPeerServiceInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsPeerServiceInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsPeerServiceInfo-objects as value to a dart map
  static Map<String, List<RsPeerServiceInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsPeerServiceInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsPeerServiceInfo.listFromJson(value);
       });
     }
     return map;
  }
}

