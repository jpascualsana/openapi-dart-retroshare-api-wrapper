part of openapi.api;

class ResGetAllBandwidthRatesRatemap {
  
  String key = null;
  
  RsConfigDataRates value = null;
  ResGetAllBandwidthRatesRatemap();

  @override
  String toString() {
    return 'ResGetAllBandwidthRatesRatemap[key=$key, value=$value, ]';
  }

  ResGetAllBandwidthRatesRatemap.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    key = json['key'];
    value = (json['value'] == null) ?
      null :
      RsConfigDataRates.fromJson(json['value']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (key != null)
      json['key'] = key;
    if (value != null)
      json['value'] = value;
    return json;
  }

  static List<ResGetAllBandwidthRatesRatemap> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetAllBandwidthRatesRatemap>() : json.map((value) => ResGetAllBandwidthRatesRatemap.fromJson(value)).toList();
  }

  static Map<String, ResGetAllBandwidthRatesRatemap> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetAllBandwidthRatesRatemap>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetAllBandwidthRatesRatemap.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetAllBandwidthRatesRatemap-objects as value to a dart map
  static Map<String, List<ResGetAllBandwidthRatesRatemap>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetAllBandwidthRatesRatemap>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetAllBandwidthRatesRatemap.listFromJson(value);
       });
     }
     return map;
  }
}

