part of openapi.api;

class RsGxsGrpMsgIdPair {
  
  String first = null;
  
  String second = null;
  RsGxsGrpMsgIdPair();

  @override
  String toString() {
    return 'RsGxsGrpMsgIdPair[first=$first, second=$second, ]';
  }

  RsGxsGrpMsgIdPair.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    first = json['first'];
    second = json['second'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (first != null)
      json['first'] = first;
    if (second != null)
      json['second'] = second;
    return json;
  }

  static List<RsGxsGrpMsgIdPair> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsGrpMsgIdPair>() : json.map((value) => RsGxsGrpMsgIdPair.fromJson(value)).toList();
  }

  static Map<String, RsGxsGrpMsgIdPair> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsGrpMsgIdPair>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsGrpMsgIdPair.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsGrpMsgIdPair-objects as value to a dart map
  static Map<String, List<RsGxsGrpMsgIdPair>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsGrpMsgIdPair>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsGrpMsgIdPair.listFromJson(value);
       });
     }
     return map;
  }
}

