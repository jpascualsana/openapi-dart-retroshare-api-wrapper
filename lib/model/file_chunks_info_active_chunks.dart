part of openapi.api;

class FileChunksInfoActiveChunks {
  
  int first = null;
  
  int second = null;
  FileChunksInfoActiveChunks();

  @override
  String toString() {
    return 'FileChunksInfoActiveChunks[first=$first, second=$second, ]';
  }

  FileChunksInfoActiveChunks.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    first = json['first'];
    second = json['second'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (first != null)
      json['first'] = first;
    if (second != null)
      json['second'] = second;
    return json;
  }

  static List<FileChunksInfoActiveChunks> listFromJson(List<dynamic> json) {
    return json == null ? List<FileChunksInfoActiveChunks>() : json.map((value) => FileChunksInfoActiveChunks.fromJson(value)).toList();
  }

  static Map<String, FileChunksInfoActiveChunks> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, FileChunksInfoActiveChunks>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = FileChunksInfoActiveChunks.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of FileChunksInfoActiveChunks-objects as value to a dart map
  static Map<String, List<FileChunksInfoActiveChunks>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<FileChunksInfoActiveChunks>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = FileChunksInfoActiveChunks.listFromJson(value);
       });
     }
     return map;
  }
}

