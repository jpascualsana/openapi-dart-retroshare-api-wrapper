part of openapi.api;

class ResGetMsgParentId {
  
  bool retval = null;
  
  String msgParentId = null;
  ResGetMsgParentId();

  @override
  String toString() {
    return 'ResGetMsgParentId[retval=$retval, msgParentId=$msgParentId, ]';
  }

  ResGetMsgParentId.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    msgParentId = json['msgParentId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (msgParentId != null)
      json['msgParentId'] = msgParentId;
    return json;
  }

  static List<ResGetMsgParentId> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetMsgParentId>() : json.map((value) => ResGetMsgParentId.fromJson(value)).toList();
  }

  static Map<String, ResGetMsgParentId> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetMsgParentId>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetMsgParentId.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetMsgParentId-objects as value to a dart map
  static Map<String, List<ResGetMsgParentId>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetMsgParentId>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetMsgParentId.listFromJson(value);
       });
     }
     return map;
  }
}

