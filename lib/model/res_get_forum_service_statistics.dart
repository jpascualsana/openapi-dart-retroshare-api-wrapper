part of openapi.api;

class ResGetForumServiceStatistics {
  
  bool retval = null;
  
  GxsServiceStatistic stat = null;
  ResGetForumServiceStatistics();

  @override
  String toString() {
    return 'ResGetForumServiceStatistics[retval=$retval, stat=$stat, ]';
  }

  ResGetForumServiceStatistics.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    stat = (json['stat'] == null) ?
      null :
      GxsServiceStatistic.fromJson(json['stat']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (stat != null)
      json['stat'] = stat;
    return json;
  }

  static List<ResGetForumServiceStatistics> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetForumServiceStatistics>() : json.map((value) => ResGetForumServiceStatistics.fromJson(value)).toList();
  }

  static Map<String, ResGetForumServiceStatistics> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetForumServiceStatistics>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetForumServiceStatistics.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetForumServiceStatistics-objects as value to a dart map
  static Map<String, List<ResGetForumServiceStatistics>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetForumServiceStatistics>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetForumServiceStatistics.listFromJson(value);
       });
     }
     return map;
  }
}

