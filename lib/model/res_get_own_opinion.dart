part of openapi.api;

class ResGetOwnOpinion {
  
  bool retval = null;
  
  RsOpinion op = null;
  //enum opEnum {  0,  1,  2,  };{
  ResGetOwnOpinion();

  @override
  String toString() {
    return 'ResGetOwnOpinion[retval=$retval, op=$op, ]';
  }

  ResGetOwnOpinion.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    op = (json['op'] == null) ?
      null :
      RsOpinion.fromJson(json['op']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (op != null)
      json['op'] = op;
    return json;
  }

  static List<ResGetOwnOpinion> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetOwnOpinion>() : json.map((value) => ResGetOwnOpinion.fromJson(value)).toList();
  }

  static Map<String, ResGetOwnOpinion> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetOwnOpinion>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetOwnOpinion.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetOwnOpinion-objects as value to a dart map
  static Map<String, List<ResGetOwnOpinion>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetOwnOpinion>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetOwnOpinion.listFromJson(value);
       });
     }
     return map;
  }
}

