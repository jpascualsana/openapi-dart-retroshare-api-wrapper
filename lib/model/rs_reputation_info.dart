part of openapi.api;

class RsReputationInfo {
  
  RsOpinion mOwnOpinion = null;
  //enum mOwnOpinionEnum {  0,  1,  2,  };{
  
  int mFriendsPositiveVotes = null;
  
  int mFriendsNegativeVotes = null;
  
  num mFriendAverageScore = null;
  
  RsReputationLevel mOverallReputationLevel = null;
  //enum mOverallReputationLevelEnum {  0,  1,  2,  3,  4,  5,  };{
  RsReputationInfo();

  @override
  String toString() {
    return 'RsReputationInfo[mOwnOpinion=$mOwnOpinion, mFriendsPositiveVotes=$mFriendsPositiveVotes, mFriendsNegativeVotes=$mFriendsNegativeVotes, mFriendAverageScore=$mFriendAverageScore, mOverallReputationLevel=$mOverallReputationLevel, ]';
  }

  RsReputationInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mOwnOpinion = (json['mOwnOpinion'] == null) ?
      null :
      RsOpinion.fromJson(json['mOwnOpinion']);
    mFriendsPositiveVotes = json['mFriendsPositiveVotes'];
    mFriendsNegativeVotes = json['mFriendsNegativeVotes'];
    mFriendAverageScore = json['mFriendAverageScore'];
    mOverallReputationLevel = (json['mOverallReputationLevel'] == null) ?
      null :
      RsReputationLevel.fromJson(json['mOverallReputationLevel']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mOwnOpinion != null)
      json['mOwnOpinion'] = mOwnOpinion;
    if (mFriendsPositiveVotes != null)
      json['mFriendsPositiveVotes'] = mFriendsPositiveVotes;
    if (mFriendsNegativeVotes != null)
      json['mFriendsNegativeVotes'] = mFriendsNegativeVotes;
    if (mFriendAverageScore != null)
      json['mFriendAverageScore'] = mFriendAverageScore;
    if (mOverallReputationLevel != null)
      json['mOverallReputationLevel'] = mOverallReputationLevel;
    return json;
  }

  static List<RsReputationInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<RsReputationInfo>() : json.map((value) => RsReputationInfo.fromJson(value)).toList();
  }

  static Map<String, RsReputationInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsReputationInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsReputationInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsReputationInfo-objects as value to a dart map
  static Map<String, List<RsReputationInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsReputationInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsReputationInfo.listFromJson(value);
       });
     }
     return map;
  }
}

