part of openapi.api;

class ReqUnbanFile {
  
  String realFileHash = null;
  ReqUnbanFile();

  @override
  String toString() {
    return 'ReqUnbanFile[realFileHash=$realFileHash, ]';
  }

  ReqUnbanFile.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    realFileHash = json['realFileHash'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (realFileHash != null)
      json['realFileHash'] = realFileHash;
    return json;
  }

  static List<ReqUnbanFile> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqUnbanFile>() : json.map((value) => ReqUnbanFile.fromJson(value)).toList();
  }

  static Map<String, ReqUnbanFile> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqUnbanFile>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqUnbanFile.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqUnbanFile-objects as value to a dart map
  static Map<String, List<ReqUnbanFile>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqUnbanFile>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqUnbanFile.listFromJson(value);
       });
     }
     return map;
  }
}

