part of openapi.api;

class ResGetForumMsgMetaData {
  
  bool retval = null;
  
  List<RsMsgMetaData> msgMetas = [];
  ResGetForumMsgMetaData();

  @override
  String toString() {
    return 'ResGetForumMsgMetaData[retval=$retval, msgMetas=$msgMetas, ]';
  }

  ResGetForumMsgMetaData.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    msgMetas = (json['msgMetas'] == null) ?
      null :
      RsMsgMetaData.listFromJson(json['msgMetas']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (msgMetas != null)
      json['msgMetas'] = msgMetas;
    return json;
  }

  static List<ResGetForumMsgMetaData> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetForumMsgMetaData>() : json.map((value) => ResGetForumMsgMetaData.fromJson(value)).toList();
  }

  static Map<String, ResGetForumMsgMetaData> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetForumMsgMetaData>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetForumMsgMetaData.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetForumMsgMetaData-objects as value to a dart map
  static Map<String, List<ResGetForumMsgMetaData>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetForumMsgMetaData>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetForumMsgMetaData.listFromJson(value);
       });
     }
     return map;
  }
}

