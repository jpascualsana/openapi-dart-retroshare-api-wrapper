part of openapi.api;

class ResSetOperatingMode {
  
  bool retval = null;
  ResSetOperatingMode();

  @override
  String toString() {
    return 'ResSetOperatingMode[retval=$retval, ]';
  }

  ResSetOperatingMode.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetOperatingMode> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetOperatingMode>() : json.map((value) => ResSetOperatingMode.fromJson(value)).toList();
  }

  static Map<String, ResSetOperatingMode> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetOperatingMode>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetOperatingMode.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetOperatingMode-objects as value to a dart map
  static Map<String, List<ResSetOperatingMode>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetOperatingMode>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetOperatingMode.listFromJson(value);
       });
     }
     return map;
  }
}

