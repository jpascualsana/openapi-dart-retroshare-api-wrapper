part of openapi.api;

class ResEditCircle {
  
  bool retval = null;
  
  RsGxsCircleGroup cData = null;
  ResEditCircle();

  @override
  String toString() {
    return 'ResEditCircle[retval=$retval, cData=$cData, ]';
  }

  ResEditCircle.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    cData = (json['cData'] == null) ?
      null :
      RsGxsCircleGroup.fromJson(json['cData']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (cData != null)
      json['cData'] = cData;
    return json;
  }

  static List<ResEditCircle> listFromJson(List<dynamic> json) {
    return json == null ? List<ResEditCircle>() : json.map((value) => ResEditCircle.fromJson(value)).toList();
  }

  static Map<String, ResEditCircle> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResEditCircle>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResEditCircle.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResEditCircle-objects as value to a dart map
  static Map<String, List<ResEditCircle>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResEditCircle>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResEditCircle.listFromJson(value);
       });
     }
     return map;
  }
}

