part of openapi.api;

class ReqTurtleChannelRequest {
  
  String channelId = null;
  
  RstimeT maxWait = null;
  ReqTurtleChannelRequest();

  @override
  String toString() {
    return 'ReqTurtleChannelRequest[channelId=$channelId, maxWait=$maxWait, ]';
  }

  ReqTurtleChannelRequest.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
    maxWait = (json['maxWait'] == null) ?
      null :
      RstimeT.fromJson(json['maxWait']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    if (maxWait != null)
      json['maxWait'] = maxWait;
    return json;
  }

  static List<ReqTurtleChannelRequest> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqTurtleChannelRequest>() : json.map((value) => ReqTurtleChannelRequest.fromJson(value)).toList();
  }

  static Map<String, ReqTurtleChannelRequest> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqTurtleChannelRequest>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqTurtleChannelRequest.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqTurtleChannelRequest-objects as value to a dart map
  static Map<String, List<ReqTurtleChannelRequest>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqTurtleChannelRequest>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqTurtleChannelRequest.listFromJson(value);
       });
     }
     return map;
  }
}

