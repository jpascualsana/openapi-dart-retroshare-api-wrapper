part of openapi.api;

class ReqGetChannelDownloadDirectory {
  
  String channelId = null;
  ReqGetChannelDownloadDirectory();

  @override
  String toString() {
    return 'ReqGetChannelDownloadDirectory[channelId=$channelId, ]';
  }

  ReqGetChannelDownloadDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    return json;
  }

  static List<ReqGetChannelDownloadDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetChannelDownloadDirectory>() : json.map((value) => ReqGetChannelDownloadDirectory.fromJson(value)).toList();
  }

  static Map<String, ReqGetChannelDownloadDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetChannelDownloadDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetChannelDownloadDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetChannelDownloadDirectory-objects as value to a dart map
  static Map<String, List<ReqGetChannelDownloadDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetChannelDownloadDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetChannelDownloadDirectory.listFromJson(value);
       });
     }
     return map;
  }
}

