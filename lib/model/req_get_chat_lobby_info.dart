part of openapi.api;

class ReqGetChatLobbyInfo {
  
  ChatLobbyId id = null;
  ReqGetChatLobbyInfo();

  @override
  String toString() {
    return 'ReqGetChatLobbyInfo[id=$id, ]';
  }

  ReqGetChatLobbyInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = (json['id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['id']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqGetChatLobbyInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetChatLobbyInfo>() : json.map((value) => ReqGetChatLobbyInfo.fromJson(value)).toList();
  }

  static Map<String, ReqGetChatLobbyInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetChatLobbyInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetChatLobbyInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetChatLobbyInfo-objects as value to a dart map
  static Map<String, List<ReqGetChatLobbyInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetChatLobbyInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetChatLobbyInfo.listFromJson(value);
       });
     }
     return map;
  }
}

