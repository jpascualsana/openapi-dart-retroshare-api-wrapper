part of openapi.api;

class ResCreateChatLobby {
  
  ChatLobbyId retval = null;
  ResCreateChatLobby();

  @override
  String toString() {
    return 'ResCreateChatLobby[retval=$retval, ]';
  }

  ResCreateChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      ChatLobbyId.fromJson(json['retval']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResCreateChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateChatLobby>() : json.map((value) => ResCreateChatLobby.fromJson(value)).toList();
  }

  static Map<String, ResCreateChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateChatLobby-objects as value to a dart map
  static Map<String, List<ResCreateChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}

