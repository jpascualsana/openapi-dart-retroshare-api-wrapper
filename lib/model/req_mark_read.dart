part of openapi.api;

class ReqMarkRead {
  
  RsGxsGrpMsgIdPair postId = null;
  
  bool read = null;
  ReqMarkRead();

  @override
  String toString() {
    return 'ReqMarkRead[postId=$postId, read=$read, ]';
  }

  ReqMarkRead.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    postId = (json['postId'] == null) ?
      null :
      RsGxsGrpMsgIdPair.fromJson(json['postId']);
    read = json['read'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (postId != null)
      json['postId'] = postId;
    if (read != null)
      json['read'] = read;
    return json;
  }

  static List<ReqMarkRead> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqMarkRead>() : json.map((value) => ReqMarkRead.fromJson(value)).toList();
  }

  static Map<String, ReqMarkRead> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqMarkRead>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqMarkRead.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqMarkRead-objects as value to a dart map
  static Map<String, List<ReqMarkRead>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqMarkRead>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqMarkRead.listFromJson(value);
       });
     }
     return map;
  }
}

