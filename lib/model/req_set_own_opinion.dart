part of openapi.api;

class ReqSetOwnOpinion {
  
  String id = null;
  
  RsOpinion op = null;
  //enum opEnum {  0,  1,  2,  };{
  ReqSetOwnOpinion();

  @override
  String toString() {
    return 'ReqSetOwnOpinion[id=$id, op=$op, ]';
  }

  ReqSetOwnOpinion.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    op = (json['op'] == null) ?
      null :
      RsOpinion.fromJson(json['op']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (op != null)
      json['op'] = op;
    return json;
  }

  static List<ReqSetOwnOpinion> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetOwnOpinion>() : json.map((value) => ReqSetOwnOpinion.fromJson(value)).toList();
  }

  static Map<String, ReqSetOwnOpinion> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetOwnOpinion>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetOwnOpinion.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetOwnOpinion-objects as value to a dart map
  static Map<String, List<ReqSetOwnOpinion>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetOwnOpinion>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetOwnOpinion.listFromJson(value);
       });
     }
     return map;
  }
}

