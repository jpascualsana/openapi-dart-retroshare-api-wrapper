part of openapi.api;

class ResCollectEntropy {
  
  bool retval = null;
  ResCollectEntropy();

  @override
  String toString() {
    return 'ResCollectEntropy[retval=$retval, ]';
  }

  ResCollectEntropy.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResCollectEntropy> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCollectEntropy>() : json.map((value) => ResCollectEntropy.fromJson(value)).toList();
  }

  static Map<String, ResCollectEntropy> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCollectEntropy>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCollectEntropy.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCollectEntropy-objects as value to a dart map
  static Map<String, List<ResCollectEntropy>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCollectEntropy>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCollectEntropy.listFromJson(value);
       });
     }
     return map;
  }
}

