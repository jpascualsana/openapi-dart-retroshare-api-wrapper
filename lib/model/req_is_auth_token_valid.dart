part of openapi.api;

class ReqIsAuthTokenValid {
  
  String token = null;
  ReqIsAuthTokenValid();

  @override
  String toString() {
    return 'ReqIsAuthTokenValid[token=$token, ]';
  }

  ReqIsAuthTokenValid.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (token != null)
      json['token'] = token;
    return json;
  }

  static List<ReqIsAuthTokenValid> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqIsAuthTokenValid>() : json.map((value) => ReqIsAuthTokenValid.fromJson(value)).toList();
  }

  static Map<String, ReqIsAuthTokenValid> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqIsAuthTokenValid>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqIsAuthTokenValid.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqIsAuthTokenValid-objects as value to a dart map
  static Map<String, List<ReqIsAuthTokenValid>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqIsAuthTokenValid>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqIsAuthTokenValid.listFromJson(value);
       });
     }
     return map;
  }
}

