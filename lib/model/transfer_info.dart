part of openapi.api;

class TransferInfo {
  
  String peerId = null;
  
  String name = null;
  
  num tfRate = null;
  
  int status = null;
  
  ReqBanFileFileSize transfered = null;
  TransferInfo();

  @override
  String toString() {
    return 'TransferInfo[peerId=$peerId, name=$name, tfRate=$tfRate, status=$status, transfered=$transfered, ]';
  }

  TransferInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    peerId = json['peerId'];
    name = json['name'];
    tfRate = json['tfRate'];
    status = json['status'];
    transfered = (json['transfered'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['transfered']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (peerId != null)
      json['peerId'] = peerId;
    if (name != null)
      json['name'] = name;
    if (tfRate != null)
      json['tfRate'] = tfRate;
    if (status != null)
      json['status'] = status;
    if (transfered != null)
      json['transfered'] = transfered;
    return json;
  }

  static List<TransferInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<TransferInfo>() : json.map((value) => TransferInfo.fromJson(value)).toList();
  }

  static Map<String, TransferInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, TransferInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = TransferInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of TransferInfo-objects as value to a dart map
  static Map<String, List<TransferInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<TransferInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = TransferInfo.listFromJson(value);
       });
     }
     return map;
  }
}

