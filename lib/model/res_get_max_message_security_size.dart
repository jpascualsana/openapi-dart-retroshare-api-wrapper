part of openapi.api;

class ResGetMaxMessageSecuritySize {
  
  int retval = null;
  ResGetMaxMessageSecuritySize();

  @override
  String toString() {
    return 'ResGetMaxMessageSecuritySize[retval=$retval, ]';
  }

  ResGetMaxMessageSecuritySize.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetMaxMessageSecuritySize> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetMaxMessageSecuritySize>() : json.map((value) => ResGetMaxMessageSecuritySize.fromJson(value)).toList();
  }

  static Map<String, ResGetMaxMessageSecuritySize> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetMaxMessageSecuritySize>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetMaxMessageSecuritySize.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetMaxMessageSecuritySize-objects as value to a dart map
  static Map<String, List<ResGetMaxMessageSecuritySize>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetMaxMessageSecuritySize>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetMaxMessageSecuritySize.listFromJson(value);
       });
     }
     return map;
  }
}

