part of openapi.api;

class RsPeerDetails {
  
  bool isOnlyGPGdetail = null;
  
  String id = null;
  
  String gpgId = null;
  
  String name = null;
  
  String email = null;
  
  String location = null;
  
  String org = null;
  
  String issuer = null;
  
  String fpr = null;
  
  String authcode = null;
  
  List<String> gpgSigners = [];
  
  int trustLvl = null;
  
  int validLvl = null;
  
  bool skipPgpSignatureValidation = null;
  
  bool ownsign = null;
  
  bool hasSignedMe = null;
  
  bool acceptConnection = null;
  
  int servicePermFlags = null;
  
  int state = null;
  
  bool actAsServer = null;
  
  String connectAddr = null;
  
  int connectPort = null;
  
  bool isHiddenNode = null;
  
  String hiddenNodeAddress = null;
  
  int hiddenNodePort = null;
  
  int hiddenType = null;
  
  String localAddr = null;
  
  int localPort = null;
  
  String extAddr = null;
  
  int extPort = null;
  
  String dyndns = null;
  
  List<String> ipAddressList = [];
  
  int netMode = null;
  
  int vsDisc = null;
  
  int vsDht = null;
  
  int lastConnect = null;
  
  int lastUsed = null;
  
  int connectState = null;
  
  String connectStateString = null;
  
  int connectPeriod = null;
  
  bool foundDHT = null;
  
  bool wasDeniedConnection = null;
  
  RstimeT deniedTS = null;
  
  int linkType = null;
  RsPeerDetails();

  @override
  String toString() {
    return 'RsPeerDetails[isOnlyGPGdetail=$isOnlyGPGdetail, id=$id, gpgId=$gpgId, name=$name, email=$email, location=$location, org=$org, issuer=$issuer, fpr=$fpr, authcode=$authcode, gpgSigners=$gpgSigners, trustLvl=$trustLvl, validLvl=$validLvl, skipPgpSignatureValidation=$skipPgpSignatureValidation, ownsign=$ownsign, hasSignedMe=$hasSignedMe, acceptConnection=$acceptConnection, servicePermFlags=$servicePermFlags, state=$state, actAsServer=$actAsServer, connectAddr=$connectAddr, connectPort=$connectPort, isHiddenNode=$isHiddenNode, hiddenNodeAddress=$hiddenNodeAddress, hiddenNodePort=$hiddenNodePort, hiddenType=$hiddenType, localAddr=$localAddr, localPort=$localPort, extAddr=$extAddr, extPort=$extPort, dyndns=$dyndns, ipAddressList=$ipAddressList, netMode=$netMode, vsDisc=$vsDisc, vsDht=$vsDht, lastConnect=$lastConnect, lastUsed=$lastUsed, connectState=$connectState, connectStateString=$connectStateString, connectPeriod=$connectPeriod, foundDHT=$foundDHT, wasDeniedConnection=$wasDeniedConnection, deniedTS=$deniedTS, linkType=$linkType, ]';
  }

  RsPeerDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    isOnlyGPGdetail = json['isOnlyGPGdetail'];
    id = json['id'];
    gpgId = json['gpg_id'];
    name = json['name'];
    email = json['email'];
    location = json['location'];
    org = json['org'];
    issuer = json['issuer'];
    fpr = json['fpr'];
    authcode = json['authcode'];
    gpgSigners = (json['gpgSigners'] == null) ?
      null :
      (json['gpgSigners'] as List).cast<String>();
    trustLvl = json['trustLvl'];
    validLvl = json['validLvl'];
    skipPgpSignatureValidation = json['skip_pgp_signature_validation'];
    ownsign = json['ownsign'];
    hasSignedMe = json['hasSignedMe'];
    acceptConnection = json['accept_connection'];
    servicePermFlags = json['service_perm_flags'];
    state = json['state'];
    actAsServer = json['actAsServer'];
    connectAddr = json['connectAddr'];
    connectPort = json['connectPort'];
    isHiddenNode = json['isHiddenNode'];
    hiddenNodeAddress = json['hiddenNodeAddress'];
    hiddenNodePort = json['hiddenNodePort'];
    hiddenType = json['hiddenType'];
    localAddr = json['localAddr'];
    localPort = json['localPort'];
    extAddr = json['extAddr'];
    extPort = json['extPort'];
    dyndns = json['dyndns'];
    ipAddressList = (json['ipAddressList'] == null) ?
      null :
      (json['ipAddressList'] as List).cast<String>();
    netMode = json['netMode'];
    vsDisc = json['vs_disc'];
    vsDht = json['vs_dht'];
    lastConnect = json['lastConnect'];
    lastUsed = json['lastUsed'];
    connectState = json['connectState'];
    connectStateString = json['connectStateString'];
    connectPeriod = json['connectPeriod'];
    foundDHT = json['foundDHT'];
    wasDeniedConnection = json['wasDeniedConnection'];
    deniedTS = (json['deniedTS'] == null) ?
      null :
      RstimeT.fromJson(json['deniedTS']);
    linkType = json['linkType'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (isOnlyGPGdetail != null)
      json['isOnlyGPGdetail'] = isOnlyGPGdetail;
    if (id != null)
      json['id'] = id;
    if (gpgId != null)
      json['gpg_id'] = gpgId;
    if (name != null)
      json['name'] = name;
    if (email != null)
      json['email'] = email;
    if (location != null)
      json['location'] = location;
    if (org != null)
      json['org'] = org;
    if (issuer != null)
      json['issuer'] = issuer;
    if (fpr != null)
      json['fpr'] = fpr;
    if (authcode != null)
      json['authcode'] = authcode;
    if (gpgSigners != null)
      json['gpgSigners'] = gpgSigners;
    if (trustLvl != null)
      json['trustLvl'] = trustLvl;
    if (validLvl != null)
      json['validLvl'] = validLvl;
    if (skipPgpSignatureValidation != null)
      json['skip_pgp_signature_validation'] = skipPgpSignatureValidation;
    if (ownsign != null)
      json['ownsign'] = ownsign;
    if (hasSignedMe != null)
      json['hasSignedMe'] = hasSignedMe;
    if (acceptConnection != null)
      json['accept_connection'] = acceptConnection;
    if (servicePermFlags != null)
      json['service_perm_flags'] = servicePermFlags;
    if (state != null)
      json['state'] = state;
    if (actAsServer != null)
      json['actAsServer'] = actAsServer;
    if (connectAddr != null)
      json['connectAddr'] = connectAddr;
    if (connectPort != null)
      json['connectPort'] = connectPort;
    if (isHiddenNode != null)
      json['isHiddenNode'] = isHiddenNode;
    if (hiddenNodeAddress != null)
      json['hiddenNodeAddress'] = hiddenNodeAddress;
    if (hiddenNodePort != null)
      json['hiddenNodePort'] = hiddenNodePort;
    if (hiddenType != null)
      json['hiddenType'] = hiddenType;
    if (localAddr != null)
      json['localAddr'] = localAddr;
    if (localPort != null)
      json['localPort'] = localPort;
    if (extAddr != null)
      json['extAddr'] = extAddr;
    if (extPort != null)
      json['extPort'] = extPort;
    if (dyndns != null)
      json['dyndns'] = dyndns;
    if (ipAddressList != null)
      json['ipAddressList'] = ipAddressList;
    if (netMode != null)
      json['netMode'] = netMode;
    if (vsDisc != null)
      json['vs_disc'] = vsDisc;
    if (vsDht != null)
      json['vs_dht'] = vsDht;
    if (lastConnect != null)
      json['lastConnect'] = lastConnect;
    if (lastUsed != null)
      json['lastUsed'] = lastUsed;
    if (connectState != null)
      json['connectState'] = connectState;
    if (connectStateString != null)
      json['connectStateString'] = connectStateString;
    if (connectPeriod != null)
      json['connectPeriod'] = connectPeriod;
    if (foundDHT != null)
      json['foundDHT'] = foundDHT;
    if (wasDeniedConnection != null)
      json['wasDeniedConnection'] = wasDeniedConnection;
    if (deniedTS != null)
      json['deniedTS'] = deniedTS;
    if (linkType != null)
      json['linkType'] = linkType;
    return json;
  }

  static List<RsPeerDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<RsPeerDetails>() : json.map((value) => RsPeerDetails.fromJson(value)).toList();
  }

  static Map<String, RsPeerDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsPeerDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsPeerDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsPeerDetails-objects as value to a dart map
  static Map<String, List<RsPeerDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsPeerDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsPeerDetails.listFromJson(value);
       });
     }
     return map;
  }
}

