part of openapi.api;

class ReqEditChannel {
  
  RsGxsChannelGroup channel = null;
  ReqEditChannel();

  @override
  String toString() {
    return 'ReqEditChannel[channel=$channel, ]';
  }

  ReqEditChannel.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channel = (json['channel'] == null) ?
      null :
      RsGxsChannelGroup.fromJson(json['channel']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channel != null)
      json['channel'] = channel;
    return json;
  }

  static List<ReqEditChannel> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqEditChannel>() : json.map((value) => ReqEditChannel.fromJson(value)).toList();
  }

  static Map<String, ReqEditChannel> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqEditChannel>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqEditChannel.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqEditChannel-objects as value to a dart map
  static Map<String, List<ReqEditChannel>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqEditChannel>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqEditChannel.listFromJson(value);
       });
     }
     return map;
  }
}

