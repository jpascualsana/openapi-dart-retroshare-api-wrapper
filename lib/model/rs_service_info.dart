part of openapi.api;

class RsServiceInfo {
  
  String mServiceName = null;
  
  int mServiceType = null;
  
  int mVersionMajor = null;
  
  int mVersionMinor = null;
  
  int mMinVersionMajor = null;
  
  int mMinVersionMinor = null;
  RsServiceInfo();

  @override
  String toString() {
    return 'RsServiceInfo[mServiceName=$mServiceName, mServiceType=$mServiceType, mVersionMajor=$mVersionMajor, mVersionMinor=$mVersionMinor, mMinVersionMajor=$mMinVersionMajor, mMinVersionMinor=$mMinVersionMinor, ]';
  }

  RsServiceInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mServiceName = json['mServiceName'];
    mServiceType = json['mServiceType'];
    mVersionMajor = json['mVersionMajor'];
    mVersionMinor = json['mVersionMinor'];
    mMinVersionMajor = json['mMinVersionMajor'];
    mMinVersionMinor = json['mMinVersionMinor'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mServiceName != null)
      json['mServiceName'] = mServiceName;
    if (mServiceType != null)
      json['mServiceType'] = mServiceType;
    if (mVersionMajor != null)
      json['mVersionMajor'] = mVersionMajor;
    if (mVersionMinor != null)
      json['mVersionMinor'] = mVersionMinor;
    if (mMinVersionMajor != null)
      json['mMinVersionMajor'] = mMinVersionMajor;
    if (mMinVersionMinor != null)
      json['mMinVersionMinor'] = mMinVersionMinor;
    return json;
  }

  static List<RsServiceInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<RsServiceInfo>() : json.map((value) => RsServiceInfo.fromJson(value)).toList();
  }

  static Map<String, RsServiceInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsServiceInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsServiceInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsServiceInfo-objects as value to a dart map
  static Map<String, List<RsServiceInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsServiceInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsServiceInfo.listFromJson(value);
       });
     }
     return map;
  }
}

