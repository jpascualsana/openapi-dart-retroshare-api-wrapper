part of openapi.api;

class ResGetCirclesInfo {
  
  bool retval = null;
  
  List<RsGxsCircleGroup> circlesInfo = [];
  ResGetCirclesInfo();

  @override
  String toString() {
    return 'ResGetCirclesInfo[retval=$retval, circlesInfo=$circlesInfo, ]';
  }

  ResGetCirclesInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    circlesInfo = (json['circlesInfo'] == null) ?
      null :
      RsGxsCircleGroup.listFromJson(json['circlesInfo']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (circlesInfo != null)
      json['circlesInfo'] = circlesInfo;
    return json;
  }

  static List<ResGetCirclesInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetCirclesInfo>() : json.map((value) => ResGetCirclesInfo.fromJson(value)).toList();
  }

  static Map<String, ResGetCirclesInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetCirclesInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetCirclesInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetCirclesInfo-objects as value to a dart map
  static Map<String, List<ResGetCirclesInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetCirclesInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetCirclesInfo.listFromJson(value);
       });
     }
     return map;
  }
}

