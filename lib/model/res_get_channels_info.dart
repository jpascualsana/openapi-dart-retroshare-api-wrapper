part of openapi.api;

class ResGetChannelsInfo {
  
  bool retval = null;
  
  List<RsGxsChannelGroup> channelsInfo = [];
  ResGetChannelsInfo();

  @override
  String toString() {
    return 'ResGetChannelsInfo[retval=$retval, channelsInfo=$channelsInfo, ]';
  }

  ResGetChannelsInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    channelsInfo = (json['channelsInfo'] == null) ?
      null :
      RsGxsChannelGroup.listFromJson(json['channelsInfo']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (channelsInfo != null)
      json['channelsInfo'] = channelsInfo;
    return json;
  }

  static List<ResGetChannelsInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetChannelsInfo>() : json.map((value) => ResGetChannelsInfo.fromJson(value)).toList();
  }

  static Map<String, ResGetChannelsInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetChannelsInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetChannelsInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetChannelsInfo-objects as value to a dart map
  static Map<String, List<ResGetChannelsInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetChannelsInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetChannelsInfo.listFromJson(value);
       });
     }
     return map;
  }
}

