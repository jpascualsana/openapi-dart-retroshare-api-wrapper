part of openapi.api;

class ResIsOwnId {
  
  bool retval = null;
  ResIsOwnId();

  @override
  String toString() {
    return 'ResIsOwnId[retval=$retval, ]';
  }

  ResIsOwnId.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsOwnId> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsOwnId>() : json.map((value) => ResIsOwnId.fromJson(value)).toList();
  }

  static Map<String, ResIsOwnId> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsOwnId>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsOwnId.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsOwnId-objects as value to a dart map
  static Map<String, List<ResIsOwnId>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsOwnId>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsOwnId.listFromJson(value);
       });
     }
     return map;
  }
}

