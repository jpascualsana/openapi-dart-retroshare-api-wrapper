part of openapi.api;

class ResRestart {
  
  ResExportCollectionLinkRetval retval = null;
  ResRestart();

  @override
  String toString() {
    return 'ResRestart[retval=$retval, ]';
  }

  ResRestart.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      ResExportCollectionLinkRetval.fromJson(json['retval']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRestart> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRestart>() : json.map((value) => ResRestart.fromJson(value)).toList();
  }

  static Map<String, ResRestart> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRestart>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRestart.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRestart-objects as value to a dart map
  static Map<String, List<ResRestart>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRestart>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRestart.listFromJson(value);
       });
     }
     return map;
  }
}

