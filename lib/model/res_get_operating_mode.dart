part of openapi.api;

class ResGetOperatingMode {
  
  int retval = null;
  ResGetOperatingMode();

  @override
  String toString() {
    return 'ResGetOperatingMode[retval=$retval, ]';
  }

  ResGetOperatingMode.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetOperatingMode> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetOperatingMode>() : json.map((value) => ResGetOperatingMode.fromJson(value)).toList();
  }

  static Map<String, ResGetOperatingMode> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetOperatingMode>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetOperatingMode.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetOperatingMode-objects as value to a dart map
  static Map<String, List<ResGetOperatingMode>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetOperatingMode>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetOperatingMode.listFromJson(value);
       });
     }
     return map;
  }
}

