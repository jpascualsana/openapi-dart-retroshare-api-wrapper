part of openapi.api;

class ReqRemoveFriendLocation {
  
  String sslId = null;
  ReqRemoveFriendLocation();

  @override
  String toString() {
    return 'ReqRemoveFriendLocation[sslId=$sslId, ]';
  }

  ReqRemoveFriendLocation.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    return json;
  }

  static List<ReqRemoveFriendLocation> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRemoveFriendLocation>() : json.map((value) => ReqRemoveFriendLocation.fromJson(value)).toList();
  }

  static Map<String, ReqRemoveFriendLocation> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRemoveFriendLocation>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRemoveFriendLocation.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRemoveFriendLocation-objects as value to a dart map
  static Map<String, List<ReqRemoveFriendLocation>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRemoveFriendLocation>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRemoveFriendLocation.listFromJson(value);
       });
     }
     return map;
  }
}

