part of openapi.api;

class ReqMessageToDraft {
  
  RsMsgsMessageInfo info = null;
  
  String msgParentId = null;
  ReqMessageToDraft();

  @override
  String toString() {
    return 'ReqMessageToDraft[info=$info, msgParentId=$msgParentId, ]';
  }

  ReqMessageToDraft.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    info = (json['info'] == null) ?
      null :
      RsMsgsMessageInfo.fromJson(json['info']);
    msgParentId = json['msgParentId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (info != null)
      json['info'] = info;
    if (msgParentId != null)
      json['msgParentId'] = msgParentId;
    return json;
  }

  static List<ReqMessageToDraft> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqMessageToDraft>() : json.map((value) => ReqMessageToDraft.fromJson(value)).toList();
  }

  static Map<String, ReqMessageToDraft> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqMessageToDraft>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqMessageToDraft.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqMessageToDraft-objects as value to a dart map
  static Map<String, List<ReqMessageToDraft>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqMessageToDraft>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqMessageToDraft.listFromJson(value);
       });
     }
     return map;
  }
}

