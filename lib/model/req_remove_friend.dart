part of openapi.api;

class ReqRemoveFriend {
  
  String pgpId = null;
  ReqRemoveFriend();

  @override
  String toString() {
    return 'ReqRemoveFriend[pgpId=$pgpId, ]';
  }

  ReqRemoveFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    pgpId = json['pgpId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (pgpId != null)
      json['pgpId'] = pgpId;
    return json;
  }

  static List<ReqRemoveFriend> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRemoveFriend>() : json.map((value) => ReqRemoveFriend.fromJson(value)).toList();
  }

  static Map<String, ReqRemoveFriend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRemoveFriend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRemoveFriend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRemoveFriend-objects as value to a dart map
  static Map<String, List<ReqRemoveFriend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRemoveFriend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRemoveFriend.listFromJson(value);
       });
     }
     return map;
  }
}

