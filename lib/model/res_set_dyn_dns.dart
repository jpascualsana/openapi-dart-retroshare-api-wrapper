part of openapi.api;

class ResSetDynDNS {
  
  bool retval = null;
  ResSetDynDNS();

  @override
  String toString() {
    return 'ResSetDynDNS[retval=$retval, ]';
  }

  ResSetDynDNS.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetDynDNS> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetDynDNS>() : json.map((value) => ResSetDynDNS.fromJson(value)).toList();
  }

  static Map<String, ResSetDynDNS> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetDynDNS>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetDynDNS.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetDynDNS-objects as value to a dart map
  static Map<String, List<ResSetDynDNS>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetDynDNS>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetDynDNS.listFromJson(value);
       });
     }
     return map;
  }
}

