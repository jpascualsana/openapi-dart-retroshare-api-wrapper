part of openapi.api;

class ReqCreatePost {
  
  RsGxsChannelPost post = null;
  ReqCreatePost();

  @override
  String toString() {
    return 'ReqCreatePost[post=$post, ]';
  }

  ReqCreatePost.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    post = (json['post'] == null) ?
      null :
      RsGxsChannelPost.fromJson(json['post']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (post != null)
      json['post'] = post;
    return json;
  }

  static List<ReqCreatePost> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreatePost>() : json.map((value) => ReqCreatePost.fromJson(value)).toList();
  }

  static Map<String, ReqCreatePost> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreatePost>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreatePost.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreatePost-objects as value to a dart map
  static Map<String, List<ReqCreatePost>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreatePost>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreatePost.listFromJson(value);
       });
     }
     return map;
  }
}

