part of openapi.api;

class RsMsgsMsgInfoSummary {
  
  String msgId = null;
  
  String srcId = null;
  
  int msgflags = null;
  
  List<int> msgtags = [];
  
  String title = null;
  
  int count = null;
  
  RstimeT ts = null;
  RsMsgsMsgInfoSummary();

  @override
  String toString() {
    return 'RsMsgsMsgInfoSummary[msgId=$msgId, srcId=$srcId, msgflags=$msgflags, msgtags=$msgtags, title=$title, count=$count, ts=$ts, ]';
  }

  RsMsgsMsgInfoSummary.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
    srcId = json['srcId'];
    msgflags = json['msgflags'];
    msgtags = (json['msgtags'] == null) ?
      null :
      (json['msgtags'] as List).cast<int>();
    title = json['title'];
    count = json['count'];
    ts = (json['ts'] == null) ?
      null :
      RstimeT.fromJson(json['ts']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    if (srcId != null)
      json['srcId'] = srcId;
    if (msgflags != null)
      json['msgflags'] = msgflags;
    if (msgtags != null)
      json['msgtags'] = msgtags;
    if (title != null)
      json['title'] = title;
    if (count != null)
      json['count'] = count;
    if (ts != null)
      json['ts'] = ts;
    return json;
  }

  static List<RsMsgsMsgInfoSummary> listFromJson(List<dynamic> json) {
    return json == null ? List<RsMsgsMsgInfoSummary>() : json.map((value) => RsMsgsMsgInfoSummary.fromJson(value)).toList();
  }

  static Map<String, RsMsgsMsgInfoSummary> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsMsgsMsgInfoSummary>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsMsgsMsgInfoSummary.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsMsgsMsgInfoSummary-objects as value to a dart map
  static Map<String, List<RsMsgsMsgInfoSummary>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsMsgsMsgInfoSummary>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsMsgsMsgInfoSummary.listFromJson(value);
       });
     }
     return map;
  }
}

