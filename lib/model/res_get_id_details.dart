part of openapi.api;

class ResGetIdDetails {
  
  bool retval = null;
  
  RsIdentityDetails details = null;
  ResGetIdDetails();

  @override
  String toString() {
    return 'ResGetIdDetails[retval=$retval, details=$details, ]';
  }

  ResGetIdDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    details = (json['details'] == null) ?
      null :
      RsIdentityDetails.fromJson(json['details']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (details != null)
      json['details'] = details;
    return json;
  }

  static List<ResGetIdDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetIdDetails>() : json.map((value) => ResGetIdDetails.fromJson(value)).toList();
  }

  static Map<String, ResGetIdDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetIdDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetIdDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetIdDetails-objects as value to a dart map
  static Map<String, List<ResGetIdDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetIdDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetIdDetails.listFromJson(value);
       });
     }
     return map;
  }
}

