part of openapi.api;

class ResGetIdentityForChatLobby {
  
  bool retval = null;
  
  String nick = null;
  ResGetIdentityForChatLobby();

  @override
  String toString() {
    return 'ResGetIdentityForChatLobby[retval=$retval, nick=$nick, ]';
  }

  ResGetIdentityForChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    nick = json['nick'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (nick != null)
      json['nick'] = nick;
    return json;
  }

  static List<ResGetIdentityForChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetIdentityForChatLobby>() : json.map((value) => ResGetIdentityForChatLobby.fromJson(value)).toList();
  }

  static Map<String, ResGetIdentityForChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetIdentityForChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetIdentityForChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetIdentityForChatLobby-objects as value to a dart map
  static Map<String, List<ResGetIdentityForChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetIdentityForChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetIdentityForChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}

