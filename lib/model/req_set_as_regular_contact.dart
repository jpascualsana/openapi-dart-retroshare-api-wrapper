part of openapi.api;

class ReqSetAsRegularContact {
  
  String id = null;
  
  bool isContact = null;
  ReqSetAsRegularContact();

  @override
  String toString() {
    return 'ReqSetAsRegularContact[id=$id, isContact=$isContact, ]';
  }

  ReqSetAsRegularContact.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    isContact = json['isContact'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (isContact != null)
      json['isContact'] = isContact;
    return json;
  }

  static List<ReqSetAsRegularContact> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetAsRegularContact>() : json.map((value) => ReqSetAsRegularContact.fromJson(value)).toList();
  }

  static Map<String, ReqSetAsRegularContact> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetAsRegularContact>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetAsRegularContact.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetAsRegularContact-objects as value to a dart map
  static Map<String, List<ReqSetAsRegularContact>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetAsRegularContact>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetAsRegularContact.listFromJson(value);
       });
     }
     return map;
  }
}

