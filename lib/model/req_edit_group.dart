part of openapi.api;

class ReqEditGroup {
  
  String groupId = null;
  
  RsGroupInfo groupInfo = null;
  ReqEditGroup();

  @override
  String toString() {
    return 'ReqEditGroup[groupId=$groupId, groupInfo=$groupInfo, ]';
  }

  ReqEditGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    groupId = json['groupId'];
    groupInfo = (json['groupInfo'] == null) ?
      null :
      RsGroupInfo.fromJson(json['groupInfo']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (groupId != null)
      json['groupId'] = groupId;
    if (groupInfo != null)
      json['groupInfo'] = groupInfo;
    return json;
  }

  static List<ReqEditGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqEditGroup>() : json.map((value) => ReqEditGroup.fromJson(value)).toList();
  }

  static Map<String, ReqEditGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqEditGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqEditGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqEditGroup-objects as value to a dart map
  static Map<String, List<ReqEditGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqEditGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqEditGroup.listFromJson(value);
       });
     }
     return map;
  }
}

