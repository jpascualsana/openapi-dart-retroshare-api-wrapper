part of openapi.api;

class ReqGetCirclesInfo {
  
  List<String> circlesIds = [];
  ReqGetCirclesInfo();

  @override
  String toString() {
    return 'ReqGetCirclesInfo[circlesIds=$circlesIds, ]';
  }

  ReqGetCirclesInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    circlesIds = (json['circlesIds'] == null) ?
      null :
      (json['circlesIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (circlesIds != null)
      json['circlesIds'] = circlesIds;
    return json;
  }

  static List<ReqGetCirclesInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetCirclesInfo>() : json.map((value) => ReqGetCirclesInfo.fromJson(value)).toList();
  }

  static Map<String, ReqGetCirclesInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetCirclesInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetCirclesInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetCirclesInfo-objects as value to a dart map
  static Map<String, List<ReqGetCirclesInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetCirclesInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetCirclesInfo.listFromJson(value);
       });
     }
     return map;
  }
}

