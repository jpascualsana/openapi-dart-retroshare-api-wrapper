part of openapi.api;

class ResThresholdForRemotelyNegativeReputation {
  
  int retval = null;
  ResThresholdForRemotelyNegativeReputation();

  @override
  String toString() {
    return 'ResThresholdForRemotelyNegativeReputation[retval=$retval, ]';
  }

  ResThresholdForRemotelyNegativeReputation.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResThresholdForRemotelyNegativeReputation> listFromJson(List<dynamic> json) {
    return json == null ? List<ResThresholdForRemotelyNegativeReputation>() : json.map((value) => ResThresholdForRemotelyNegativeReputation.fromJson(value)).toList();
  }

  static Map<String, ResThresholdForRemotelyNegativeReputation> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResThresholdForRemotelyNegativeReputation>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResThresholdForRemotelyNegativeReputation.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResThresholdForRemotelyNegativeReputation-objects as value to a dart map
  static Map<String, List<ResThresholdForRemotelyNegativeReputation>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResThresholdForRemotelyNegativeReputation>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResThresholdForRemotelyNegativeReputation.listFromJson(value);
       });
     }
     return map;
  }
}

