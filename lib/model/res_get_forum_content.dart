part of openapi.api;

class ResGetForumContent {
  
  bool retval = null;
  
  List<RsGxsForumMsg> msgs = [];
  ResGetForumContent();

  @override
  String toString() {
    return 'ResGetForumContent[retval=$retval, msgs=$msgs, ]';
  }

  ResGetForumContent.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    msgs = (json['msgs'] == null) ?
      null :
      RsGxsForumMsg.listFromJson(json['msgs']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (msgs != null)
      json['msgs'] = msgs;
    return json;
  }

  static List<ResGetForumContent> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetForumContent>() : json.map((value) => ResGetForumContent.fromJson(value)).toList();
  }

  static Map<String, ResGetForumContent> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetForumContent>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetForumContent.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetForumContent-objects as value to a dart map
  static Map<String, List<ResGetForumContent>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetForumContent>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetForumContent.listFromJson(value);
       });
     }
     return map;
  }
}

