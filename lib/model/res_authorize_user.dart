part of openapi.api;

class ResAuthorizeUser {
  
  ResExportCollectionLinkRetval retval = null;
  ResAuthorizeUser();

  @override
  String toString() {
    return 'ResAuthorizeUser[retval=$retval, ]';
  }

  ResAuthorizeUser.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      ResExportCollectionLinkRetval.fromJson(json['retval']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResAuthorizeUser> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAuthorizeUser>() : json.map((value) => ResAuthorizeUser.fromJson(value)).toList();
  }

  static Map<String, ResAuthorizeUser> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAuthorizeUser>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAuthorizeUser.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAuthorizeUser-objects as value to a dart map
  static Map<String, List<ResAuthorizeUser>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAuthorizeUser>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAuthorizeUser.listFromJson(value);
       });
     }
     return map;
  }
}

