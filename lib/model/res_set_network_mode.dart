part of openapi.api;

class ResSetNetworkMode {
  
  bool retval = null;
  ResSetNetworkMode();

  @override
  String toString() {
    return 'ResSetNetworkMode[retval=$retval, ]';
  }

  ResSetNetworkMode.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetNetworkMode> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetNetworkMode>() : json.map((value) => ResSetNetworkMode.fromJson(value)).toList();
  }

  static Map<String, ResSetNetworkMode> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetNetworkMode>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetNetworkMode.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetNetworkMode-objects as value to a dart map
  static Map<String, List<ResSetNetworkMode>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetNetworkMode>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetNetworkMode.listFromJson(value);
       });
     }
     return map;
  }
}

