part of openapi.api;

class ResFileCancel {
  
  bool retval = null;
  ResFileCancel();

  @override
  String toString() {
    return 'ResFileCancel[retval=$retval, ]';
  }

  ResFileCancel.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResFileCancel> listFromJson(List<dynamic> json) {
    return json == null ? List<ResFileCancel>() : json.map((value) => ResFileCancel.fromJson(value)).toList();
  }

  static Map<String, ResFileCancel> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResFileCancel>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResFileCancel.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResFileCancel-objects as value to a dart map
  static Map<String, List<ResFileCancel>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResFileCancel>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResFileCancel.listFromJson(value);
       });
     }
     return map;
  }
}

