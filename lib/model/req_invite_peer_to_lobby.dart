part of openapi.api;

class ReqInvitePeerToLobby {
  
  ChatLobbyId lobbyId = null;
  
  String peerId = null;
  ReqInvitePeerToLobby();

  @override
  String toString() {
    return 'ReqInvitePeerToLobby[lobbyId=$lobbyId, peerId=$peerId, ]';
  }

  ReqInvitePeerToLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    lobbyId = (json['lobby_id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['lobby_id']);
    peerId = json['peer_id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (lobbyId != null)
      json['lobby_id'] = lobbyId;
    if (peerId != null)
      json['peer_id'] = peerId;
    return json;
  }

  static List<ReqInvitePeerToLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqInvitePeerToLobby>() : json.map((value) => ReqInvitePeerToLobby.fromJson(value)).toList();
  }

  static Map<String, ReqInvitePeerToLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqInvitePeerToLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqInvitePeerToLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqInvitePeerToLobby-objects as value to a dart map
  static Map<String, List<ReqInvitePeerToLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqInvitePeerToLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqInvitePeerToLobby.listFromJson(value);
       });
     }
     return map;
  }
}

