part of openapi.api;

class ResIsAuthTokenValid {
  
  bool retval = null;
  
  ResExportCollectionLinkRetval error = null;
  ResIsAuthTokenValid();

  @override
  String toString() {
    return 'ResIsAuthTokenValid[retval=$retval, error=$error, ]';
  }

  ResIsAuthTokenValid.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    error = (json['error'] == null) ?
      null :
      ResExportCollectionLinkRetval.fromJson(json['error']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (error != null)
      json['error'] = error;
    return json;
  }

  static List<ResIsAuthTokenValid> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsAuthTokenValid>() : json.map((value) => ResIsAuthTokenValid.fromJson(value)).toList();
  }

  static Map<String, ResIsAuthTokenValid> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsAuthTokenValid>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsAuthTokenValid.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsAuthTokenValid-objects as value to a dart map
  static Map<String, List<ResIsAuthTokenValid>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsAuthTokenValid>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsAuthTokenValid.listFromJson(value);
       });
     }
     return map;
  }
}

