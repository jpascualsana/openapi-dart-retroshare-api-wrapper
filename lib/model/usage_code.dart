part of openapi.api;

class UsageCode {
  /// The underlying value of this enum member.
  final int value;

  const UsageCode._internal(this.value);

  static const UsageCode number0_ = const UsageCode._internal(0);
  static const UsageCode number1_ = const UsageCode._internal(1);
  static const UsageCode number2_ = const UsageCode._internal(2);
  static const UsageCode number3_ = const UsageCode._internal(3);
  static const UsageCode number4_ = const UsageCode._internal(4);
  static const UsageCode number5_ = const UsageCode._internal(5);
  static const UsageCode number6_ = const UsageCode._internal(6);
  static const UsageCode number7_ = const UsageCode._internal(7);
  static const UsageCode number8_ = const UsageCode._internal(8);
  static const UsageCode number9_ = const UsageCode._internal(9);
  static const UsageCode number10_ = const UsageCode._internal(10);
  static const UsageCode number11_ = const UsageCode._internal(11);
  static const UsageCode number12_ = const UsageCode._internal(12);
  static const UsageCode number13_ = const UsageCode._internal(13);
  static const UsageCode number14_ = const UsageCode._internal(14);
  static const UsageCode number15_ = const UsageCode._internal(15);
  static const UsageCode number16_ = const UsageCode._internal(16);
  static const UsageCode number17_ = const UsageCode._internal(17);
  static const UsageCode number18_ = const UsageCode._internal(18);
  static const UsageCode number19_ = const UsageCode._internal(19);
  
  int toJson (){
    return this.value;
  }

  static UsageCode fromJson(int value) {
    return new UsageCodeTypeTransformer().decode(value);
  }
  
  static List<UsageCode> listFromJson(List<dynamic> json) {
    return json == null ? new List<UsageCode>() : json.map((value) => UsageCode.fromJson(value)).toList();
  }
}

class UsageCodeTypeTransformer {

  dynamic encode(UsageCode data) {
    return data.value;
  }

  UsageCode decode(dynamic data) {
    switch (data) {
      case 0: return UsageCode.number0_;
      case 1: return UsageCode.number1_;
      case 2: return UsageCode.number2_;
      case 3: return UsageCode.number3_;
      case 4: return UsageCode.number4_;
      case 5: return UsageCode.number5_;
      case 6: return UsageCode.number6_;
      case 7: return UsageCode.number7_;
      case 8: return UsageCode.number8_;
      case 9: return UsageCode.number9_;
      case 10: return UsageCode.number10_;
      case 11: return UsageCode.number11_;
      case 12: return UsageCode.number12_;
      case 13: return UsageCode.number13_;
      case 14: return UsageCode.number14_;
      case 15: return UsageCode.number15_;
      case 16: return UsageCode.number16_;
      case 17: return UsageCode.number17_;
      case 18: return UsageCode.number18_;
      case 19: return UsageCode.number19_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}

