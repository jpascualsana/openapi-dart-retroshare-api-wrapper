part of openapi.api;

class ResGetServiceItemNames {
  
  bool retval = null;
  
  List<ResGetServiceItemNamesNames> names = [];
  ResGetServiceItemNames();

  @override
  String toString() {
    return 'ResGetServiceItemNames[retval=$retval, names=$names, ]';
  }

  ResGetServiceItemNames.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    names = (json['names'] == null) ?
      null :
      ResGetServiceItemNamesNames.listFromJson(json['names']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (names != null)
      json['names'] = names;
    return json;
  }

  static List<ResGetServiceItemNames> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetServiceItemNames>() : json.map((value) => ResGetServiceItemNames.fromJson(value)).toList();
  }

  static Map<String, ResGetServiceItemNames> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetServiceItemNames>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetServiceItemNames.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetServiceItemNames-objects as value to a dart map
  static Map<String, List<ResGetServiceItemNames>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetServiceItemNames>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetServiceItemNames.listFromJson(value);
       });
     }
     return map;
  }
}

