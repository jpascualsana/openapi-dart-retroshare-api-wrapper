part of openapi.api;

class ResGetCircleRequests {
  
  bool retval = null;
  
  List<RsGxsCircleMsg> requests = [];
  ResGetCircleRequests();

  @override
  String toString() {
    return 'ResGetCircleRequests[retval=$retval, requests=$requests, ]';
  }

  ResGetCircleRequests.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    requests = (json['requests'] == null) ?
      null :
      RsGxsCircleMsg.listFromJson(json['requests']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (requests != null)
      json['requests'] = requests;
    return json;
  }

  static List<ResGetCircleRequests> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetCircleRequests>() : json.map((value) => ResGetCircleRequests.fromJson(value)).toList();
  }

  static Map<String, ResGetCircleRequests> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetCircleRequests>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetCircleRequests.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetCircleRequests-objects as value to a dart map
  static Map<String, List<ResGetCircleRequests>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetCircleRequests>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetCircleRequests.listFromJson(value);
       });
     }
     return map;
  }
}

