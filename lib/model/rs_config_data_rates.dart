part of openapi.api;

class RsConfigDataRates {
  
  num mRateIn = null;
  
  num mRateMaxIn = null;
  
  num mAllocIn = null;
  
  RstimeT mAllocTs = null;
  
  num mRateOut = null;
  
  num mRateMaxOut = null;
  
  num mAllowedOut = null;
  
  RstimeT mAllowedTs = null;
  
  int mQueueIn = null;
  
  int mQueueOut = null;
  RsConfigDataRates();

  @override
  String toString() {
    return 'RsConfigDataRates[mRateIn=$mRateIn, mRateMaxIn=$mRateMaxIn, mAllocIn=$mAllocIn, mAllocTs=$mAllocTs, mRateOut=$mRateOut, mRateMaxOut=$mRateMaxOut, mAllowedOut=$mAllowedOut, mAllowedTs=$mAllowedTs, mQueueIn=$mQueueIn, mQueueOut=$mQueueOut, ]';
  }

  RsConfigDataRates.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mRateIn = json['mRateIn'];
    mRateMaxIn = json['mRateMaxIn'];
    mAllocIn = json['mAllocIn'];
    mAllocTs = (json['mAllocTs'] == null) ?
      null :
      RstimeT.fromJson(json['mAllocTs']);
    mRateOut = json['mRateOut'];
    mRateMaxOut = json['mRateMaxOut'];
    mAllowedOut = json['mAllowedOut'];
    mAllowedTs = (json['mAllowedTs'] == null) ?
      null :
      RstimeT.fromJson(json['mAllowedTs']);
    mQueueIn = json['mQueueIn'];
    mQueueOut = json['mQueueOut'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mRateIn != null)
      json['mRateIn'] = mRateIn;
    if (mRateMaxIn != null)
      json['mRateMaxIn'] = mRateMaxIn;
    if (mAllocIn != null)
      json['mAllocIn'] = mAllocIn;
    if (mAllocTs != null)
      json['mAllocTs'] = mAllocTs;
    if (mRateOut != null)
      json['mRateOut'] = mRateOut;
    if (mRateMaxOut != null)
      json['mRateMaxOut'] = mRateMaxOut;
    if (mAllowedOut != null)
      json['mAllowedOut'] = mAllowedOut;
    if (mAllowedTs != null)
      json['mAllowedTs'] = mAllowedTs;
    if (mQueueIn != null)
      json['mQueueIn'] = mQueueIn;
    if (mQueueOut != null)
      json['mQueueOut'] = mQueueOut;
    return json;
  }

  static List<RsConfigDataRates> listFromJson(List<dynamic> json) {
    return json == null ? List<RsConfigDataRates>() : json.map((value) => RsConfigDataRates.fromJson(value)).toList();
  }

  static Map<String, RsConfigDataRates> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsConfigDataRates>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsConfigDataRates.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsConfigDataRates-objects as value to a dart map
  static Map<String, List<RsConfigDataRates>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsConfigDataRates>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsConfigDataRates.listFromJson(value);
       });
     }
     return map;
  }
}

