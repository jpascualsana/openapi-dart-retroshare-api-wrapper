part of openapi.api;

class ResGetForumsSummaries {
  
  bool retval = null;
  
  List<RsGroupMetaData> forums = [];
  ResGetForumsSummaries();

  @override
  String toString() {
    return 'ResGetForumsSummaries[retval=$retval, forums=$forums, ]';
  }

  ResGetForumsSummaries.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    forums = (json['forums'] == null) ?
      null :
      RsGroupMetaData.listFromJson(json['forums']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (forums != null)
      json['forums'] = forums;
    return json;
  }

  static List<ResGetForumsSummaries> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetForumsSummaries>() : json.map((value) => ResGetForumsSummaries.fromJson(value)).toList();
  }

  static Map<String, ResGetForumsSummaries> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetForumsSummaries>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetForumsSummaries.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetForumsSummaries-objects as value to a dart map
  static Map<String, List<ResGetForumsSummaries>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetForumsSummaries>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetForumsSummaries.listFromJson(value);
       });
     }
     return map;
  }
}

