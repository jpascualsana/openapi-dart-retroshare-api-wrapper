part of openapi.api;

class ReqExportCollectionLink {
  
  ReqBanFileFileSize handle = null;
  
  bool fragSneak = null;
  
  String baseUrl = null;
  ReqExportCollectionLink();

  @override
  String toString() {
    return 'ReqExportCollectionLink[handle=$handle, fragSneak=$fragSneak, baseUrl=$baseUrl, ]';
  }

  ReqExportCollectionLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    handle = (json['handle'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['handle']);
    fragSneak = json['fragSneak'];
    baseUrl = json['baseUrl'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (handle != null)
      json['handle'] = handle;
    if (fragSneak != null)
      json['fragSneak'] = fragSneak;
    if (baseUrl != null)
      json['baseUrl'] = baseUrl;
    return json;
  }

  static List<ReqExportCollectionLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqExportCollectionLink>() : json.map((value) => ReqExportCollectionLink.fromJson(value)).toList();
  }

  static Map<String, ReqExportCollectionLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqExportCollectionLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqExportCollectionLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqExportCollectionLink-objects as value to a dart map
  static Map<String, List<ReqExportCollectionLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqExportCollectionLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqExportCollectionLink.listFromJson(value);
       });
     }
     return map;
  }
}

