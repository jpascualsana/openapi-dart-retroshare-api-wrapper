part of openapi.api;

class ReqCreateChannelV2 {
  
  String name = null;
  
  String description = null;
  
  RsGxsImage thumbnail = null;
  
  String authorId = null;
  
  RsGxsCircleType circleType = null;
  //enum circleTypeEnum {  0,  1,  2,  3,  4,  5,  6,  };{
  
  String circleId = null;
  ReqCreateChannelV2();

  @override
  String toString() {
    return 'ReqCreateChannelV2[name=$name, description=$description, thumbnail=$thumbnail, authorId=$authorId, circleType=$circleType, circleId=$circleId, ]';
  }

  ReqCreateChannelV2.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    name = json['name'];
    description = json['description'];
    thumbnail = (json['thumbnail'] == null) ?
      null :
      RsGxsImage.fromJson(json['thumbnail']);
    authorId = json['authorId'];
    circleType = (json['circleType'] == null) ?
      null :
      RsGxsCircleType.fromJson(json['circleType']);
    circleId = json['circleId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (name != null)
      json['name'] = name;
    if (description != null)
      json['description'] = description;
    if (thumbnail != null)
      json['thumbnail'] = thumbnail;
    if (authorId != null)
      json['authorId'] = authorId;
    if (circleType != null)
      json['circleType'] = circleType;
    if (circleId != null)
      json['circleId'] = circleId;
    return json;
  }

  static List<ReqCreateChannelV2> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateChannelV2>() : json.map((value) => ReqCreateChannelV2.fromJson(value)).toList();
  }

  static Map<String, ReqCreateChannelV2> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateChannelV2>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateChannelV2.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateChannelV2-objects as value to a dart map
  static Map<String, List<ReqCreateChannelV2>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateChannelV2>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateChannelV2.listFromJson(value);
       });
     }
     return map;
  }
}

