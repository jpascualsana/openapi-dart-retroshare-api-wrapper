part of openapi.api;

class ReqGetServicePermissions {
  
  int serviceId = null;
  ReqGetServicePermissions();

  @override
  String toString() {
    return 'ReqGetServicePermissions[serviceId=$serviceId, ]';
  }

  ReqGetServicePermissions.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    serviceId = json['serviceId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (serviceId != null)
      json['serviceId'] = serviceId;
    return json;
  }

  static List<ReqGetServicePermissions> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetServicePermissions>() : json.map((value) => ReqGetServicePermissions.fromJson(value)).toList();
  }

  static Map<String, ReqGetServicePermissions> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetServicePermissions>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetServicePermissions.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetServicePermissions-objects as value to a dart map
  static Map<String, List<ReqGetServicePermissions>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetServicePermissions>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetServicePermissions.listFromJson(value);
       });
     }
     return map;
  }
}

