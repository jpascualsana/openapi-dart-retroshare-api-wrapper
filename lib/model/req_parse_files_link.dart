part of openapi.api;

class ReqParseFilesLink {
  
  String link = null;
  ReqParseFilesLink();

  @override
  String toString() {
    return 'ReqParseFilesLink[link=$link, ]';
  }

  ReqParseFilesLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (link != null)
      json['link'] = link;
    return json;
  }

  static List<ReqParseFilesLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqParseFilesLink>() : json.map((value) => ReqParseFilesLink.fromJson(value)).toList();
  }

  static Map<String, ReqParseFilesLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqParseFilesLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqParseFilesLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqParseFilesLink-objects as value to a dart map
  static Map<String, List<ReqParseFilesLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqParseFilesLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqParseFilesLink.listFromJson(value);
       });
     }
     return map;
  }
}

