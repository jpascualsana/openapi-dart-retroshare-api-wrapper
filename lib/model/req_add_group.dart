part of openapi.api;

class ReqAddGroup {
  
  RsGroupInfo groupInfo = null;
  ReqAddGroup();

  @override
  String toString() {
    return 'ReqAddGroup[groupInfo=$groupInfo, ]';
  }

  ReqAddGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    groupInfo = (json['groupInfo'] == null) ?
      null :
      RsGroupInfo.fromJson(json['groupInfo']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (groupInfo != null)
      json['groupInfo'] = groupInfo;
    return json;
  }

  static List<ReqAddGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqAddGroup>() : json.map((value) => ReqAddGroup.fromJson(value)).toList();
  }

  static Map<String, ReqAddGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqAddGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqAddGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqAddGroup-objects as value to a dart map
  static Map<String, List<ReqAddGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqAddGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqAddGroup.listFromJson(value);
       });
     }
     return map;
  }
}

