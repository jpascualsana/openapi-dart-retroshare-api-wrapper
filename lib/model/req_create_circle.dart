part of openapi.api;

class ReqCreateCircle {
  
  String circleName = null;
  
  RsGxsCircleType circleType = null;
  //enum circleTypeEnum {  0,  1,  2,  3,  4,  5,  6,  };{
  
  String restrictedId = null;
  
  String authorId = null;
  
  List<String> gxsIdMembers = [];
  
  List<String> localMembers = [];
  ReqCreateCircle();

  @override
  String toString() {
    return 'ReqCreateCircle[circleName=$circleName, circleType=$circleType, restrictedId=$restrictedId, authorId=$authorId, gxsIdMembers=$gxsIdMembers, localMembers=$localMembers, ]';
  }

  ReqCreateCircle.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    circleName = json['circleName'];
    circleType = (json['circleType'] == null) ?
      null :
      RsGxsCircleType.fromJson(json['circleType']);
    restrictedId = json['restrictedId'];
    authorId = json['authorId'];
    gxsIdMembers = (json['gxsIdMembers'] == null) ?
      null :
      (json['gxsIdMembers'] as List).cast<String>();
    localMembers = (json['localMembers'] == null) ?
      null :
      (json['localMembers'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (circleName != null)
      json['circleName'] = circleName;
    if (circleType != null)
      json['circleType'] = circleType;
    if (restrictedId != null)
      json['restrictedId'] = restrictedId;
    if (authorId != null)
      json['authorId'] = authorId;
    if (gxsIdMembers != null)
      json['gxsIdMembers'] = gxsIdMembers;
    if (localMembers != null)
      json['localMembers'] = localMembers;
    return json;
  }

  static List<ReqCreateCircle> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateCircle>() : json.map((value) => ReqCreateCircle.fromJson(value)).toList();
  }

  static Map<String, ReqCreateCircle> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateCircle>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateCircle.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateCircle-objects as value to a dart map
  static Map<String, List<ReqCreateCircle>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateCircle>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateCircle.listFromJson(value);
       });
     }
     return map;
  }
}

