part of openapi.api;

class DirStub {
  
  int type = null;
  
  String name = null;
  
  ReqBanFileFileSize ref = null;
  DirStub();

  @override
  String toString() {
    return 'DirStub[type=$type, name=$name, ref=$ref, ]';
  }

  DirStub.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    type = json['type'];
    name = json['name'];
    ref = (json['ref'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['ref']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (type != null)
      json['type'] = type;
    if (name != null)
      json['name'] = name;
    if (ref != null)
      json['ref'] = ref;
    return json;
  }

  static List<DirStub> listFromJson(List<dynamic> json) {
    return json == null ? List<DirStub>() : json.map((value) => DirStub.fromJson(value)).toList();
  }

  static Map<String, DirStub> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, DirStub>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = DirStub.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of DirStub-objects as value to a dart map
  static Map<String, List<DirStub>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<DirStub>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = DirStub.listFromJson(value);
       });
     }
     return map;
  }
}

