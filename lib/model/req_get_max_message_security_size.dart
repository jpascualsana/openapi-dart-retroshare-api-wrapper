part of openapi.api;

class ReqGetMaxMessageSecuritySize {
  
  int type = null;
  ReqGetMaxMessageSecuritySize();

  @override
  String toString() {
    return 'ReqGetMaxMessageSecuritySize[type=$type, ]';
  }

  ReqGetMaxMessageSecuritySize.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (type != null)
      json['type'] = type;
    return json;
  }

  static List<ReqGetMaxMessageSecuritySize> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetMaxMessageSecuritySize>() : json.map((value) => ReqGetMaxMessageSecuritySize.fromJson(value)).toList();
  }

  static Map<String, ReqGetMaxMessageSecuritySize> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetMaxMessageSecuritySize>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetMaxMessageSecuritySize.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetMaxMessageSecuritySize-objects as value to a dart map
  static Map<String, List<ReqGetMaxMessageSecuritySize>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetMaxMessageSecuritySize>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetMaxMessageSecuritySize.listFromJson(value);
       });
     }
     return map;
  }
}

