part of openapi.api;

class ResSetExtAddress {
  
  bool retval = null;
  ResSetExtAddress();

  @override
  String toString() {
    return 'ResSetExtAddress[retval=$retval, ]';
  }

  ResSetExtAddress.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetExtAddress> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetExtAddress>() : json.map((value) => ResSetExtAddress.fromJson(value)).toList();
  }

  static Map<String, ResSetExtAddress> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetExtAddress>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetExtAddress.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetExtAddress-objects as value to a dart map
  static Map<String, List<ResSetExtAddress>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetExtAddress>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetExtAddress.listFromJson(value);
       });
     }
     return map;
  }
}

