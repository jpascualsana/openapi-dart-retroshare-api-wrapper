part of openapi.api;

class ResDeleteBannedNodesThreshold {
  
  int retval = null;
  ResDeleteBannedNodesThreshold();

  @override
  String toString() {
    return 'ResDeleteBannedNodesThreshold[retval=$retval, ]';
  }

  ResDeleteBannedNodesThreshold.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResDeleteBannedNodesThreshold> listFromJson(List<dynamic> json) {
    return json == null ? List<ResDeleteBannedNodesThreshold>() : json.map((value) => ResDeleteBannedNodesThreshold.fromJson(value)).toList();
  }

  static Map<String, ResDeleteBannedNodesThreshold> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResDeleteBannedNodesThreshold>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResDeleteBannedNodesThreshold.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResDeleteBannedNodesThreshold-objects as value to a dart map
  static Map<String, List<ResDeleteBannedNodesThreshold>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResDeleteBannedNodesThreshold>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResDeleteBannedNodesThreshold.listFromJson(value);
       });
     }
     return map;
  }
}

