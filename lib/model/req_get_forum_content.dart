part of openapi.api;

class ReqGetForumContent {
  
  String forumId = null;
  
  List<String> msgsIds = [];
  ReqGetForumContent();

  @override
  String toString() {
    return 'ReqGetForumContent[forumId=$forumId, msgsIds=$msgsIds, ]';
  }

  ReqGetForumContent.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    forumId = json['forumId'];
    msgsIds = (json['msgsIds'] == null) ?
      null :
      (json['msgsIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (forumId != null)
      json['forumId'] = forumId;
    if (msgsIds != null)
      json['msgsIds'] = msgsIds;
    return json;
  }

  static List<ReqGetForumContent> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetForumContent>() : json.map((value) => ReqGetForumContent.fromJson(value)).toList();
  }

  static Map<String, ReqGetForumContent> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetForumContent>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetForumContent.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetForumContent-objects as value to a dart map
  static Map<String, List<ReqGetForumContent>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetForumContent>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetForumContent.listFromJson(value);
       });
     }
     return map;
  }
}

