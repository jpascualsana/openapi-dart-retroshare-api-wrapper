part of openapi.api;

class ResAssignPeerToGroup {
  
  bool retval = null;
  ResAssignPeerToGroup();

  @override
  String toString() {
    return 'ResAssignPeerToGroup[retval=$retval, ]';
  }

  ResAssignPeerToGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResAssignPeerToGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAssignPeerToGroup>() : json.map((value) => ResAssignPeerToGroup.fromJson(value)).toList();
  }

  static Map<String, ResAssignPeerToGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAssignPeerToGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAssignPeerToGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAssignPeerToGroup-objects as value to a dart map
  static Map<String, List<ResAssignPeerToGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAssignPeerToGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAssignPeerToGroup.listFromJson(value);
       });
     }
     return map;
  }
}

