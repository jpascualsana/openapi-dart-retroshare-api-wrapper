part of openapi.api;

class RsServiceType {
  /// The underlying value of this enum member.
  final int value;

  const RsServiceType._internal(this.value);

  static const RsServiceType number0_ = const RsServiceType._internal(0);
  static const RsServiceType number17_ = const RsServiceType._internal(17);
  static const RsServiceType number18_ = const RsServiceType._internal(18);
  static const RsServiceType number19_ = const RsServiceType._internal(19);
  static const RsServiceType number20_ = const RsServiceType._internal(20);
  static const RsServiceType number21_ = const RsServiceType._internal(21);
  static const RsServiceType number22_ = const RsServiceType._internal(22);
  static const RsServiceType number23_ = const RsServiceType._internal(23);
  static const RsServiceType number24_ = const RsServiceType._internal(24);
  static const RsServiceType number25_ = const RsServiceType._internal(25);
  static const RsServiceType number32_ = const RsServiceType._internal(32);
  static const RsServiceType number33_ = const RsServiceType._internal(33);
  static const RsServiceType number34_ = const RsServiceType._internal(34);
  static const RsServiceType number35_ = const RsServiceType._internal(35);
  static const RsServiceType number36_ = const RsServiceType._internal(36);
  static const RsServiceType number37_ = const RsServiceType._internal(37);
  static const RsServiceType number38_ = const RsServiceType._internal(38);
  static const RsServiceType number39_ = const RsServiceType._internal(39);
  static const RsServiceType number40_ = const RsServiceType._internal(40);
  static const RsServiceType number257_ = const RsServiceType._internal(257);
  static const RsServiceType number258_ = const RsServiceType._internal(258);
  static const RsServiceType number512_ = const RsServiceType._internal(512);
  static const RsServiceType number529_ = const RsServiceType._internal(529);
  static const RsServiceType number530_ = const RsServiceType._internal(530);
  static const RsServiceType number531_ = const RsServiceType._internal(531);
  static const RsServiceType number532_ = const RsServiceType._internal(532);
  static const RsServiceType number533_ = const RsServiceType._internal(533);
  static const RsServiceType number534_ = const RsServiceType._internal(534);
  static const RsServiceType number535_ = const RsServiceType._internal(535);
  static const RsServiceType number536_ = const RsServiceType._internal(536);
  static const RsServiceType number537_ = const RsServiceType._internal(537);
  static const RsServiceType number544_ = const RsServiceType._internal(544);
  static const RsServiceType number560_ = const RsServiceType._internal(560);
  static const RsServiceType number576_ = const RsServiceType._internal(576);
  static const RsServiceType number789_ = const RsServiceType._internal(789);
  static const RsServiceType number790_ = const RsServiceType._internal(790);
  static const RsServiceType number791_ = const RsServiceType._internal(791);
  static const RsServiceType number4113_ = const RsServiceType._internal(4113);
  static const RsServiceType number8193_ = const RsServiceType._internal(8193);
  static const RsServiceType number8194_ = const RsServiceType._internal(8194);
  static const RsServiceType number8195_ = const RsServiceType._internal(8195);
  static const RsServiceType number43707_ = const RsServiceType._internal(43707);
  static const RsServiceType number61904_ = const RsServiceType._internal(61904);
  static const RsServiceType number48879_ = const RsServiceType._internal(48879);
  
  int toJson (){
    return this.value;
  }

  static RsServiceType fromJson(int value) {
    return new RsServiceTypeTypeTransformer().decode(value);
  }
  
  static List<RsServiceType> listFromJson(List<dynamic> json) {
    return json == null ? new List<RsServiceType>() : json.map((value) => RsServiceType.fromJson(value)).toList();
  }
}

class RsServiceTypeTypeTransformer {

  dynamic encode(RsServiceType data) {
    return data.value;
  }

  RsServiceType decode(dynamic data) {
    switch (data) {
      case 0: return RsServiceType.number0_;
      case 17: return RsServiceType.number17_;
      case 18: return RsServiceType.number18_;
      case 19: return RsServiceType.number19_;
      case 20: return RsServiceType.number20_;
      case 21: return RsServiceType.number21_;
      case 22: return RsServiceType.number22_;
      case 23: return RsServiceType.number23_;
      case 24: return RsServiceType.number24_;
      case 25: return RsServiceType.number25_;
      case 32: return RsServiceType.number32_;
      case 33: return RsServiceType.number33_;
      case 34: return RsServiceType.number34_;
      case 35: return RsServiceType.number35_;
      case 36: return RsServiceType.number36_;
      case 37: return RsServiceType.number37_;
      case 38: return RsServiceType.number38_;
      case 39: return RsServiceType.number39_;
      case 40: return RsServiceType.number40_;
      case 257: return RsServiceType.number257_;
      case 258: return RsServiceType.number258_;
      case 512: return RsServiceType.number512_;
      case 529: return RsServiceType.number529_;
      case 530: return RsServiceType.number530_;
      case 531: return RsServiceType.number531_;
      case 532: return RsServiceType.number532_;
      case 533: return RsServiceType.number533_;
      case 534: return RsServiceType.number534_;
      case 535: return RsServiceType.number535_;
      case 536: return RsServiceType.number536_;
      case 537: return RsServiceType.number537_;
      case 544: return RsServiceType.number544_;
      case 560: return RsServiceType.number560_;
      case 576: return RsServiceType.number576_;
      case 789: return RsServiceType.number789_;
      case 790: return RsServiceType.number790_;
      case 791: return RsServiceType.number791_;
      case 4113: return RsServiceType.number4113_;
      case 8193: return RsServiceType.number8193_;
      case 8194: return RsServiceType.number8194_;
      case 8195: return RsServiceType.number8195_;
      case 43707: return RsServiceType.number43707_;
      case 61904: return RsServiceType.number61904_;
      case 48879: return RsServiceType.number48879_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}

