part of openapi.api;

class ResGetMessageTagTypes {
  
  bool retval = null;
  
  RsMsgsMsgTagType tags = null;
  ResGetMessageTagTypes();

  @override
  String toString() {
    return 'ResGetMessageTagTypes[retval=$retval, tags=$tags, ]';
  }

  ResGetMessageTagTypes.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    tags = (json['tags'] == null) ?
      null :
      RsMsgsMsgTagType.fromJson(json['tags']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (tags != null)
      json['tags'] = tags;
    return json;
  }

  static List<ResGetMessageTagTypes> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetMessageTagTypes>() : json.map((value) => ResGetMessageTagTypes.fromJson(value)).toList();
  }

  static Map<String, ResGetMessageTagTypes> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetMessageTagTypes>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetMessageTagTypes.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetMessageTagTypes-objects as value to a dart map
  static Map<String, List<ResGetMessageTagTypes>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetMessageTagTypes>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetMessageTagTypes.listFromJson(value);
       });
     }
     return map;
  }
}

