part of openapi.api;

class ResAssignPeersToGroup {
  
  bool retval = null;
  ResAssignPeersToGroup();

  @override
  String toString() {
    return 'ResAssignPeersToGroup[retval=$retval, ]';
  }

  ResAssignPeersToGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResAssignPeersToGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAssignPeersToGroup>() : json.map((value) => ResAssignPeersToGroup.fromJson(value)).toList();
  }

  static Map<String, ResAssignPeersToGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAssignPeersToGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAssignPeersToGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAssignPeersToGroup-objects as value to a dart map
  static Map<String, List<ResAssignPeersToGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAssignPeersToGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAssignPeersToGroup.listFromJson(value);
       });
     }
     return map;
  }
}

