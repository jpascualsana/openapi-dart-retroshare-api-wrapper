part of openapi.api;

class ResUnbanFile {
  
  int retval = null;
  ResUnbanFile();

  @override
  String toString() {
    return 'ResUnbanFile[retval=$retval, ]';
  }

  ResUnbanFile.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResUnbanFile> listFromJson(List<dynamic> json) {
    return json == null ? List<ResUnbanFile>() : json.map((value) => ResUnbanFile.fromJson(value)).toList();
  }

  static Map<String, ResUnbanFile> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResUnbanFile>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResUnbanFile.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResUnbanFile-objects as value to a dart map
  static Map<String, List<ResUnbanFile>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResUnbanFile>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResUnbanFile.listFromJson(value);
       });
     }
     return map;
  }
}

