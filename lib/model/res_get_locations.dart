part of openapi.api;

class ResGetLocations {
  
  List<RsLoginHelperLocation> locations = [];
  ResGetLocations();

  @override
  String toString() {
    return 'ResGetLocations[locations=$locations, ]';
  }

  ResGetLocations.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    locations = (json['locations'] == null) ?
      null :
      RsLoginHelperLocation.listFromJson(json['locations']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (locations != null)
      json['locations'] = locations;
    return json;
  }

  static List<ResGetLocations> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetLocations>() : json.map((value) => ResGetLocations.fromJson(value)).toList();
  }

  static Map<String, ResGetLocations> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetLocations>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetLocations.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetLocations-objects as value to a dart map
  static Map<String, List<ResGetLocations>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetLocations>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetLocations.listFromJson(value);
       });
     }
     return map;
  }
}

