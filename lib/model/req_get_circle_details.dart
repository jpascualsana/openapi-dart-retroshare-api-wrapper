part of openapi.api;

class ReqGetCircleDetails {
  
  String id = null;
  ReqGetCircleDetails();

  @override
  String toString() {
    return 'ReqGetCircleDetails[id=$id, ]';
  }

  ReqGetCircleDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqGetCircleDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetCircleDetails>() : json.map((value) => ReqGetCircleDetails.fromJson(value)).toList();
  }

  static Map<String, ReqGetCircleDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetCircleDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetCircleDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetCircleDetails-objects as value to a dart map
  static Map<String, List<ReqGetCircleDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetCircleDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetCircleDetails.listFromJson(value);
       });
     }
     return map;
  }
}

