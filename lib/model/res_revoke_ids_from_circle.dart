part of openapi.api;

class ResRevokeIdsFromCircle {
  
  bool retval = null;
  ResRevokeIdsFromCircle();

  @override
  String toString() {
    return 'ResRevokeIdsFromCircle[retval=$retval, ]';
  }

  ResRevokeIdsFromCircle.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRevokeIdsFromCircle> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRevokeIdsFromCircle>() : json.map((value) => ResRevokeIdsFromCircle.fromJson(value)).toList();
  }

  static Map<String, ResRevokeIdsFromCircle> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRevokeIdsFromCircle>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRevokeIdsFromCircle.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRevokeIdsFromCircle-objects as value to a dart map
  static Map<String, List<ResRevokeIdsFromCircle>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRevokeIdsFromCircle>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRevokeIdsFromCircle.listFromJson(value);
       });
     }
     return map;
  }
}

