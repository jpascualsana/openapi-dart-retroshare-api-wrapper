part of openapi.api;

class ResRemoveMessageTagType {
  
  bool retval = null;
  ResRemoveMessageTagType();

  @override
  String toString() {
    return 'ResRemoveMessageTagType[retval=$retval, ]';
  }

  ResRemoveMessageTagType.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRemoveMessageTagType> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRemoveMessageTagType>() : json.map((value) => ResRemoveMessageTagType.fromJson(value)).toList();
  }

  static Map<String, ResRemoveMessageTagType> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRemoveMessageTagType>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRemoveMessageTagType.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRemoveMessageTagType-objects as value to a dart map
  static Map<String, List<ResRemoveMessageTagType>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRemoveMessageTagType>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRemoveMessageTagType.listFromJson(value);
       });
     }
     return map;
  }
}

