part of openapi.api;

class ReqSetDownloadDirectory {
  
  String path = null;
  ReqSetDownloadDirectory();

  @override
  String toString() {
    return 'ReqSetDownloadDirectory[path=$path, ]';
  }

  ReqSetDownloadDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    path = json['path'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (path != null)
      json['path'] = path;
    return json;
  }

  static List<ReqSetDownloadDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetDownloadDirectory>() : json.map((value) => ReqSetDownloadDirectory.fromJson(value)).toList();
  }

  static Map<String, ReqSetDownloadDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetDownloadDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetDownloadDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetDownloadDirectory-objects as value to a dart map
  static Map<String, List<ReqSetDownloadDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetDownloadDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetDownloadDirectory.listFromJson(value);
       });
     }
     return map;
  }
}

