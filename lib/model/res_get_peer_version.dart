part of openapi.api;

class ResGetPeerVersion {
  
  bool retval = null;
  
  String version = null;
  ResGetPeerVersion();

  @override
  String toString() {
    return 'ResGetPeerVersion[retval=$retval, version=$version, ]';
  }

  ResGetPeerVersion.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    version = json['version'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (version != null)
      json['version'] = version;
    return json;
  }

  static List<ResGetPeerVersion> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetPeerVersion>() : json.map((value) => ResGetPeerVersion.fromJson(value)).toList();
  }

  static Map<String, ResGetPeerVersion> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetPeerVersion>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetPeerVersion.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetPeerVersion-objects as value to a dart map
  static Map<String, List<ResGetPeerVersion>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetPeerVersion>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetPeerVersion.listFromJson(value);
       });
     }
     return map;
  }
}

