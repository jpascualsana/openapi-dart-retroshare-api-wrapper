part of openapi.api;

class ReqBanNode {
  
  String id = null;
  
  bool b = null;
  ReqBanNode();

  @override
  String toString() {
    return 'ReqBanNode[id=$id, b=$b, ]';
  }

  ReqBanNode.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    b = json['b'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (b != null)
      json['b'] = b;
    return json;
  }

  static List<ReqBanNode> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqBanNode>() : json.map((value) => ReqBanNode.fromJson(value)).toList();
  }

  static Map<String, ReqBanNode> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqBanNode>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqBanNode.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqBanNode-objects as value to a dart map
  static Map<String, List<ReqBanNode>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqBanNode>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqBanNode.listFromJson(value);
       });
     }
     return map;
  }
}

