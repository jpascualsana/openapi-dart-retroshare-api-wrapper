part of openapi.api;

class ReqMessageJunk {
  
  String msgId = null;
  
  bool mark = null;
  ReqMessageJunk();

  @override
  String toString() {
    return 'ReqMessageJunk[msgId=$msgId, mark=$mark, ]';
  }

  ReqMessageJunk.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
    mark = json['mark'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    if (mark != null)
      json['mark'] = mark;
    return json;
  }

  static List<ReqMessageJunk> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqMessageJunk>() : json.map((value) => ReqMessageJunk.fromJson(value)).toList();
  }

  static Map<String, ReqMessageJunk> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqMessageJunk>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqMessageJunk.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqMessageJunk-objects as value to a dart map
  static Map<String, List<ReqMessageJunk>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqMessageJunk>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqMessageJunk.listFromJson(value);
       });
     }
     return map;
  }
}

