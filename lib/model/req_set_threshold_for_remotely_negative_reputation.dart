part of openapi.api;

class ReqSetThresholdForRemotelyNegativeReputation {
  
  int thresh = null;
  ReqSetThresholdForRemotelyNegativeReputation();

  @override
  String toString() {
    return 'ReqSetThresholdForRemotelyNegativeReputation[thresh=$thresh, ]';
  }

  ReqSetThresholdForRemotelyNegativeReputation.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    thresh = json['thresh'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (thresh != null)
      json['thresh'] = thresh;
    return json;
  }

  static List<ReqSetThresholdForRemotelyNegativeReputation> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetThresholdForRemotelyNegativeReputation>() : json.map((value) => ReqSetThresholdForRemotelyNegativeReputation.fromJson(value)).toList();
  }

  static Map<String, ReqSetThresholdForRemotelyNegativeReputation> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetThresholdForRemotelyNegativeReputation>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetThresholdForRemotelyNegativeReputation.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetThresholdForRemotelyNegativeReputation-objects as value to a dart map
  static Map<String, List<ReqSetThresholdForRemotelyNegativeReputation>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetThresholdForRemotelyNegativeReputation>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetThresholdForRemotelyNegativeReputation.listFromJson(value);
       });
     }
     return map;
  }
}

