part of openapi.api;

class RsGxsForumMsg {
  
  RsMsgMetaData mMeta = null;
  
  String mMsg = null;
  RsGxsForumMsg();

  @override
  String toString() {
    return 'RsGxsForumMsg[mMeta=$mMeta, mMsg=$mMsg, ]';
  }

  RsGxsForumMsg.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mMeta = (json['mMeta'] == null) ?
      null :
      RsMsgMetaData.fromJson(json['mMeta']);
    mMsg = json['mMsg'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mMeta != null)
      json['mMeta'] = mMeta;
    if (mMsg != null)
      json['mMsg'] = mMsg;
    return json;
  }

  static List<RsGxsForumMsg> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsForumMsg>() : json.map((value) => RsGxsForumMsg.fromJson(value)).toList();
  }

  static Map<String, RsGxsForumMsg> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsForumMsg>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsForumMsg.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsForumMsg-objects as value to a dart map
  static Map<String, List<RsGxsForumMsg>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsForumMsg>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsForumMsg.listFromJson(value);
       });
     }
     return map;
  }
}

