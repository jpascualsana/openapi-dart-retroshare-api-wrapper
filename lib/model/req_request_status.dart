part of openapi.api;

class ReqRequestStatus {
  
  int token = null;
  ReqRequestStatus();

  @override
  String toString() {
    return 'ReqRequestStatus[token=$token, ]';
  }

  ReqRequestStatus.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (token != null)
      json['token'] = token;
    return json;
  }

  static List<ReqRequestStatus> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRequestStatus>() : json.map((value) => ReqRequestStatus.fromJson(value)).toList();
  }

  static Map<String, ReqRequestStatus> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRequestStatus>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRequestStatus.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRequestStatus-objects as value to a dart map
  static Map<String, List<ReqRequestStatus>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRequestStatus>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRequestStatus.listFromJson(value);
       });
     }
     return map;
  }
}

