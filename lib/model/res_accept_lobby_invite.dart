part of openapi.api;

class ResAcceptLobbyInvite {
  
  bool retval = null;
  ResAcceptLobbyInvite();

  @override
  String toString() {
    return 'ResAcceptLobbyInvite[retval=$retval, ]';
  }

  ResAcceptLobbyInvite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResAcceptLobbyInvite> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAcceptLobbyInvite>() : json.map((value) => ResAcceptLobbyInvite.fromJson(value)).toList();
  }

  static Map<String, ResAcceptLobbyInvite> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAcceptLobbyInvite>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAcceptLobbyInvite.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAcceptLobbyInvite-objects as value to a dart map
  static Map<String, List<ResAcceptLobbyInvite>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAcceptLobbyInvite>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAcceptLobbyInvite.listFromJson(value);
       });
     }
     return map;
  }
}

