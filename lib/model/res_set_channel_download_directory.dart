part of openapi.api;

class ResSetChannelDownloadDirectory {
  
  bool retval = null;
  ResSetChannelDownloadDirectory();

  @override
  String toString() {
    return 'ResSetChannelDownloadDirectory[retval=$retval, ]';
  }

  ResSetChannelDownloadDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetChannelDownloadDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetChannelDownloadDirectory>() : json.map((value) => ResSetChannelDownloadDirectory.fromJson(value)).toList();
  }

  static Map<String, ResSetChannelDownloadDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetChannelDownloadDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetChannelDownloadDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetChannelDownloadDirectory-objects as value to a dart map
  static Map<String, List<ResSetChannelDownloadDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetChannelDownloadDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetChannelDownloadDirectory.listFromJson(value);
       });
     }
     return map;
  }
}

