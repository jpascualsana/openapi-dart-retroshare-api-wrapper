part of openapi.api;

class ReqSetSharedDirectories {
  
  List<SharedDirInfo> dirs = [];
  ReqSetSharedDirectories();

  @override
  String toString() {
    return 'ReqSetSharedDirectories[dirs=$dirs, ]';
  }

  ReqSetSharedDirectories.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dirs = (json['dirs'] == null) ?
      null :
      SharedDirInfo.listFromJson(json['dirs']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (dirs != null)
      json['dirs'] = dirs;
    return json;
  }

  static List<ReqSetSharedDirectories> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetSharedDirectories>() : json.map((value) => ReqSetSharedDirectories.fromJson(value)).toList();
  }

  static Map<String, ReqSetSharedDirectories> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetSharedDirectories>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetSharedDirectories.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetSharedDirectories-objects as value to a dart map
  static Map<String, List<ReqSetSharedDirectories>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetSharedDirectories>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetSharedDirectories.listFromJson(value);
       });
     }
     return map;
  }
}

