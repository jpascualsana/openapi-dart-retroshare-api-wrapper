part of openapi.api;

class FileRequestFlags {
  /// The underlying value of this enum member.
  final int value;

  const FileRequestFlags._internal(this.value);

  static const FileRequestFlags number64_ = const FileRequestFlags._internal(64);
  static const FileRequestFlags number128_ = const FileRequestFlags._internal(128);
  static const FileRequestFlags number256_ = const FileRequestFlags._internal(256);
  static const FileRequestFlags number8192_ = const FileRequestFlags._internal(8192);
  static const FileRequestFlags number33554432_ = const FileRequestFlags._internal(33554432);
  
  int toJson (){
    return this.value;
  }

  static FileRequestFlags fromJson(int value) {
    return new FileRequestFlagsTypeTransformer().decode(value);
  }
  
  static List<FileRequestFlags> listFromJson(List<dynamic> json) {
    return json == null ? new List<FileRequestFlags>() : json.map((value) => FileRequestFlags.fromJson(value)).toList();
  }
}

class FileRequestFlagsTypeTransformer {

  dynamic encode(FileRequestFlags data) {
    return data.value;
  }

  FileRequestFlags decode(dynamic data) {
    switch (data) {
      case 64: return FileRequestFlags.number64_;
      case 128: return FileRequestFlags.number128_;
      case 256: return FileRequestFlags.number256_;
      case 8192: return FileRequestFlags.number8192_;
      case 33554432: return FileRequestFlags.number33554432_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}

