part of openapi.api;

class ChatLobbyId {
  
  num xint64 = null;
  
  String xstr64 = null;
  ChatLobbyId();

  @override
  String toString() {
    return 'ChatLobbyId[xint64=$xint64, xstr64=$xstr64, ]';
  }

  ChatLobbyId.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    xint64 = json['xint64'];
    xstr64 = json['xstr64'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (xint64 != null)
      json['xint64'] = xint64;
    if (xstr64 != null)
      json['xstr64'] = xstr64;
    return json;
  }

  static List<ChatLobbyId> listFromJson(List<dynamic> json) {
    return json == null ? List<ChatLobbyId>() : json.map((value) => ChatLobbyId.fromJson(value)).toList();
  }

  static Map<String, ChatLobbyId> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ChatLobbyId>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ChatLobbyId.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ChatLobbyId-objects as value to a dart map
  static Map<String, List<ChatLobbyId>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ChatLobbyId>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ChatLobbyId.listFromJson(value);
       });
     }
     return map;
  }
}

