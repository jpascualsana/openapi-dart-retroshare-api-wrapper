part of openapi.api;

class ResCreateForumV2 {
  
  bool retval = null;
  
  String forumId = null;
  
  String errorMessage = null;
  ResCreateForumV2();

  @override
  String toString() {
    return 'ResCreateForumV2[retval=$retval, forumId=$forumId, errorMessage=$errorMessage, ]';
  }

  ResCreateForumV2.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    forumId = json['forumId'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (forumId != null)
      json['forumId'] = forumId;
    if (errorMessage != null)
      json['errorMessage'] = errorMessage;
    return json;
  }

  static List<ResCreateForumV2> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateForumV2>() : json.map((value) => ResCreateForumV2.fromJson(value)).toList();
  }

  static Map<String, ResCreateForumV2> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateForumV2>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateForumV2.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateForumV2-objects as value to a dart map
  static Map<String, List<ResCreateForumV2>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateForumV2>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateForumV2.listFromJson(value);
       });
     }
     return map;
  }
}

