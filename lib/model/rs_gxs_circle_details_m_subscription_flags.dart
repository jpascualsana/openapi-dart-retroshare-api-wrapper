part of openapi.api;

class RsGxsCircleDetailsMSubscriptionFlags {
  
  String key = null;
  
  int value = null;
  RsGxsCircleDetailsMSubscriptionFlags();

  @override
  String toString() {
    return 'RsGxsCircleDetailsMSubscriptionFlags[key=$key, value=$value, ]';
  }

  RsGxsCircleDetailsMSubscriptionFlags.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    key = json['key'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (key != null)
      json['key'] = key;
    if (value != null)
      json['value'] = value;
    return json;
  }

  static List<RsGxsCircleDetailsMSubscriptionFlags> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsCircleDetailsMSubscriptionFlags>() : json.map((value) => RsGxsCircleDetailsMSubscriptionFlags.fromJson(value)).toList();
  }

  static Map<String, RsGxsCircleDetailsMSubscriptionFlags> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsCircleDetailsMSubscriptionFlags>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsCircleDetailsMSubscriptionFlags.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsCircleDetailsMSubscriptionFlags-objects as value to a dart map
  static Map<String, List<RsGxsCircleDetailsMSubscriptionFlags>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsCircleDetailsMSubscriptionFlags>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsCircleDetailsMSubscriptionFlags.listFromJson(value);
       });
     }
     return map;
  }
}

