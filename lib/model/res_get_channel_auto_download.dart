part of openapi.api;

class ResGetChannelAutoDownload {
  
  bool retval = null;
  
  bool enabled = null;
  ResGetChannelAutoDownload();

  @override
  String toString() {
    return 'ResGetChannelAutoDownload[retval=$retval, enabled=$enabled, ]';
  }

  ResGetChannelAutoDownload.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    enabled = json['enabled'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (enabled != null)
      json['enabled'] = enabled;
    return json;
  }

  static List<ResGetChannelAutoDownload> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetChannelAutoDownload>() : json.map((value) => ResGetChannelAutoDownload.fromJson(value)).toList();
  }

  static Map<String, ResGetChannelAutoDownload> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetChannelAutoDownload>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetChannelAutoDownload.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetChannelAutoDownload-objects as value to a dart map
  static Map<String, List<ResGetChannelAutoDownload>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetChannelAutoDownload>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetChannelAutoDownload.listFromJson(value);
       });
     }
     return map;
  }
}

