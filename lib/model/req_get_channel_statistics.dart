part of openapi.api;

class ReqGetChannelStatistics {
  
  String channelId = null;
  ReqGetChannelStatistics();

  @override
  String toString() {
    return 'ReqGetChannelStatistics[channelId=$channelId, ]';
  }

  ReqGetChannelStatistics.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    return json;
  }

  static List<ReqGetChannelStatistics> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetChannelStatistics>() : json.map((value) => ReqGetChannelStatistics.fromJson(value)).toList();
  }

  static Map<String, ReqGetChannelStatistics> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetChannelStatistics>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetChannelStatistics.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetChannelStatistics-objects as value to a dart map
  static Map<String, List<ReqGetChannelStatistics>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetChannelStatistics>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetChannelStatistics.listFromJson(value);
       });
     }
     return map;
  }
}

