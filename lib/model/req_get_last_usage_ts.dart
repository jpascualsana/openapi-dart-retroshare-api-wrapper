part of openapi.api;

class ReqGetLastUsageTS {
  
  String id = null;
  ReqGetLastUsageTS();

  @override
  String toString() {
    return 'ReqGetLastUsageTS[id=$id, ]';
  }

  ReqGetLastUsageTS.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqGetLastUsageTS> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetLastUsageTS>() : json.map((value) => ReqGetLastUsageTS.fromJson(value)).toList();
  }

  static Map<String, ReqGetLastUsageTS> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetLastUsageTS>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetLastUsageTS.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetLastUsageTS-objects as value to a dart map
  static Map<String, List<ReqGetLastUsageTS>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetLastUsageTS>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetLastUsageTS.listFromJson(value);
       });
     }
     return map;
  }
}

