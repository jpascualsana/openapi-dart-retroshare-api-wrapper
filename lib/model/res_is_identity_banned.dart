part of openapi.api;

class ResIsIdentityBanned {
  
  bool retval = null;
  ResIsIdentityBanned();

  @override
  String toString() {
    return 'ResIsIdentityBanned[retval=$retval, ]';
  }

  ResIsIdentityBanned.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsIdentityBanned> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsIdentityBanned>() : json.map((value) => ResIsIdentityBanned.fromJson(value)).toList();
  }

  static Map<String, ResIsIdentityBanned> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsIdentityBanned>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsIdentityBanned.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsIdentityBanned-objects as value to a dart map
  static Map<String, List<ResIsIdentityBanned>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsIdentityBanned>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsIdentityBanned.listFromJson(value);
       });
     }
     return map;
  }
}

