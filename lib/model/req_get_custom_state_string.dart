part of openapi.api;

class ReqGetCustomStateString {
  
  String peerId = null;
  ReqGetCustomStateString();

  @override
  String toString() {
    return 'ReqGetCustomStateString[peerId=$peerId, ]';
  }

  ReqGetCustomStateString.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    peerId = json['peer_id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (peerId != null)
      json['peer_id'] = peerId;
    return json;
  }

  static List<ReqGetCustomStateString> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetCustomStateString>() : json.map((value) => ReqGetCustomStateString.fromJson(value)).toList();
  }

  static Map<String, ReqGetCustomStateString> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetCustomStateString>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetCustomStateString.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetCustomStateString-objects as value to a dart map
  static Map<String, List<ReqGetCustomStateString>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetCustomStateString>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetCustomStateString.listFromJson(value);
       });
     }
     return map;
  }
}

