part of openapi.api;

class ResGetGPGId {
  
  String retval = null;
  ResGetGPGId();

  @override
  String toString() {
    return 'ResGetGPGId[retval=$retval, ]';
  }

  ResGetGPGId.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetGPGId> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetGPGId>() : json.map((value) => ResGetGPGId.fromJson(value)).toList();
  }

  static Map<String, ResGetGPGId> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetGPGId>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetGPGId.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetGPGId-objects as value to a dart map
  static Map<String, List<ResGetGPGId>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetGPGId>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetGPGId.listFromJson(value);
       });
     }
     return map;
  }
}

