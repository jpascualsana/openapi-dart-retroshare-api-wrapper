part of openapi.api;

class ReqAlreadyHaveFile {
  
  String hash = null;
  ReqAlreadyHaveFile();

  @override
  String toString() {
    return 'ReqAlreadyHaveFile[hash=$hash, ]';
  }

  ReqAlreadyHaveFile.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hash = json['hash'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hash != null)
      json['hash'] = hash;
    return json;
  }

  static List<ReqAlreadyHaveFile> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqAlreadyHaveFile>() : json.map((value) => ReqAlreadyHaveFile.fromJson(value)).toList();
  }

  static Map<String, ReqAlreadyHaveFile> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqAlreadyHaveFile>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqAlreadyHaveFile.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqAlreadyHaveFile-objects as value to a dart map
  static Map<String, List<ReqAlreadyHaveFile>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqAlreadyHaveFile>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqAlreadyHaveFile.listFromJson(value);
       });
     }
     return map;
  }
}

