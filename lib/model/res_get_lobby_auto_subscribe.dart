part of openapi.api;

class ResGetLobbyAutoSubscribe {
  
  bool retval = null;
  ResGetLobbyAutoSubscribe();

  @override
  String toString() {
    return 'ResGetLobbyAutoSubscribe[retval=$retval, ]';
  }

  ResGetLobbyAutoSubscribe.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetLobbyAutoSubscribe> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetLobbyAutoSubscribe>() : json.map((value) => ResGetLobbyAutoSubscribe.fromJson(value)).toList();
  }

  static Map<String, ResGetLobbyAutoSubscribe> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetLobbyAutoSubscribe>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetLobbyAutoSubscribe.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetLobbyAutoSubscribe-objects as value to a dart map
  static Map<String, List<ResGetLobbyAutoSubscribe>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetLobbyAutoSubscribe>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetLobbyAutoSubscribe.listFromJson(value);
       });
     }
     return map;
  }
}

