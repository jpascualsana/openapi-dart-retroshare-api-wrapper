part of openapi.api;

class RsIdentityDetailsMUseCases {
  
  RsIdentityUsage key = null;
  
  RstimeT value = null;
  RsIdentityDetailsMUseCases();

  @override
  String toString() {
    return 'RsIdentityDetailsMUseCases[key=$key, value=$value, ]';
  }

  RsIdentityDetailsMUseCases.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    key = (json['key'] == null) ?
      null :
      RsIdentityUsage.fromJson(json['key']);
    value = (json['value'] == null) ?
      null :
      RstimeT.fromJson(json['value']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (key != null)
      json['key'] = key;
    if (value != null)
      json['value'] = value;
    return json;
  }

  static List<RsIdentityDetailsMUseCases> listFromJson(List<dynamic> json) {
    return json == null ? List<RsIdentityDetailsMUseCases>() : json.map((value) => RsIdentityDetailsMUseCases.fromJson(value)).toList();
  }

  static Map<String, RsIdentityDetailsMUseCases> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsIdentityDetailsMUseCases>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsIdentityDetailsMUseCases.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsIdentityDetailsMUseCases-objects as value to a dart map
  static Map<String, List<RsIdentityDetailsMUseCases>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsIdentityDetailsMUseCases>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsIdentityDetailsMUseCases.listFromJson(value);
       });
     }
     return map;
  }
}

