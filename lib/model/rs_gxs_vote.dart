part of openapi.api;

class RsGxsVote {
  
  RsMsgMetaData mMeta = null;
  
  int mVoteType = null;
  RsGxsVote();

  @override
  String toString() {
    return 'RsGxsVote[mMeta=$mMeta, mVoteType=$mVoteType, ]';
  }

  RsGxsVote.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mMeta = (json['mMeta'] == null) ?
      null :
      RsMsgMetaData.fromJson(json['mMeta']);
    mVoteType = json['mVoteType'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mMeta != null)
      json['mMeta'] = mMeta;
    if (mVoteType != null)
      json['mVoteType'] = mVoteType;
    return json;
  }

  static List<RsGxsVote> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsVote>() : json.map((value) => RsGxsVote.fromJson(value)).toList();
  }

  static Map<String, RsGxsVote> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsVote>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsVote.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsVote-objects as value to a dart map
  static Map<String, List<RsGxsVote>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsVote>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsVote.listFromJson(value);
       });
     }
     return map;
  }
}

