part of openapi.api;

class ReqGetFileData {
  
  String hash = null;
  
  ReqBanFileFileSize offset = null;
  
  int requestedSize = null;
  ReqGetFileData();

  @override
  String toString() {
    return 'ReqGetFileData[hash=$hash, offset=$offset, requestedSize=$requestedSize, ]';
  }

  ReqGetFileData.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hash = json['hash'];
    offset = (json['offset'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['offset']);
    requestedSize = json['requested_size'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hash != null)
      json['hash'] = hash;
    if (offset != null)
      json['offset'] = offset;
    if (requestedSize != null)
      json['requested_size'] = requestedSize;
    return json;
  }

  static List<ReqGetFileData> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetFileData>() : json.map((value) => ReqGetFileData.fromJson(value)).toList();
  }

  static Map<String, ReqGetFileData> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetFileData>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetFileData.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetFileData-objects as value to a dart map
  static Map<String, List<ReqGetFileData>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetFileData>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetFileData.listFromJson(value);
       });
     }
     return map;
  }
}

