part of openapi.api;

class ResJoinVisibleChatLobby {
  
  bool retval = null;
  ResJoinVisibleChatLobby();

  @override
  String toString() {
    return 'ResJoinVisibleChatLobby[retval=$retval, ]';
  }

  ResJoinVisibleChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResJoinVisibleChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ResJoinVisibleChatLobby>() : json.map((value) => ResJoinVisibleChatLobby.fromJson(value)).toList();
  }

  static Map<String, ResJoinVisibleChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResJoinVisibleChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResJoinVisibleChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResJoinVisibleChatLobby-objects as value to a dart map
  static Map<String, List<ResJoinVisibleChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResJoinVisibleChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResJoinVisibleChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}

