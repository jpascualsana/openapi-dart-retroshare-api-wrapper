part of openapi.api;

class ChunkState {
  /// The underlying value of this enum member.
  final int value;

  const ChunkState._internal(this.value);

  static const ChunkState number0_ = const ChunkState._internal(0);
  static const ChunkState number1_ = const ChunkState._internal(1);
  static const ChunkState number2_ = const ChunkState._internal(2);
  static const ChunkState number3_ = const ChunkState._internal(3);
  
  int toJson (){
    return this.value;
  }

  static ChunkState fromJson(int value) {
    return new ChunkStateTypeTransformer().decode(value);
  }
  
  static List<ChunkState> listFromJson(List<dynamic> json) {
    return json == null ? new List<ChunkState>() : json.map((value) => ChunkState.fromJson(value)).toList();
  }
}

class ChunkStateTypeTransformer {

  dynamic encode(ChunkState data) {
    return data.value;
  }

  ChunkState decode(dynamic data) {
    switch (data) {
      case 0: return ChunkState.number0_;
      case 1: return ChunkState.number1_;
      case 2: return ChunkState.number2_;
      case 3: return ChunkState.number3_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}

