part of openapi.api;

class ReqIsKnownId {
  
  String id = null;
  ReqIsKnownId();

  @override
  String toString() {
    return 'ReqIsKnownId[id=$id, ]';
  }

  ReqIsKnownId.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqIsKnownId> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqIsKnownId>() : json.map((value) => ReqIsKnownId.fromJson(value)).toList();
  }

  static Map<String, ReqIsKnownId> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqIsKnownId>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqIsKnownId.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqIsKnownId-objects as value to a dart map
  static Map<String, List<ReqIsKnownId>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqIsKnownId>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqIsKnownId.listFromJson(value);
       });
     }
     return map;
  }
}

