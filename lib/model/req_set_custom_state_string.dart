part of openapi.api;

class ReqSetCustomStateString {
  
  String statusString = null;
  ReqSetCustomStateString();

  @override
  String toString() {
    return 'ReqSetCustomStateString[statusString=$statusString, ]';
  }

  ReqSetCustomStateString.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    statusString = json['status_string'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (statusString != null)
      json['status_string'] = statusString;
    return json;
  }

  static List<ReqSetCustomStateString> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetCustomStateString>() : json.map((value) => ReqSetCustomStateString.fromJson(value)).toList();
  }

  static Map<String, ReqSetCustomStateString> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetCustomStateString>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetCustomStateString.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetCustomStateString-objects as value to a dart map
  static Map<String, List<ReqSetCustomStateString>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetCustomStateString>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetCustomStateString.listFromJson(value);
       });
     }
     return map;
  }
}

