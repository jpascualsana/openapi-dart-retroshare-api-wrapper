part of openapi.api;

class RsMsgMetaData {
  
  String mGroupId = null;
  
  String mMsgId = null;
  
  String mThreadId = null;
  
  String mParentId = null;
  
  String mOrigMsgId = null;
  
  String mAuthorId = null;
  
  String mMsgName = null;
  
  RstimeT mPublishTs = null;
  
  int mMsgFlags = null;
  
  int mMsgStatus = null;
  
  RstimeT mChildTs = null;
  
  String mServiceString = null;
  RsMsgMetaData();

  @override
  String toString() {
    return 'RsMsgMetaData[mGroupId=$mGroupId, mMsgId=$mMsgId, mThreadId=$mThreadId, mParentId=$mParentId, mOrigMsgId=$mOrigMsgId, mAuthorId=$mAuthorId, mMsgName=$mMsgName, mPublishTs=$mPublishTs, mMsgFlags=$mMsgFlags, mMsgStatus=$mMsgStatus, mChildTs=$mChildTs, mServiceString=$mServiceString, ]';
  }

  RsMsgMetaData.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mGroupId = json['mGroupId'];
    mMsgId = json['mMsgId'];
    mThreadId = json['mThreadId'];
    mParentId = json['mParentId'];
    mOrigMsgId = json['mOrigMsgId'];
    mAuthorId = json['mAuthorId'];
    mMsgName = json['mMsgName'];
    mPublishTs = (json['mPublishTs'] == null) ?
      null :
      RstimeT.fromJson(json['mPublishTs']);
    mMsgFlags = json['mMsgFlags'];
    mMsgStatus = json['mMsgStatus'];
    mChildTs = (json['mChildTs'] == null) ?
      null :
      RstimeT.fromJson(json['mChildTs']);
    mServiceString = json['mServiceString'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mGroupId != null)
      json['mGroupId'] = mGroupId;
    if (mMsgId != null)
      json['mMsgId'] = mMsgId;
    if (mThreadId != null)
      json['mThreadId'] = mThreadId;
    if (mParentId != null)
      json['mParentId'] = mParentId;
    if (mOrigMsgId != null)
      json['mOrigMsgId'] = mOrigMsgId;
    if (mAuthorId != null)
      json['mAuthorId'] = mAuthorId;
    if (mMsgName != null)
      json['mMsgName'] = mMsgName;
    if (mPublishTs != null)
      json['mPublishTs'] = mPublishTs;
    if (mMsgFlags != null)
      json['mMsgFlags'] = mMsgFlags;
    if (mMsgStatus != null)
      json['mMsgStatus'] = mMsgStatus;
    if (mChildTs != null)
      json['mChildTs'] = mChildTs;
    if (mServiceString != null)
      json['mServiceString'] = mServiceString;
    return json;
  }

  static List<RsMsgMetaData> listFromJson(List<dynamic> json) {
    return json == null ? List<RsMsgMetaData>() : json.map((value) => RsMsgMetaData.fromJson(value)).toList();
  }

  static Map<String, RsMsgMetaData> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsMsgMetaData>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsMsgMetaData.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsMsgMetaData-objects as value to a dart map
  static Map<String, List<RsMsgMetaData>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsMsgMetaData>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsMsgMetaData.listFromJson(value);
       });
     }
     return map;
  }
}

