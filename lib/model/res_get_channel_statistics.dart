part of openapi.api;

class ResGetChannelStatistics {
  
  bool retval = null;
  
  GxsGroupStatistic stat = null;
  ResGetChannelStatistics();

  @override
  String toString() {
    return 'ResGetChannelStatistics[retval=$retval, stat=$stat, ]';
  }

  ResGetChannelStatistics.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    stat = (json['stat'] == null) ?
      null :
      GxsGroupStatistic.fromJson(json['stat']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (stat != null)
      json['stat'] = stat;
    return json;
  }

  static List<ResGetChannelStatistics> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetChannelStatistics>() : json.map((value) => ResGetChannelStatistics.fromJson(value)).toList();
  }

  static Map<String, ResGetChannelStatistics> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetChannelStatistics>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetChannelStatistics.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetChannelStatistics-objects as value to a dart map
  static Map<String, List<ResGetChannelStatistics>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetChannelStatistics>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetChannelStatistics.listFromJson(value);
       });
     }
     return map;
  }
}

