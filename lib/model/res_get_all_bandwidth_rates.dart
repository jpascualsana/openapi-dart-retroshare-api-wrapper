part of openapi.api;

class ResGetAllBandwidthRates {
  
  int retval = null;
  
  List<ResGetAllBandwidthRatesRatemap> ratemap = [];
  ResGetAllBandwidthRates();

  @override
  String toString() {
    return 'ResGetAllBandwidthRates[retval=$retval, ratemap=$ratemap, ]';
  }

  ResGetAllBandwidthRates.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    ratemap = (json['ratemap'] == null) ?
      null :
      ResGetAllBandwidthRatesRatemap.listFromJson(json['ratemap']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (ratemap != null)
      json['ratemap'] = ratemap;
    return json;
  }

  static List<ResGetAllBandwidthRates> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetAllBandwidthRates>() : json.map((value) => ResGetAllBandwidthRates.fromJson(value)).toList();
  }

  static Map<String, ResGetAllBandwidthRates> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetAllBandwidthRates>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetAllBandwidthRates.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetAllBandwidthRates-objects as value to a dart map
  static Map<String, List<ResGetAllBandwidthRates>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetAllBandwidthRates>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetAllBandwidthRates.listFromJson(value);
       });
     }
     return map;
  }
}

