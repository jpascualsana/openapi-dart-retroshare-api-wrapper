part of openapi.api;

class ResEnableMulticastListening {
  
  bool retval = null;
  ResEnableMulticastListening();

  @override
  String toString() {
    return 'ResEnableMulticastListening[retval=$retval, ]';
  }

  ResEnableMulticastListening.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResEnableMulticastListening> listFromJson(List<dynamic> json) {
    return json == null ? List<ResEnableMulticastListening>() : json.map((value) => ResEnableMulticastListening.fromJson(value)).toList();
  }

  static Map<String, ResEnableMulticastListening> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResEnableMulticastListening>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResEnableMulticastListening.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResEnableMulticastListening-objects as value to a dart map
  static Map<String, List<ResEnableMulticastListening>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResEnableMulticastListening>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResEnableMulticastListening.listFromJson(value);
       });
     }
     return map;
  }
}

