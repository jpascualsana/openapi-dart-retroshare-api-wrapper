part of openapi.api;

class ReqIsSslOnlyFriend {
  
  String sslId = null;
  ReqIsSslOnlyFriend();

  @override
  String toString() {
    return 'ReqIsSslOnlyFriend[sslId=$sslId, ]';
  }

  ReqIsSslOnlyFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    return json;
  }

  static List<ReqIsSslOnlyFriend> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqIsSslOnlyFriend>() : json.map((value) => ReqIsSslOnlyFriend.fromJson(value)).toList();
  }

  static Map<String, ReqIsSslOnlyFriend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqIsSslOnlyFriend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqIsSslOnlyFriend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqIsSslOnlyFriend-objects as value to a dart map
  static Map<String, List<ReqIsSslOnlyFriend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqIsSslOnlyFriend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqIsSslOnlyFriend.listFromJson(value);
       });
     }
     return map;
  }
}

