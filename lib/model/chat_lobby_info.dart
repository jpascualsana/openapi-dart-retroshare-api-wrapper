part of openapi.api;

class ChatLobbyInfo {
  
  ChatLobbyId lobbyId = null;
  
  String lobbyName = null;
  
  String lobbyTopic = null;
  
  List<String> participatingFriends = [];
  
  String gxsId = null;
  
  int lobbyFlags = null;
  
  List<ChatLobbyInfoGxsIds> gxsIds = [];
  
  RstimeT lastActivity = null;
  ChatLobbyInfo();

  @override
  String toString() {
    return 'ChatLobbyInfo[lobbyId=$lobbyId, lobbyName=$lobbyName, lobbyTopic=$lobbyTopic, participatingFriends=$participatingFriends, gxsId=$gxsId, lobbyFlags=$lobbyFlags, gxsIds=$gxsIds, lastActivity=$lastActivity, ]';
  }

  ChatLobbyInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    lobbyId = (json['lobby_id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['lobby_id']);
    lobbyName = json['lobby_name'];
    lobbyTopic = json['lobby_topic'];
    participatingFriends = (json['participating_friends'] == null) ?
      null :
      (json['participating_friends'] as List).cast<String>();
    gxsId = json['gxs_id'];
    lobbyFlags = json['lobby_flags'];
    gxsIds = (json['gxs_ids'] == null) ?
      null :
      ChatLobbyInfoGxsIds.listFromJson(json['gxs_ids']);
    lastActivity = (json['last_activity'] == null) ?
      null :
      RstimeT.fromJson(json['last_activity']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (lobbyId != null)
      json['lobby_id'] = lobbyId;
    if (lobbyName != null)
      json['lobby_name'] = lobbyName;
    if (lobbyTopic != null)
      json['lobby_topic'] = lobbyTopic;
    if (participatingFriends != null)
      json['participating_friends'] = participatingFriends;
    if (gxsId != null)
      json['gxs_id'] = gxsId;
    if (lobbyFlags != null)
      json['lobby_flags'] = lobbyFlags;
    if (gxsIds != null)
      json['gxs_ids'] = gxsIds;
    if (lastActivity != null)
      json['last_activity'] = lastActivity;
    return json;
  }

  static List<ChatLobbyInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ChatLobbyInfo>() : json.map((value) => ChatLobbyInfo.fromJson(value)).toList();
  }

  static Map<String, ChatLobbyInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ChatLobbyInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ChatLobbyInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ChatLobbyInfo-objects as value to a dart map
  static Map<String, List<ChatLobbyInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ChatLobbyInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ChatLobbyInfo.listFromJson(value);
       });
     }
     return map;
  }
}

