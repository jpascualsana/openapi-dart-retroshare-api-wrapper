part of openapi.api;

class ResUpdateServicePermissions {
  
  bool retval = null;
  ResUpdateServicePermissions();

  @override
  String toString() {
    return 'ResUpdateServicePermissions[retval=$retval, ]';
  }

  ResUpdateServicePermissions.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResUpdateServicePermissions> listFromJson(List<dynamic> json) {
    return json == null ? List<ResUpdateServicePermissions>() : json.map((value) => ResUpdateServicePermissions.fromJson(value)).toList();
  }

  static Map<String, ResUpdateServicePermissions> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResUpdateServicePermissions>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResUpdateServicePermissions.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResUpdateServicePermissions-objects as value to a dart map
  static Map<String, List<ResUpdateServicePermissions>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResUpdateServicePermissions>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResUpdateServicePermissions.listFromJson(value);
       });
     }
     return map;
  }
}

