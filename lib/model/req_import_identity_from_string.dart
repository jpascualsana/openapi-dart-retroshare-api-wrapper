part of openapi.api;

class ReqImportIdentityFromString {
  
  String data = null;
  ReqImportIdentityFromString();

  @override
  String toString() {
    return 'ReqImportIdentityFromString[data=$data, ]';
  }

  ReqImportIdentityFromString.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (data != null)
      json['data'] = data;
    return json;
  }

  static List<ReqImportIdentityFromString> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqImportIdentityFromString>() : json.map((value) => ReqImportIdentityFromString.fromJson(value)).toList();
  }

  static Map<String, ReqImportIdentityFromString> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqImportIdentityFromString>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqImportIdentityFromString.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqImportIdentityFromString-objects as value to a dart map
  static Map<String, List<ReqImportIdentityFromString>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqImportIdentityFromString>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqImportIdentityFromString.listFromJson(value);
       });
     }
     return map;
  }
}

