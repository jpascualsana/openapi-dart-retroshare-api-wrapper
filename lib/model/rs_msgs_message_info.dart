part of openapi.api;

class RsMsgsMessageInfo {
  
  String msgId = null;
  
  String rspeeridSrcId = null;
  
  String rsgxsidSrcId = null;
  
  int msgflags = null;
  
  List<String> rspeeridMsgto = [];
  
  List<String> rspeeridMsgcc = [];
  
  List<String> rspeeridMsgbcc = [];
  
  List<String> rsgxsidMsgto = [];
  
  List<String> rsgxsidMsgcc = [];
  
  List<String> rsgxsidMsgbcc = [];
  
  String title = null;
  
  String msg = null;
  
  String attachTitle = null;
  
  String attachComment = null;
  
  List<FileInfo> files = [];
  
  int size = null;
  
  int count = null;
  
  int ts = null;
  RsMsgsMessageInfo();

  @override
  String toString() {
    return 'RsMsgsMessageInfo[msgId=$msgId, rspeeridSrcId=$rspeeridSrcId, rsgxsidSrcId=$rsgxsidSrcId, msgflags=$msgflags, rspeeridMsgto=$rspeeridMsgto, rspeeridMsgcc=$rspeeridMsgcc, rspeeridMsgbcc=$rspeeridMsgbcc, rsgxsidMsgto=$rsgxsidMsgto, rsgxsidMsgcc=$rsgxsidMsgcc, rsgxsidMsgbcc=$rsgxsidMsgbcc, title=$title, msg=$msg, attachTitle=$attachTitle, attachComment=$attachComment, files=$files, size=$size, count=$count, ts=$ts, ]';
  }

  RsMsgsMessageInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
    rspeeridSrcId = json['rspeerid_srcId'];
    rsgxsidSrcId = json['rsgxsid_srcId'];
    msgflags = json['msgflags'];
    rspeeridMsgto = (json['rspeerid_msgto'] == null) ?
      null :
      (json['rspeerid_msgto'] as List).cast<String>();
    rspeeridMsgcc = (json['rspeerid_msgcc'] == null) ?
      null :
      (json['rspeerid_msgcc'] as List).cast<String>();
    rspeeridMsgbcc = (json['rspeerid_msgbcc'] == null) ?
      null :
      (json['rspeerid_msgbcc'] as List).cast<String>();
    rsgxsidMsgto = (json['rsgxsid_msgto'] == null) ?
      null :
      (json['rsgxsid_msgto'] as List).cast<String>();
    rsgxsidMsgcc = (json['rsgxsid_msgcc'] == null) ?
      null :
      (json['rsgxsid_msgcc'] as List).cast<String>();
    rsgxsidMsgbcc = (json['rsgxsid_msgbcc'] == null) ?
      null :
      (json['rsgxsid_msgbcc'] as List).cast<String>();
    title = json['title'];
    msg = json['msg'];
    attachTitle = json['attach_title'];
    attachComment = json['attach_comment'];
    files = (json['files'] == null) ?
      null :
      FileInfo.listFromJson(json['files']);
    size = json['size'];
    count = json['count'];
    ts = json['ts'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    if (rspeeridSrcId != null)
      json['rspeerid_srcId'] = rspeeridSrcId;
    if (rsgxsidSrcId != null)
      json['rsgxsid_srcId'] = rsgxsidSrcId;
    if (msgflags != null)
      json['msgflags'] = msgflags;
    if (rspeeridMsgto != null)
      json['rspeerid_msgto'] = rspeeridMsgto;
    if (rspeeridMsgcc != null)
      json['rspeerid_msgcc'] = rspeeridMsgcc;
    if (rspeeridMsgbcc != null)
      json['rspeerid_msgbcc'] = rspeeridMsgbcc;
    if (rsgxsidMsgto != null)
      json['rsgxsid_msgto'] = rsgxsidMsgto;
    if (rsgxsidMsgcc != null)
      json['rsgxsid_msgcc'] = rsgxsidMsgcc;
    if (rsgxsidMsgbcc != null)
      json['rsgxsid_msgbcc'] = rsgxsidMsgbcc;
    if (title != null)
      json['title'] = title;
    if (msg != null)
      json['msg'] = msg;
    if (attachTitle != null)
      json['attach_title'] = attachTitle;
    if (attachComment != null)
      json['attach_comment'] = attachComment;
    if (files != null)
      json['files'] = files;
    if (size != null)
      json['size'] = size;
    if (count != null)
      json['count'] = count;
    if (ts != null)
      json['ts'] = ts;
    return json;
  }

  static List<RsMsgsMessageInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<RsMsgsMessageInfo>() : json.map((value) => RsMsgsMessageInfo.fromJson(value)).toList();
  }

  static Map<String, RsMsgsMessageInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsMsgsMessageInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsMsgsMessageInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsMsgsMessageInfo-objects as value to a dart map
  static Map<String, List<RsMsgsMessageInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsMsgsMessageInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsMsgsMessageInfo.listFromJson(value);
       });
     }
     return map;
  }
}

