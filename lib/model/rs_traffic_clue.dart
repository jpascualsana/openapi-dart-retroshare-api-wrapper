part of openapi.api;

class RSTrafficClue {
  
  RstimeT TS = null;
  
  int size = null;
  
  int priority = null;
  
  int serviceId = null;
  
  int serviceSubId = null;
  
  String peerId = null;
  
  int count = null;
  RSTrafficClue();

  @override
  String toString() {
    return 'RSTrafficClue[TS=$TS, size=$size, priority=$priority, serviceId=$serviceId, serviceSubId=$serviceSubId, peerId=$peerId, count=$count, ]';
  }

  RSTrafficClue.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    TS = (json['TS'] == null) ?
      null :
      RstimeT.fromJson(json['TS']);
    size = json['size'];
    priority = json['priority'];
    serviceId = json['service_id'];
    serviceSubId = json['service_sub_id'];
    peerId = json['peer_id'];
    count = json['count'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (TS != null)
      json['TS'] = TS;
    if (size != null)
      json['size'] = size;
    if (priority != null)
      json['priority'] = priority;
    if (serviceId != null)
      json['service_id'] = serviceId;
    if (serviceSubId != null)
      json['service_sub_id'] = serviceSubId;
    if (peerId != null)
      json['peer_id'] = peerId;
    if (count != null)
      json['count'] = count;
    return json;
  }

  static List<RSTrafficClue> listFromJson(List<dynamic> json) {
    return json == null ? List<RSTrafficClue>() : json.map((value) => RSTrafficClue.fromJson(value)).toList();
  }

  static Map<String, RSTrafficClue> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RSTrafficClue>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RSTrafficClue.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RSTrafficClue-objects as value to a dart map
  static Map<String, List<RSTrafficClue>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RSTrafficClue>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RSTrafficClue.listFromJson(value);
       });
     }
     return map;
  }
}

