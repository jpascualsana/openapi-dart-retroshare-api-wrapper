part of openapi.api;

class ReqAcceptLobbyInvite {
  
  ChatLobbyId id = null;
  
  String identity = null;
  ReqAcceptLobbyInvite();

  @override
  String toString() {
    return 'ReqAcceptLobbyInvite[id=$id, identity=$identity, ]';
  }

  ReqAcceptLobbyInvite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = (json['id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['id']);
    identity = json['identity'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (identity != null)
      json['identity'] = identity;
    return json;
  }

  static List<ReqAcceptLobbyInvite> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqAcceptLobbyInvite>() : json.map((value) => ReqAcceptLobbyInvite.fromJson(value)).toList();
  }

  static Map<String, ReqAcceptLobbyInvite> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqAcceptLobbyInvite>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqAcceptLobbyInvite.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqAcceptLobbyInvite-objects as value to a dart map
  static Map<String, List<ReqAcceptLobbyInvite>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqAcceptLobbyInvite>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqAcceptLobbyInvite.listFromJson(value);
       });
     }
     return map;
  }
}

