part of openapi.api;

class ReqGetGroupInfoByName {
  
  String groupName = null;
  ReqGetGroupInfoByName();

  @override
  String toString() {
    return 'ReqGetGroupInfoByName[groupName=$groupName, ]';
  }

  ReqGetGroupInfoByName.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    groupName = json['groupName'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (groupName != null)
      json['groupName'] = groupName;
    return json;
  }

  static List<ReqGetGroupInfoByName> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetGroupInfoByName>() : json.map((value) => ReqGetGroupInfoByName.fromJson(value)).toList();
  }

  static Map<String, ReqGetGroupInfoByName> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetGroupInfoByName>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetGroupInfoByName.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetGroupInfoByName-objects as value to a dart map
  static Map<String, List<ReqGetGroupInfoByName>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetGroupInfoByName>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetGroupInfoByName.listFromJson(value);
       });
     }
     return map;
  }
}

