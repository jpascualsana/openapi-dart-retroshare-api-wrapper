part of openapi.api;

class ReqEditForum {
  
  RsGxsForumGroup forum = null;
  ReqEditForum();

  @override
  String toString() {
    return 'ReqEditForum[forum=$forum, ]';
  }

  ReqEditForum.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    forum = (json['forum'] == null) ?
      null :
      RsGxsForumGroup.fromJson(json['forum']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (forum != null)
      json['forum'] = forum;
    return json;
  }

  static List<ReqEditForum> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqEditForum>() : json.map((value) => ReqEditForum.fromJson(value)).toList();
  }

  static Map<String, ReqEditForum> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqEditForum>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqEditForum.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqEditForum-objects as value to a dart map
  static Map<String, List<ReqEditForum>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqEditForum>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqEditForum.listFromJson(value);
       });
     }
     return map;
  }
}

