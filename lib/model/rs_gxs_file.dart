part of openapi.api;

class RsGxsFile {
  
  String mName = null;
  
  String mHash = null;
  
  ReqBanFileFileSize mSize = null;
  RsGxsFile();

  @override
  String toString() {
    return 'RsGxsFile[mName=$mName, mHash=$mHash, mSize=$mSize, ]';
  }

  RsGxsFile.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mName = json['mName'];
    mHash = json['mHash'];
    mSize = (json['mSize'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['mSize']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mName != null)
      json['mName'] = mName;
    if (mHash != null)
      json['mHash'] = mHash;
    if (mSize != null)
      json['mSize'] = mSize;
    return json;
  }

  static List<RsGxsFile> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsFile>() : json.map((value) => RsGxsFile.fromJson(value)).toList();
  }

  static Map<String, RsGxsFile> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsFile>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsFile.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsFile-objects as value to a dart map
  static Map<String, List<RsGxsFile>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsFile>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsFile.listFromJson(value);
       });
     }
     return map;
  }
}

