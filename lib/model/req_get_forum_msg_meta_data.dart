part of openapi.api;

class ReqGetForumMsgMetaData {
  
  String forumId = null;
  ReqGetForumMsgMetaData();

  @override
  String toString() {
    return 'ReqGetForumMsgMetaData[forumId=$forumId, ]';
  }

  ReqGetForumMsgMetaData.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    forumId = json['forumId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (forumId != null)
      json['forumId'] = forumId;
    return json;
  }

  static List<ReqGetForumMsgMetaData> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetForumMsgMetaData>() : json.map((value) => ReqGetForumMsgMetaData.fromJson(value)).toList();
  }

  static Map<String, ReqGetForumMsgMetaData> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetForumMsgMetaData>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetForumMsgMetaData.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetForumMsgMetaData-objects as value to a dart map
  static Map<String, List<ReqGetForumMsgMetaData>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetForumMsgMetaData>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetForumMsgMetaData.listFromJson(value);
       });
     }
     return map;
  }
}

