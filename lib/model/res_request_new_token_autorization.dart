part of openapi.api;

class ResRequestNewTokenAutorization {
  
  ResExportCollectionLinkRetval retval = null;
  ResRequestNewTokenAutorization();

  @override
  String toString() {
    return 'ResRequestNewTokenAutorization[retval=$retval, ]';
  }

  ResRequestNewTokenAutorization.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      ResExportCollectionLinkRetval.fromJson(json['retval']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRequestNewTokenAutorization> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRequestNewTokenAutorization>() : json.map((value) => ResRequestNewTokenAutorization.fromJson(value)).toList();
  }

  static Map<String, ResRequestNewTokenAutorization> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRequestNewTokenAutorization>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRequestNewTokenAutorization.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRequestNewTokenAutorization-objects as value to a dart map
  static Map<String, List<ResRequestNewTokenAutorization>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRequestNewTokenAutorization>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRequestNewTokenAutorization.listFromJson(value);
       });
     }
     return map;
  }
}

