part of openapi.api;

class ResRememberBannedIdThreshold {
  
  int retval = null;
  ResRememberBannedIdThreshold();

  @override
  String toString() {
    return 'ResRememberBannedIdThreshold[retval=$retval, ]';
  }

  ResRememberBannedIdThreshold.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRememberBannedIdThreshold> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRememberBannedIdThreshold>() : json.map((value) => ResRememberBannedIdThreshold.fromJson(value)).toList();
  }

  static Map<String, ResRememberBannedIdThreshold> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRememberBannedIdThreshold>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRememberBannedIdThreshold.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRememberBannedIdThreshold-objects as value to a dart map
  static Map<String, List<ResRememberBannedIdThreshold>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRememberBannedIdThreshold>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRememberBannedIdThreshold.listFromJson(value);
       });
     }
     return map;
  }
}

