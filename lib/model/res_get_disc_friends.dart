part of openapi.api;

class ResGetDiscFriends {
  
  bool retval = null;
  
  List<String> friends = [];
  ResGetDiscFriends();

  @override
  String toString() {
    return 'ResGetDiscFriends[retval=$retval, friends=$friends, ]';
  }

  ResGetDiscFriends.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    friends = (json['friends'] == null) ?
      null :
      (json['friends'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (friends != null)
      json['friends'] = friends;
    return json;
  }

  static List<ResGetDiscFriends> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetDiscFriends>() : json.map((value) => ResGetDiscFriends.fromJson(value)).toList();
  }

  static Map<String, ResGetDiscFriends> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetDiscFriends>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetDiscFriends.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetDiscFriends-objects as value to a dart map
  static Map<String, List<ResGetDiscFriends>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetDiscFriends>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetDiscFriends.listFromJson(value);
       });
     }
     return map;
  }
}

