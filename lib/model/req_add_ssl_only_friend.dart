part of openapi.api;

class ReqAddSslOnlyFriend {
  
  String sslId = null;
  
  String pgpId = null;
  
  RsPeerDetails details = null;
  ReqAddSslOnlyFriend();

  @override
  String toString() {
    return 'ReqAddSslOnlyFriend[sslId=$sslId, pgpId=$pgpId, details=$details, ]';
  }

  ReqAddSslOnlyFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
    pgpId = json['pgpId'];
    details = (json['details'] == null) ?
      null :
      RsPeerDetails.fromJson(json['details']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    if (pgpId != null)
      json['pgpId'] = pgpId;
    if (details != null)
      json['details'] = details;
    return json;
  }

  static List<ReqAddSslOnlyFriend> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqAddSslOnlyFriend>() : json.map((value) => ReqAddSslOnlyFriend.fromJson(value)).toList();
  }

  static Map<String, ReqAddSslOnlyFriend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqAddSslOnlyFriend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqAddSslOnlyFriend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqAddSslOnlyFriend-objects as value to a dart map
  static Map<String, List<ReqAddSslOnlyFriend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqAddSslOnlyFriend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqAddSslOnlyFriend.listFromJson(value);
       });
     }
     return map;
  }
}

