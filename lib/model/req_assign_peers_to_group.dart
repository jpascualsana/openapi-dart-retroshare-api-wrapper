part of openapi.api;

class ReqAssignPeersToGroup {
  
  String groupId = null;
  
  List<String> peerIds = [];
  
  bool assign = null;
  ReqAssignPeersToGroup();

  @override
  String toString() {
    return 'ReqAssignPeersToGroup[groupId=$groupId, peerIds=$peerIds, assign=$assign, ]';
  }

  ReqAssignPeersToGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    groupId = json['groupId'];
    peerIds = (json['peerIds'] == null) ?
      null :
      (json['peerIds'] as List).cast<String>();
    assign = json['assign'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (groupId != null)
      json['groupId'] = groupId;
    if (peerIds != null)
      json['peerIds'] = peerIds;
    if (assign != null)
      json['assign'] = assign;
    return json;
  }

  static List<ReqAssignPeersToGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqAssignPeersToGroup>() : json.map((value) => ReqAssignPeersToGroup.fromJson(value)).toList();
  }

  static Map<String, ReqAssignPeersToGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqAssignPeersToGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqAssignPeersToGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqAssignPeersToGroup-objects as value to a dart map
  static Map<String, List<ReqAssignPeersToGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqAssignPeersToGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqAssignPeersToGroup.listFromJson(value);
       });
     }
     return map;
  }
}

