part of openapi.api;

class ResGetPeersCount {
  
  bool retval = null;
  
  int peersCount = null;
  
  int onlinePeersCount = null;
  ResGetPeersCount();

  @override
  String toString() {
    return 'ResGetPeersCount[retval=$retval, peersCount=$peersCount, onlinePeersCount=$onlinePeersCount, ]';
  }

  ResGetPeersCount.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    peersCount = json['peersCount'];
    onlinePeersCount = json['onlinePeersCount'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (peersCount != null)
      json['peersCount'] = peersCount;
    if (onlinePeersCount != null)
      json['onlinePeersCount'] = onlinePeersCount;
    return json;
  }

  static List<ResGetPeersCount> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetPeersCount>() : json.map((value) => ResGetPeersCount.fromJson(value)).toList();
  }

  static Map<String, ResGetPeersCount> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetPeersCount>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetPeersCount.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetPeersCount-objects as value to a dart map
  static Map<String, List<ResGetPeersCount>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetPeersCount>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetPeersCount.listFromJson(value);
       });
     }
     return map;
  }
}

