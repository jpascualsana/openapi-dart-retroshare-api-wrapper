part of openapi.api;

class ResGetWaitingDiscCount {
  
  bool retval = null;
  
  int sendCount = null;
  
  int recvCount = null;
  ResGetWaitingDiscCount();

  @override
  String toString() {
    return 'ResGetWaitingDiscCount[retval=$retval, sendCount=$sendCount, recvCount=$recvCount, ]';
  }

  ResGetWaitingDiscCount.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    sendCount = json['sendCount'];
    recvCount = json['recvCount'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (sendCount != null)
      json['sendCount'] = sendCount;
    if (recvCount != null)
      json['recvCount'] = recvCount;
    return json;
  }

  static List<ResGetWaitingDiscCount> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetWaitingDiscCount>() : json.map((value) => ResGetWaitingDiscCount.fromJson(value)).toList();
  }

  static Map<String, ResGetWaitingDiscCount> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetWaitingDiscCount>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetWaitingDiscCount.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetWaitingDiscCount-objects as value to a dart map
  static Map<String, List<ResGetWaitingDiscCount>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetWaitingDiscCount>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetWaitingDiscCount.listFromJson(value);
       });
     }
     return map;
  }
}

