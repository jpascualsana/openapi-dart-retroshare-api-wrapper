part of openapi.api;

class ReqSetVisState {
  
  String sslId = null;
  
  int vsDisc = null;
  
  int vsDht = null;
  ReqSetVisState();

  @override
  String toString() {
    return 'ReqSetVisState[sslId=$sslId, vsDisc=$vsDisc, vsDht=$vsDht, ]';
  }

  ReqSetVisState.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
    vsDisc = json['vsDisc'];
    vsDht = json['vsDht'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    if (vsDisc != null)
      json['vsDisc'] = vsDisc;
    if (vsDht != null)
      json['vsDht'] = vsDht;
    return json;
  }

  static List<ReqSetVisState> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetVisState>() : json.map((value) => ReqSetVisState.fromJson(value)).toList();
  }

  static Map<String, ReqSetVisState> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetVisState>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetVisState.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetVisState-objects as value to a dart map
  static Map<String, List<ReqSetVisState>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetVisState>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetVisState.listFromJson(value);
       });
     }
     return map;
  }
}

