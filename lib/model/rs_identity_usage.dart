part of openapi.api;

class RsIdentityUsage {
  
  RsServiceType mServiceId = null;
  //enum mServiceIdEnum {  0,  17,  18,  19,  20,  21,  22,  23,  24,  25,  32,  33,  34,  35,  36,  37,  38,  39,  40,  257,  258,  512,  529,  530,  531,  532,  533,  534,  535,  536,  537,  544,  560,  576,  789,  790,  791,  4113,  8193,  8194,  8195,  43707,  61904,  48879,  };{
  
  UsageCode mUsageCode = null;
  //enum mUsageCodeEnum {  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  };{
  
  String mGrpId = null;
  
  String mMsgId = null;
  
  ReqBanFileFileSize mAdditionalId = null;
  
  String mComment = null;
  
  String mHash = null;
  RsIdentityUsage();

  @override
  String toString() {
    return 'RsIdentityUsage[mServiceId=$mServiceId, mUsageCode=$mUsageCode, mGrpId=$mGrpId, mMsgId=$mMsgId, mAdditionalId=$mAdditionalId, mComment=$mComment, mHash=$mHash, ]';
  }

  RsIdentityUsage.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mServiceId = (json['mServiceId'] == null) ?
      null :
      RsServiceType.fromJson(json['mServiceId']);
    mUsageCode = (json['mUsageCode'] == null) ?
      null :
      UsageCode.fromJson(json['mUsageCode']);
    mGrpId = json['mGrpId'];
    mMsgId = json['mMsgId'];
    mAdditionalId = (json['mAdditionalId'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['mAdditionalId']);
    mComment = json['mComment'];
    mHash = json['mHash'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mServiceId != null)
      json['mServiceId'] = mServiceId;
    if (mUsageCode != null)
      json['mUsageCode'] = mUsageCode;
    if (mGrpId != null)
      json['mGrpId'] = mGrpId;
    if (mMsgId != null)
      json['mMsgId'] = mMsgId;
    if (mAdditionalId != null)
      json['mAdditionalId'] = mAdditionalId;
    if (mComment != null)
      json['mComment'] = mComment;
    if (mHash != null)
      json['mHash'] = mHash;
    return json;
  }

  static List<RsIdentityUsage> listFromJson(List<dynamic> json) {
    return json == null ? List<RsIdentityUsage>() : json.map((value) => RsIdentityUsage.fromJson(value)).toList();
  }

  static Map<String, RsIdentityUsage> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsIdentityUsage>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsIdentityUsage.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsIdentityUsage-objects as value to a dart map
  static Map<String, List<RsIdentityUsage>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsIdentityUsage>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsIdentityUsage.listFromJson(value);
       });
     }
     return map;
  }
}

