part of openapi.api;

class GxsServiceStatistic {
  
  int mNumMsgs = null;
  
  int mNumGrps = null;
  
  int mSizeOfMsgs = null;
  
  int mSizeOfGrps = null;
  
  int mNumGrpsSubscribed = null;
  
  int mNumThreadMsgsNew = null;
  
  int mNumThreadMsgsUnread = null;
  
  int mNumChildMsgsNew = null;
  
  int mNumChildMsgsUnread = null;
  
  int mSizeStore = null;
  GxsServiceStatistic();

  @override
  String toString() {
    return 'GxsServiceStatistic[mNumMsgs=$mNumMsgs, mNumGrps=$mNumGrps, mSizeOfMsgs=$mSizeOfMsgs, mSizeOfGrps=$mSizeOfGrps, mNumGrpsSubscribed=$mNumGrpsSubscribed, mNumThreadMsgsNew=$mNumThreadMsgsNew, mNumThreadMsgsUnread=$mNumThreadMsgsUnread, mNumChildMsgsNew=$mNumChildMsgsNew, mNumChildMsgsUnread=$mNumChildMsgsUnread, mSizeStore=$mSizeStore, ]';
  }

  GxsServiceStatistic.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mNumMsgs = json['mNumMsgs'];
    mNumGrps = json['mNumGrps'];
    mSizeOfMsgs = json['mSizeOfMsgs'];
    mSizeOfGrps = json['mSizeOfGrps'];
    mNumGrpsSubscribed = json['mNumGrpsSubscribed'];
    mNumThreadMsgsNew = json['mNumThreadMsgsNew'];
    mNumThreadMsgsUnread = json['mNumThreadMsgsUnread'];
    mNumChildMsgsNew = json['mNumChildMsgsNew'];
    mNumChildMsgsUnread = json['mNumChildMsgsUnread'];
    mSizeStore = json['mSizeStore'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mNumMsgs != null)
      json['mNumMsgs'] = mNumMsgs;
    if (mNumGrps != null)
      json['mNumGrps'] = mNumGrps;
    if (mSizeOfMsgs != null)
      json['mSizeOfMsgs'] = mSizeOfMsgs;
    if (mSizeOfGrps != null)
      json['mSizeOfGrps'] = mSizeOfGrps;
    if (mNumGrpsSubscribed != null)
      json['mNumGrpsSubscribed'] = mNumGrpsSubscribed;
    if (mNumThreadMsgsNew != null)
      json['mNumThreadMsgsNew'] = mNumThreadMsgsNew;
    if (mNumThreadMsgsUnread != null)
      json['mNumThreadMsgsUnread'] = mNumThreadMsgsUnread;
    if (mNumChildMsgsNew != null)
      json['mNumChildMsgsNew'] = mNumChildMsgsNew;
    if (mNumChildMsgsUnread != null)
      json['mNumChildMsgsUnread'] = mNumChildMsgsUnread;
    if (mSizeStore != null)
      json['mSizeStore'] = mSizeStore;
    return json;
  }

  static List<GxsServiceStatistic> listFromJson(List<dynamic> json) {
    return json == null ? List<GxsServiceStatistic>() : json.map((value) => GxsServiceStatistic.fromJson(value)).toList();
  }

  static Map<String, GxsServiceStatistic> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, GxsServiceStatistic>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = GxsServiceStatistic.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of GxsServiceStatistic-objects as value to a dart map
  static Map<String, List<GxsServiceStatistic>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<GxsServiceStatistic>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = GxsServiceStatistic.listFromJson(value);
       });
     }
     return map;
  }
}

