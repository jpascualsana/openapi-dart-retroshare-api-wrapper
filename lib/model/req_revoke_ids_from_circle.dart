part of openapi.api;

class ReqRevokeIdsFromCircle {
  
  List<String> identities = [];
  
  String circleId = null;
  ReqRevokeIdsFromCircle();

  @override
  String toString() {
    return 'ReqRevokeIdsFromCircle[identities=$identities, circleId=$circleId, ]';
  }

  ReqRevokeIdsFromCircle.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    identities = (json['identities'] == null) ?
      null :
      (json['identities'] as List).cast<String>();
    circleId = json['circleId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (identities != null)
      json['identities'] = identities;
    if (circleId != null)
      json['circleId'] = circleId;
    return json;
  }

  static List<ReqRevokeIdsFromCircle> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRevokeIdsFromCircle>() : json.map((value) => ReqRevokeIdsFromCircle.fromJson(value)).toList();
  }

  static Map<String, ReqRevokeIdsFromCircle> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRevokeIdsFromCircle>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRevokeIdsFromCircle.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRevokeIdsFromCircle-objects as value to a dart map
  static Map<String, List<ReqRevokeIdsFromCircle>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRevokeIdsFromCircle>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRevokeIdsFromCircle.listFromJson(value);
       });
     }
     return map;
  }
}

