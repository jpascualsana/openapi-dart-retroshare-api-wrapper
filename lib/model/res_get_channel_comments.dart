part of openapi.api;

class ResGetChannelComments {
  
  bool retval = null;
  
  List<RsGxsComment> comments = [];
  ResGetChannelComments();

  @override
  String toString() {
    return 'ResGetChannelComments[retval=$retval, comments=$comments, ]';
  }

  ResGetChannelComments.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    comments = (json['comments'] == null) ?
      null :
      RsGxsComment.listFromJson(json['comments']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (comments != null)
      json['comments'] = comments;
    return json;
  }

  static List<ResGetChannelComments> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetChannelComments>() : json.map((value) => ResGetChannelComments.fromJson(value)).toList();
  }

  static Map<String, ResGetChannelComments> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetChannelComments>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetChannelComments.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetChannelComments-objects as value to a dart map
  static Map<String, List<ResGetChannelComments>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetChannelComments>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetChannelComments.listFromJson(value);
       });
     }
     return map;
  }
}

