part of openapi.api;

class ReqSetIdentityForChatLobby {
  
  ChatLobbyId lobbyId = null;
  
  String nick = null;
  ReqSetIdentityForChatLobby();

  @override
  String toString() {
    return 'ReqSetIdentityForChatLobby[lobbyId=$lobbyId, nick=$nick, ]';
  }

  ReqSetIdentityForChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    lobbyId = (json['lobby_id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['lobby_id']);
    nick = json['nick'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (lobbyId != null)
      json['lobby_id'] = lobbyId;
    if (nick != null)
      json['nick'] = nick;
    return json;
  }

  static List<ReqSetIdentityForChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetIdentityForChatLobby>() : json.map((value) => ReqSetIdentityForChatLobby.fromJson(value)).toList();
  }

  static Map<String, ReqSetIdentityForChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetIdentityForChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetIdentityForChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetIdentityForChatLobby-objects as value to a dart map
  static Map<String, List<ReqSetIdentityForChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetIdentityForChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetIdentityForChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}

