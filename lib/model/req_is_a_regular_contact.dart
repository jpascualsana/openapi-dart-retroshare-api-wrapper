part of openapi.api;

class ReqIsARegularContact {
  
  String id = null;
  ReqIsARegularContact();

  @override
  String toString() {
    return 'ReqIsARegularContact[id=$id, ]';
  }

  ReqIsARegularContact.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqIsARegularContact> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqIsARegularContact>() : json.map((value) => ReqIsARegularContact.fromJson(value)).toList();
  }

  static Map<String, ReqIsARegularContact> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqIsARegularContact>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqIsARegularContact.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqIsARegularContact-objects as value to a dart map
  static Map<String, List<ReqIsARegularContact>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqIsARegularContact>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqIsARegularContact.listFromJson(value);
       });
     }
     return map;
  }
}

