part of openapi.api;

class ResGetTotalBandwidthRates {
  
  int retval = null;
  
  RsConfigDataRates rates = null;
  ResGetTotalBandwidthRates();

  @override
  String toString() {
    return 'ResGetTotalBandwidthRates[retval=$retval, rates=$rates, ]';
  }

  ResGetTotalBandwidthRates.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    rates = (json['rates'] == null) ?
      null :
      RsConfigDataRates.fromJson(json['rates']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (rates != null)
      json['rates'] = rates;
    return json;
  }

  static List<ResGetTotalBandwidthRates> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetTotalBandwidthRates>() : json.map((value) => ResGetTotalBandwidthRates.fromJson(value)).toList();
  }

  static Map<String, ResGetTotalBandwidthRates> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetTotalBandwidthRates>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetTotalBandwidthRates.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetTotalBandwidthRates-objects as value to a dart map
  static Map<String, List<ResGetTotalBandwidthRates>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetTotalBandwidthRates>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetTotalBandwidthRates.listFromJson(value);
       });
     }
     return map;
  }
}

