part of openapi.api;

class ResRemoveGroup {
  
  bool retval = null;
  ResRemoveGroup();

  @override
  String toString() {
    return 'ResRemoveGroup[retval=$retval, ]';
  }

  ResRemoveGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRemoveGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRemoveGroup>() : json.map((value) => ResRemoveGroup.fromJson(value)).toList();
  }

  static Map<String, ResRemoveGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRemoveGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRemoveGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRemoveGroup-objects as value to a dart map
  static Map<String, List<ResRemoveGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRemoveGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRemoveGroup.listFromJson(value);
       });
     }
     return map;
  }
}

