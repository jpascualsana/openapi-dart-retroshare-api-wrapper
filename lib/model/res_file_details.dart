part of openapi.api;

class ResFileDetails {
  
  bool retval = null;
  
  FileInfo info = null;
  ResFileDetails();

  @override
  String toString() {
    return 'ResFileDetails[retval=$retval, info=$info, ]';
  }

  ResFileDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    info = (json['info'] == null) ?
      null :
      FileInfo.fromJson(json['info']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (info != null)
      json['info'] = info;
    return json;
  }

  static List<ResFileDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ResFileDetails>() : json.map((value) => ResFileDetails.fromJson(value)).toList();
  }

  static Map<String, ResFileDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResFileDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResFileDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResFileDetails-objects as value to a dart map
  static Map<String, List<ResFileDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResFileDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResFileDetails.listFromJson(value);
       });
     }
     return map;
  }
}

