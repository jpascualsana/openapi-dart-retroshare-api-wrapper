part of openapi.api;

class ReqIsOnline {
  
  String sslId = null;
  ReqIsOnline();

  @override
  String toString() {
    return 'ReqIsOnline[sslId=$sslId, ]';
  }

  ReqIsOnline.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    return json;
  }

  static List<ReqIsOnline> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqIsOnline>() : json.map((value) => ReqIsOnline.fromJson(value)).toList();
  }

  static Map<String, ReqIsOnline> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqIsOnline>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqIsOnline.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqIsOnline-objects as value to a dart map
  static Map<String, List<ReqIsOnline>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqIsOnline>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqIsOnline.listFromJson(value);
       });
     }
     return map;
  }
}

