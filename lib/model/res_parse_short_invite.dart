part of openapi.api;

class ResParseShortInvite {
  
  bool retval = null;
  
  RsPeerDetails details = null;
  
  int errCode = null;
  ResParseShortInvite();

  @override
  String toString() {
    return 'ResParseShortInvite[retval=$retval, details=$details, errCode=$errCode, ]';
  }

  ResParseShortInvite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    details = (json['details'] == null) ?
      null :
      RsPeerDetails.fromJson(json['details']);
    errCode = json['err_code'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (details != null)
      json['details'] = details;
    if (errCode != null)
      json['err_code'] = errCode;
    return json;
  }

  static List<ResParseShortInvite> listFromJson(List<dynamic> json) {
    return json == null ? List<ResParseShortInvite>() : json.map((value) => ResParseShortInvite.fromJson(value)).toList();
  }

  static Map<String, ResParseShortInvite> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResParseShortInvite>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResParseShortInvite.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResParseShortInvite-objects as value to a dart map
  static Map<String, List<ResParseShortInvite>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResParseShortInvite>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResParseShortInvite.listFromJson(value);
       });
     }
     return map;
  }
}

