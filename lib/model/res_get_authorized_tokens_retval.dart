part of openapi.api;

class ResGetAuthorizedTokensRetval {
  
  String key = null;
  
  String value = null;
  ResGetAuthorizedTokensRetval();

  @override
  String toString() {
    return 'ResGetAuthorizedTokensRetval[key=$key, value=$value, ]';
  }

  ResGetAuthorizedTokensRetval.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    key = json['key'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (key != null)
      json['key'] = key;
    if (value != null)
      json['value'] = value;
    return json;
  }

  static List<ResGetAuthorizedTokensRetval> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetAuthorizedTokensRetval>() : json.map((value) => ResGetAuthorizedTokensRetval.fromJson(value)).toList();
  }

  static Map<String, ResGetAuthorizedTokensRetval> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetAuthorizedTokensRetval>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetAuthorizedTokensRetval.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetAuthorizedTokensRetval-objects as value to a dart map
  static Map<String, List<ResGetAuthorizedTokensRetval>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetAuthorizedTokensRetval>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetAuthorizedTokensRetval.listFromJson(value);
       });
     }
     return map;
  }
}

