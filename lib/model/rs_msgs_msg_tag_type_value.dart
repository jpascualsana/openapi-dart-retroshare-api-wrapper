part of openapi.api;

class RsMsgsMsgTagTypeValue {
  
  String first = null;
  
  int second = null;
  RsMsgsMsgTagTypeValue();

  @override
  String toString() {
    return 'RsMsgsMsgTagTypeValue[first=$first, second=$second, ]';
  }

  RsMsgsMsgTagTypeValue.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    first = json['first'];
    second = json['second'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (first != null)
      json['first'] = first;
    if (second != null)
      json['second'] = second;
    return json;
  }

  static List<RsMsgsMsgTagTypeValue> listFromJson(List<dynamic> json) {
    return json == null ? List<RsMsgsMsgTagTypeValue>() : json.map((value) => RsMsgsMsgTagTypeValue.fromJson(value)).toList();
  }

  static Map<String, RsMsgsMsgTagTypeValue> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsMsgsMsgTagTypeValue>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsMsgsMsgTagTypeValue.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsMsgsMsgTagTypeValue-objects as value to a dart map
  static Map<String, List<RsMsgsMsgTagTypeValue>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsMsgsMsgTagTypeValue>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsMsgsMsgTagTypeValue.listFromJson(value);
       });
     }
     return map;
  }
}

