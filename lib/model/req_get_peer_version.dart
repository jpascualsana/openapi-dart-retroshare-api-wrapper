part of openapi.api;

class ReqGetPeerVersion {
  
  String id = null;
  ReqGetPeerVersion();

  @override
  String toString() {
    return 'ReqGetPeerVersion[id=$id, ]';
  }

  ReqGetPeerVersion.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqGetPeerVersion> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetPeerVersion>() : json.map((value) => ReqGetPeerVersion.fromJson(value)).toList();
  }

  static Map<String, ReqGetPeerVersion> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetPeerVersion>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetPeerVersion.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetPeerVersion-objects as value to a dart map
  static Map<String, List<ReqGetPeerVersion>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetPeerVersion>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetPeerVersion.listFromJson(value);
       });
     }
     return map;
  }
}

