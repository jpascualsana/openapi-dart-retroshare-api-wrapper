part of openapi.api;

class ReqSubscribeToForum {
  
  String forumId = null;
  
  bool subscribe = null;
  ReqSubscribeToForum();

  @override
  String toString() {
    return 'ReqSubscribeToForum[forumId=$forumId, subscribe=$subscribe, ]';
  }

  ReqSubscribeToForum.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    forumId = json['forumId'];
    subscribe = json['subscribe'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (forumId != null)
      json['forumId'] = forumId;
    if (subscribe != null)
      json['subscribe'] = subscribe;
    return json;
  }

  static List<ReqSubscribeToForum> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSubscribeToForum>() : json.map((value) => ReqSubscribeToForum.fromJson(value)).toList();
  }

  static Map<String, ReqSubscribeToForum> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSubscribeToForum>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSubscribeToForum.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSubscribeToForum-objects as value to a dart map
  static Map<String, List<ReqSubscribeToForum>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSubscribeToForum>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSubscribeToForum.listFromJson(value);
       });
     }
     return map;
  }
}

