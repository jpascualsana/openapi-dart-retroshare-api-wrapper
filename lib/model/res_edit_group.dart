part of openapi.api;

class ResEditGroup {
  
  bool retval = null;
  ResEditGroup();

  @override
  String toString() {
    return 'ResEditGroup[retval=$retval, ]';
  }

  ResEditGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResEditGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<ResEditGroup>() : json.map((value) => ResEditGroup.fromJson(value)).toList();
  }

  static Map<String, ResEditGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResEditGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResEditGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResEditGroup-objects as value to a dart map
  static Map<String, List<ResEditGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResEditGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResEditGroup.listFromJson(value);
       });
     }
     return map;
  }
}

