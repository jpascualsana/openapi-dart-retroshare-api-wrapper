part of openapi.api;

class ReqFileCancel {
  
  String hash = null;
  ReqFileCancel();

  @override
  String toString() {
    return 'ReqFileCancel[hash=$hash, ]';
  }

  ReqFileCancel.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hash = json['hash'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hash != null)
      json['hash'] = hash;
    return json;
  }

  static List<ReqFileCancel> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqFileCancel>() : json.map((value) => ReqFileCancel.fromJson(value)).toList();
  }

  static Map<String, ReqFileCancel> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqFileCancel>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqFileCancel.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqFileCancel-objects as value to a dart map
  static Map<String, List<ReqFileCancel>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqFileCancel>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqFileCancel.listFromJson(value);
       });
     }
     return map;
  }
}

