part of openapi.api;

class ReqAcceptInvite {
  
  String invite = null;
  
  int flags = null;
  ReqAcceptInvite();

  @override
  String toString() {
    return 'ReqAcceptInvite[invite=$invite, flags=$flags, ]';
  }

  ReqAcceptInvite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    invite = json['invite'];
    flags = json['flags'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (invite != null)
      json['invite'] = invite;
    if (flags != null)
      json['flags'] = flags;
    return json;
  }

  static List<ReqAcceptInvite> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqAcceptInvite>() : json.map((value) => ReqAcceptInvite.fromJson(value)).toList();
  }

  static Map<String, ReqAcceptInvite> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqAcceptInvite>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqAcceptInvite.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqAcceptInvite-objects as value to a dart map
  static Map<String, List<ReqAcceptInvite>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqAcceptInvite>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqAcceptInvite.listFromJson(value);
       });
     }
     return map;
  }
}

