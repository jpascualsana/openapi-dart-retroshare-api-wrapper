part of openapi.api;

class ReqSetThresholdForRemotelyPositiveReputation {
  
  int thresh = null;
  ReqSetThresholdForRemotelyPositiveReputation();

  @override
  String toString() {
    return 'ReqSetThresholdForRemotelyPositiveReputation[thresh=$thresh, ]';
  }

  ReqSetThresholdForRemotelyPositiveReputation.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    thresh = json['thresh'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (thresh != null)
      json['thresh'] = thresh;
    return json;
  }

  static List<ReqSetThresholdForRemotelyPositiveReputation> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetThresholdForRemotelyPositiveReputation>() : json.map((value) => ReqSetThresholdForRemotelyPositiveReputation.fromJson(value)).toList();
  }

  static Map<String, ReqSetThresholdForRemotelyPositiveReputation> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetThresholdForRemotelyPositiveReputation>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetThresholdForRemotelyPositiveReputation.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetThresholdForRemotelyPositiveReputation-objects as value to a dart map
  static Map<String, List<ReqSetThresholdForRemotelyPositiveReputation>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetThresholdForRemotelyPositiveReputation>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetThresholdForRemotelyPositiveReputation.listFromJson(value);
       });
     }
     return map;
  }
}

