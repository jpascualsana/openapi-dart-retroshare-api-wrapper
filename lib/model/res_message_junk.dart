part of openapi.api;

class ResMessageJunk {
  
  bool retval = null;
  ResMessageJunk();

  @override
  String toString() {
    return 'ResMessageJunk[retval=$retval, ]';
  }

  ResMessageJunk.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResMessageJunk> listFromJson(List<dynamic> json) {
    return json == null ? List<ResMessageJunk>() : json.map((value) => ResMessageJunk.fromJson(value)).toList();
  }

  static Map<String, ResMessageJunk> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResMessageJunk>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResMessageJunk.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResMessageJunk-objects as value to a dart map
  static Map<String, List<ResMessageJunk>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResMessageJunk>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResMessageJunk.listFromJson(value);
       });
     }
     return map;
  }
}

