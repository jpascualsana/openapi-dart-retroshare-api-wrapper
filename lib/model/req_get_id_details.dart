part of openapi.api;

class ReqGetIdDetails {
  
  String id = null;
  ReqGetIdDetails();

  @override
  String toString() {
    return 'ReqGetIdDetails[id=$id, ]';
  }

  ReqGetIdDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqGetIdDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetIdDetails>() : json.map((value) => ReqGetIdDetails.fromJson(value)).toList();
  }

  static Map<String, ReqGetIdDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetIdDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetIdDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetIdDetails-objects as value to a dart map
  static Map<String, List<ReqGetIdDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetIdDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetIdDetails.listFromJson(value);
       });
     }
     return map;
  }
}

