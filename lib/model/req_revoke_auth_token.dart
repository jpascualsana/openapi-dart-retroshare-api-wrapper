part of openapi.api;

class ReqRevokeAuthToken {
  
  String user = null;
  ReqRevokeAuthToken();

  @override
  String toString() {
    return 'ReqRevokeAuthToken[user=$user, ]';
  }

  ReqRevokeAuthToken.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    user = json['user'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (user != null)
      json['user'] = user;
    return json;
  }

  static List<ReqRevokeAuthToken> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRevokeAuthToken>() : json.map((value) => ReqRevokeAuthToken.fromJson(value)).toList();
  }

  static Map<String, ReqRevokeAuthToken> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRevokeAuthToken>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRevokeAuthToken.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRevokeAuthToken-objects as value to a dart map
  static Map<String, List<ReqRevokeAuthToken>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRevokeAuthToken>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRevokeAuthToken.listFromJson(value);
       });
     }
     return map;
  }
}

