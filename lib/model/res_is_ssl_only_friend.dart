part of openapi.api;

class ResIsSslOnlyFriend {
  
  bool retval = null;
  ResIsSslOnlyFriend();

  @override
  String toString() {
    return 'ResIsSslOnlyFriend[retval=$retval, ]';
  }

  ResIsSslOnlyFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsSslOnlyFriend> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsSslOnlyFriend>() : json.map((value) => ResIsSslOnlyFriend.fromJson(value)).toList();
  }

  static Map<String, ResIsSslOnlyFriend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsSslOnlyFriend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsSslOnlyFriend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsSslOnlyFriend-objects as value to a dart map
  static Map<String, List<ResIsSslOnlyFriend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsSslOnlyFriend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsSslOnlyFriend.listFromJson(value);
       });
     }
     return map;
  }
}

