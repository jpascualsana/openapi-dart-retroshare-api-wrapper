part of openapi.api;

class RsFileTreeDirData {
  
  String name = null;
  
  List<ReqBanFileFileSize> subdirs = [];
  
  List<ReqBanFileFileSize> subfiles = [];
  RsFileTreeDirData();

  @override
  String toString() {
    return 'RsFileTreeDirData[name=$name, subdirs=$subdirs, subfiles=$subfiles, ]';
  }

  RsFileTreeDirData.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    name = json['name'];
    subdirs = (json['subdirs'] == null) ?
      null :
      ReqBanFileFileSize.listFromJson(json['subdirs']);
    subfiles = (json['subfiles'] == null) ?
      null :
      ReqBanFileFileSize.listFromJson(json['subfiles']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (name != null)
      json['name'] = name;
    if (subdirs != null)
      json['subdirs'] = subdirs;
    if (subfiles != null)
      json['subfiles'] = subfiles;
    return json;
  }

  static List<RsFileTreeDirData> listFromJson(List<dynamic> json) {
    return json == null ? List<RsFileTreeDirData>() : json.map((value) => RsFileTreeDirData.fromJson(value)).toList();
  }

  static Map<String, RsFileTreeDirData> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsFileTreeDirData>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsFileTreeDirData.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsFileTreeDirData-objects as value to a dart map
  static Map<String, List<RsFileTreeDirData>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsFileTreeDirData>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsFileTreeDirData.listFromJson(value);
       });
     }
     return map;
  }
}

