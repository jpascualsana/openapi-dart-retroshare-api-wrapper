part of openapi.api;

class ReqSetBindingAddress {
  
  String address = null;
  ReqSetBindingAddress();

  @override
  String toString() {
    return 'ReqSetBindingAddress[address=$address, ]';
  }

  ReqSetBindingAddress.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    address = json['address'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (address != null)
      json['address'] = address;
    return json;
  }

  static List<ReqSetBindingAddress> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetBindingAddress>() : json.map((value) => ReqSetBindingAddress.fromJson(value)).toList();
  }

  static Map<String, ReqSetBindingAddress> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetBindingAddress>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetBindingAddress.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetBindingAddress-objects as value to a dart map
  static Map<String, List<ReqSetBindingAddress>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetBindingAddress>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetBindingAddress.listFromJson(value);
       });
     }
     return map;
  }
}

