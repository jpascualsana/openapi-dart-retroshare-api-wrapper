part of openapi.api;

class ReqGetShortInvite {
  
  String sslId = null;
  
  bool formatRadix = null;
  
  bool bareBones = null;
  
  String baseUrl = null;
  ReqGetShortInvite();

  @override
  String toString() {
    return 'ReqGetShortInvite[sslId=$sslId, formatRadix=$formatRadix, bareBones=$bareBones, baseUrl=$baseUrl, ]';
  }

  ReqGetShortInvite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
    formatRadix = json['formatRadix'];
    bareBones = json['bareBones'];
    baseUrl = json['baseUrl'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    if (formatRadix != null)
      json['formatRadix'] = formatRadix;
    if (bareBones != null)
      json['bareBones'] = bareBones;
    if (baseUrl != null)
      json['baseUrl'] = baseUrl;
    return json;
  }

  static List<ReqGetShortInvite> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetShortInvite>() : json.map((value) => ReqGetShortInvite.fromJson(value)).toList();
  }

  static Map<String, ReqGetShortInvite> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetShortInvite>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetShortInvite.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetShortInvite-objects as value to a dart map
  static Map<String, List<ReqGetShortInvite>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetShortInvite>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetShortInvite.listFromJson(value);
       });
     }
     return map;
  }
}

