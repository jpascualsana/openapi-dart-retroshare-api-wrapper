part of openapi.api;

class ReqSetDestinationName {
  
  String hash = null;
  
  String newName = null;
  ReqSetDestinationName();

  @override
  String toString() {
    return 'ReqSetDestinationName[hash=$hash, newName=$newName, ]';
  }

  ReqSetDestinationName.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hash = json['hash'];
    newName = json['newName'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hash != null)
      json['hash'] = hash;
    if (newName != null)
      json['newName'] = newName;
    return json;
  }

  static List<ReqSetDestinationName> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetDestinationName>() : json.map((value) => ReqSetDestinationName.fromJson(value)).toList();
  }

  static Map<String, ReqSetDestinationName> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetDestinationName>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetDestinationName.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetDestinationName-objects as value to a dart map
  static Map<String, List<ReqSetDestinationName>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetDestinationName>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetDestinationName.listFromJson(value);
       });
     }
     return map;
  }
}

