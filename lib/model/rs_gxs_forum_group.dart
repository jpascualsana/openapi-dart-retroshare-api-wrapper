part of openapi.api;

class RsGxsForumGroup {
  
  String mDescription = null;
  
  String mAdminList = null;
  
  String mPinnedPosts = null;
  RsGxsForumGroup();

  @override
  String toString() {
    return 'RsGxsForumGroup[mDescription=$mDescription, mAdminList=$mAdminList, mPinnedPosts=$mPinnedPosts, ]';
  }

  RsGxsForumGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mDescription = json['mDescription'];
    mAdminList = json['mAdminList'];
    mPinnedPosts = json['mPinnedPosts'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mDescription != null)
      json['mDescription'] = mDescription;
    if (mAdminList != null)
      json['mAdminList'] = mAdminList;
    if (mPinnedPosts != null)
      json['mPinnedPosts'] = mPinnedPosts;
    return json;
  }

  static List<RsGxsForumGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsForumGroup>() : json.map((value) => RsGxsForumGroup.fromJson(value)).toList();
  }

  static Map<String, RsGxsForumGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsForumGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsForumGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsForumGroup-objects as value to a dart map
  static Map<String, List<RsGxsForumGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsForumGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsForumGroup.listFromJson(value);
       });
     }
     return map;
  }
}

