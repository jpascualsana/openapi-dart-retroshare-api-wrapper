part of openapi.api;

class ReqCreateChatLobby {
  
  String lobbyName = null;
  
  String lobbyIdentity = null;
  
  String lobbyTopic = null;
  
  List<String> invitedFriends = [];
  
  int lobbyPrivacyType = null;
  ReqCreateChatLobby();

  @override
  String toString() {
    return 'ReqCreateChatLobby[lobbyName=$lobbyName, lobbyIdentity=$lobbyIdentity, lobbyTopic=$lobbyTopic, invitedFriends=$invitedFriends, lobbyPrivacyType=$lobbyPrivacyType, ]';
  }

  ReqCreateChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    lobbyName = json['lobby_name'];
    lobbyIdentity = json['lobby_identity'];
    lobbyTopic = json['lobby_topic'];
    invitedFriends = (json['invited_friends'] == null) ?
      null :
      (json['invited_friends'] as List).cast<String>();
    lobbyPrivacyType = json['lobby_privacy_type'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (lobbyName != null)
      json['lobby_name'] = lobbyName;
    if (lobbyIdentity != null)
      json['lobby_identity'] = lobbyIdentity;
    if (lobbyTopic != null)
      json['lobby_topic'] = lobbyTopic;
    if (invitedFriends != null)
      json['invited_friends'] = invitedFriends;
    if (lobbyPrivacyType != null)
      json['lobby_privacy_type'] = lobbyPrivacyType;
    return json;
  }

  static List<ReqCreateChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateChatLobby>() : json.map((value) => ReqCreateChatLobby.fromJson(value)).toList();
  }

  static Map<String, ReqCreateChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateChatLobby-objects as value to a dart map
  static Map<String, List<ReqCreateChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}

