part of openapi.api;

class ReqExtraFileStatus {
  
  String localpath = null;
  ReqExtraFileStatus();

  @override
  String toString() {
    return 'ReqExtraFileStatus[localpath=$localpath, ]';
  }

  ReqExtraFileStatus.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    localpath = json['localpath'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (localpath != null)
      json['localpath'] = localpath;
    return json;
  }

  static List<ReqExtraFileStatus> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqExtraFileStatus>() : json.map((value) => ReqExtraFileStatus.fromJson(value)).toList();
  }

  static Map<String, ReqExtraFileStatus> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqExtraFileStatus>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqExtraFileStatus.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqExtraFileStatus-objects as value to a dart map
  static Map<String, List<ReqExtraFileStatus>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqExtraFileStatus>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqExtraFileStatus.listFromJson(value);
       });
     }
     return map;
  }
}

