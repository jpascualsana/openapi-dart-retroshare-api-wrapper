part of openapi.api;

class RsTokenServiceGxsRequestStatus {
  /// The underlying value of this enum member.
  final int value;

  const RsTokenServiceGxsRequestStatus._internal(this.value);

  static const RsTokenServiceGxsRequestStatus number0_ = const RsTokenServiceGxsRequestStatus._internal(0);
  static const RsTokenServiceGxsRequestStatus number1_ = const RsTokenServiceGxsRequestStatus._internal(1);
  static const RsTokenServiceGxsRequestStatus number2_ = const RsTokenServiceGxsRequestStatus._internal(2);
  static const RsTokenServiceGxsRequestStatus number3_ = const RsTokenServiceGxsRequestStatus._internal(3);
  static const RsTokenServiceGxsRequestStatus number4_ = const RsTokenServiceGxsRequestStatus._internal(4);
  static const RsTokenServiceGxsRequestStatus number5_ = const RsTokenServiceGxsRequestStatus._internal(5);
  
  int toJson (){
    return this.value;
  }

  static RsTokenServiceGxsRequestStatus fromJson(int value) {
    return new RsTokenServiceGxsRequestStatusTypeTransformer().decode(value);
  }
  
  static List<RsTokenServiceGxsRequestStatus> listFromJson(List<dynamic> json) {
    return json == null ? new List<RsTokenServiceGxsRequestStatus>() : json.map((value) => RsTokenServiceGxsRequestStatus.fromJson(value)).toList();
  }
}

class RsTokenServiceGxsRequestStatusTypeTransformer {

  dynamic encode(RsTokenServiceGxsRequestStatus data) {
    return data.value;
  }

  RsTokenServiceGxsRequestStatus decode(dynamic data) {
    switch (data) {
      case 0: return RsTokenServiceGxsRequestStatus.number0_;
      case 1: return RsTokenServiceGxsRequestStatus.number1_;
      case 2: return RsTokenServiceGxsRequestStatus.number2_;
      case 3: return RsTokenServiceGxsRequestStatus.number3_;
      case 4: return RsTokenServiceGxsRequestStatus.number4_;
      case 5: return RsTokenServiceGxsRequestStatus.number5_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}

