part of openapi.api;

class ReqGetCircleExternalIdList {
  
  List<String> circleIds = [];
  ReqGetCircleExternalIdList();

  @override
  String toString() {
    return 'ReqGetCircleExternalIdList[circleIds=$circleIds, ]';
  }

  ReqGetCircleExternalIdList.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    circleIds = (json['circleIds'] == null) ?
      null :
      (json['circleIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (circleIds != null)
      json['circleIds'] = circleIds;
    return json;
  }

  static List<ReqGetCircleExternalIdList> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetCircleExternalIdList>() : json.map((value) => ReqGetCircleExternalIdList.fromJson(value)).toList();
  }

  static Map<String, ReqGetCircleExternalIdList> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetCircleExternalIdList>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetCircleExternalIdList.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetCircleExternalIdList-objects as value to a dart map
  static Map<String, List<ReqGetCircleExternalIdList>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetCircleExternalIdList>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetCircleExternalIdList.listFromJson(value);
       });
     }
     return map;
  }
}

