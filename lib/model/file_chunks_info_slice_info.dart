part of openapi.api;

class FileChunksInfoSliceInfo {
  
  int start = null;
  
  int size = null;
  
  String peerId = null;
  FileChunksInfoSliceInfo();

  @override
  String toString() {
    return 'FileChunksInfoSliceInfo[start=$start, size=$size, peerId=$peerId, ]';
  }

  FileChunksInfoSliceInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    start = json['start'];
    size = json['size'];
    peerId = json['peer_id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (start != null)
      json['start'] = start;
    if (size != null)
      json['size'] = size;
    if (peerId != null)
      json['peer_id'] = peerId;
    return json;
  }

  static List<FileChunksInfoSliceInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<FileChunksInfoSliceInfo>() : json.map((value) => FileChunksInfoSliceInfo.fromJson(value)).toList();
  }

  static Map<String, FileChunksInfoSliceInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, FileChunksInfoSliceInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = FileChunksInfoSliceInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of FileChunksInfoSliceInfo-objects as value to a dart map
  static Map<String, List<FileChunksInfoSliceInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<FileChunksInfoSliceInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = FileChunksInfoSliceInfo.listFromJson(value);
       });
     }
     return map;
  }
}

