part of openapi.api;

class ResRequestIdentity {
  
  bool retval = null;
  ResRequestIdentity();

  @override
  String toString() {
    return 'ResRequestIdentity[retval=$retval, ]';
  }

  ResRequestIdentity.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRequestIdentity> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRequestIdentity>() : json.map((value) => ResRequestIdentity.fromJson(value)).toList();
  }

  static Map<String, ResRequestIdentity> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRequestIdentity>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRequestIdentity.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRequestIdentity-objects as value to a dart map
  static Map<String, List<ResRequestIdentity>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRequestIdentity>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRequestIdentity.listFromJson(value);
       });
     }
     return map;
  }
}

