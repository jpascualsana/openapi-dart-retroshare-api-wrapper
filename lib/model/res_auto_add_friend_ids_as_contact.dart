part of openapi.api;

class ResAutoAddFriendIdsAsContact {
  
  bool retval = null;
  ResAutoAddFriendIdsAsContact();

  @override
  String toString() {
    return 'ResAutoAddFriendIdsAsContact[retval=$retval, ]';
  }

  ResAutoAddFriendIdsAsContact.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResAutoAddFriendIdsAsContact> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAutoAddFriendIdsAsContact>() : json.map((value) => ResAutoAddFriendIdsAsContact.fromJson(value)).toList();
  }

  static Map<String, ResAutoAddFriendIdsAsContact> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAutoAddFriendIdsAsContact>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAutoAddFriendIdsAsContact.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAutoAddFriendIdsAsContact-objects as value to a dart map
  static Map<String, List<ResAutoAddFriendIdsAsContact>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAutoAddFriendIdsAsContact>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAutoAddFriendIdsAsContact.listFromJson(value);
       });
     }
     return map;
  }
}

