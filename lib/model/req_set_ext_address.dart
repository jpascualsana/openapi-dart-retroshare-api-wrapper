part of openapi.api;

class ReqSetExtAddress {
  
  String sslId = null;
  
  String addr = null;
  
  int port = null;
  ReqSetExtAddress();

  @override
  String toString() {
    return 'ReqSetExtAddress[sslId=$sslId, addr=$addr, port=$port, ]';
  }

  ReqSetExtAddress.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
    addr = json['addr'];
    port = json['port'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    if (addr != null)
      json['addr'] = addr;
    if (port != null)
      json['port'] = port;
    return json;
  }

  static List<ReqSetExtAddress> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetExtAddress>() : json.map((value) => ReqSetExtAddress.fromJson(value)).toList();
  }

  static Map<String, ReqSetExtAddress> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetExtAddress>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetExtAddress.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetExtAddress-objects as value to a dart map
  static Map<String, List<ReqSetExtAddress>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetExtAddress>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetExtAddress.listFromJson(value);
       });
     }
     return map;
  }
}

