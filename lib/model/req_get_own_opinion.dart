part of openapi.api;

class ReqGetOwnOpinion {
  
  String id = null;
  ReqGetOwnOpinion();

  @override
  String toString() {
    return 'ReqGetOwnOpinion[id=$id, ]';
  }

  ReqGetOwnOpinion.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqGetOwnOpinion> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetOwnOpinion>() : json.map((value) => ReqGetOwnOpinion.fromJson(value)).toList();
  }

  static Map<String, ReqGetOwnOpinion> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetOwnOpinion>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetOwnOpinion.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetOwnOpinion-objects as value to a dart map
  static Map<String, List<ReqGetOwnOpinion>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetOwnOpinion>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetOwnOpinion.listFromJson(value);
       });
     }
     return map;
  }
}

