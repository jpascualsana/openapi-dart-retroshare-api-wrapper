part of openapi.api;

class RsUrl {
  
  String schemeSeparator = null;
  
  String ipv6WrapOpen = null;
  
  String ipv6Separator = null;
  
  String ipv6WrapClose = null;
  
  String portSeparator = null;
  
  String pathSeparator = null;
  
  String querySeparator = null;
  
  String queryAssign = null;
  
  String queryFieldSep = null;
  
  String fragmentSeparator = null;
  RsUrl();

  @override
  String toString() {
    return 'RsUrl[schemeSeparator=$schemeSeparator, ipv6WrapOpen=$ipv6WrapOpen, ipv6Separator=$ipv6Separator, ipv6WrapClose=$ipv6WrapClose, portSeparator=$portSeparator, pathSeparator=$pathSeparator, querySeparator=$querySeparator, queryAssign=$queryAssign, queryFieldSep=$queryFieldSep, fragmentSeparator=$fragmentSeparator, ]';
  }

  RsUrl.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    schemeSeparator = json['schemeSeparator'];
    ipv6WrapOpen = json['ipv6WrapOpen'];
    ipv6Separator = json['ipv6Separator'];
    ipv6WrapClose = json['ipv6WrapClose'];
    portSeparator = json['portSeparator'];
    pathSeparator = json['pathSeparator'];
    querySeparator = json['querySeparator'];
    queryAssign = json['queryAssign'];
    queryFieldSep = json['queryFieldSep'];
    fragmentSeparator = json['fragmentSeparator'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (schemeSeparator != null)
      json['schemeSeparator'] = schemeSeparator;
    if (ipv6WrapOpen != null)
      json['ipv6WrapOpen'] = ipv6WrapOpen;
    if (ipv6Separator != null)
      json['ipv6Separator'] = ipv6Separator;
    if (ipv6WrapClose != null)
      json['ipv6WrapClose'] = ipv6WrapClose;
    if (portSeparator != null)
      json['portSeparator'] = portSeparator;
    if (pathSeparator != null)
      json['pathSeparator'] = pathSeparator;
    if (querySeparator != null)
      json['querySeparator'] = querySeparator;
    if (queryAssign != null)
      json['queryAssign'] = queryAssign;
    if (queryFieldSep != null)
      json['queryFieldSep'] = queryFieldSep;
    if (fragmentSeparator != null)
      json['fragmentSeparator'] = fragmentSeparator;
    return json;
  }

  static List<RsUrl> listFromJson(List<dynamic> json) {
    return json == null ? List<RsUrl>() : json.map((value) => RsUrl.fromJson(value)).toList();
  }

  static Map<String, RsUrl> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsUrl>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsUrl.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsUrl-objects as value to a dart map
  static Map<String, List<RsUrl>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsUrl>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsUrl.listFromJson(value);
       });
     }
     return map;
  }
}

