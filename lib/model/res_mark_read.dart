part of openapi.api;

class ResMarkRead {
  
  bool retval = null;
  ResMarkRead();

  @override
  String toString() {
    return 'ResMarkRead[retval=$retval, ]';
  }

  ResMarkRead.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResMarkRead> listFromJson(List<dynamic> json) {
    return json == null ? List<ResMarkRead>() : json.map((value) => ResMarkRead.fromJson(value)).toList();
  }

  static Map<String, ResMarkRead> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResMarkRead>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResMarkRead.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResMarkRead-objects as value to a dart map
  static Map<String, List<ResMarkRead>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResMarkRead>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResMarkRead.listFromJson(value);
       });
     }
     return map;
  }
}

