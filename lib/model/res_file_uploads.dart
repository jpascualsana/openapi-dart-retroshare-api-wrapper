part of openapi.api;

class ResFileUploads {
  
  bool retval = null;
  
  List<String> hashs = [];
  ResFileUploads();

  @override
  String toString() {
    return 'ResFileUploads[retval=$retval, hashs=$hashs, ]';
  }

  ResFileUploads.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    hashs = (json['hashs'] == null) ?
      null :
      (json['hashs'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (hashs != null)
      json['hashs'] = hashs;
    return json;
  }

  static List<ResFileUploads> listFromJson(List<dynamic> json) {
    return json == null ? List<ResFileUploads>() : json.map((value) => ResFileUploads.fromJson(value)).toList();
  }

  static Map<String, ResFileUploads> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResFileUploads>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResFileUploads.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResFileUploads-objects as value to a dart map
  static Map<String, List<ResFileUploads>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResFileUploads>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResFileUploads.listFromJson(value);
       });
     }
     return map;
  }
}

