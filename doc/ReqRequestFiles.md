# openapi.model.ReqRequestFiles

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**collection** | [**RsFileTree**](RsFileTree.md) |  | [optional] [default to null]
**destPath** | **String** |  | [optional] [default to null]
**srcIds** | **List&lt;String&gt;** |  | [optional] [default to []]
**flags** | [**FileRequestFlags**](FileRequestFlags.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


