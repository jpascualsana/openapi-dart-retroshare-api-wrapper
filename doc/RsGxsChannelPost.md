# openapi.model.RsGxsChannelPost

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mOlderVersions** | **List&lt;String&gt;** |  | [optional] [default to []]
**mMsg** | **String** |  | [optional] [default to null]
**mFiles** | [**List&lt;RsGxsFile&gt;**](RsGxsFile.md) |  | [optional] [default to []]
**mCount** | **int** |  | [optional] [default to null]
**mSize** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**mThumbnail** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


