# openapi.model.ReqCreatePostV2

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] [default to null]
**title** | **String** |  | [optional] [default to null]
**mBody** | **String** |  | [optional] [default to null]
**files** | [**List&lt;RsGxsFile&gt;**](RsGxsFile.md) |  | [optional] [default to []]
**thumbnail** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] [default to null]
**origPostId** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


