# openapi.model.ReqMarkRead

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**postId** | [**RsGxsGrpMsgIdPair**](RsGxsGrpMsgIdPair.md) |  | [optional] [default to null]
**read** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


