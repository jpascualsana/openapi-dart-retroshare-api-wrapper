# openapi.model.RsIdentityDetails

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mId** | **String** |  | [optional] [default to null]
**mNickname** | **String** |  | [optional] [default to null]
**mFlags** | **int** |  | [optional] [default to null]
**mPgpId** | **String** |  | [optional] [default to null]
**mRecognTags** | [**List&lt;RsRecognTag&gt;**](RsRecognTag.md) |  | [optional] [default to []]
**mReputation** | [**RsReputationInfo**](RsReputationInfo.md) |  | [optional] [default to null]
**mAvatar** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] [default to null]
**mPublishTS** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**mLastUsageTS** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**mUseCases** | [**List&lt;RsIdentityDetailsMUseCases&gt;**](RsIdentityDetailsMUseCases.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


