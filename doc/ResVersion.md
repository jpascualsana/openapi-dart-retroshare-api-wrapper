# openapi.model.ResVersion

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**major** | **int** |  | [optional] [default to null]
**minor** | **int** |  | [optional] [default to null]
**mini** | **int** |  | [optional] [default to null]
**extra** | **String** |  | [optional] [default to null]
**human** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


