# openapi.model.ReqCreateForumV2

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] [default to null]
**description** | **String** |  | [optional] [default to null]
**authorId** | **String** |  | [optional] [default to null]
**moderatorsIds** | **List&lt;String&gt;** |  | [optional] [default to []]
**circleType** | [**RsGxsCircleType**](RsGxsCircleType.md) |  | [optional] [default to null]
**circleId** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


