# openapi.model.RsMsgsMessageInfo

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] [default to null]
**rspeeridSrcId** | **String** |  | [optional] [default to null]
**rsgxsidSrcId** | **String** |  | [optional] [default to null]
**msgflags** | **int** |  | [optional] [default to null]
**rspeeridMsgto** | **List&lt;String&gt;** |  | [optional] [default to []]
**rspeeridMsgcc** | **List&lt;String&gt;** |  | [optional] [default to []]
**rspeeridMsgbcc** | **List&lt;String&gt;** |  | [optional] [default to []]
**rsgxsidMsgto** | **List&lt;String&gt;** |  | [optional] [default to []]
**rsgxsidMsgcc** | **List&lt;String&gt;** |  | [optional] [default to []]
**rsgxsidMsgbcc** | **List&lt;String&gt;** |  | [optional] [default to []]
**title** | **String** |  | [optional] [default to null]
**msg** | **String** |  | [optional] [default to null]
**attachTitle** | **String** |  | [optional] [default to null]
**attachComment** | **String** |  | [optional] [default to null]
**files** | [**List&lt;FileInfo&gt;**](FileInfo.md) |  | [optional] [default to []]
**size** | **int** |  | [optional] [default to null]
**count** | **int** |  | [optional] [default to null]
**ts** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


