# openapi.model.ResParseFilesLink

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | [**ResExportCollectionLinkRetval**](ResExportCollectionLinkRetval.md) |  | [optional] [default to null]
**collection** | [**RsFileTree**](RsFileTree.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


