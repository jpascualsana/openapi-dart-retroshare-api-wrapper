# openapi.model.FileInfo

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**storagePermissionFlags** | **int** |  | [optional] [default to null]
**transferInfoFlags** | **int** |  | [optional] [default to null]
**mId** | **int** |  | [optional] [default to null]
**searchId** | **int** |  | [optional] [default to null]
**path** | **String** |  | [optional] [default to null]
**fname** | **String** |  | [optional] [default to null]
**hash** | **String** |  | [optional] [default to null]
**ext** | **String** |  | [optional] [default to null]
**size** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**avail** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**rank** | **num** |  | [optional] [default to null]
**age** | **int** |  | [optional] [default to null]
**queuePosition** | **int** |  | [optional] [default to null]
**transfered** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**tfRate** | **num** |  | [optional] [default to null]
**downloadStatus** | **int** |  | [optional] [default to null]
**peers** | [**List&lt;TransferInfo&gt;**](TransferInfo.md) |  | [optional] [default to []]
**priority** | [**DwlSpeed**](DwlSpeed.md) |  | [optional] [default to null]
**lastTS** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**parentGroups** | **List&lt;String&gt;** |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


