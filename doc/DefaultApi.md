# openapi.api.DefaultApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *http://127.0.0.1:9092*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rsAccountsExportIdentity**](DefaultApi.md#rsAccountsExportIdentity) | **POST** /rsAccounts/ExportIdentity | Export full encrypted PGP identity to file
[**rsAccountsExportIdentityToString**](DefaultApi.md#rsAccountsExportIdentityToString) | **POST** /rsAccounts/exportIdentityToString | Export full encrypted PGP identity to string
[**rsAccountsGetCurrentAccountId**](DefaultApi.md#rsAccountsGetCurrentAccountId) | **POST** /rsAccounts/getCurrentAccountId | Get current account id. Beware that an account may be selected without actually logging in.
[**rsAccountsGetPGPLogins**](DefaultApi.md#rsAccountsGetPGPLogins) | **POST** /rsAccounts/GetPGPLogins | Get available PGP identities id list
[**rsAccountsImportIdentity**](DefaultApi.md#rsAccountsImportIdentity) | **POST** /rsAccounts/ImportIdentity | Import full encrypted PGP identity from file
[**rsAccountsImportIdentityFromString**](DefaultApi.md#rsAccountsImportIdentityFromString) | **POST** /rsAccounts/importIdentityFromString | Import full encrypted PGP identity from string
[**rsBanListEnableIPFiltering**](DefaultApi.md#rsBanListEnableIPFiltering) | **POST** /rsBanList/enableIPFiltering | Enable or disable IP filtering service
[**rsBanListIpFilteringEnabled**](DefaultApi.md#rsBanListIpFilteringEnabled) | **POST** /rsBanList/ipFilteringEnabled | Get ip filtering service status
[**rsBroadcastDiscoveryDisableMulticastListening**](DefaultApi.md#rsBroadcastDiscoveryDisableMulticastListening) | **POST** /rsBroadcastDiscovery/disableMulticastListening | Disable multicast listening
[**rsBroadcastDiscoveryEnableMulticastListening**](DefaultApi.md#rsBroadcastDiscoveryEnableMulticastListening) | **POST** /rsBroadcastDiscovery/enableMulticastListening | On platforms that need it enable low level multicast listening
[**rsBroadcastDiscoveryGetDiscoveredPeers**](DefaultApi.md#rsBroadcastDiscoveryGetDiscoveredPeers) | **POST** /rsBroadcastDiscovery/getDiscoveredPeers | Get potential peers that have been discovered up until now
[**rsBroadcastDiscoveryIsMulticastListeningEnabled**](DefaultApi.md#rsBroadcastDiscoveryIsMulticastListeningEnabled) | **POST** /rsBroadcastDiscovery/isMulticastListeningEnabled | Check if multicast listening is enabled
[**rsConfigGetAllBandwidthRates**](DefaultApi.md#rsConfigGetAllBandwidthRates) | **POST** /rsConfig/getAllBandwidthRates | getAllBandwidthRates get the bandwidth rates for all peers
[**rsConfigGetConfigNetStatus**](DefaultApi.md#rsConfigGetConfigNetStatus) | **POST** /rsConfig/getConfigNetStatus | getConfigNetStatus return the net status
[**rsConfigGetCurrentDataRates**](DefaultApi.md#rsConfigGetCurrentDataRates) | **POST** /rsConfig/GetCurrentDataRates | GetCurrentDataRates get current upload and download rates
[**rsConfigGetMaxDataRates**](DefaultApi.md#rsConfigGetMaxDataRates) | **POST** /rsConfig/GetMaxDataRates | GetMaxDataRates get maximum upload and download rates
[**rsConfigGetOperatingMode**](DefaultApi.md#rsConfigGetOperatingMode) | **POST** /rsConfig/getOperatingMode | getOperatingMode get current operating mode
[**rsConfigGetTotalBandwidthRates**](DefaultApi.md#rsConfigGetTotalBandwidthRates) | **POST** /rsConfig/getTotalBandwidthRates | getTotalBandwidthRates returns the current bandwidths rates
[**rsConfigGetTrafficInfo**](DefaultApi.md#rsConfigGetTrafficInfo) | **POST** /rsConfig/getTrafficInfo | getTrafficInfo returns a list of all tracked traffic clues
[**rsConfigSetMaxDataRates**](DefaultApi.md#rsConfigSetMaxDataRates) | **POST** /rsConfig/SetMaxDataRates | SetMaxDataRates set maximum upload and download rates
[**rsConfigSetOperatingMode**](DefaultApi.md#rsConfigSetOperatingMode) | **POST** /rsConfig/setOperatingMode | setOperatingMode set the current oprating mode
[**rsControlIsReady**](DefaultApi.md#rsControlIsReady) | **POST** /rsControl/isReady | Check if core is fully ready, true only after
[**rsControlRsGlobalShutDown**](DefaultApi.md#rsControlRsGlobalShutDown) | **POST** /rsControl/rsGlobalShutDown | Turn off RetroShare
[**rsFilesAddSharedDirectory**](DefaultApi.md#rsFilesAddSharedDirectory) | **POST** /rsFiles/addSharedDirectory | Add shared directory
[**rsFilesAlreadyHaveFile**](DefaultApi.md#rsFilesAlreadyHaveFile) | **POST** /rsFiles/alreadyHaveFile | Check if we already have a file
[**rsFilesBanFile**](DefaultApi.md#rsFilesBanFile) | **POST** /rsFiles/banFile | Ban unwanted file from being, searched and forwarded by this node
[**rsFilesDefaultChunkStrategy**](DefaultApi.md#rsFilesDefaultChunkStrategy) | **POST** /rsFiles/defaultChunkStrategy | Get default chunk strategy
[**rsFilesExportCollectionLink**](DefaultApi.md#rsFilesExportCollectionLink) | **POST** /rsFiles/exportCollectionLink | Export link to a collection of files
[**rsFilesExportFileLink**](DefaultApi.md#rsFilesExportFileLink) | **POST** /rsFiles/exportFileLink | Export link to a file
[**rsFilesExtraFileHash**](DefaultApi.md#rsFilesExtraFileHash) | **POST** /rsFiles/ExtraFileHash | Add file to extra shared file list
[**rsFilesExtraFileRemove**](DefaultApi.md#rsFilesExtraFileRemove) | **POST** /rsFiles/ExtraFileRemove | Remove file from extra fila shared list
[**rsFilesExtraFileStatus**](DefaultApi.md#rsFilesExtraFileStatus) | **POST** /rsFiles/ExtraFileStatus | Get extra file information
[**rsFilesFileCancel**](DefaultApi.md#rsFilesFileCancel) | **POST** /rsFiles/FileCancel | Cancel file downloading
[**rsFilesFileClearCompleted**](DefaultApi.md#rsFilesFileClearCompleted) | **POST** /rsFiles/FileClearCompleted | Clear completed downloaded files list
[**rsFilesFileControl**](DefaultApi.md#rsFilesFileControl) | **POST** /rsFiles/FileControl | Controls file transfer
[**rsFilesFileDetails**](DefaultApi.md#rsFilesFileDetails) | **POST** /rsFiles/FileDetails | Get file details
[**rsFilesFileDownloadChunksDetails**](DefaultApi.md#rsFilesFileDownloadChunksDetails) | **POST** /rsFiles/FileDownloadChunksDetails | Get chunk details about the downloaded file with given hash.
[**rsFilesFileDownloads**](DefaultApi.md#rsFilesFileDownloads) | **POST** /rsFiles/FileDownloads | Get incoming files list
[**rsFilesFileRequest**](DefaultApi.md#rsFilesFileRequest) | **POST** /rsFiles/FileRequest | Initiate downloading of a file
[**rsFilesFileUploadChunksDetails**](DefaultApi.md#rsFilesFileUploadChunksDetails) | **POST** /rsFiles/FileUploadChunksDetails | Get details about the upload with given hash
[**rsFilesFileUploads**](DefaultApi.md#rsFilesFileUploads) | **POST** /rsFiles/FileUploads | Get outgoing files list
[**rsFilesForceDirectoryCheck**](DefaultApi.md#rsFilesForceDirectoryCheck) | **POST** /rsFiles/ForceDirectoryCheck | Force shared directories check.
[**rsFilesFreeDiskSpaceLimit**](DefaultApi.md#rsFilesFreeDiskSpaceLimit) | **POST** /rsFiles/freeDiskSpaceLimit | Get free disk space limit
[**rsFilesGetDownloadDirectory**](DefaultApi.md#rsFilesGetDownloadDirectory) | **POST** /rsFiles/getDownloadDirectory | Get default complete downloads directory
[**rsFilesGetFileData**](DefaultApi.md#rsFilesGetFileData) | **POST** /rsFiles/getFileData | Provides file data for the GUI, media streaming or API clients. It may return unverified chunks. This allows streaming without having to wait for hashes or completion of the file. This function returns an unspecified amount of bytes. Either as much data as available or a sensible maximum. Expect a block size of around 1MiB. To get more data, call this function repeatedly with different offsets.
[**rsFilesGetPartialsDirectory**](DefaultApi.md#rsFilesGetPartialsDirectory) | **POST** /rsFiles/getPartialsDirectory | Get partial downloads directory
[**rsFilesGetPrimaryBannedFilesList**](DefaultApi.md#rsFilesGetPrimaryBannedFilesList) | **POST** /rsFiles/getPrimaryBannedFilesList | Get list of banned files
[**rsFilesGetSharedDirectories**](DefaultApi.md#rsFilesGetSharedDirectories) | **POST** /rsFiles/getSharedDirectories | Get list of current shared directories
[**rsFilesIsHashBanned**](DefaultApi.md#rsFilesIsHashBanned) | **POST** /rsFiles/isHashBanned | Check if a file is on banned list
[**rsFilesParseFilesLink**](DefaultApi.md#rsFilesParseFilesLink) | **POST** /rsFiles/parseFilesLink | Parse RetroShare files link
[**rsFilesRemoveSharedDirectory**](DefaultApi.md#rsFilesRemoveSharedDirectory) | **POST** /rsFiles/removeSharedDirectory | Remove directory from shared list
[**rsFilesRequestDirDetails**](DefaultApi.md#rsFilesRequestDirDetails) | **POST** /rsFiles/requestDirDetails | Request directory details, subsequent multiple call may be used to explore a whole directory tree.
[**rsFilesRequestFiles**](DefaultApi.md#rsFilesRequestFiles) | **POST** /rsFiles/requestFiles | Initiate download of a files collection
[**rsFilesSetChunkStrategy**](DefaultApi.md#rsFilesSetChunkStrategy) | **POST** /rsFiles/setChunkStrategy | Set chunk strategy for file, useful to set streaming mode to be able of see video or other media preview while it is still downloading
[**rsFilesSetDefaultChunkStrategy**](DefaultApi.md#rsFilesSetDefaultChunkStrategy) | **POST** /rsFiles/setDefaultChunkStrategy | Set default chunk strategy
[**rsFilesSetDestinationDirectory**](DefaultApi.md#rsFilesSetDestinationDirectory) | **POST** /rsFiles/setDestinationDirectory | Set destination directory for given file
[**rsFilesSetDestinationName**](DefaultApi.md#rsFilesSetDestinationName) | **POST** /rsFiles/setDestinationName | Set name for dowloaded file
[**rsFilesSetDownloadDirectory**](DefaultApi.md#rsFilesSetDownloadDirectory) | **POST** /rsFiles/setDownloadDirectory | Set default complete downloads directory
[**rsFilesSetFreeDiskSpaceLimit**](DefaultApi.md#rsFilesSetFreeDiskSpaceLimit) | **POST** /rsFiles/setFreeDiskSpaceLimit | Set minimum free disk space limit
[**rsFilesSetPartialsDirectory**](DefaultApi.md#rsFilesSetPartialsDirectory) | **POST** /rsFiles/setPartialsDirectory | Set partial downloads directory
[**rsFilesSetSharedDirectories**](DefaultApi.md#rsFilesSetSharedDirectories) | **POST** /rsFiles/setSharedDirectories | Set shared directories
[**rsFilesTurtleSearchRequest**](DefaultApi.md#rsFilesTurtleSearchRequest) | **POST** /rsFiles/turtleSearchRequest | This method is asynchronous. Request remote files search
[**rsFilesUnbanFile**](DefaultApi.md#rsFilesUnbanFile) | **POST** /rsFiles/unbanFile | Remove file from unwanted list
[**rsFilesUpdateShareFlags**](DefaultApi.md#rsFilesUpdateShareFlags) | **POST** /rsFiles/updateShareFlags | Updates shared directory sharing flags. The directory should be already shared!
[**rsGossipDiscoveryGetDiscFriends**](DefaultApi.md#rsGossipDiscoveryGetDiscFriends) | **POST** /rsGossipDiscovery/getDiscFriends | getDiscFriends get a list of all friends of a given friend
[**rsGossipDiscoveryGetDiscPgpFriends**](DefaultApi.md#rsGossipDiscoveryGetDiscPgpFriends) | **POST** /rsGossipDiscovery/getDiscPgpFriends | getDiscPgpFriends get a list of all friends of a given friend
[**rsGossipDiscoveryGetPeerVersion**](DefaultApi.md#rsGossipDiscoveryGetPeerVersion) | **POST** /rsGossipDiscovery/getPeerVersion | getPeerVersion get the version string of a peer.
[**rsGossipDiscoveryGetWaitingDiscCount**](DefaultApi.md#rsGossipDiscoveryGetWaitingDiscCount) | **POST** /rsGossipDiscovery/getWaitingDiscCount | getWaitingDiscCount get the number of queued discovery packets.
[**rsGxsChannelsCreateChannel**](DefaultApi.md#rsGxsChannelsCreateChannel) | **POST** /rsGxsChannels/createChannel | Deprecated{ substituted by createChannelV2 }
[**rsGxsChannelsCreateChannelV2**](DefaultApi.md#rsGxsChannelsCreateChannelV2) | **POST** /rsGxsChannels/createChannelV2 | Create channel. Blocking API.
[**rsGxsChannelsCreateComment**](DefaultApi.md#rsGxsChannelsCreateComment) | **POST** /rsGxsChannels/createComment | Deprecated
[**rsGxsChannelsCreateCommentV2**](DefaultApi.md#rsGxsChannelsCreateCommentV2) | **POST** /rsGxsChannels/createCommentV2 | Add a comment on a post or on another comment. Blocking API.
[**rsGxsChannelsCreatePost**](DefaultApi.md#rsGxsChannelsCreatePost) | **POST** /rsGxsChannels/createPost | Deprecated
[**rsGxsChannelsCreatePostV2**](DefaultApi.md#rsGxsChannelsCreatePostV2) | **POST** /rsGxsChannels/createPostV2 | Create channel post. Blocking API.
[**rsGxsChannelsCreateVote**](DefaultApi.md#rsGxsChannelsCreateVote) | **POST** /rsGxsChannels/createVote | Deprecated
[**rsGxsChannelsCreateVoteV2**](DefaultApi.md#rsGxsChannelsCreateVoteV2) | **POST** /rsGxsChannels/createVoteV2 | Create a vote
[**rsGxsChannelsEditChannel**](DefaultApi.md#rsGxsChannelsEditChannel) | **POST** /rsGxsChannels/editChannel | Edit channel details.
[**rsGxsChannelsExportChannelLink**](DefaultApi.md#rsGxsChannelsExportChannelLink) | **POST** /rsGxsChannels/exportChannelLink | Get link to a channel
[**rsGxsChannelsExtraFileHash**](DefaultApi.md#rsGxsChannelsExtraFileHash) | **POST** /rsGxsChannels/ExtraFileHash | Share extra file Can be used to share extra file attached to a channel post
[**rsGxsChannelsExtraFileRemove**](DefaultApi.md#rsGxsChannelsExtraFileRemove) | **POST** /rsGxsChannels/ExtraFileRemove | Remove extra file from shared files
[**rsGxsChannelsGetChannelAllContent**](DefaultApi.md#rsGxsChannelsGetChannelAllContent) | **POST** /rsGxsChannels/getChannelAllContent | Get all channel messages and comments in a given channel
[**rsGxsChannelsGetChannelAutoDownload**](DefaultApi.md#rsGxsChannelsGetChannelAutoDownload) | **POST** /rsGxsChannels/getChannelAutoDownload | DeprecatedThis feature rely on very buggy code, the returned value is not reliable
[**rsGxsChannelsGetChannelComments**](DefaultApi.md#rsGxsChannelsGetChannelComments) | **POST** /rsGxsChannels/getChannelComments | Get channel comments corresponding to the given IDs. If the set is empty, nothing is returned.
[**rsGxsChannelsGetChannelContent**](DefaultApi.md#rsGxsChannelsGetChannelContent) | **POST** /rsGxsChannels/getChannelContent | Get channel messages and comments corresponding to the given IDs. If the set is empty, nothing is returned.
[**rsGxsChannelsGetChannelDownloadDirectory**](DefaultApi.md#rsGxsChannelsGetChannelDownloadDirectory) | **POST** /rsGxsChannels/getChannelDownloadDirectory | Deprecated
[**rsGxsChannelsGetChannelServiceStatistics**](DefaultApi.md#rsGxsChannelsGetChannelServiceStatistics) | **POST** /rsGxsChannels/getChannelServiceStatistics | Retrieve statistics about the channel service
[**rsGxsChannelsGetChannelStatistics**](DefaultApi.md#rsGxsChannelsGetChannelStatistics) | **POST** /rsGxsChannels/getChannelStatistics | Retrieve statistics about the given channel
[**rsGxsChannelsGetChannelsInfo**](DefaultApi.md#rsGxsChannelsGetChannelsInfo) | **POST** /rsGxsChannels/getChannelsInfo | Get channels information (description, thumbnail...). Blocking API.
[**rsGxsChannelsGetChannelsSummaries**](DefaultApi.md#rsGxsChannelsGetChannelsSummaries) | **POST** /rsGxsChannels/getChannelsSummaries | Get channels summaries list. Blocking API.
[**rsGxsChannelsGetContentSummaries**](DefaultApi.md#rsGxsChannelsGetContentSummaries) | **POST** /rsGxsChannels/getContentSummaries | Get channel content summaries
[**rsGxsChannelsLocalSearchRequest**](DefaultApi.md#rsGxsChannelsLocalSearchRequest) | **POST** /rsGxsChannels/localSearchRequest | This method is asynchronous. Search local channels
[**rsGxsChannelsMarkRead**](DefaultApi.md#rsGxsChannelsMarkRead) | **POST** /rsGxsChannels/markRead | Toggle post read status. Blocking API.
[**rsGxsChannelsRequestStatus**](DefaultApi.md#rsGxsChannelsRequestStatus) | **POST** /rsGxsChannels/requestStatus | null
[**rsGxsChannelsSetChannelAutoDownload**](DefaultApi.md#rsGxsChannelsSetChannelAutoDownload) | **POST** /rsGxsChannels/setChannelAutoDownload | DeprecatedThis feature rely on very buggy code, when enabled the channel service start flooding erratically log with error messages, apparently without more dangerous consequences. Still those messages hints that something out of control is happening under the hood, use at your own risk. A safe alternative to this method can easly implemented at API client level instead.
[**rsGxsChannelsSetChannelDownloadDirectory**](DefaultApi.md#rsGxsChannelsSetChannelDownloadDirectory) | **POST** /rsGxsChannels/setChannelDownloadDirectory | Deprecated
[**rsGxsChannelsShareChannelKeys**](DefaultApi.md#rsGxsChannelsShareChannelKeys) | **POST** /rsGxsChannels/shareChannelKeys | Share channel publishing key This can be used to authorize other peers to post on the channel
[**rsGxsChannelsSubscribeToChannel**](DefaultApi.md#rsGxsChannelsSubscribeToChannel) | **POST** /rsGxsChannels/subscribeToChannel | Subscrbe to a channel. Blocking API
[**rsGxsChannelsTurtleChannelRequest**](DefaultApi.md#rsGxsChannelsTurtleChannelRequest) | **POST** /rsGxsChannels/turtleChannelRequest | This method is asynchronous. Request remote channel
[**rsGxsChannelsTurtleSearchRequest**](DefaultApi.md#rsGxsChannelsTurtleSearchRequest) | **POST** /rsGxsChannels/turtleSearchRequest | This method is asynchronous. Request remote channels search
[**rsGxsCirclesCancelCircleMembership**](DefaultApi.md#rsGxsCirclesCancelCircleMembership) | **POST** /rsGxsCircles/cancelCircleMembership | Leave given circle
[**rsGxsCirclesCreateCircle**](DefaultApi.md#rsGxsCirclesCreateCircle) | **POST** /rsGxsCircles/createCircle | Create new circle
[**rsGxsCirclesEditCircle**](DefaultApi.md#rsGxsCirclesEditCircle) | **POST** /rsGxsCircles/editCircle | Edit own existing circle
[**rsGxsCirclesExportCircleLink**](DefaultApi.md#rsGxsCirclesExportCircleLink) | **POST** /rsGxsCircles/exportCircleLink | Get link to a circle
[**rsGxsCirclesGetCircleDetails**](DefaultApi.md#rsGxsCirclesGetCircleDetails) | **POST** /rsGxsCircles/getCircleDetails | Get circle details. Memory cached
[**rsGxsCirclesGetCircleExternalIdList**](DefaultApi.md#rsGxsCirclesGetCircleExternalIdList) | **POST** /rsGxsCircles/getCircleExternalIdList | Get list of known external circles ids. Memory cached
[**rsGxsCirclesGetCircleRequest**](DefaultApi.md#rsGxsCirclesGetCircleRequest) | **POST** /rsGxsCircles/getCircleRequest | Get specific circle request
[**rsGxsCirclesGetCircleRequests**](DefaultApi.md#rsGxsCirclesGetCircleRequests) | **POST** /rsGxsCircles/getCircleRequests | Get circle requests
[**rsGxsCirclesGetCirclesInfo**](DefaultApi.md#rsGxsCirclesGetCirclesInfo) | **POST** /rsGxsCircles/getCirclesInfo | Get circles information
[**rsGxsCirclesGetCirclesSummaries**](DefaultApi.md#rsGxsCirclesGetCirclesSummaries) | **POST** /rsGxsCircles/getCirclesSummaries | Get circles summaries list.
[**rsGxsCirclesInviteIdsToCircle**](DefaultApi.md#rsGxsCirclesInviteIdsToCircle) | **POST** /rsGxsCircles/inviteIdsToCircle | Invite identities to circle (admin key is required)
[**rsGxsCirclesRequestCircleMembership**](DefaultApi.md#rsGxsCirclesRequestCircleMembership) | **POST** /rsGxsCircles/requestCircleMembership | Request circle membership, or accept circle invitation
[**rsGxsCirclesRequestStatus**](DefaultApi.md#rsGxsCirclesRequestStatus) | **POST** /rsGxsCircles/requestStatus | null
[**rsGxsCirclesRevokeIdsFromCircle**](DefaultApi.md#rsGxsCirclesRevokeIdsFromCircle) | **POST** /rsGxsCircles/revokeIdsFromCircle | Remove identities from circle (admin key is required)
[**rsGxsForumsCreateForum**](DefaultApi.md#rsGxsForumsCreateForum) | **POST** /rsGxsForums/createForum | Deprecated
[**rsGxsForumsCreateForumV2**](DefaultApi.md#rsGxsForumsCreateForumV2) | **POST** /rsGxsForums/createForumV2 | Create forum.
[**rsGxsForumsCreateMessage**](DefaultApi.md#rsGxsForumsCreateMessage) | **POST** /rsGxsForums/createMessage | Deprecated
[**rsGxsForumsCreatePost**](DefaultApi.md#rsGxsForumsCreatePost) | **POST** /rsGxsForums/createPost | Create a post on the given forum.
[**rsGxsForumsEditForum**](DefaultApi.md#rsGxsForumsEditForum) | **POST** /rsGxsForums/editForum | Edit forum details.
[**rsGxsForumsExportForumLink**](DefaultApi.md#rsGxsForumsExportForumLink) | **POST** /rsGxsForums/exportForumLink | Get link to a forum
[**rsGxsForumsGetForumContent**](DefaultApi.md#rsGxsForumsGetForumContent) | **POST** /rsGxsForums/getForumContent | Get specific list of messages from a single forum. Blocking API
[**rsGxsForumsGetForumMsgMetaData**](DefaultApi.md#rsGxsForumsGetForumMsgMetaData) | **POST** /rsGxsForums/getForumMsgMetaData | Get message metadatas for a specific forum. Blocking API
[**rsGxsForumsGetForumServiceStatistics**](DefaultApi.md#rsGxsForumsGetForumServiceStatistics) | **POST** /rsGxsForums/getForumServiceStatistics | returns statistics for the forum service
[**rsGxsForumsGetForumStatistics**](DefaultApi.md#rsGxsForumsGetForumStatistics) | **POST** /rsGxsForums/getForumStatistics | returns statistics about a particular forum
[**rsGxsForumsGetForumsInfo**](DefaultApi.md#rsGxsForumsGetForumsInfo) | **POST** /rsGxsForums/getForumsInfo | Get forums information (description, thumbnail...). Blocking API.
[**rsGxsForumsGetForumsSummaries**](DefaultApi.md#rsGxsForumsGetForumsSummaries) | **POST** /rsGxsForums/getForumsSummaries | Get forums summaries list. Blocking API.
[**rsGxsForumsMarkRead**](DefaultApi.md#rsGxsForumsMarkRead) | **POST** /rsGxsForums/markRead | Toggle message read status. Blocking API.
[**rsGxsForumsRequestStatus**](DefaultApi.md#rsGxsForumsRequestStatus) | **POST** /rsGxsForums/requestStatus | null
[**rsGxsForumsSubscribeToForum**](DefaultApi.md#rsGxsForumsSubscribeToForum) | **POST** /rsGxsForums/subscribeToForum | Subscrbe to a forum. Blocking API
[**rsIdentityAutoAddFriendIdsAsContact**](DefaultApi.md#rsIdentityAutoAddFriendIdsAsContact) | **POST** /rsIdentity/autoAddFriendIdsAsContact | Check if automatic signed by friend identity contact flagging is enabled
[**rsIdentityCreateIdentity**](DefaultApi.md#rsIdentityCreateIdentity) | **POST** /rsIdentity/createIdentity | Create a new identity
[**rsIdentityDeleteBannedNodesThreshold**](DefaultApi.md#rsIdentityDeleteBannedNodesThreshold) | **POST** /rsIdentity/deleteBannedNodesThreshold | Get number of days after which delete a banned identities
[**rsIdentityDeleteIdentity**](DefaultApi.md#rsIdentityDeleteIdentity) | **POST** /rsIdentity/deleteIdentity | Locally delete given identity
[**rsIdentityExportIdentityLink**](DefaultApi.md#rsIdentityExportIdentityLink) | **POST** /rsIdentity/exportIdentityLink | Get link to a identity
[**rsIdentityGetIdDetails**](DefaultApi.md#rsIdentityGetIdDetails) | **POST** /rsIdentity/getIdDetails | Get identity details, from the cache
[**rsIdentityGetIdentitiesInfo**](DefaultApi.md#rsIdentityGetIdentitiesInfo) | **POST** /rsIdentity/getIdentitiesInfo | Get identities information (name, avatar...). Blocking API.
[**rsIdentityGetIdentitiesSummaries**](DefaultApi.md#rsIdentityGetIdentitiesSummaries) | **POST** /rsIdentity/getIdentitiesSummaries | Get identities summaries list.
[**rsIdentityGetLastUsageTS**](DefaultApi.md#rsIdentityGetLastUsageTS) | **POST** /rsIdentity/getLastUsageTS | Get last seen usage time of given identity
[**rsIdentityGetOwnPseudonimousIds**](DefaultApi.md#rsIdentityGetOwnPseudonimousIds) | **POST** /rsIdentity/getOwnPseudonimousIds | Get own pseudonimous (unsigned) ids
[**rsIdentityGetOwnSignedIds**](DefaultApi.md#rsIdentityGetOwnSignedIds) | **POST** /rsIdentity/getOwnSignedIds | Get own signed ids
[**rsIdentityIsARegularContact**](DefaultApi.md#rsIdentityIsARegularContact) | **POST** /rsIdentity/isARegularContact | Check if an identity is contact
[**rsIdentityIsKnownId**](DefaultApi.md#rsIdentityIsKnownId) | **POST** /rsIdentity/isKnownId | Check if an id is known
[**rsIdentityIsOwnId**](DefaultApi.md#rsIdentityIsOwnId) | **POST** /rsIdentity/isOwnId | Check if an id is own
[**rsIdentityRequestIdentity**](DefaultApi.md#rsIdentityRequestIdentity) | **POST** /rsIdentity/requestIdentity | request details of a not yet known identity to the network
[**rsIdentityRequestStatus**](DefaultApi.md#rsIdentityRequestStatus) | **POST** /rsIdentity/requestStatus | null
[**rsIdentitySetAsRegularContact**](DefaultApi.md#rsIdentitySetAsRegularContact) | **POST** /rsIdentity/setAsRegularContact | Set/unset identity as contact
[**rsIdentitySetAutoAddFriendIdsAsContact**](DefaultApi.md#rsIdentitySetAutoAddFriendIdsAsContact) | **POST** /rsIdentity/setAutoAddFriendIdsAsContact | Toggle automatic flagging signed by friends identity as contact
[**rsIdentitySetDeleteBannedNodesThreshold**](DefaultApi.md#rsIdentitySetDeleteBannedNodesThreshold) | **POST** /rsIdentity/setDeleteBannedNodesThreshold | Set number of days after which delete a banned identities
[**rsIdentityUpdateIdentity**](DefaultApi.md#rsIdentityUpdateIdentity) | **POST** /rsIdentity/updateIdentity | Update identity data (name, avatar...)
[**rsJsonApiAskForStop**](DefaultApi.md#rsJsonApiAskForStop) | **POST** /rsJsonApi/askForStop | Request
[**rsJsonApiAuthorizeUser**](DefaultApi.md#rsJsonApiAuthorizeUser) | **POST** /rsJsonApi/authorizeUser | null
[**rsJsonApiGetAuthorizedTokens**](DefaultApi.md#rsJsonApiGetAuthorizedTokens) | **POST** /rsJsonApi/getAuthorizedTokens | Get authorized tokens
[**rsJsonApiGetBindingAddress**](DefaultApi.md#rsJsonApiGetBindingAddress) | **POST** /rsJsonApi/getBindingAddress | null
[**rsJsonApiIsAuthTokenValid**](DefaultApi.md#rsJsonApiIsAuthTokenValid) | **POST** /rsJsonApi/isAuthTokenValid | Check if given JSON API auth token is authorized
[**rsJsonApiListeningPort**](DefaultApi.md#rsJsonApiListeningPort) | **POST** /rsJsonApi/listeningPort | null
[**rsJsonApiRequestNewTokenAutorization**](DefaultApi.md#rsJsonApiRequestNewTokenAutorization) | **POST** /rsJsonApi/requestNewTokenAutorization | This function should be used by JSON API clients that aren&#39;t authenticated yet, to ask their token to be authorized, the success or failure will depend on mNewAccessRequestCallback return value, and it will likely need human user interaction in the process.
[**rsJsonApiRestart**](DefaultApi.md#rsJsonApiRestart) | **POST** /rsJsonApi/restart | Restart
[**rsJsonApiRevokeAuthToken**](DefaultApi.md#rsJsonApiRevokeAuthToken) | **POST** /rsJsonApi/revokeAuthToken | Revoke given auth token
[**rsJsonApiSetBindingAddress**](DefaultApi.md#rsJsonApiSetBindingAddress) | **POST** /rsJsonApi/setBindingAddress | null
[**rsJsonApiSetListeningPort**](DefaultApi.md#rsJsonApiSetListeningPort) | **POST** /rsJsonApi/setListeningPort | null
[**rsJsonApiVersion**](DefaultApi.md#rsJsonApiVersion) | **POST** /rsJsonApi/version | Write version information to given paramethers
[**rsLoginHelperAttemptLogin**](DefaultApi.md#rsLoginHelperAttemptLogin) | **POST** /rsLoginHelper/attemptLogin | Normal way to attempt login
[**rsLoginHelperCollectEntropy**](DefaultApi.md#rsLoginHelperCollectEntropy) | **POST** /rsLoginHelper/collectEntropy | Feed extra entropy to the crypto libraries
[**rsLoginHelperCreateLocation**](DefaultApi.md#rsLoginHelperCreateLocation) | **POST** /rsLoginHelper/createLocation | Creates a new RetroShare location, and log in once is created
[**rsLoginHelperGetLocations**](DefaultApi.md#rsLoginHelperGetLocations) | **POST** /rsLoginHelper/getLocations | Get locations and associated information
[**rsLoginHelperIsLoggedIn**](DefaultApi.md#rsLoginHelperIsLoggedIn) | **POST** /rsLoginHelper/isLoggedIn | Check if RetroShare is already logged in, this usually return true after a successfull
[**rsMsgsAcceptLobbyInvite**](DefaultApi.md#rsMsgsAcceptLobbyInvite) | **POST** /rsMsgs/acceptLobbyInvite | acceptLobbyInvite accept a chat invite
[**rsMsgsClearChatLobby**](DefaultApi.md#rsMsgsClearChatLobby) | **POST** /rsMsgs/clearChatLobby | clearChatLobby clear a chat lobby
[**rsMsgsCloseDistantChatConnexion**](DefaultApi.md#rsMsgsCloseDistantChatConnexion) | **POST** /rsMsgs/closeDistantChatConnexion | closeDistantChatConnexion
[**rsMsgsCreateChatLobby**](DefaultApi.md#rsMsgsCreateChatLobby) | **POST** /rsMsgs/createChatLobby | createChatLobby create a new chat lobby
[**rsMsgsDenyLobbyInvite**](DefaultApi.md#rsMsgsDenyLobbyInvite) | **POST** /rsMsgs/denyLobbyInvite | denyLobbyInvite deny a chat lobby invite
[**rsMsgsGetChatLobbyInfo**](DefaultApi.md#rsMsgsGetChatLobbyInfo) | **POST** /rsMsgs/getChatLobbyInfo | getChatLobbyInfo get lobby info of a subscribed chat lobby. Returns true if lobby id is valid.
[**rsMsgsGetChatLobbyList**](DefaultApi.md#rsMsgsGetChatLobbyList) | **POST** /rsMsgs/getChatLobbyList | getChatLobbyList get ids of subscribed lobbies
[**rsMsgsGetCustomStateString**](DefaultApi.md#rsMsgsGetCustomStateString) | **POST** /rsMsgs/getCustomStateString | getCustomStateString get the custom status message from a peer
[**rsMsgsGetDefaultIdentityForChatLobby**](DefaultApi.md#rsMsgsGetDefaultIdentityForChatLobby) | **POST** /rsMsgs/getDefaultIdentityForChatLobby | getDefaultIdentityForChatLobby get the default identity used for chat lobbies
[**rsMsgsGetDistantChatStatus**](DefaultApi.md#rsMsgsGetDistantChatStatus) | **POST** /rsMsgs/getDistantChatStatus | getDistantChatStatus receives distant chat info to a given distant chat id
[**rsMsgsGetIdentityForChatLobby**](DefaultApi.md#rsMsgsGetIdentityForChatLobby) | **POST** /rsMsgs/getIdentityForChatLobby | getIdentityForChatLobby
[**rsMsgsGetListOfNearbyChatLobbies**](DefaultApi.md#rsMsgsGetListOfNearbyChatLobbies) | **POST** /rsMsgs/getListOfNearbyChatLobbies | getListOfNearbyChatLobbies get info about all lobbies, subscribed and unsubscribed
[**rsMsgsGetLobbyAutoSubscribe**](DefaultApi.md#rsMsgsGetLobbyAutoSubscribe) | **POST** /rsMsgs/getLobbyAutoSubscribe | getLobbyAutoSubscribe get current value of auto subscribe
[**rsMsgsGetMaxMessageSecuritySize**](DefaultApi.md#rsMsgsGetMaxMessageSecuritySize) | **POST** /rsMsgs/getMaxMessageSecuritySize | getMaxMessageSecuritySize get the maximum size of a chta message
[**rsMsgsGetMessage**](DefaultApi.md#rsMsgsGetMessage) | **POST** /rsMsgs/getMessage | getMessage
[**rsMsgsGetMessageCount**](DefaultApi.md#rsMsgsGetMessageCount) | **POST** /rsMsgs/getMessageCount | getMessageCount
[**rsMsgsGetMessageSummaries**](DefaultApi.md#rsMsgsGetMessageSummaries) | **POST** /rsMsgs/getMessageSummaries | getMessageSummaries
[**rsMsgsGetMessageTag**](DefaultApi.md#rsMsgsGetMessageTag) | **POST** /rsMsgs/getMessageTag | getMessageTag
[**rsMsgsGetMessageTagTypes**](DefaultApi.md#rsMsgsGetMessageTagTypes) | **POST** /rsMsgs/getMessageTagTypes | getMessageTagTypes
[**rsMsgsGetMsgParentId**](DefaultApi.md#rsMsgsGetMsgParentId) | **POST** /rsMsgs/getMsgParentId | getMsgParentId
[**rsMsgsGetPendingChatLobbyInvites**](DefaultApi.md#rsMsgsGetPendingChatLobbyInvites) | **POST** /rsMsgs/getPendingChatLobbyInvites | getPendingChatLobbyInvites get a list of all pending chat lobby invites
[**rsMsgsInitiateDistantChatConnexion**](DefaultApi.md#rsMsgsInitiateDistantChatConnexion) | **POST** /rsMsgs/initiateDistantChatConnexion | initiateDistantChatConnexion initiate a connexion for a distant chat
[**rsMsgsInvitePeerToLobby**](DefaultApi.md#rsMsgsInvitePeerToLobby) | **POST** /rsMsgs/invitePeerToLobby | invitePeerToLobby invite a peer to join a lobby
[**rsMsgsJoinVisibleChatLobby**](DefaultApi.md#rsMsgsJoinVisibleChatLobby) | **POST** /rsMsgs/joinVisibleChatLobby | joinVisibleChatLobby join a lobby that is visible
[**rsMsgsMessageDelete**](DefaultApi.md#rsMsgsMessageDelete) | **POST** /rsMsgs/MessageDelete | MessageDelete
[**rsMsgsMessageForwarded**](DefaultApi.md#rsMsgsMessageForwarded) | **POST** /rsMsgs/MessageForwarded | MessageForwarded
[**rsMsgsMessageJunk**](DefaultApi.md#rsMsgsMessageJunk) | **POST** /rsMsgs/MessageJunk | MessageJunk
[**rsMsgsMessageLoadEmbeddedImages**](DefaultApi.md#rsMsgsMessageLoadEmbeddedImages) | **POST** /rsMsgs/MessageLoadEmbeddedImages | MessageLoadEmbeddedImages
[**rsMsgsMessageRead**](DefaultApi.md#rsMsgsMessageRead) | **POST** /rsMsgs/MessageRead | MessageRead
[**rsMsgsMessageReplied**](DefaultApi.md#rsMsgsMessageReplied) | **POST** /rsMsgs/MessageReplied | MessageReplied
[**rsMsgsMessageSend**](DefaultApi.md#rsMsgsMessageSend) | **POST** /rsMsgs/MessageSend | MessageSend
[**rsMsgsMessageStar**](DefaultApi.md#rsMsgsMessageStar) | **POST** /rsMsgs/MessageStar | MessageStar
[**rsMsgsMessageToDraft**](DefaultApi.md#rsMsgsMessageToDraft) | **POST** /rsMsgs/MessageToDraft | MessageToDraft
[**rsMsgsMessageToTrash**](DefaultApi.md#rsMsgsMessageToTrash) | **POST** /rsMsgs/MessageToTrash | MessageToTrash
[**rsMsgsRemoveMessageTagType**](DefaultApi.md#rsMsgsRemoveMessageTagType) | **POST** /rsMsgs/removeMessageTagType | removeMessageTagType
[**rsMsgsResetMessageStandardTagTypes**](DefaultApi.md#rsMsgsResetMessageStandardTagTypes) | **POST** /rsMsgs/resetMessageStandardTagTypes | resetMessageStandardTagTypes
[**rsMsgsSendChat**](DefaultApi.md#rsMsgsSendChat) | **POST** /rsMsgs/sendChat | sendChat send a chat message to a given id
[**rsMsgsSendLobbyStatusPeerLeaving**](DefaultApi.md#rsMsgsSendLobbyStatusPeerLeaving) | **POST** /rsMsgs/sendLobbyStatusPeerLeaving | sendLobbyStatusPeerLeaving notify friend nodes that we&#39;re leaving a subscribed lobby
[**rsMsgsSendMail**](DefaultApi.md#rsMsgsSendMail) | **POST** /rsMsgs/sendMail | sendMail
[**rsMsgsSendStatusString**](DefaultApi.md#rsMsgsSendStatusString) | **POST** /rsMsgs/sendStatusString | sendStatusString send a status string
[**rsMsgsSetCustomStateString**](DefaultApi.md#rsMsgsSetCustomStateString) | **POST** /rsMsgs/setCustomStateString | setCustomStateString set your custom status message
[**rsMsgsSetDefaultIdentityForChatLobby**](DefaultApi.md#rsMsgsSetDefaultIdentityForChatLobby) | **POST** /rsMsgs/setDefaultIdentityForChatLobby | setDefaultIdentityForChatLobby set the default identity used for chat lobbies
[**rsMsgsSetIdentityForChatLobby**](DefaultApi.md#rsMsgsSetIdentityForChatLobby) | **POST** /rsMsgs/setIdentityForChatLobby | setIdentityForChatLobby set the chat identit
[**rsMsgsSetLobbyAutoSubscribe**](DefaultApi.md#rsMsgsSetLobbyAutoSubscribe) | **POST** /rsMsgs/setLobbyAutoSubscribe | setLobbyAutoSubscribe enable or disable auto subscribe for a chat lobby
[**rsMsgsSetMessageTag**](DefaultApi.md#rsMsgsSetMessageTag) | **POST** /rsMsgs/setMessageTag | setMessageTag set &#x3D;&#x3D; false &amp;&amp; tagId &#x3D;&#x3D; 0
[**rsMsgsSetMessageTagType**](DefaultApi.md#rsMsgsSetMessageTagType) | **POST** /rsMsgs/setMessageTagType | setMessageTagType
[**rsMsgsSystemMessage**](DefaultApi.md#rsMsgsSystemMessage) | **POST** /rsMsgs/SystemMessage | SystemMessage
[**rsMsgsUnsubscribeChatLobby**](DefaultApi.md#rsMsgsUnsubscribeChatLobby) | **POST** /rsMsgs/unsubscribeChatLobby | unsubscribeChatLobby leave a chat lobby
[**rsPeersAcceptInvite**](DefaultApi.md#rsPeersAcceptInvite) | **POST** /rsPeers/acceptInvite | Add trusted node from invite
[**rsPeersAddFriend**](DefaultApi.md#rsPeersAddFriend) | **POST** /rsPeers/addFriend | Add trusted node
[**rsPeersAddGroup**](DefaultApi.md#rsPeersAddGroup) | **POST** /rsPeers/addGroup | addGroup create a new group
[**rsPeersAddPeerLocator**](DefaultApi.md#rsPeersAddPeerLocator) | **POST** /rsPeers/addPeerLocator | Add URL locator for given peer
[**rsPeersAddSslOnlyFriend**](DefaultApi.md#rsPeersAddSslOnlyFriend) | **POST** /rsPeers/addSslOnlyFriend | Add SSL-only trusted node When adding an SSL-only node, it is authorized to connect. Every time a connection is established the user is notified about the need to verify the PGP fingerprint, until she does, at that point the node become a full SSL+PGP friend.
[**rsPeersAssignPeerToGroup**](DefaultApi.md#rsPeersAssignPeerToGroup) | **POST** /rsPeers/assignPeerToGroup | assignPeerToGroup add a peer to a group
[**rsPeersAssignPeersToGroup**](DefaultApi.md#rsPeersAssignPeersToGroup) | **POST** /rsPeers/assignPeersToGroup | assignPeersToGroup add a list of peers to a group
[**rsPeersConnectAttempt**](DefaultApi.md#rsPeersConnectAttempt) | **POST** /rsPeers/connectAttempt | Trigger connection attempt to given node
[**rsPeersEditGroup**](DefaultApi.md#rsPeersEditGroup) | **POST** /rsPeers/editGroup | editGroup edit an existing group
[**rsPeersGetFriendList**](DefaultApi.md#rsPeersGetFriendList) | **POST** /rsPeers/getFriendList | Get trusted peers list
[**rsPeersGetGPGId**](DefaultApi.md#rsPeersGetGPGId) | **POST** /rsPeers/getGPGId | Get PGP id for the given peer
[**rsPeersGetGroupInfo**](DefaultApi.md#rsPeersGetGroupInfo) | **POST** /rsPeers/getGroupInfo | getGroupInfo get group information to one group
[**rsPeersGetGroupInfoByName**](DefaultApi.md#rsPeersGetGroupInfoByName) | **POST** /rsPeers/getGroupInfoByName | getGroupInfoByName get group information by group name
[**rsPeersGetGroupInfoList**](DefaultApi.md#rsPeersGetGroupInfoList) | **POST** /rsPeers/getGroupInfoList | getGroupInfoList get list of all groups
[**rsPeersGetOnlineList**](DefaultApi.md#rsPeersGetOnlineList) | **POST** /rsPeers/getOnlineList | Get connected peers list
[**rsPeersGetPeerDetails**](DefaultApi.md#rsPeersGetPeerDetails) | **POST** /rsPeers/getPeerDetails | Get details details of the given peer
[**rsPeersGetPeersCount**](DefaultApi.md#rsPeersGetPeersCount) | **POST** /rsPeers/getPeersCount | Get peers count
[**rsPeersGetPgpFriendList**](DefaultApi.md#rsPeersGetPgpFriendList) | **POST** /rsPeers/getPgpFriendList | Get trusted PGP ids list
[**rsPeersGetRetroshareInvite**](DefaultApi.md#rsPeersGetRetroshareInvite) | **POST** /rsPeers/GetRetroshareInvite | Get RetroShare invite of the given peer
[**rsPeersGetShortInvite**](DefaultApi.md#rsPeersGetShortInvite) | **POST** /rsPeers/getShortInvite | Get RetroShare short invite of the given peer
[**rsPeersIsFriend**](DefaultApi.md#rsPeersIsFriend) | **POST** /rsPeers/isFriend | Check if given peer is a trusted node
[**rsPeersIsOnline**](DefaultApi.md#rsPeersIsOnline) | **POST** /rsPeers/isOnline | Check if there is an established connection to the given peer
[**rsPeersIsPgpFriend**](DefaultApi.md#rsPeersIsPgpFriend) | **POST** /rsPeers/isPgpFriend | Check if given PGP id is trusted
[**rsPeersIsSslOnlyFriend**](DefaultApi.md#rsPeersIsSslOnlyFriend) | **POST** /rsPeers/isSslOnlyFriend | Check if given peer is a trusted SSL node pending PGP approval Peers added through short invite remain in this state as long as their PGP key is not received and verified/approved by the user.
[**rsPeersLoadCertificateFromString**](DefaultApi.md#rsPeersLoadCertificateFromString) | **POST** /rsPeers/loadCertificateFromString | Import certificate into the keyring
[**rsPeersLoadDetailsFromStringCert**](DefaultApi.md#rsPeersLoadDetailsFromStringCert) | **POST** /rsPeers/loadDetailsFromStringCert | Examine certificate and get details without importing into the keyring
[**rsPeersParseShortInvite**](DefaultApi.md#rsPeersParseShortInvite) | **POST** /rsPeers/parseShortInvite | Parse the give short invite to extract contained information
[**rsPeersPgpIdFromFingerprint**](DefaultApi.md#rsPeersPgpIdFromFingerprint) | **POST** /rsPeers/pgpIdFromFingerprint | Convert PGP fingerprint to PGP id
[**rsPeersRemoveFriend**](DefaultApi.md#rsPeersRemoveFriend) | **POST** /rsPeers/removeFriend | Revoke connection trust from to node
[**rsPeersRemoveFriendLocation**](DefaultApi.md#rsPeersRemoveFriendLocation) | **POST** /rsPeers/removeFriendLocation | Remove location of a trusted node, useful to prune old unused locations of a trusted peer without revoking trust
[**rsPeersRemoveGroup**](DefaultApi.md#rsPeersRemoveGroup) | **POST** /rsPeers/removeGroup | removeGroup remove a group
[**rsPeersSetDynDNS**](DefaultApi.md#rsPeersSetDynDNS) | **POST** /rsPeers/setDynDNS | Set (dynamical) domain name associated to the given peer
[**rsPeersSetExtAddress**](DefaultApi.md#rsPeersSetExtAddress) | **POST** /rsPeers/setExtAddress | Set external IPv4 address for given peer
[**rsPeersSetLocalAddress**](DefaultApi.md#rsPeersSetLocalAddress) | **POST** /rsPeers/setLocalAddress | Set local IPv4 address for the given peer
[**rsPeersSetNetworkMode**](DefaultApi.md#rsPeersSetNetworkMode) | **POST** /rsPeers/setNetworkMode | Set network mode of the given peer
[**rsPeersSetVisState**](DefaultApi.md#rsPeersSetVisState) | **POST** /rsPeers/setVisState | set DHT and discovery modes
[**rsPostedRequestStatus**](DefaultApi.md#rsPostedRequestStatus) | **POST** /rsPosted/requestStatus | null
[**rsReputationsAutoPositiveOpinionForContacts**](DefaultApi.md#rsReputationsAutoPositiveOpinionForContacts) | **POST** /rsReputations/autoPositiveOpinionForContacts | check if giving automatic positive opinion when flagging as contact is enbaled
[**rsReputationsBanNode**](DefaultApi.md#rsReputationsBanNode) | **POST** /rsReputations/banNode | Enable automatic banning of all identities signed by the given node
[**rsReputationsGetOwnOpinion**](DefaultApi.md#rsReputationsGetOwnOpinion) | **POST** /rsReputations/getOwnOpinion | Get own opition about the given identity
[**rsReputationsGetReputationInfo**](DefaultApi.md#rsReputationsGetReputationInfo) | **POST** /rsReputations/getReputationInfo | Get reputation data of given identity
[**rsReputationsIsIdentityBanned**](DefaultApi.md#rsReputationsIsIdentityBanned) | **POST** /rsReputations/isIdentityBanned | This method allow fast checking if a GXS identity is banned.
[**rsReputationsIsNodeBanned**](DefaultApi.md#rsReputationsIsNodeBanned) | **POST** /rsReputations/isNodeBanned | Check if automatic banning of all identities signed by the given node is enabled
[**rsReputationsOverallReputationLevel**](DefaultApi.md#rsReputationsOverallReputationLevel) | **POST** /rsReputations/overallReputationLevel | Get overall reputation level of given identity
[**rsReputationsRememberBannedIdThreshold**](DefaultApi.md#rsReputationsRememberBannedIdThreshold) | **POST** /rsReputations/rememberBannedIdThreshold | Get number of days to wait before deleting a banned identity from local storage
[**rsReputationsSetAutoPositiveOpinionForContacts**](DefaultApi.md#rsReputationsSetAutoPositiveOpinionForContacts) | **POST** /rsReputations/setAutoPositiveOpinionForContacts | Enable giving automatic positive opinion when flagging as contact
[**rsReputationsSetOwnOpinion**](DefaultApi.md#rsReputationsSetOwnOpinion) | **POST** /rsReputations/setOwnOpinion | Set own opinion about the given identity
[**rsReputationsSetRememberBannedIdThreshold**](DefaultApi.md#rsReputationsSetRememberBannedIdThreshold) | **POST** /rsReputations/setRememberBannedIdThreshold | Set number of days to wait before deleting a banned identity from local storage
[**rsReputationsSetThresholdForRemotelyNegativeReputation**](DefaultApi.md#rsReputationsSetThresholdForRemotelyNegativeReputation) | **POST** /rsReputations/setThresholdForRemotelyNegativeReputation | Set threshold on remote reputation to consider it remotely negative
[**rsReputationsSetThresholdForRemotelyPositiveReputation**](DefaultApi.md#rsReputationsSetThresholdForRemotelyPositiveReputation) | **POST** /rsReputations/setThresholdForRemotelyPositiveReputation | Set threshold on remote reputation to consider it remotely positive
[**rsReputationsThresholdForRemotelyNegativeReputation**](DefaultApi.md#rsReputationsThresholdForRemotelyNegativeReputation) | **POST** /rsReputations/thresholdForRemotelyNegativeReputation | Get threshold on remote reputation to consider it remotely negative
[**rsReputationsThresholdForRemotelyPositiveReputation**](DefaultApi.md#rsReputationsThresholdForRemotelyPositiveReputation) | **POST** /rsReputations/thresholdForRemotelyPositiveReputation | Get threshold on remote reputation to consider it remotely negative
[**rsServiceControlGetOwnServices**](DefaultApi.md#rsServiceControlGetOwnServices) | **POST** /rsServiceControl/getOwnServices | get a map off all services.
[**rsServiceControlGetPeersConnected**](DefaultApi.md#rsServiceControlGetPeersConnected) | **POST** /rsServiceControl/getPeersConnected | getPeersConnected return peers using a service.
[**rsServiceControlGetServiceItemNames**](DefaultApi.md#rsServiceControlGetServiceItemNames) | **POST** /rsServiceControl/getServiceItemNames | getServiceItemNames return a map of service item names.
[**rsServiceControlGetServiceName**](DefaultApi.md#rsServiceControlGetServiceName) | **POST** /rsServiceControl/getServiceName | getServiceName lookup the name of a service.
[**rsServiceControlGetServicePermissions**](DefaultApi.md#rsServiceControlGetServicePermissions) | **POST** /rsServiceControl/getServicePermissions | getServicePermissions return permissions of one service.
[**rsServiceControlGetServicesAllowed**](DefaultApi.md#rsServiceControlGetServicesAllowed) | **POST** /rsServiceControl/getServicesAllowed | getServicesAllowed return a mpa with allowed service information.
[**rsServiceControlGetServicesProvided**](DefaultApi.md#rsServiceControlGetServicesProvided) | **POST** /rsServiceControl/getServicesProvided | getServicesProvided return services provided by a peer.
[**rsServiceControlUpdateServicePermissions**](DefaultApi.md#rsServiceControlUpdateServicePermissions) | **POST** /rsServiceControl/updateServicePermissions | updateServicePermissions update service permissions of one service.


# **rsAccountsExportIdentity**
> ResExportIdentity rsAccountsExportIdentity(reqExportIdentity)

Export full encrypted PGP identity to file

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExportIdentity = ReqExportIdentity(); // ReqExportIdentity | filePath: >              (string)path of certificate file          pgpId: >              (RsPgpId)PGP id to export  

try { 
    var result = api_instance.rsAccountsExportIdentity(reqExportIdentity);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsAccountsExportIdentity: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExportIdentity** | [**ReqExportIdentity**](ReqExportIdentity.md)| filePath: &gt;              (string)path of certificate file          pgpId: &gt;              (RsPgpId)PGP id to export   | [optional] 

### Return type

[**ResExportIdentity**](ResExportIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsAccountsExportIdentityToString**
> ResExportIdentityToString rsAccountsExportIdentityToString(reqExportIdentityToString)

Export full encrypted PGP identity to string

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExportIdentityToString = ReqExportIdentityToString(); // ReqExportIdentityToString | pgpId: >              (RsPgpId)PGP id to export          includeSignatures: >              (boolean)true to include signatures  

try { 
    var result = api_instance.rsAccountsExportIdentityToString(reqExportIdentityToString);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsAccountsExportIdentityToString: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExportIdentityToString** | [**ReqExportIdentityToString**](ReqExportIdentityToString.md)| pgpId: &gt;              (RsPgpId)PGP id to export          includeSignatures: &gt;              (boolean)true to include signatures   | [optional] 

### Return type

[**ResExportIdentityToString**](ResExportIdentityToString.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsAccountsGetCurrentAccountId**
> ResGetCurrentAccountId rsAccountsGetCurrentAccountId()

Get current account id. Beware that an account may be selected without actually logging in.

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsAccountsGetCurrentAccountId();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsAccountsGetCurrentAccountId: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetCurrentAccountId**](ResGetCurrentAccountId.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsAccountsGetPGPLogins**
> ResGetPGPLogins rsAccountsGetPGPLogins()

Get available PGP identities id list

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsAccountsGetPGPLogins();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsAccountsGetPGPLogins: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetPGPLogins**](ResGetPGPLogins.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsAccountsImportIdentity**
> ResImportIdentity rsAccountsImportIdentity(reqImportIdentity)

Import full encrypted PGP identity from file

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var reqImportIdentity = ReqImportIdentity(); // ReqImportIdentity | filePath: >              (string)path of certificate file  

try { 
    var result = api_instance.rsAccountsImportIdentity(reqImportIdentity);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsAccountsImportIdentity: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqImportIdentity** | [**ReqImportIdentity**](ReqImportIdentity.md)| filePath: &gt;              (string)path of certificate file   | [optional] 

### Return type

[**ResImportIdentity**](ResImportIdentity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsAccountsImportIdentityFromString**
> ResImportIdentityFromString rsAccountsImportIdentityFromString(reqImportIdentityFromString)

Import full encrypted PGP identity from string

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var reqImportIdentityFromString = ReqImportIdentityFromString(); // ReqImportIdentityFromString | data: >              (string)certificate string  

try { 
    var result = api_instance.rsAccountsImportIdentityFromString(reqImportIdentityFromString);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsAccountsImportIdentityFromString: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqImportIdentityFromString** | [**ReqImportIdentityFromString**](ReqImportIdentityFromString.md)| data: &gt;              (string)certificate string   | [optional] 

### Return type

[**ResImportIdentityFromString**](ResImportIdentityFromString.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsBanListEnableIPFiltering**
> rsBanListEnableIPFiltering(reqEnableIPFiltering)

Enable or disable IP filtering service

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqEnableIPFiltering = ReqEnableIPFiltering(); // ReqEnableIPFiltering | enable: >              (boolean)pass true to enable, false to disable  

try { 
    api_instance.rsBanListEnableIPFiltering(reqEnableIPFiltering);
} catch (e) {
    print("Exception when calling DefaultApi->rsBanListEnableIPFiltering: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqEnableIPFiltering** | [**ReqEnableIPFiltering**](ReqEnableIPFiltering.md)| enable: &gt;              (boolean)pass true to enable, false to disable   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsBanListIpFilteringEnabled**
> ResIpFilteringEnabled rsBanListIpFilteringEnabled()

Get ip filtering service status

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsBanListIpFilteringEnabled();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsBanListIpFilteringEnabled: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResIpFilteringEnabled**](ResIpFilteringEnabled.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsBroadcastDiscoveryDisableMulticastListening**
> ResDisableMulticastListening rsBroadcastDiscoveryDisableMulticastListening()

Disable multicast listening

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsBroadcastDiscoveryDisableMulticastListening();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsBroadcastDiscoveryDisableMulticastListening: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResDisableMulticastListening**](ResDisableMulticastListening.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsBroadcastDiscoveryEnableMulticastListening**
> ResEnableMulticastListening rsBroadcastDiscoveryEnableMulticastListening()

On platforms that need it enable low level multicast listening

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsBroadcastDiscoveryEnableMulticastListening();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsBroadcastDiscoveryEnableMulticastListening: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResEnableMulticastListening**](ResEnableMulticastListening.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsBroadcastDiscoveryGetDiscoveredPeers**
> ResGetDiscoveredPeers rsBroadcastDiscoveryGetDiscoveredPeers()

Get potential peers that have been discovered up until now

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsBroadcastDiscoveryGetDiscoveredPeers();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsBroadcastDiscoveryGetDiscoveredPeers: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetDiscoveredPeers**](ResGetDiscoveredPeers.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsBroadcastDiscoveryIsMulticastListeningEnabled**
> ResIsMulticastListeningEnabled rsBroadcastDiscoveryIsMulticastListeningEnabled()

Check if multicast listening is enabled

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsBroadcastDiscoveryIsMulticastListeningEnabled();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsBroadcastDiscoveryIsMulticastListeningEnabled: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResIsMulticastListeningEnabled**](ResIsMulticastListeningEnabled.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsConfigGetAllBandwidthRates**
> ResGetAllBandwidthRates rsConfigGetAllBandwidthRates()

getAllBandwidthRates get the bandwidth rates for all peers

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsConfigGetAllBandwidthRates();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsConfigGetAllBandwidthRates: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetAllBandwidthRates**](ResGetAllBandwidthRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsConfigGetConfigNetStatus**
> ResGetConfigNetStatus rsConfigGetConfigNetStatus()

getConfigNetStatus return the net status

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsConfigGetConfigNetStatus();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsConfigGetConfigNetStatus: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetConfigNetStatus**](ResGetConfigNetStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsConfigGetCurrentDataRates**
> ResGetCurrentDataRates rsConfigGetCurrentDataRates()

GetCurrentDataRates get current upload and download rates

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsConfigGetCurrentDataRates();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsConfigGetCurrentDataRates: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetCurrentDataRates**](ResGetCurrentDataRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsConfigGetMaxDataRates**
> ResGetMaxDataRates rsConfigGetMaxDataRates()

GetMaxDataRates get maximum upload and download rates

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsConfigGetMaxDataRates();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsConfigGetMaxDataRates: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetMaxDataRates**](ResGetMaxDataRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsConfigGetOperatingMode**
> ResGetOperatingMode rsConfigGetOperatingMode()

getOperatingMode get current operating mode

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsConfigGetOperatingMode();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsConfigGetOperatingMode: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetOperatingMode**](ResGetOperatingMode.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsConfigGetTotalBandwidthRates**
> ResGetTotalBandwidthRates rsConfigGetTotalBandwidthRates()

getTotalBandwidthRates returns the current bandwidths rates

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsConfigGetTotalBandwidthRates();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsConfigGetTotalBandwidthRates: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetTotalBandwidthRates**](ResGetTotalBandwidthRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsConfigGetTrafficInfo**
> ResGetTrafficInfo rsConfigGetTrafficInfo()

getTrafficInfo returns a list of all tracked traffic clues

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsConfigGetTrafficInfo();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsConfigGetTrafficInfo: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetTrafficInfo**](ResGetTrafficInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsConfigSetMaxDataRates**
> ResSetMaxDataRates rsConfigSetMaxDataRates(reqSetMaxDataRates)

SetMaxDataRates set maximum upload and download rates

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetMaxDataRates = ReqSetMaxDataRates(); // ReqSetMaxDataRates | downKb: >              (integer)download rate in kB          upKb: >              (integer)upload rate in kB  

try { 
    var result = api_instance.rsConfigSetMaxDataRates(reqSetMaxDataRates);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsConfigSetMaxDataRates: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetMaxDataRates** | [**ReqSetMaxDataRates**](ReqSetMaxDataRates.md)| downKb: &gt;              (integer)download rate in kB          upKb: &gt;              (integer)upload rate in kB   | [optional] 

### Return type

[**ResSetMaxDataRates**](ResSetMaxDataRates.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsConfigSetOperatingMode**
> ResSetOperatingMode rsConfigSetOperatingMode(reqSetOperatingMode)

setOperatingMode set the current oprating mode

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetOperatingMode = ReqSetOperatingMode(); // ReqSetOperatingMode | opMode: >              (integer)new opearting mode  

try { 
    var result = api_instance.rsConfigSetOperatingMode(reqSetOperatingMode);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsConfigSetOperatingMode: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetOperatingMode** | [**ReqSetOperatingMode**](ReqSetOperatingMode.md)| opMode: &gt;              (integer)new opearting mode   | [optional] 

### Return type

[**ResSetOperatingMode**](ResSetOperatingMode.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsControlIsReady**
> ResIsReady rsControlIsReady()

Check if core is fully ready, true only after

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsControlIsReady();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsControlIsReady: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResIsReady**](ResIsReady.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsControlRsGlobalShutDown**
> rsControlRsGlobalShutDown()

Turn off RetroShare

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    api_instance.rsControlRsGlobalShutDown();
} catch (e) {
    print("Exception when calling DefaultApi->rsControlRsGlobalShutDown: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesAddSharedDirectory**
> ResAddSharedDirectory rsFilesAddSharedDirectory(reqAddSharedDirectory)

Add shared directory

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqAddSharedDirectory = ReqAddSharedDirectory(); // ReqAddSharedDirectory | dir: >              (SharedDirInfo)directory to share with sharing options  

try { 
    var result = api_instance.rsFilesAddSharedDirectory(reqAddSharedDirectory);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesAddSharedDirectory: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqAddSharedDirectory** | [**ReqAddSharedDirectory**](ReqAddSharedDirectory.md)| dir: &gt;              (SharedDirInfo)directory to share with sharing options   | [optional] 

### Return type

[**ResAddSharedDirectory**](ResAddSharedDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesAlreadyHaveFile**
> ResAlreadyHaveFile rsFilesAlreadyHaveFile(reqAlreadyHaveFile)

Check if we already have a file

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqAlreadyHaveFile = ReqAlreadyHaveFile(); // ReqAlreadyHaveFile | hash: >              (RsFileHash)file identifier  

try { 
    var result = api_instance.rsFilesAlreadyHaveFile(reqAlreadyHaveFile);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesAlreadyHaveFile: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqAlreadyHaveFile** | [**ReqAlreadyHaveFile**](ReqAlreadyHaveFile.md)| hash: &gt;              (RsFileHash)file identifier   | [optional] 

### Return type

[**ResAlreadyHaveFile**](ResAlreadyHaveFile.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesBanFile**
> ResBanFile rsFilesBanFile(reqBanFile)

Ban unwanted file from being, searched and forwarded by this node

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqBanFile = ReqBanFile(); // ReqBanFile | realFileHash: >              (RsFileHash)this is what will really enforce banning          filename: >              (string)expected name of the file, for the user to read          fileSize: >              (integer64)expected file size, for the user to read  

try { 
    var result = api_instance.rsFilesBanFile(reqBanFile);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesBanFile: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqBanFile** | [**ReqBanFile**](ReqBanFile.md)| realFileHash: &gt;              (RsFileHash)this is what will really enforce banning          filename: &gt;              (string)expected name of the file, for the user to read          fileSize: &gt;              (integer64)expected file size, for the user to read   | [optional] 

### Return type

[**ResBanFile**](ResBanFile.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesDefaultChunkStrategy**
> ResDefaultChunkStrategy rsFilesDefaultChunkStrategy()

Get default chunk strategy

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsFilesDefaultChunkStrategy();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesDefaultChunkStrategy: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResDefaultChunkStrategy**](ResDefaultChunkStrategy.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesExportCollectionLink**
> ResExportCollectionLink rsFilesExportCollectionLink(reqExportCollectionLink)

Export link to a collection of files

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExportCollectionLink = ReqExportCollectionLink(); // ReqExportCollectionLink | handle: >              (integer64)directory RetroShare handle          fragSneak: >              (boolean)when true the file data is sneaked into fragment instead of FILES_URL_DATA_FIELD query field, this way if using an http[s] link to pass around a disguised file link a misconfigured host attempting to visit that link with a web browser will not send the file data to the server thus protecting at least some of the privacy of the user even in a misconfiguration scenario.          baseUrl: >              (string)URL into which to sneak in the RetroShare file link base64, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare link into a \"normal\" looking web link. If empty the collection data link will be outputted in plain base64 format.  

try { 
    var result = api_instance.rsFilesExportCollectionLink(reqExportCollectionLink);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesExportCollectionLink: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExportCollectionLink** | [**ReqExportCollectionLink**](ReqExportCollectionLink.md)| handle: &gt;              (integer64)directory RetroShare handle          fragSneak: &gt;              (boolean)when true the file data is sneaked into fragment instead of FILES_URL_DATA_FIELD query field, this way if using an http[s] link to pass around a disguised file link a misconfigured host attempting to visit that link with a web browser will not send the file data to the server thus protecting at least some of the privacy of the user even in a misconfiguration scenario.          baseUrl: &gt;              (string)URL into which to sneak in the RetroShare file link base64, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare link into a \&quot;normal\&quot; looking web link. If empty the collection data link will be outputted in plain base64 format.   | [optional] 

### Return type

[**ResExportCollectionLink**](ResExportCollectionLink.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesExportFileLink**
> ResExportFileLink rsFilesExportFileLink(reqExportFileLink)

Export link to a file

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExportFileLink = ReqExportFileLink(); // ReqExportFileLink | fileHash: >              (RsFileHash)hash of the file          fileSize: >              (integer64)size of the file          fileName: >              (string)name of the file          fragSneak: >              (boolean)None         baseUrl: >              (string)None 

try { 
    var result = api_instance.rsFilesExportFileLink(reqExportFileLink);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesExportFileLink: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExportFileLink** | [**ReqExportFileLink**](ReqExportFileLink.md)| fileHash: &gt;              (RsFileHash)hash of the file          fileSize: &gt;              (integer64)size of the file          fileName: &gt;              (string)name of the file          fragSneak: &gt;              (boolean)None         baseUrl: &gt;              (string)None  | [optional] 

### Return type

[**ResExportFileLink**](ResExportFileLink.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesExtraFileHash**
> ResExtraFileHash rsFilesExtraFileHash(reqExtraFileHash)

Add file to extra shared file list

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExtraFileHash = ReqExtraFileHash(); // ReqExtraFileHash | localpath: >              (string)path of the file          period: >              (rstime_t)how much time the file will be kept in extra list in seconds          flags: >              (TransferRequestFlags)sharing policy flags ex: RS_FILE_REQ_ANONYMOUS_ROUTING  

try { 
    var result = api_instance.rsFilesExtraFileHash(reqExtraFileHash);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesExtraFileHash: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExtraFileHash** | [**ReqExtraFileHash**](ReqExtraFileHash.md)| localpath: &gt;              (string)path of the file          period: &gt;              (rstime_t)how much time the file will be kept in extra list in seconds          flags: &gt;              (TransferRequestFlags)sharing policy flags ex: RS_FILE_REQ_ANONYMOUS_ROUTING   | [optional] 

### Return type

[**ResExtraFileHash**](ResExtraFileHash.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesExtraFileRemove**
> ResExtraFileRemove rsFilesExtraFileRemove(reqExtraFileRemove)

Remove file from extra fila shared list

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExtraFileRemove = ReqExtraFileRemove(); // ReqExtraFileRemove | hash: >              (RsFileHash)hash of the file to remove  

try { 
    var result = api_instance.rsFilesExtraFileRemove(reqExtraFileRemove);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesExtraFileRemove: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExtraFileRemove** | [**ReqExtraFileRemove**](ReqExtraFileRemove.md)| hash: &gt;              (RsFileHash)hash of the file to remove   | [optional] 

### Return type

[**ResExtraFileRemove**](ResExtraFileRemove.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesExtraFileStatus**
> ResExtraFileStatus rsFilesExtraFileStatus(reqExtraFileStatus)

Get extra file information

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExtraFileStatus = ReqExtraFileStatus(); // ReqExtraFileStatus | localpath: >              (string)path of the file  

try { 
    var result = api_instance.rsFilesExtraFileStatus(reqExtraFileStatus);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesExtraFileStatus: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExtraFileStatus** | [**ReqExtraFileStatus**](ReqExtraFileStatus.md)| localpath: &gt;              (string)path of the file   | [optional] 

### Return type

[**ResExtraFileStatus**](ResExtraFileStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesFileCancel**
> ResFileCancel rsFilesFileCancel(reqFileCancel)

Cancel file downloading

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqFileCancel = ReqFileCancel(); // ReqFileCancel | hash: >              (RsFileHash)None 

try { 
    var result = api_instance.rsFilesFileCancel(reqFileCancel);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesFileCancel: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqFileCancel** | [**ReqFileCancel**](ReqFileCancel.md)| hash: &gt;              (RsFileHash)None  | [optional] 

### Return type

[**ResFileCancel**](ResFileCancel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesFileClearCompleted**
> ResFileClearCompleted rsFilesFileClearCompleted()

Clear completed downloaded files list

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsFilesFileClearCompleted();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesFileClearCompleted: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResFileClearCompleted**](ResFileClearCompleted.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesFileControl**
> ResFileControl rsFilesFileControl(reqFileControl)

Controls file transfer

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqFileControl = ReqFileControl(); // ReqFileControl | hash: >              (RsFileHash)file identifier          flags: >              (integer)action to perform. Pict into { RS_FILE_CTRL_PAUSE, RS_FILE_CTRL_START, RS_FILE_CTRL_FORCE_CHECK } }  

try { 
    var result = api_instance.rsFilesFileControl(reqFileControl);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesFileControl: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqFileControl** | [**ReqFileControl**](ReqFileControl.md)| hash: &gt;              (RsFileHash)file identifier          flags: &gt;              (integer)action to perform. Pict into { RS_FILE_CTRL_PAUSE, RS_FILE_CTRL_START, RS_FILE_CTRL_FORCE_CHECK } }   | [optional] 

### Return type

[**ResFileControl**](ResFileControl.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesFileDetails**
> ResFileDetails rsFilesFileDetails(reqFileDetails)

Get file details

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqFileDetails = ReqFileDetails(); // ReqFileDetails | hash: >              (RsFileHash)file identifier          hintflags: >              (FileSearchFlags)filtering hint (RS_FILE_HINTS_EXTRA|...|RS_FILE_HINTS_LOCAL)  

try { 
    var result = api_instance.rsFilesFileDetails(reqFileDetails);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesFileDetails: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqFileDetails** | [**ReqFileDetails**](ReqFileDetails.md)| hash: &gt;              (RsFileHash)file identifier          hintflags: &gt;              (FileSearchFlags)filtering hint (RS_FILE_HINTS_EXTRA|...|RS_FILE_HINTS_LOCAL)   | [optional] 

### Return type

[**ResFileDetails**](ResFileDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesFileDownloadChunksDetails**
> ResFileDownloadChunksDetails rsFilesFileDownloadChunksDetails(reqFileDownloadChunksDetails)

Get chunk details about the downloaded file with given hash.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqFileDownloadChunksDetails = ReqFileDownloadChunksDetails(); // ReqFileDownloadChunksDetails | hash: >              (RsFileHash)file identifier  

try { 
    var result = api_instance.rsFilesFileDownloadChunksDetails(reqFileDownloadChunksDetails);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesFileDownloadChunksDetails: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqFileDownloadChunksDetails** | [**ReqFileDownloadChunksDetails**](ReqFileDownloadChunksDetails.md)| hash: &gt;              (RsFileHash)file identifier   | [optional] 

### Return type

[**ResFileDownloadChunksDetails**](ResFileDownloadChunksDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesFileDownloads**
> ResFileDownloads rsFilesFileDownloads()

Get incoming files list

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsFilesFileDownloads();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesFileDownloads: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResFileDownloads**](ResFileDownloads.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesFileRequest**
> ResFileRequest rsFilesFileRequest(reqFileRequest)

Initiate downloading of a file

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqFileRequest = ReqFileRequest(); // ReqFileRequest | fileName: >              (string)file name          hash: >              (RsFileHash)file hash          size: >              (integer64)file size          destPath: >              (string)optional specify the destination directory          flags: >              (TransferRequestFlags)you usually want RS_FILE_REQ_ANONYMOUS_ROUTING          srcIds: >              (list<RsPeerId>)eventually specify known sources  

try { 
    var result = api_instance.rsFilesFileRequest(reqFileRequest);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesFileRequest: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqFileRequest** | [**ReqFileRequest**](ReqFileRequest.md)| fileName: &gt;              (string)file name          hash: &gt;              (RsFileHash)file hash          size: &gt;              (integer64)file size          destPath: &gt;              (string)optional specify the destination directory          flags: &gt;              (TransferRequestFlags)you usually want RS_FILE_REQ_ANONYMOUS_ROUTING          srcIds: &gt;              (list&lt;RsPeerId&gt;)eventually specify known sources   | [optional] 

### Return type

[**ResFileRequest**](ResFileRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesFileUploadChunksDetails**
> ResFileUploadChunksDetails rsFilesFileUploadChunksDetails(reqFileUploadChunksDetails)

Get details about the upload with given hash

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqFileUploadChunksDetails = ReqFileUploadChunksDetails(); // ReqFileUploadChunksDetails | hash: >              (RsFileHash)file identifier          peerId: >              (RsPeerId)peer identifier  

try { 
    var result = api_instance.rsFilesFileUploadChunksDetails(reqFileUploadChunksDetails);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesFileUploadChunksDetails: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqFileUploadChunksDetails** | [**ReqFileUploadChunksDetails**](ReqFileUploadChunksDetails.md)| hash: &gt;              (RsFileHash)file identifier          peerId: &gt;              (RsPeerId)peer identifier   | [optional] 

### Return type

[**ResFileUploadChunksDetails**](ResFileUploadChunksDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesFileUploads**
> ResFileUploads rsFilesFileUploads()

Get outgoing files list

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsFilesFileUploads();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesFileUploads: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResFileUploads**](ResFileUploads.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesForceDirectoryCheck**
> rsFilesForceDirectoryCheck(reqForceDirectoryCheck)

Force shared directories check.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqForceDirectoryCheck = ReqForceDirectoryCheck(); // ReqForceDirectoryCheck | add_safe_delay: >              (boolean)Schedule the check 20 seconds from now, to ensure to capture files written just now.   

try { 
    api_instance.rsFilesForceDirectoryCheck(reqForceDirectoryCheck);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesForceDirectoryCheck: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqForceDirectoryCheck** | [**ReqForceDirectoryCheck**](ReqForceDirectoryCheck.md)| add_safe_delay: &gt;              (boolean)Schedule the check 20 seconds from now, to ensure to capture files written just now.    | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesFreeDiskSpaceLimit**
> ResFreeDiskSpaceLimit rsFilesFreeDiskSpaceLimit()

Get free disk space limit

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsFilesFreeDiskSpaceLimit();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesFreeDiskSpaceLimit: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResFreeDiskSpaceLimit**](ResFreeDiskSpaceLimit.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesGetDownloadDirectory**
> ResGetDownloadDirectory rsFilesGetDownloadDirectory()

Get default complete downloads directory

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsFilesGetDownloadDirectory();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesGetDownloadDirectory: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetDownloadDirectory**](ResGetDownloadDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesGetFileData**
> ResGetFileData rsFilesGetFileData(reqGetFileData)

Provides file data for the GUI, media streaming or API clients. It may return unverified chunks. This allows streaming without having to wait for hashes or completion of the file. This function returns an unspecified amount of bytes. Either as much data as available or a sensible maximum. Expect a block size of around 1MiB. To get more data, call this function repeatedly with different offsets.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetFileData = ReqGetFileData(); // ReqGetFileData | hash: >              (RsFileHash)hash of the file. The file has to be available on this node or it has to be in downloading state.          offset: >              (integer64)where the desired block starts          requested_size: >              (integer)size of pre-allocated data. Will be updated by the function.  

try { 
    var result = api_instance.rsFilesGetFileData(reqGetFileData);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesGetFileData: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetFileData** | [**ReqGetFileData**](ReqGetFileData.md)| hash: &gt;              (RsFileHash)hash of the file. The file has to be available on this node or it has to be in downloading state.          offset: &gt;              (integer64)where the desired block starts          requested_size: &gt;              (integer)size of pre-allocated data. Will be updated by the function.   | [optional] 

### Return type

[**ResGetFileData**](ResGetFileData.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesGetPartialsDirectory**
> ResGetPartialsDirectory rsFilesGetPartialsDirectory()

Get partial downloads directory

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsFilesGetPartialsDirectory();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesGetPartialsDirectory: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetPartialsDirectory**](ResGetPartialsDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesGetPrimaryBannedFilesList**
> ResGetPrimaryBannedFilesList rsFilesGetPrimaryBannedFilesList()

Get list of banned files

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsFilesGetPrimaryBannedFilesList();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesGetPrimaryBannedFilesList: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetPrimaryBannedFilesList**](ResGetPrimaryBannedFilesList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesGetSharedDirectories**
> ResGetSharedDirectories rsFilesGetSharedDirectories()

Get list of current shared directories

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsFilesGetSharedDirectories();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesGetSharedDirectories: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetSharedDirectories**](ResGetSharedDirectories.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesIsHashBanned**
> ResIsHashBanned rsFilesIsHashBanned(reqIsHashBanned)

Check if a file is on banned list

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqIsHashBanned = ReqIsHashBanned(); // ReqIsHashBanned | hash: >              (RsFileHash)hash of the file  

try { 
    var result = api_instance.rsFilesIsHashBanned(reqIsHashBanned);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesIsHashBanned: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqIsHashBanned** | [**ReqIsHashBanned**](ReqIsHashBanned.md)| hash: &gt;              (RsFileHash)hash of the file   | [optional] 

### Return type

[**ResIsHashBanned**](ResIsHashBanned.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesParseFilesLink**
> ResParseFilesLink rsFilesParseFilesLink(reqParseFilesLink)

Parse RetroShare files link

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqParseFilesLink = ReqParseFilesLink(); // ReqParseFilesLink | link: >              (string)files link either in base64 or URL format  

try { 
    var result = api_instance.rsFilesParseFilesLink(reqParseFilesLink);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesParseFilesLink: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqParseFilesLink** | [**ReqParseFilesLink**](ReqParseFilesLink.md)| link: &gt;              (string)files link either in base64 or URL format   | [optional] 

### Return type

[**ResParseFilesLink**](ResParseFilesLink.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesRemoveSharedDirectory**
> ResRemoveSharedDirectory rsFilesRemoveSharedDirectory(reqRemoveSharedDirectory)

Remove directory from shared list

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRemoveSharedDirectory = ReqRemoveSharedDirectory(); // ReqRemoveSharedDirectory | dir: >              (string)Path of the directory to remove from shared list  

try { 
    var result = api_instance.rsFilesRemoveSharedDirectory(reqRemoveSharedDirectory);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesRemoveSharedDirectory: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRemoveSharedDirectory** | [**ReqRemoveSharedDirectory**](ReqRemoveSharedDirectory.md)| dir: &gt;              (string)Path of the directory to remove from shared list   | [optional] 

### Return type

[**ResRemoveSharedDirectory**](ResRemoveSharedDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesRequestDirDetails**
> ResRequestDirDetails rsFilesRequestDirDetails(reqRequestDirDetails)

Request directory details, subsequent multiple call may be used to explore a whole directory tree.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRequestDirDetails = ReqRequestDirDetails(); // ReqRequestDirDetails | handle: >              (integer64)element handle 0 for root, pass the content of DirDetails::child[x].ref after first call to explore deeper, be aware that is not a real pointer but an index used internally by RetroShare.          flags: >              (FileSearchFlags)file search flags RS_FILE_HINTS_*  

try { 
    var result = api_instance.rsFilesRequestDirDetails(reqRequestDirDetails);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesRequestDirDetails: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRequestDirDetails** | [**ReqRequestDirDetails**](ReqRequestDirDetails.md)| handle: &gt;              (integer64)element handle 0 for root, pass the content of DirDetails::child[x].ref after first call to explore deeper, be aware that is not a real pointer but an index used internally by RetroShare.          flags: &gt;              (FileSearchFlags)file search flags RS_FILE_HINTS_*   | [optional] 

### Return type

[**ResRequestDirDetails**](ResRequestDirDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesRequestFiles**
> ResRequestFiles rsFilesRequestFiles(reqRequestFiles)

Initiate download of a files collection

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRequestFiles = ReqRequestFiles(); // ReqRequestFiles | collection: >              (RsFileTree)collection of files to download          destPath: >              (string)optional base path on which to download the collection, if left empty the default download directory will be used          srcIds: >              (vector<RsPeerId>)optional peers id known as direct source of the collection          flags: >              (FileRequestFlags)optional flags to fine tune search and download algorithm  

try { 
    var result = api_instance.rsFilesRequestFiles(reqRequestFiles);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesRequestFiles: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRequestFiles** | [**ReqRequestFiles**](ReqRequestFiles.md)| collection: &gt;              (RsFileTree)collection of files to download          destPath: &gt;              (string)optional base path on which to download the collection, if left empty the default download directory will be used          srcIds: &gt;              (vector&lt;RsPeerId&gt;)optional peers id known as direct source of the collection          flags: &gt;              (FileRequestFlags)optional flags to fine tune search and download algorithm   | [optional] 

### Return type

[**ResRequestFiles**](ResRequestFiles.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesSetChunkStrategy**
> ResSetChunkStrategy rsFilesSetChunkStrategy(reqSetChunkStrategy)

Set chunk strategy for file, useful to set streaming mode to be able of see video or other media preview while it is still downloading

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetChunkStrategy = ReqSetChunkStrategy(); // ReqSetChunkStrategy | hash: >              (RsFileHash)file identifier          newStrategy: >              (FileChunksInfo_ChunkStrategy)None 

try { 
    var result = api_instance.rsFilesSetChunkStrategy(reqSetChunkStrategy);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesSetChunkStrategy: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetChunkStrategy** | [**ReqSetChunkStrategy**](ReqSetChunkStrategy.md)| hash: &gt;              (RsFileHash)file identifier          newStrategy: &gt;              (FileChunksInfo_ChunkStrategy)None  | [optional] 

### Return type

[**ResSetChunkStrategy**](ResSetChunkStrategy.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesSetDefaultChunkStrategy**
> rsFilesSetDefaultChunkStrategy(reqSetDefaultChunkStrategy)

Set default chunk strategy

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetDefaultChunkStrategy = ReqSetDefaultChunkStrategy(); // ReqSetDefaultChunkStrategy | strategy: >              (FileChunksInfo_ChunkStrategy)None 

try { 
    api_instance.rsFilesSetDefaultChunkStrategy(reqSetDefaultChunkStrategy);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesSetDefaultChunkStrategy: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetDefaultChunkStrategy** | [**ReqSetDefaultChunkStrategy**](ReqSetDefaultChunkStrategy.md)| strategy: &gt;              (FileChunksInfo_ChunkStrategy)None  | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesSetDestinationDirectory**
> ResSetDestinationDirectory rsFilesSetDestinationDirectory(reqSetDestinationDirectory)

Set destination directory for given file

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetDestinationDirectory = ReqSetDestinationDirectory(); // ReqSetDestinationDirectory | hash: >              (RsFileHash)file identifier          newPath: >              (string)None 

try { 
    var result = api_instance.rsFilesSetDestinationDirectory(reqSetDestinationDirectory);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesSetDestinationDirectory: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetDestinationDirectory** | [**ReqSetDestinationDirectory**](ReqSetDestinationDirectory.md)| hash: &gt;              (RsFileHash)file identifier          newPath: &gt;              (string)None  | [optional] 

### Return type

[**ResSetDestinationDirectory**](ResSetDestinationDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesSetDestinationName**
> ResSetDestinationName rsFilesSetDestinationName(reqSetDestinationName)

Set name for dowloaded file

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetDestinationName = ReqSetDestinationName(); // ReqSetDestinationName | hash: >              (RsFileHash)file identifier          newName: >              (string)None 

try { 
    var result = api_instance.rsFilesSetDestinationName(reqSetDestinationName);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesSetDestinationName: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetDestinationName** | [**ReqSetDestinationName**](ReqSetDestinationName.md)| hash: &gt;              (RsFileHash)file identifier          newName: &gt;              (string)None  | [optional] 

### Return type

[**ResSetDestinationName**](ResSetDestinationName.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesSetDownloadDirectory**
> ResSetDownloadDirectory rsFilesSetDownloadDirectory(reqSetDownloadDirectory)

Set default complete downloads directory

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetDownloadDirectory = ReqSetDownloadDirectory(); // ReqSetDownloadDirectory | path: >              (string)directory path  

try { 
    var result = api_instance.rsFilesSetDownloadDirectory(reqSetDownloadDirectory);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesSetDownloadDirectory: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetDownloadDirectory** | [**ReqSetDownloadDirectory**](ReqSetDownloadDirectory.md)| path: &gt;              (string)directory path   | [optional] 

### Return type

[**ResSetDownloadDirectory**](ResSetDownloadDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesSetFreeDiskSpaceLimit**
> rsFilesSetFreeDiskSpaceLimit(reqSetFreeDiskSpaceLimit)

Set minimum free disk space limit

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetFreeDiskSpaceLimit = ReqSetFreeDiskSpaceLimit(); // ReqSetFreeDiskSpaceLimit | minimumFreeMB: >              (integer)minimum free space in MB  

try { 
    api_instance.rsFilesSetFreeDiskSpaceLimit(reqSetFreeDiskSpaceLimit);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesSetFreeDiskSpaceLimit: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetFreeDiskSpaceLimit** | [**ReqSetFreeDiskSpaceLimit**](ReqSetFreeDiskSpaceLimit.md)| minimumFreeMB: &gt;              (integer)minimum free space in MB   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesSetPartialsDirectory**
> ResSetPartialsDirectory rsFilesSetPartialsDirectory(reqSetPartialsDirectory)

Set partial downloads directory

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetPartialsDirectory = ReqSetPartialsDirectory(); // ReqSetPartialsDirectory | path: >              (string)directory path  

try { 
    var result = api_instance.rsFilesSetPartialsDirectory(reqSetPartialsDirectory);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesSetPartialsDirectory: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetPartialsDirectory** | [**ReqSetPartialsDirectory**](ReqSetPartialsDirectory.md)| path: &gt;              (string)directory path   | [optional] 

### Return type

[**ResSetPartialsDirectory**](ResSetPartialsDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesSetSharedDirectories**
> ResSetSharedDirectories rsFilesSetSharedDirectories(reqSetSharedDirectories)

Set shared directories

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetSharedDirectories = ReqSetSharedDirectories(); // ReqSetSharedDirectories | dirs: >              (list<SharedDirInfo>)list of shared directories with share options  

try { 
    var result = api_instance.rsFilesSetSharedDirectories(reqSetSharedDirectories);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesSetSharedDirectories: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetSharedDirectories** | [**ReqSetSharedDirectories**](ReqSetSharedDirectories.md)| dirs: &gt;              (list&lt;SharedDirInfo&gt;)list of shared directories with share options   | [optional] 

### Return type

[**ResSetSharedDirectories**](ResSetSharedDirectories.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesTurtleSearchRequest**
> ResTurtleSearchRequest rsFilesTurtleSearchRequest(reqTurtleSearchRequest)

This method is asynchronous. Request remote files search

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqTurtleSearchRequest = ReqTurtleSearchRequest(); // ReqTurtleSearchRequest | matchString: >              (string)string to look for in the search. If files deep indexing is enabled at compile time support advanced features described at          maxWait: >              (rstime_t)maximum wait time in seconds for search results  

try { 
    var result = api_instance.rsFilesTurtleSearchRequest(reqTurtleSearchRequest);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesTurtleSearchRequest: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqTurtleSearchRequest** | [**ReqTurtleSearchRequest**](ReqTurtleSearchRequest.md)| matchString: &gt;              (string)string to look for in the search. If files deep indexing is enabled at compile time support advanced features described at          maxWait: &gt;              (rstime_t)maximum wait time in seconds for search results   | [optional] 

### Return type

[**ResTurtleSearchRequest**](ResTurtleSearchRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesUnbanFile**
> ResUnbanFile rsFilesUnbanFile(reqUnbanFile)

Remove file from unwanted list

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqUnbanFile = ReqUnbanFile(); // ReqUnbanFile | realFileHash: >              (RsFileHash)hash of the file  

try { 
    var result = api_instance.rsFilesUnbanFile(reqUnbanFile);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesUnbanFile: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqUnbanFile** | [**ReqUnbanFile**](ReqUnbanFile.md)| realFileHash: &gt;              (RsFileHash)hash of the file   | [optional] 

### Return type

[**ResUnbanFile**](ResUnbanFile.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsFilesUpdateShareFlags**
> ResUpdateShareFlags rsFilesUpdateShareFlags(reqUpdateShareFlags)

Updates shared directory sharing flags. The directory should be already shared!

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqUpdateShareFlags = ReqUpdateShareFlags(); // ReqUpdateShareFlags | dir: >              (SharedDirInfo)Shared directory with updated sharing options  

try { 
    var result = api_instance.rsFilesUpdateShareFlags(reqUpdateShareFlags);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsFilesUpdateShareFlags: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqUpdateShareFlags** | [**ReqUpdateShareFlags**](ReqUpdateShareFlags.md)| dir: &gt;              (SharedDirInfo)Shared directory with updated sharing options   | [optional] 

### Return type

[**ResUpdateShareFlags**](ResUpdateShareFlags.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGossipDiscoveryGetDiscFriends**
> ResGetDiscFriends rsGossipDiscoveryGetDiscFriends(reqGetDiscFriends)

getDiscFriends get a list of all friends of a given friend

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetDiscFriends = ReqGetDiscFriends(); // ReqGetDiscFriends | id: >              (RsPeerId)peer to get the friends of  

try { 
    var result = api_instance.rsGossipDiscoveryGetDiscFriends(reqGetDiscFriends);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGossipDiscoveryGetDiscFriends: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetDiscFriends** | [**ReqGetDiscFriends**](ReqGetDiscFriends.md)| id: &gt;              (RsPeerId)peer to get the friends of   | [optional] 

### Return type

[**ResGetDiscFriends**](ResGetDiscFriends.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGossipDiscoveryGetDiscPgpFriends**
> ResGetDiscPgpFriends rsGossipDiscoveryGetDiscPgpFriends(reqGetDiscPgpFriends)

getDiscPgpFriends get a list of all friends of a given friend

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetDiscPgpFriends = ReqGetDiscPgpFriends(); // ReqGetDiscPgpFriends | pgpid: >              (RsPgpId)peer to get the friends of  

try { 
    var result = api_instance.rsGossipDiscoveryGetDiscPgpFriends(reqGetDiscPgpFriends);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGossipDiscoveryGetDiscPgpFriends: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetDiscPgpFriends** | [**ReqGetDiscPgpFriends**](ReqGetDiscPgpFriends.md)| pgpid: &gt;              (RsPgpId)peer to get the friends of   | [optional] 

### Return type

[**ResGetDiscPgpFriends**](ResGetDiscPgpFriends.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGossipDiscoveryGetPeerVersion**
> ResGetPeerVersion rsGossipDiscoveryGetPeerVersion(reqGetPeerVersion)

getPeerVersion get the version string of a peer.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetPeerVersion = ReqGetPeerVersion(); // ReqGetPeerVersion | id: >              (RsPeerId)peer to get the version string of  

try { 
    var result = api_instance.rsGossipDiscoveryGetPeerVersion(reqGetPeerVersion);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGossipDiscoveryGetPeerVersion: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetPeerVersion** | [**ReqGetPeerVersion**](ReqGetPeerVersion.md)| id: &gt;              (RsPeerId)peer to get the version string of   | [optional] 

### Return type

[**ResGetPeerVersion**](ResGetPeerVersion.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGossipDiscoveryGetWaitingDiscCount**
> ResGetWaitingDiscCount rsGossipDiscoveryGetWaitingDiscCount()

getWaitingDiscCount get the number of queued discovery packets.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsGossipDiscoveryGetWaitingDiscCount();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGossipDiscoveryGetWaitingDiscCount: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetWaitingDiscCount**](ResGetWaitingDiscCount.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsCreateChannel**
> ResCreateChannel rsGxsChannelsCreateChannel(reqCreateChannel)

Deprecated{ substituted by createChannelV2 }

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateChannel = ReqCreateChannel(); // ReqCreateChannel | channel: >              (RsGxsChannelGroup)Channel data (name, description...)  

try { 
    var result = api_instance.rsGxsChannelsCreateChannel(reqCreateChannel);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsCreateChannel: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateChannel** | [**ReqCreateChannel**](ReqCreateChannel.md)| channel: &gt;              (RsGxsChannelGroup)Channel data (name, description...)   | [optional] 

### Return type

[**ResCreateChannel**](ResCreateChannel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsCreateChannelV2**
> ResCreateChannelV2 rsGxsChannelsCreateChannelV2(reqCreateChannelV2)

Create channel. Blocking API.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateChannelV2 = ReqCreateChannelV2(); // ReqCreateChannelV2 | name: >              (string)Name of the channel          description: >              (string)Description of the channel          thumbnail: >              (RsGxsImage)Optional image to show as channel thumbnail.          authorId: >              (RsGxsId)Optional id of the author. Leave empty for an anonymous channel.          circleType: >              (RsGxsCircleType)Optional visibility rule, default public.          circleId: >              (RsGxsCircleId)If the channel is not public specify the id of the circle who can see the channel. Depending on the value you pass for circleType this should be be an external circle if EXTERNAL is passed, a local friend group id if NODES_GROUP is passed, empty otherwise.  

try { 
    var result = api_instance.rsGxsChannelsCreateChannelV2(reqCreateChannelV2);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsCreateChannelV2: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateChannelV2** | [**ReqCreateChannelV2**](ReqCreateChannelV2.md)| name: &gt;              (string)Name of the channel          description: &gt;              (string)Description of the channel          thumbnail: &gt;              (RsGxsImage)Optional image to show as channel thumbnail.          authorId: &gt;              (RsGxsId)Optional id of the author. Leave empty for an anonymous channel.          circleType: &gt;              (RsGxsCircleType)Optional visibility rule, default public.          circleId: &gt;              (RsGxsCircleId)If the channel is not public specify the id of the circle who can see the channel. Depending on the value you pass for circleType this should be be an external circle if EXTERNAL is passed, a local friend group id if NODES_GROUP is passed, empty otherwise.   | [optional] 

### Return type

[**ResCreateChannelV2**](ResCreateChannelV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsCreateComment**
> ResCreateComment rsGxsChannelsCreateComment(reqCreateComment)

Deprecated

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateComment = ReqCreateComment(); // ReqCreateComment | comment: >              (RsGxsComment)None 

try { 
    var result = api_instance.rsGxsChannelsCreateComment(reqCreateComment);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsCreateComment: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateComment** | [**ReqCreateComment**](ReqCreateComment.md)| comment: &gt;              (RsGxsComment)None  | [optional] 

### Return type

[**ResCreateComment**](ResCreateComment.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsCreateCommentV2**
> ResCreateCommentV2 rsGxsChannelsCreateCommentV2(reqCreateCommentV2)

Add a comment on a post or on another comment. Blocking API.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateCommentV2 = ReqCreateCommentV2(); // ReqCreateCommentV2 | channelId: >              (RsGxsGroupId)Id of the channel in which the comment is to be posted          threadId: >              (RsGxsMessageId)Id of the post (that is a thread) in the channel where the comment is placed          comment: >              (string)UTF-8 string containing the comment itself          authorId: >              (RsGxsId)Id of the author of the comment          parentId: >              (RsGxsMessageId)Id of the parent of the comment that is either a channel post Id or the Id of another comment.          origCommentId: >              (RsGxsMessageId)If this is supposed to replace an already existent comment, the id of the old post. If left blank a new post will be created.  

try { 
    var result = api_instance.rsGxsChannelsCreateCommentV2(reqCreateCommentV2);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsCreateCommentV2: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateCommentV2** | [**ReqCreateCommentV2**](ReqCreateCommentV2.md)| channelId: &gt;              (RsGxsGroupId)Id of the channel in which the comment is to be posted          threadId: &gt;              (RsGxsMessageId)Id of the post (that is a thread) in the channel where the comment is placed          comment: &gt;              (string)UTF-8 string containing the comment itself          authorId: &gt;              (RsGxsId)Id of the author of the comment          parentId: &gt;              (RsGxsMessageId)Id of the parent of the comment that is either a channel post Id or the Id of another comment.          origCommentId: &gt;              (RsGxsMessageId)If this is supposed to replace an already existent comment, the id of the old post. If left blank a new post will be created.   | [optional] 

### Return type

[**ResCreateCommentV2**](ResCreateCommentV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsCreatePost**
> ResCreatePost rsGxsChannelsCreatePost(reqCreatePost)

Deprecated

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreatePost = ReqCreatePost(); // ReqCreatePost | post: >              (RsGxsChannelPost)None 

try { 
    var result = api_instance.rsGxsChannelsCreatePost(reqCreatePost);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsCreatePost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreatePost** | [**ReqCreatePost**](ReqCreatePost.md)| post: &gt;              (RsGxsChannelPost)None  | [optional] 

### Return type

[**ResCreatePost**](ResCreatePost.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsCreatePostV2**
> ResCreatePostV2 rsGxsChannelsCreatePostV2(reqCreatePostV2)

Create channel post. Blocking API.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreatePostV2 = ReqCreatePostV2(); // ReqCreatePostV2 | channelId: >              (RsGxsGroupId)Id of the channel where to put the post. Beware you need publish rights on that channel to post.          title: >              (string)Title of the post          mBody: >              (string)Text content of the post          files: >              (list<RsGxsFile>)Optional list of attached files. These are supposed to be already shared,          thumbnail: >              (RsGxsImage)Optional thumbnail image for the post.          origPostId: >              (RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created.  

try { 
    var result = api_instance.rsGxsChannelsCreatePostV2(reqCreatePostV2);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsCreatePostV2: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreatePostV2** | [**ReqCreatePostV2**](ReqCreatePostV2.md)| channelId: &gt;              (RsGxsGroupId)Id of the channel where to put the post. Beware you need publish rights on that channel to post.          title: &gt;              (string)Title of the post          mBody: &gt;              (string)Text content of the post          files: &gt;              (list&lt;RsGxsFile&gt;)Optional list of attached files. These are supposed to be already shared,          thumbnail: &gt;              (RsGxsImage)Optional thumbnail image for the post.          origPostId: &gt;              (RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created.   | [optional] 

### Return type

[**ResCreatePostV2**](ResCreatePostV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsCreateVote**
> ResCreateVote rsGxsChannelsCreateVote(reqCreateVote)

Deprecated

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateVote = ReqCreateVote(); // ReqCreateVote | vote: >              (RsGxsVote)None 

try { 
    var result = api_instance.rsGxsChannelsCreateVote(reqCreateVote);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsCreateVote: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateVote** | [**ReqCreateVote**](ReqCreateVote.md)| vote: &gt;              (RsGxsVote)None  | [optional] 

### Return type

[**ResCreateVote**](ResCreateVote.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsCreateVoteV2**
> ResCreateVoteV2 rsGxsChannelsCreateVoteV2(reqCreateVoteV2)

Create a vote

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateVoteV2 = ReqCreateVoteV2(); // ReqCreateVoteV2 | channelId: >              (RsGxsGroupId)Id of the channel where to vote          postId: >              (RsGxsMessageId)Id of the channel post of which a comment is voted.          commentId: >              (RsGxsMessageId)Id of the comment that is voted          authorId: >              (RsGxsId)Id of the author. Needs to be of an owned identity.          vote: >              (RsGxsVoteType)Vote value, either  

try { 
    var result = api_instance.rsGxsChannelsCreateVoteV2(reqCreateVoteV2);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsCreateVoteV2: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateVoteV2** | [**ReqCreateVoteV2**](ReqCreateVoteV2.md)| channelId: &gt;              (RsGxsGroupId)Id of the channel where to vote          postId: &gt;              (RsGxsMessageId)Id of the channel post of which a comment is voted.          commentId: &gt;              (RsGxsMessageId)Id of the comment that is voted          authorId: &gt;              (RsGxsId)Id of the author. Needs to be of an owned identity.          vote: &gt;              (RsGxsVoteType)Vote value, either   | [optional] 

### Return type

[**ResCreateVoteV2**](ResCreateVoteV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsEditChannel**
> ResEditChannel rsGxsChannelsEditChannel(reqEditChannel)

Edit channel details.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqEditChannel = ReqEditChannel(); // ReqEditChannel | channel: >              (RsGxsChannelGroup)Channel data (name, description...) with modifications  

try { 
    var result = api_instance.rsGxsChannelsEditChannel(reqEditChannel);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsEditChannel: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqEditChannel** | [**ReqEditChannel**](ReqEditChannel.md)| channel: &gt;              (RsGxsChannelGroup)Channel data (name, description...) with modifications   | [optional] 

### Return type

[**ResEditChannel**](ResEditChannel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsExportChannelLink**
> ResExportChannelLink rsGxsChannelsExportChannelLink(reqExportChannelLink)

Get link to a channel

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExportChannelLink = ReqExportChannelLink(); // ReqExportChannelLink | chanId: >              (RsGxsGroupId)Id of the channel of which we want to generate a link          includeGxsData: >              (boolean)if true include the channel GXS group data so the receiver can subscribe to the channel even if she hasn't received it through GXS yet          baseUrl: >              (string)URL into which to sneak in the RetroShare link radix, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare link into a \"normal\" looking web link. If empty the GXS data link will be outputted in plain base64 format.  

try { 
    var result = api_instance.rsGxsChannelsExportChannelLink(reqExportChannelLink);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsExportChannelLink: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExportChannelLink** | [**ReqExportChannelLink**](ReqExportChannelLink.md)| chanId: &gt;              (RsGxsGroupId)Id of the channel of which we want to generate a link          includeGxsData: &gt;              (boolean)if true include the channel GXS group data so the receiver can subscribe to the channel even if she hasn&#39;t received it through GXS yet          baseUrl: &gt;              (string)URL into which to sneak in the RetroShare link radix, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare link into a \&quot;normal\&quot; looking web link. If empty the GXS data link will be outputted in plain base64 format.   | [optional] 

### Return type

[**ResExportChannelLink**](ResExportChannelLink.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsExtraFileHash**
> ResExtraFileHash rsGxsChannelsExtraFileHash(reqExtraFileHash)

Share extra file Can be used to share extra file attached to a channel post

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExtraFileHash = ReqExtraFileHash(); // ReqExtraFileHash | path: >              (string)file path  

try { 
    var result = api_instance.rsGxsChannelsExtraFileHash(reqExtraFileHash);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsExtraFileHash: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExtraFileHash** | [**ReqExtraFileHash**](ReqExtraFileHash.md)| path: &gt;              (string)file path   | [optional] 

### Return type

[**ResExtraFileHash**](ResExtraFileHash.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsExtraFileRemove**
> ResExtraFileRemove rsGxsChannelsExtraFileRemove(reqExtraFileRemove)

Remove extra file from shared files

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExtraFileRemove = ReqExtraFileRemove(); // ReqExtraFileRemove | hash: >              (RsFileHash)hash of the file to remove  

try { 
    var result = api_instance.rsGxsChannelsExtraFileRemove(reqExtraFileRemove);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsExtraFileRemove: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExtraFileRemove** | [**ReqExtraFileRemove**](ReqExtraFileRemove.md)| hash: &gt;              (RsFileHash)hash of the file to remove   | [optional] 

### Return type

[**ResExtraFileRemove**](ResExtraFileRemove.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsGetChannelAllContent**
> ResGetChannelAllContent rsGxsChannelsGetChannelAllContent(reqGetChannelAllContent)

Get all channel messages and comments in a given channel

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetChannelAllContent = ReqGetChannelAllContent(); // ReqGetChannelAllContent | channelId: >              (RsGxsGroupId)id of the channel of which the content is requested  

try { 
    var result = api_instance.rsGxsChannelsGetChannelAllContent(reqGetChannelAllContent);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsGetChannelAllContent: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetChannelAllContent** | [**ReqGetChannelAllContent**](ReqGetChannelAllContent.md)| channelId: &gt;              (RsGxsGroupId)id of the channel of which the content is requested   | [optional] 

### Return type

[**ResGetChannelAllContent**](ResGetChannelAllContent.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsGetChannelAutoDownload**
> ResGetChannelAutoDownload rsGxsChannelsGetChannelAutoDownload(reqGetChannelAutoDownload)

DeprecatedThis feature rely on very buggy code, the returned value is not reliable

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetChannelAutoDownload = ReqGetChannelAutoDownload(); // ReqGetChannelAutoDownload | channelId: >              (RsGxsGroupId)channel id  

try { 
    var result = api_instance.rsGxsChannelsGetChannelAutoDownload(reqGetChannelAutoDownload);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsGetChannelAutoDownload: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetChannelAutoDownload** | [**ReqGetChannelAutoDownload**](ReqGetChannelAutoDownload.md)| channelId: &gt;              (RsGxsGroupId)channel id   | [optional] 

### Return type

[**ResGetChannelAutoDownload**](ResGetChannelAutoDownload.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsGetChannelComments**
> ResGetChannelComments rsGxsChannelsGetChannelComments(reqGetChannelComments)

Get channel comments corresponding to the given IDs. If the set is empty, nothing is returned.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetChannelComments = ReqGetChannelComments(); // ReqGetChannelComments | channelId: >              (RsGxsGroupId)id of the channel of which the content is requested          contentIds: >              (set<RsGxsMessageId>)ids of requested contents  

try { 
    var result = api_instance.rsGxsChannelsGetChannelComments(reqGetChannelComments);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsGetChannelComments: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetChannelComments** | [**ReqGetChannelComments**](ReqGetChannelComments.md)| channelId: &gt;              (RsGxsGroupId)id of the channel of which the content is requested          contentIds: &gt;              (set&lt;RsGxsMessageId&gt;)ids of requested contents   | [optional] 

### Return type

[**ResGetChannelComments**](ResGetChannelComments.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsGetChannelContent**
> ResGetChannelContent rsGxsChannelsGetChannelContent(reqGetChannelContent)

Get channel messages and comments corresponding to the given IDs. If the set is empty, nothing is returned.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetChannelContent = ReqGetChannelContent(); // ReqGetChannelContent | channelId: >              (RsGxsGroupId)id of the channel of which the content is requested          contentsIds: >              (set<RsGxsMessageId>)ids of requested contents  

try { 
    var result = api_instance.rsGxsChannelsGetChannelContent(reqGetChannelContent);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsGetChannelContent: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetChannelContent** | [**ReqGetChannelContent**](ReqGetChannelContent.md)| channelId: &gt;              (RsGxsGroupId)id of the channel of which the content is requested          contentsIds: &gt;              (set&lt;RsGxsMessageId&gt;)ids of requested contents   | [optional] 

### Return type

[**ResGetChannelContent**](ResGetChannelContent.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsGetChannelDownloadDirectory**
> ResGetChannelDownloadDirectory rsGxsChannelsGetChannelDownloadDirectory(reqGetChannelDownloadDirectory)

Deprecated

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetChannelDownloadDirectory = ReqGetChannelDownloadDirectory(); // ReqGetChannelDownloadDirectory | channelId: >              (RsGxsGroupId)id of the channel  

try { 
    var result = api_instance.rsGxsChannelsGetChannelDownloadDirectory(reqGetChannelDownloadDirectory);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsGetChannelDownloadDirectory: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetChannelDownloadDirectory** | [**ReqGetChannelDownloadDirectory**](ReqGetChannelDownloadDirectory.md)| channelId: &gt;              (RsGxsGroupId)id of the channel   | [optional] 

### Return type

[**ResGetChannelDownloadDirectory**](ResGetChannelDownloadDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsGetChannelServiceStatistics**
> ResGetChannelServiceStatistics rsGxsChannelsGetChannelServiceStatistics()

Retrieve statistics about the channel service

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsGxsChannelsGetChannelServiceStatistics();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsGetChannelServiceStatistics: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetChannelServiceStatistics**](ResGetChannelServiceStatistics.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsGetChannelStatistics**
> ResGetChannelStatistics rsGxsChannelsGetChannelStatistics(reqGetChannelStatistics)

Retrieve statistics about the given channel

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetChannelStatistics = ReqGetChannelStatistics(); // ReqGetChannelStatistics | channelId: >              (RsGxsGroupId)Id of the channel group  

try { 
    var result = api_instance.rsGxsChannelsGetChannelStatistics(reqGetChannelStatistics);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsGetChannelStatistics: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetChannelStatistics** | [**ReqGetChannelStatistics**](ReqGetChannelStatistics.md)| channelId: &gt;              (RsGxsGroupId)Id of the channel group   | [optional] 

### Return type

[**ResGetChannelStatistics**](ResGetChannelStatistics.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsGetChannelsInfo**
> ResGetChannelsInfo rsGxsChannelsGetChannelsInfo(reqGetChannelsInfo)

Get channels information (description, thumbnail...). Blocking API.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetChannelsInfo = ReqGetChannelsInfo(); // ReqGetChannelsInfo | chanIds: >              (list<RsGxsGroupId>)ids of the channels of which to get the informations  

try { 
    var result = api_instance.rsGxsChannelsGetChannelsInfo(reqGetChannelsInfo);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsGetChannelsInfo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetChannelsInfo** | [**ReqGetChannelsInfo**](ReqGetChannelsInfo.md)| chanIds: &gt;              (list&lt;RsGxsGroupId&gt;)ids of the channels of which to get the informations   | [optional] 

### Return type

[**ResGetChannelsInfo**](ResGetChannelsInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsGetChannelsSummaries**
> ResGetChannelsSummaries rsGxsChannelsGetChannelsSummaries()

Get channels summaries list. Blocking API.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsGxsChannelsGetChannelsSummaries();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsGetChannelsSummaries: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetChannelsSummaries**](ResGetChannelsSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsGetContentSummaries**
> ResGetContentSummaries rsGxsChannelsGetContentSummaries(reqGetContentSummaries)

Get channel content summaries

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetContentSummaries = ReqGetContentSummaries(); // ReqGetContentSummaries | channelId: >              (RsGxsGroupId)id of the channel of which the content is requested  

try { 
    var result = api_instance.rsGxsChannelsGetContentSummaries(reqGetContentSummaries);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsGetContentSummaries: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetContentSummaries** | [**ReqGetContentSummaries**](ReqGetContentSummaries.md)| channelId: &gt;              (RsGxsGroupId)id of the channel of which the content is requested   | [optional] 

### Return type

[**ResGetContentSummaries**](ResGetContentSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsLocalSearchRequest**
> ResLocalSearchRequest rsGxsChannelsLocalSearchRequest(reqLocalSearchRequest)

This method is asynchronous. Search local channels

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqLocalSearchRequest = ReqLocalSearchRequest(); // ReqLocalSearchRequest | matchString: >              (string)string to look for in the search          maxWait: >              (rstime_t)maximum wait time in seconds for search results  

try { 
    var result = api_instance.rsGxsChannelsLocalSearchRequest(reqLocalSearchRequest);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsLocalSearchRequest: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqLocalSearchRequest** | [**ReqLocalSearchRequest**](ReqLocalSearchRequest.md)| matchString: &gt;              (string)string to look for in the search          maxWait: &gt;              (rstime_t)maximum wait time in seconds for search results   | [optional] 

### Return type

[**ResLocalSearchRequest**](ResLocalSearchRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsMarkRead**
> ResMarkRead rsGxsChannelsMarkRead(reqMarkRead)

Toggle post read status. Blocking API.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqMarkRead = ReqMarkRead(); // ReqMarkRead | postId: >              (RsGxsGrpMsgIdPair)post identifier          read: >              (boolean)true to mark as read, false to mark as unread  

try { 
    var result = api_instance.rsGxsChannelsMarkRead(reqMarkRead);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsMarkRead: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqMarkRead** | [**ReqMarkRead**](ReqMarkRead.md)| postId: &gt;              (RsGxsGrpMsgIdPair)post identifier          read: &gt;              (boolean)true to mark as read, false to mark as unread   | [optional] 

### Return type

[**ResMarkRead**](ResMarkRead.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsRequestStatus**
> ResRequestStatus rsGxsChannelsRequestStatus(reqRequestStatus)

null

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRequestStatus = ReqRequestStatus(); // ReqRequestStatus | token: >              (integer)None 

try { 
    var result = api_instance.rsGxsChannelsRequestStatus(reqRequestStatus);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsRequestStatus: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRequestStatus** | [**ReqRequestStatus**](ReqRequestStatus.md)| token: &gt;              (integer)None  | [optional] 

### Return type

[**ResRequestStatus**](ResRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsSetChannelAutoDownload**
> ResSetChannelAutoDownload rsGxsChannelsSetChannelAutoDownload(reqSetChannelAutoDownload)

DeprecatedThis feature rely on very buggy code, when enabled the channel service start flooding erratically log with error messages, apparently without more dangerous consequences. Still those messages hints that something out of control is happening under the hood, use at your own risk. A safe alternative to this method can easly implemented at API client level instead.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetChannelAutoDownload = ReqSetChannelAutoDownload(); // ReqSetChannelAutoDownload | channelId: >              (RsGxsGroupId)channel id          enable: >              (boolean)true to enable, false to disable  

try { 
    var result = api_instance.rsGxsChannelsSetChannelAutoDownload(reqSetChannelAutoDownload);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsSetChannelAutoDownload: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetChannelAutoDownload** | [**ReqSetChannelAutoDownload**](ReqSetChannelAutoDownload.md)| channelId: &gt;              (RsGxsGroupId)channel id          enable: &gt;              (boolean)true to enable, false to disable   | [optional] 

### Return type

[**ResSetChannelAutoDownload**](ResSetChannelAutoDownload.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsSetChannelDownloadDirectory**
> ResSetChannelDownloadDirectory rsGxsChannelsSetChannelDownloadDirectory(reqSetChannelDownloadDirectory)

Deprecated

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetChannelDownloadDirectory = ReqSetChannelDownloadDirectory(); // ReqSetChannelDownloadDirectory | channelId: >              (RsGxsGroupId)id of the channel          directory: >              (string)path  

try { 
    var result = api_instance.rsGxsChannelsSetChannelDownloadDirectory(reqSetChannelDownloadDirectory);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsSetChannelDownloadDirectory: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetChannelDownloadDirectory** | [**ReqSetChannelDownloadDirectory**](ReqSetChannelDownloadDirectory.md)| channelId: &gt;              (RsGxsGroupId)id of the channel          directory: &gt;              (string)path   | [optional] 

### Return type

[**ResSetChannelDownloadDirectory**](ResSetChannelDownloadDirectory.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsShareChannelKeys**
> ResShareChannelKeys rsGxsChannelsShareChannelKeys(reqShareChannelKeys)

Share channel publishing key This can be used to authorize other peers to post on the channel

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqShareChannelKeys = ReqShareChannelKeys(); // ReqShareChannelKeys | channelId: >              (RsGxsGroupId)id of the channel          peers: >              (set<RsPeerId>)peers to share the key with  

try { 
    var result = api_instance.rsGxsChannelsShareChannelKeys(reqShareChannelKeys);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsShareChannelKeys: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqShareChannelKeys** | [**ReqShareChannelKeys**](ReqShareChannelKeys.md)| channelId: &gt;              (RsGxsGroupId)id of the channel          peers: &gt;              (set&lt;RsPeerId&gt;)peers to share the key with   | [optional] 

### Return type

[**ResShareChannelKeys**](ResShareChannelKeys.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsSubscribeToChannel**
> ResSubscribeToChannel rsGxsChannelsSubscribeToChannel(reqSubscribeToChannel)

Subscrbe to a channel. Blocking API

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSubscribeToChannel = ReqSubscribeToChannel(); // ReqSubscribeToChannel | channelId: >              (RsGxsGroupId)Channel id          subscribe: >              (boolean)true to subscribe, false to unsubscribe  

try { 
    var result = api_instance.rsGxsChannelsSubscribeToChannel(reqSubscribeToChannel);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsSubscribeToChannel: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSubscribeToChannel** | [**ReqSubscribeToChannel**](ReqSubscribeToChannel.md)| channelId: &gt;              (RsGxsGroupId)Channel id          subscribe: &gt;              (boolean)true to subscribe, false to unsubscribe   | [optional] 

### Return type

[**ResSubscribeToChannel**](ResSubscribeToChannel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsTurtleChannelRequest**
> ResTurtleChannelRequest rsGxsChannelsTurtleChannelRequest(reqTurtleChannelRequest)

This method is asynchronous. Request remote channel

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqTurtleChannelRequest = ReqTurtleChannelRequest(); // ReqTurtleChannelRequest | channelId: >              (RsGxsGroupId)id of the channel to request to distants peers          maxWait: >              (rstime_t)maximum wait time in seconds for search results  

try { 
    var result = api_instance.rsGxsChannelsTurtleChannelRequest(reqTurtleChannelRequest);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsTurtleChannelRequest: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqTurtleChannelRequest** | [**ReqTurtleChannelRequest**](ReqTurtleChannelRequest.md)| channelId: &gt;              (RsGxsGroupId)id of the channel to request to distants peers          maxWait: &gt;              (rstime_t)maximum wait time in seconds for search results   | [optional] 

### Return type

[**ResTurtleChannelRequest**](ResTurtleChannelRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsChannelsTurtleSearchRequest**
> ResTurtleSearchRequest rsGxsChannelsTurtleSearchRequest(reqTurtleSearchRequest)

This method is asynchronous. Request remote channels search

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqTurtleSearchRequest = ReqTurtleSearchRequest(); // ReqTurtleSearchRequest | matchString: >              (string)string to look for in the search          maxWait: >              (rstime_t)maximum wait time in seconds for search results  

try { 
    var result = api_instance.rsGxsChannelsTurtleSearchRequest(reqTurtleSearchRequest);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsChannelsTurtleSearchRequest: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqTurtleSearchRequest** | [**ReqTurtleSearchRequest**](ReqTurtleSearchRequest.md)| matchString: &gt;              (string)string to look for in the search          maxWait: &gt;              (rstime_t)maximum wait time in seconds for search results   | [optional] 

### Return type

[**ResTurtleSearchRequest**](ResTurtleSearchRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesCancelCircleMembership**
> ResCancelCircleMembership rsGxsCirclesCancelCircleMembership(reqCancelCircleMembership)

Leave given circle

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCancelCircleMembership = ReqCancelCircleMembership(); // ReqCancelCircleMembership | ownGxsId: >              (RsGxsId)Own id to remove from the circle          circleId: >              (RsGxsCircleId)Id of the circle to leave  

try { 
    var result = api_instance.rsGxsCirclesCancelCircleMembership(reqCancelCircleMembership);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesCancelCircleMembership: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCancelCircleMembership** | [**ReqCancelCircleMembership**](ReqCancelCircleMembership.md)| ownGxsId: &gt;              (RsGxsId)Own id to remove from the circle          circleId: &gt;              (RsGxsCircleId)Id of the circle to leave   | [optional] 

### Return type

[**ResCancelCircleMembership**](ResCancelCircleMembership.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesCreateCircle**
> ResCreateCircle rsGxsCirclesCreateCircle(reqCreateCircle)

Create new circle

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateCircle = ReqCreateCircle(); // ReqCreateCircle | circleName: >              (string)String containing cirlce name          circleType: >              (RsGxsCircleType)Circle type          restrictedId: >              (RsGxsCircleId)Optional id of a pre-existent circle that see the created circle. Meaningful only if circleType == EXTERNAL, must be null in all other cases.          authorId: >              (RsGxsId)Optional author of the circle.          gxsIdMembers: >              (set<RsGxsId>)GXS ids of the members of the circle.          localMembers: >              (set<RsPgpId>)PGP ids of the members if the circle.  

try { 
    var result = api_instance.rsGxsCirclesCreateCircle(reqCreateCircle);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesCreateCircle: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateCircle** | [**ReqCreateCircle**](ReqCreateCircle.md)| circleName: &gt;              (string)String containing cirlce name          circleType: &gt;              (RsGxsCircleType)Circle type          restrictedId: &gt;              (RsGxsCircleId)Optional id of a pre-existent circle that see the created circle. Meaningful only if circleType &#x3D;&#x3D; EXTERNAL, must be null in all other cases.          authorId: &gt;              (RsGxsId)Optional author of the circle.          gxsIdMembers: &gt;              (set&lt;RsGxsId&gt;)GXS ids of the members of the circle.          localMembers: &gt;              (set&lt;RsPgpId&gt;)PGP ids of the members if the circle.   | [optional] 

### Return type

[**ResCreateCircle**](ResCreateCircle.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesEditCircle**
> ResEditCircle rsGxsCirclesEditCircle(reqEditCircle)

Edit own existing circle

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqEditCircle = ReqEditCircle(); // ReqEditCircle | cData: >              (RsGxsCircleGroup)Circle data with modifications, storage for data updatedad during the operation.  

try { 
    var result = api_instance.rsGxsCirclesEditCircle(reqEditCircle);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesEditCircle: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqEditCircle** | [**ReqEditCircle**](ReqEditCircle.md)| cData: &gt;              (RsGxsCircleGroup)Circle data with modifications, storage for data updatedad during the operation.   | [optional] 

### Return type

[**ResEditCircle**](ResEditCircle.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesExportCircleLink**
> ResExportCircleLink rsGxsCirclesExportCircleLink(reqExportCircleLink)

Get link to a circle

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExportCircleLink = ReqExportCircleLink(); // ReqExportCircleLink | circleId: >              (RsGxsCircleId)Id of the circle of which we want to generate a link          includeGxsData: >              (boolean)if true include the circle GXS group data so the receiver can request circle membership even if the circle hasn't propagated through GXS to her yet          baseUrl: >              (string)URL into which to sneak in the RetroShare circle link radix, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare circle link into a \"normal\" looking web link. If empty the circle data link will be outputted in plain base64 format.  

try { 
    var result = api_instance.rsGxsCirclesExportCircleLink(reqExportCircleLink);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesExportCircleLink: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExportCircleLink** | [**ReqExportCircleLink**](ReqExportCircleLink.md)| circleId: &gt;              (RsGxsCircleId)Id of the circle of which we want to generate a link          includeGxsData: &gt;              (boolean)if true include the circle GXS group data so the receiver can request circle membership even if the circle hasn&#39;t propagated through GXS to her yet          baseUrl: &gt;              (string)URL into which to sneak in the RetroShare circle link radix, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare circle link into a \&quot;normal\&quot; looking web link. If empty the circle data link will be outputted in plain base64 format.   | [optional] 

### Return type

[**ResExportCircleLink**](ResExportCircleLink.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesGetCircleDetails**
> ResGetCircleDetails rsGxsCirclesGetCircleDetails(reqGetCircleDetails)

Get circle details. Memory cached

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetCircleDetails = ReqGetCircleDetails(); // ReqGetCircleDetails | id: >              (RsGxsCircleId)Id of the circle  

try { 
    var result = api_instance.rsGxsCirclesGetCircleDetails(reqGetCircleDetails);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesGetCircleDetails: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetCircleDetails** | [**ReqGetCircleDetails**](ReqGetCircleDetails.md)| id: &gt;              (RsGxsCircleId)Id of the circle   | [optional] 

### Return type

[**ResGetCircleDetails**](ResGetCircleDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesGetCircleExternalIdList**
> ResGetCircleExternalIdList rsGxsCirclesGetCircleExternalIdList(reqGetCircleExternalIdList)

Get list of known external circles ids. Memory cached

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetCircleExternalIdList = ReqGetCircleExternalIdList(); // ReqGetCircleExternalIdList | circleIds: >              (list<RsGxsCircleId>)Storage for circles id list  

try { 
    var result = api_instance.rsGxsCirclesGetCircleExternalIdList(reqGetCircleExternalIdList);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesGetCircleExternalIdList: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetCircleExternalIdList** | [**ReqGetCircleExternalIdList**](ReqGetCircleExternalIdList.md)| circleIds: &gt;              (list&lt;RsGxsCircleId&gt;)Storage for circles id list   | [optional] 

### Return type

[**ResGetCircleExternalIdList**](ResGetCircleExternalIdList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesGetCircleRequest**
> ResGetCircleRequest rsGxsCirclesGetCircleRequest(reqGetCircleRequest)

Get specific circle request

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetCircleRequest = ReqGetCircleRequest(); // ReqGetCircleRequest | circleId: >              (RsGxsGroupId)id of the circle of which the requests are requested          msgId: >              (RsGxsMessageId)id of the request  

try { 
    var result = api_instance.rsGxsCirclesGetCircleRequest(reqGetCircleRequest);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesGetCircleRequest: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetCircleRequest** | [**ReqGetCircleRequest**](ReqGetCircleRequest.md)| circleId: &gt;              (RsGxsGroupId)id of the circle of which the requests are requested          msgId: &gt;              (RsGxsMessageId)id of the request   | [optional] 

### Return type

[**ResGetCircleRequest**](ResGetCircleRequest.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesGetCircleRequests**
> ResGetCircleRequests rsGxsCirclesGetCircleRequests(reqGetCircleRequests)

Get circle requests

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetCircleRequests = ReqGetCircleRequests(); // ReqGetCircleRequests | circleId: >              (RsGxsGroupId)id of the circle of which the requests are requested  

try { 
    var result = api_instance.rsGxsCirclesGetCircleRequests(reqGetCircleRequests);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesGetCircleRequests: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetCircleRequests** | [**ReqGetCircleRequests**](ReqGetCircleRequests.md)| circleId: &gt;              (RsGxsGroupId)id of the circle of which the requests are requested   | [optional] 

### Return type

[**ResGetCircleRequests**](ResGetCircleRequests.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesGetCirclesInfo**
> ResGetCirclesInfo rsGxsCirclesGetCirclesInfo(reqGetCirclesInfo)

Get circles information

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetCirclesInfo = ReqGetCirclesInfo(); // ReqGetCirclesInfo | circlesIds: >              (list<RsGxsGroupId>)ids of the circles of which to get the informations  

try { 
    var result = api_instance.rsGxsCirclesGetCirclesInfo(reqGetCirclesInfo);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesGetCirclesInfo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetCirclesInfo** | [**ReqGetCirclesInfo**](ReqGetCirclesInfo.md)| circlesIds: &gt;              (list&lt;RsGxsGroupId&gt;)ids of the circles of which to get the informations   | [optional] 

### Return type

[**ResGetCirclesInfo**](ResGetCirclesInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesGetCirclesSummaries**
> ResGetCirclesSummaries rsGxsCirclesGetCirclesSummaries()

Get circles summaries list.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsGxsCirclesGetCirclesSummaries();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesGetCirclesSummaries: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetCirclesSummaries**](ResGetCirclesSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesInviteIdsToCircle**
> ResInviteIdsToCircle rsGxsCirclesInviteIdsToCircle(reqInviteIdsToCircle)

Invite identities to circle (admin key is required)

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqInviteIdsToCircle = ReqInviteIdsToCircle(); // ReqInviteIdsToCircle | identities: >              (set<RsGxsId>)ids of the identities to invite          circleId: >              (RsGxsCircleId)Id of the circle you own and want to invite ids in  

try { 
    var result = api_instance.rsGxsCirclesInviteIdsToCircle(reqInviteIdsToCircle);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesInviteIdsToCircle: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqInviteIdsToCircle** | [**ReqInviteIdsToCircle**](ReqInviteIdsToCircle.md)| identities: &gt;              (set&lt;RsGxsId&gt;)ids of the identities to invite          circleId: &gt;              (RsGxsCircleId)Id of the circle you own and want to invite ids in   | [optional] 

### Return type

[**ResInviteIdsToCircle**](ResInviteIdsToCircle.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesRequestCircleMembership**
> ResRequestCircleMembership rsGxsCirclesRequestCircleMembership(reqRequestCircleMembership)

Request circle membership, or accept circle invitation

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRequestCircleMembership = ReqRequestCircleMembership(); // ReqRequestCircleMembership | ownGxsId: >              (RsGxsId)Id of own identity to introduce to the circle          circleId: >              (RsGxsCircleId)Id of the circle to which ask for inclusion  

try { 
    var result = api_instance.rsGxsCirclesRequestCircleMembership(reqRequestCircleMembership);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesRequestCircleMembership: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRequestCircleMembership** | [**ReqRequestCircleMembership**](ReqRequestCircleMembership.md)| ownGxsId: &gt;              (RsGxsId)Id of own identity to introduce to the circle          circleId: &gt;              (RsGxsCircleId)Id of the circle to which ask for inclusion   | [optional] 

### Return type

[**ResRequestCircleMembership**](ResRequestCircleMembership.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesRequestStatus**
> ResRequestStatus rsGxsCirclesRequestStatus(reqRequestStatus)

null

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRequestStatus = ReqRequestStatus(); // ReqRequestStatus | token: >              (integer)None 

try { 
    var result = api_instance.rsGxsCirclesRequestStatus(reqRequestStatus);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesRequestStatus: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRequestStatus** | [**ReqRequestStatus**](ReqRequestStatus.md)| token: &gt;              (integer)None  | [optional] 

### Return type

[**ResRequestStatus**](ResRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsCirclesRevokeIdsFromCircle**
> ResRevokeIdsFromCircle rsGxsCirclesRevokeIdsFromCircle(reqRevokeIdsFromCircle)

Remove identities from circle (admin key is required)

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRevokeIdsFromCircle = ReqRevokeIdsFromCircle(); // ReqRevokeIdsFromCircle | identities: >              (set<RsGxsId>)ids of the identities to remove from the invite list          circleId: >              (RsGxsCircleId)Id of the circle you own and want to invite ids in  

try { 
    var result = api_instance.rsGxsCirclesRevokeIdsFromCircle(reqRevokeIdsFromCircle);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsCirclesRevokeIdsFromCircle: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRevokeIdsFromCircle** | [**ReqRevokeIdsFromCircle**](ReqRevokeIdsFromCircle.md)| identities: &gt;              (set&lt;RsGxsId&gt;)ids of the identities to remove from the invite list          circleId: &gt;              (RsGxsCircleId)Id of the circle you own and want to invite ids in   | [optional] 

### Return type

[**ResRevokeIdsFromCircle**](ResRevokeIdsFromCircle.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsCreateForum**
> ResCreateForum rsGxsForumsCreateForum(reqCreateForum)

Deprecated

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateForum = ReqCreateForum(); // ReqCreateForum | forum: >              (RsGxsForumGroup)Forum data (name, description...)  

try { 
    var result = api_instance.rsGxsForumsCreateForum(reqCreateForum);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsCreateForum: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateForum** | [**ReqCreateForum**](ReqCreateForum.md)| forum: &gt;              (RsGxsForumGroup)Forum data (name, description...)   | [optional] 

### Return type

[**ResCreateForum**](ResCreateForum.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsCreateForumV2**
> ResCreateForumV2 rsGxsForumsCreateForumV2(reqCreateForumV2)

Create forum.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateForumV2 = ReqCreateForumV2(); // ReqCreateForumV2 | name: >              (string)Name of the forum          description: >              (string)Optional description of the forum          authorId: >              (RsGxsId)Optional id of the froum owner author          moderatorsIds: >              (set<RsGxsId>)Optional list of forum moderators          circleType: >              (RsGxsCircleType)Optional visibility rule, default public.          circleId: >              (RsGxsCircleId)If the forum is not public specify the id of the circle who can see the forum. Depending on the value you pass for circleType this should be a circle if EXTERNAL is passed, a local friends group id if NODES_GROUP is passed, empty otherwise.  

try { 
    var result = api_instance.rsGxsForumsCreateForumV2(reqCreateForumV2);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsCreateForumV2: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateForumV2** | [**ReqCreateForumV2**](ReqCreateForumV2.md)| name: &gt;              (string)Name of the forum          description: &gt;              (string)Optional description of the forum          authorId: &gt;              (RsGxsId)Optional id of the froum owner author          moderatorsIds: &gt;              (set&lt;RsGxsId&gt;)Optional list of forum moderators          circleType: &gt;              (RsGxsCircleType)Optional visibility rule, default public.          circleId: &gt;              (RsGxsCircleId)If the forum is not public specify the id of the circle who can see the forum. Depending on the value you pass for circleType this should be a circle if EXTERNAL is passed, a local friends group id if NODES_GROUP is passed, empty otherwise.   | [optional] 

### Return type

[**ResCreateForumV2**](ResCreateForumV2.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsCreateMessage**
> ResCreateMessage rsGxsForumsCreateMessage(reqCreateMessage)

Deprecated

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateMessage = ReqCreateMessage(); // ReqCreateMessage | message: >              (RsGxsForumMsg)None 

try { 
    var result = api_instance.rsGxsForumsCreateMessage(reqCreateMessage);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsCreateMessage: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateMessage** | [**ReqCreateMessage**](ReqCreateMessage.md)| message: &gt;              (RsGxsForumMsg)None  | [optional] 

### Return type

[**ResCreateMessage**](ResCreateMessage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsCreatePost**
> ResCreatePost rsGxsForumsCreatePost(reqCreatePost)

Create a post on the given forum.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreatePost = ReqCreatePost(); // ReqCreatePost | forumId: >              (RsGxsGroupId)Id of the forum in which the post is to be submitted          title: >              (string)UTF-8 string containing the title of the post          mBody: >              (string)UTF-8 string containing the text of the post          authorId: >              (RsGxsId)Id of the author of the comment          parentId: >              (RsGxsMessageId)Optional Id of the parent post if this post is a reply to another post, empty otherwise.          origPostId: >              (RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created.  

try { 
    var result = api_instance.rsGxsForumsCreatePost(reqCreatePost);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsCreatePost: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreatePost** | [**ReqCreatePost**](ReqCreatePost.md)| forumId: &gt;              (RsGxsGroupId)Id of the forum in which the post is to be submitted          title: &gt;              (string)UTF-8 string containing the title of the post          mBody: &gt;              (string)UTF-8 string containing the text of the post          authorId: &gt;              (RsGxsId)Id of the author of the comment          parentId: &gt;              (RsGxsMessageId)Optional Id of the parent post if this post is a reply to another post, empty otherwise.          origPostId: &gt;              (RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created.   | [optional] 

### Return type

[**ResCreatePost**](ResCreatePost.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsEditForum**
> ResEditForum rsGxsForumsEditForum(reqEditForum)

Edit forum details.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqEditForum = ReqEditForum(); // ReqEditForum | forum: >              (RsGxsForumGroup)Forum data (name, description...) with modifications  

try { 
    var result = api_instance.rsGxsForumsEditForum(reqEditForum);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsEditForum: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqEditForum** | [**ReqEditForum**](ReqEditForum.md)| forum: &gt;              (RsGxsForumGroup)Forum data (name, description...) with modifications   | [optional] 

### Return type

[**ResEditForum**](ResEditForum.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsExportForumLink**
> ResExportForumLink rsGxsForumsExportForumLink(reqExportForumLink)

Get link to a forum

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExportForumLink = ReqExportForumLink(); // ReqExportForumLink | forumId: >              (RsGxsGroupId)Id of the forum of which we want to generate a link          includeGxsData: >              (boolean)if true include the forum GXS group data so the receiver can subscribe to the forum even if she hasn't received it through GXS yet          baseUrl: >              (string)URL into which to sneak in the RetroShare link radix, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare link into a \"normal\" looking web link. If empty the GXS data link will be outputted in plain base64 format.  

try { 
    var result = api_instance.rsGxsForumsExportForumLink(reqExportForumLink);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsExportForumLink: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExportForumLink** | [**ReqExportForumLink**](ReqExportForumLink.md)| forumId: &gt;              (RsGxsGroupId)Id of the forum of which we want to generate a link          includeGxsData: &gt;              (boolean)if true include the forum GXS group data so the receiver can subscribe to the forum even if she hasn&#39;t received it through GXS yet          baseUrl: &gt;              (string)URL into which to sneak in the RetroShare link radix, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare link into a \&quot;normal\&quot; looking web link. If empty the GXS data link will be outputted in plain base64 format.   | [optional] 

### Return type

[**ResExportForumLink**](ResExportForumLink.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsGetForumContent**
> ResGetForumContent rsGxsForumsGetForumContent(reqGetForumContent)

Get specific list of messages from a single forum. Blocking API

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetForumContent = ReqGetForumContent(); // ReqGetForumContent | forumId: >              (RsGxsGroupId)id of the forum of which the content is requested          msgsIds: >              (set<RsGxsMessageId>)list of message ids to request  

try { 
    var result = api_instance.rsGxsForumsGetForumContent(reqGetForumContent);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsGetForumContent: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetForumContent** | [**ReqGetForumContent**](ReqGetForumContent.md)| forumId: &gt;              (RsGxsGroupId)id of the forum of which the content is requested          msgsIds: &gt;              (set&lt;RsGxsMessageId&gt;)list of message ids to request   | [optional] 

### Return type

[**ResGetForumContent**](ResGetForumContent.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsGetForumMsgMetaData**
> ResGetForumMsgMetaData rsGxsForumsGetForumMsgMetaData(reqGetForumMsgMetaData)

Get message metadatas for a specific forum. Blocking API

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetForumMsgMetaData = ReqGetForumMsgMetaData(); // ReqGetForumMsgMetaData | forumId: >              (RsGxsGroupId)id of the forum of which the content is requested  

try { 
    var result = api_instance.rsGxsForumsGetForumMsgMetaData(reqGetForumMsgMetaData);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsGetForumMsgMetaData: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetForumMsgMetaData** | [**ReqGetForumMsgMetaData**](ReqGetForumMsgMetaData.md)| forumId: &gt;              (RsGxsGroupId)id of the forum of which the content is requested   | [optional] 

### Return type

[**ResGetForumMsgMetaData**](ResGetForumMsgMetaData.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsGetForumServiceStatistics**
> ResGetForumServiceStatistics rsGxsForumsGetForumServiceStatistics()

returns statistics for the forum service

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsGxsForumsGetForumServiceStatistics();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsGetForumServiceStatistics: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetForumServiceStatistics**](ResGetForumServiceStatistics.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsGetForumStatistics**
> ResGetForumStatistics rsGxsForumsGetForumStatistics(reqGetForumStatistics)

returns statistics about a particular forum

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetForumStatistics = ReqGetForumStatistics(); // ReqGetForumStatistics | forumId: >              (RsGxsGroupId)Id of the forum  

try { 
    var result = api_instance.rsGxsForumsGetForumStatistics(reqGetForumStatistics);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsGetForumStatistics: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetForumStatistics** | [**ReqGetForumStatistics**](ReqGetForumStatistics.md)| forumId: &gt;              (RsGxsGroupId)Id of the forum   | [optional] 

### Return type

[**ResGetForumStatistics**](ResGetForumStatistics.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsGetForumsInfo**
> ResGetForumsInfo rsGxsForumsGetForumsInfo(reqGetForumsInfo)

Get forums information (description, thumbnail...). Blocking API.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetForumsInfo = ReqGetForumsInfo(); // ReqGetForumsInfo | forumIds: >              (list<RsGxsGroupId>)ids of the forums of which to get the informations  

try { 
    var result = api_instance.rsGxsForumsGetForumsInfo(reqGetForumsInfo);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsGetForumsInfo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetForumsInfo** | [**ReqGetForumsInfo**](ReqGetForumsInfo.md)| forumIds: &gt;              (list&lt;RsGxsGroupId&gt;)ids of the forums of which to get the informations   | [optional] 

### Return type

[**ResGetForumsInfo**](ResGetForumsInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsGetForumsSummaries**
> ResGetForumsSummaries rsGxsForumsGetForumsSummaries()

Get forums summaries list. Blocking API.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsGxsForumsGetForumsSummaries();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsGetForumsSummaries: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetForumsSummaries**](ResGetForumsSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsMarkRead**
> ResMarkRead rsGxsForumsMarkRead(reqMarkRead)

Toggle message read status. Blocking API.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqMarkRead = ReqMarkRead(); // ReqMarkRead | messageId: >              (RsGxsGrpMsgIdPair)post identifier          read: >              (boolean)true to mark as read, false to mark as unread  

try { 
    var result = api_instance.rsGxsForumsMarkRead(reqMarkRead);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsMarkRead: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqMarkRead** | [**ReqMarkRead**](ReqMarkRead.md)| messageId: &gt;              (RsGxsGrpMsgIdPair)post identifier          read: &gt;              (boolean)true to mark as read, false to mark as unread   | [optional] 

### Return type

[**ResMarkRead**](ResMarkRead.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsRequestStatus**
> ResRequestStatus rsGxsForumsRequestStatus(reqRequestStatus)

null

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRequestStatus = ReqRequestStatus(); // ReqRequestStatus | token: >              (integer)None 

try { 
    var result = api_instance.rsGxsForumsRequestStatus(reqRequestStatus);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsRequestStatus: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRequestStatus** | [**ReqRequestStatus**](ReqRequestStatus.md)| token: &gt;              (integer)None  | [optional] 

### Return type

[**ResRequestStatus**](ResRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsGxsForumsSubscribeToForum**
> ResSubscribeToForum rsGxsForumsSubscribeToForum(reqSubscribeToForum)

Subscrbe to a forum. Blocking API

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSubscribeToForum = ReqSubscribeToForum(); // ReqSubscribeToForum | forumId: >              (RsGxsGroupId)Forum id          subscribe: >              (boolean)true to subscribe, false to unsubscribe  

try { 
    var result = api_instance.rsGxsForumsSubscribeToForum(reqSubscribeToForum);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsGxsForumsSubscribeToForum: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSubscribeToForum** | [**ReqSubscribeToForum**](ReqSubscribeToForum.md)| forumId: &gt;              (RsGxsGroupId)Forum id          subscribe: &gt;              (boolean)true to subscribe, false to unsubscribe   | [optional] 

### Return type

[**ResSubscribeToForum**](ResSubscribeToForum.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityAutoAddFriendIdsAsContact**
> ResAutoAddFriendIdsAsContact rsIdentityAutoAddFriendIdsAsContact()

Check if automatic signed by friend identity contact flagging is enabled

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsIdentityAutoAddFriendIdsAsContact();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityAutoAddFriendIdsAsContact: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResAutoAddFriendIdsAsContact**](ResAutoAddFriendIdsAsContact.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityCreateIdentity**
> ResCreateIdentity rsIdentityCreateIdentity(reqCreateIdentity)

Create a new identity

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateIdentity = ReqCreateIdentity(); // ReqCreateIdentity | name: >              (string)Name of the identity          avatar: >              (RsGxsImage)Image associated to the identity          pseudonimous: >              (boolean)true for unsigned identity, false otherwise          pgpPassword: >              (string)password to unlock PGP to sign identity, not implemented yet  

try { 
    var result = api_instance.rsIdentityCreateIdentity(reqCreateIdentity);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityCreateIdentity: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateIdentity** | [**ReqCreateIdentity**](ReqCreateIdentity.md)| name: &gt;              (string)Name of the identity          avatar: &gt;              (RsGxsImage)Image associated to the identity          pseudonimous: &gt;              (boolean)true for unsigned identity, false otherwise          pgpPassword: &gt;              (string)password to unlock PGP to sign identity, not implemented yet   | [optional] 

### Return type

[**ResCreateIdentity**](ResCreateIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityDeleteBannedNodesThreshold**
> ResDeleteBannedNodesThreshold rsIdentityDeleteBannedNodesThreshold()

Get number of days after which delete a banned identities

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsIdentityDeleteBannedNodesThreshold();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityDeleteBannedNodesThreshold: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResDeleteBannedNodesThreshold**](ResDeleteBannedNodesThreshold.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityDeleteIdentity**
> ResDeleteIdentity rsIdentityDeleteIdentity(reqDeleteIdentity)

Locally delete given identity

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqDeleteIdentity = ReqDeleteIdentity(); // ReqDeleteIdentity | id: >              (RsGxsId)Id of the identity  

try { 
    var result = api_instance.rsIdentityDeleteIdentity(reqDeleteIdentity);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityDeleteIdentity: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqDeleteIdentity** | [**ReqDeleteIdentity**](ReqDeleteIdentity.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**ResDeleteIdentity**](ResDeleteIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityExportIdentityLink**
> ResExportIdentityLink rsIdentityExportIdentityLink(reqExportIdentityLink)

Get link to a identity

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqExportIdentityLink = ReqExportIdentityLink(); // ReqExportIdentityLink | id: >              (RsGxsId)Id of the identity of which you want to generate a link          includeGxsData: >              (boolean)if true include the identity GXS group data so the receiver can make use of the identity even if she hasn't received it through GXS yet          baseUrl: >              (string)URL into which to sneak in the RetroShare link radix, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare link into a \"normal\" looking web link. If empty the GXS data link will be outputted in plain base64 format.  

try { 
    var result = api_instance.rsIdentityExportIdentityLink(reqExportIdentityLink);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityExportIdentityLink: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqExportIdentityLink** | [**ReqExportIdentityLink**](ReqExportIdentityLink.md)| id: &gt;              (RsGxsId)Id of the identity of which you want to generate a link          includeGxsData: &gt;              (boolean)if true include the identity GXS group data so the receiver can make use of the identity even if she hasn&#39;t received it through GXS yet          baseUrl: &gt;              (string)URL into which to sneak in the RetroShare link radix, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare link into a \&quot;normal\&quot; looking web link. If empty the GXS data link will be outputted in plain base64 format.   | [optional] 

### Return type

[**ResExportIdentityLink**](ResExportIdentityLink.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityGetIdDetails**
> ResGetIdDetails rsIdentityGetIdDetails(reqGetIdDetails)

Get identity details, from the cache

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetIdDetails = ReqGetIdDetails(); // ReqGetIdDetails | id: >              (RsGxsId)Id of the identity  

try { 
    var result = api_instance.rsIdentityGetIdDetails(reqGetIdDetails);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityGetIdDetails: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetIdDetails** | [**ReqGetIdDetails**](ReqGetIdDetails.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**ResGetIdDetails**](ResGetIdDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityGetIdentitiesInfo**
> ResGetIdentitiesInfo rsIdentityGetIdentitiesInfo(reqGetIdentitiesInfo)

Get identities information (name, avatar...). Blocking API.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetIdentitiesInfo = ReqGetIdentitiesInfo(); // ReqGetIdentitiesInfo | ids: >              (set<RsGxsId>)ids of the channels of which to get the informations  

try { 
    var result = api_instance.rsIdentityGetIdentitiesInfo(reqGetIdentitiesInfo);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityGetIdentitiesInfo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetIdentitiesInfo** | [**ReqGetIdentitiesInfo**](ReqGetIdentitiesInfo.md)| ids: &gt;              (set&lt;RsGxsId&gt;)ids of the channels of which to get the informations   | [optional] 

### Return type

[**ResGetIdentitiesInfo**](ResGetIdentitiesInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityGetIdentitiesSummaries**
> ResGetIdentitiesSummaries rsIdentityGetIdentitiesSummaries()

Get identities summaries list.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsIdentityGetIdentitiesSummaries();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityGetIdentitiesSummaries: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetIdentitiesSummaries**](ResGetIdentitiesSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityGetLastUsageTS**
> ResGetLastUsageTS rsIdentityGetLastUsageTS(reqGetLastUsageTS)

Get last seen usage time of given identity

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetLastUsageTS = ReqGetLastUsageTS(); // ReqGetLastUsageTS | id: >              (RsGxsId)Id of the identity  

try { 
    var result = api_instance.rsIdentityGetLastUsageTS(reqGetLastUsageTS);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityGetLastUsageTS: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetLastUsageTS** | [**ReqGetLastUsageTS**](ReqGetLastUsageTS.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**ResGetLastUsageTS**](ResGetLastUsageTS.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityGetOwnPseudonimousIds**
> ResGetOwnPseudonimousIds rsIdentityGetOwnPseudonimousIds()

Get own pseudonimous (unsigned) ids

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsIdentityGetOwnPseudonimousIds();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityGetOwnPseudonimousIds: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetOwnPseudonimousIds**](ResGetOwnPseudonimousIds.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityGetOwnSignedIds**
> ResGetOwnSignedIds rsIdentityGetOwnSignedIds()

Get own signed ids

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsIdentityGetOwnSignedIds();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityGetOwnSignedIds: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetOwnSignedIds**](ResGetOwnSignedIds.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityIsARegularContact**
> ResIsARegularContact rsIdentityIsARegularContact(reqIsARegularContact)

Check if an identity is contact

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqIsARegularContact = ReqIsARegularContact(); // ReqIsARegularContact | id: >              (RsGxsId)Id of the identity  

try { 
    var result = api_instance.rsIdentityIsARegularContact(reqIsARegularContact);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityIsARegularContact: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqIsARegularContact** | [**ReqIsARegularContact**](ReqIsARegularContact.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**ResIsARegularContact**](ResIsARegularContact.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityIsKnownId**
> ResIsKnownId rsIdentityIsKnownId(reqIsKnownId)

Check if an id is known

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqIsKnownId = ReqIsKnownId(); // ReqIsKnownId | id: >              (RsGxsId)Id to check  

try { 
    var result = api_instance.rsIdentityIsKnownId(reqIsKnownId);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityIsKnownId: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqIsKnownId** | [**ReqIsKnownId**](ReqIsKnownId.md)| id: &gt;              (RsGxsId)Id to check   | [optional] 

### Return type

[**ResIsKnownId**](ResIsKnownId.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityIsOwnId**
> ResIsOwnId rsIdentityIsOwnId(reqIsOwnId)

Check if an id is own

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqIsOwnId = ReqIsOwnId(); // ReqIsOwnId | id: >              (RsGxsId)Id to check  

try { 
    var result = api_instance.rsIdentityIsOwnId(reqIsOwnId);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityIsOwnId: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqIsOwnId** | [**ReqIsOwnId**](ReqIsOwnId.md)| id: &gt;              (RsGxsId)Id to check   | [optional] 

### Return type

[**ResIsOwnId**](ResIsOwnId.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityRequestIdentity**
> ResRequestIdentity rsIdentityRequestIdentity(reqRequestIdentity)

request details of a not yet known identity to the network

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRequestIdentity = ReqRequestIdentity(); // ReqRequestIdentity | id: >              (RsGxsId)id of the identity to request          peers: >              (vector<RsPeerId>)optional list of the peers to ask for the key, if empty all online peers are asked.  

try { 
    var result = api_instance.rsIdentityRequestIdentity(reqRequestIdentity);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityRequestIdentity: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRequestIdentity** | [**ReqRequestIdentity**](ReqRequestIdentity.md)| id: &gt;              (RsGxsId)id of the identity to request          peers: &gt;              (vector&lt;RsPeerId&gt;)optional list of the peers to ask for the key, if empty all online peers are asked.   | [optional] 

### Return type

[**ResRequestIdentity**](ResRequestIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityRequestStatus**
> ResRequestStatus rsIdentityRequestStatus(reqRequestStatus)

null

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRequestStatus = ReqRequestStatus(); // ReqRequestStatus | token: >              (integer)None 

try { 
    var result = api_instance.rsIdentityRequestStatus(reqRequestStatus);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityRequestStatus: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRequestStatus** | [**ReqRequestStatus**](ReqRequestStatus.md)| token: &gt;              (integer)None  | [optional] 

### Return type

[**ResRequestStatus**](ResRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentitySetAsRegularContact**
> ResSetAsRegularContact rsIdentitySetAsRegularContact(reqSetAsRegularContact)

Set/unset identity as contact

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetAsRegularContact = ReqSetAsRegularContact(); // ReqSetAsRegularContact | id: >              (RsGxsId)Id of the identity          isContact: >              (boolean)true to set, false to unset  

try { 
    var result = api_instance.rsIdentitySetAsRegularContact(reqSetAsRegularContact);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentitySetAsRegularContact: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetAsRegularContact** | [**ReqSetAsRegularContact**](ReqSetAsRegularContact.md)| id: &gt;              (RsGxsId)Id of the identity          isContact: &gt;              (boolean)true to set, false to unset   | [optional] 

### Return type

[**ResSetAsRegularContact**](ResSetAsRegularContact.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentitySetAutoAddFriendIdsAsContact**
> rsIdentitySetAutoAddFriendIdsAsContact(reqSetAutoAddFriendIdsAsContact)

Toggle automatic flagging signed by friends identity as contact

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetAutoAddFriendIdsAsContact = ReqSetAutoAddFriendIdsAsContact(); // ReqSetAutoAddFriendIdsAsContact | enabled: >              (boolean)true to enable, false to disable  

try { 
    api_instance.rsIdentitySetAutoAddFriendIdsAsContact(reqSetAutoAddFriendIdsAsContact);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentitySetAutoAddFriendIdsAsContact: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetAutoAddFriendIdsAsContact** | [**ReqSetAutoAddFriendIdsAsContact**](ReqSetAutoAddFriendIdsAsContact.md)| enabled: &gt;              (boolean)true to enable, false to disable   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentitySetDeleteBannedNodesThreshold**
> rsIdentitySetDeleteBannedNodesThreshold(reqSetDeleteBannedNodesThreshold)

Set number of days after which delete a banned identities

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetDeleteBannedNodesThreshold = ReqSetDeleteBannedNodesThreshold(); // ReqSetDeleteBannedNodesThreshold | days: >              (integer)number of days  

try { 
    api_instance.rsIdentitySetDeleteBannedNodesThreshold(reqSetDeleteBannedNodesThreshold);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentitySetDeleteBannedNodesThreshold: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetDeleteBannedNodesThreshold** | [**ReqSetDeleteBannedNodesThreshold**](ReqSetDeleteBannedNodesThreshold.md)| days: &gt;              (integer)number of days   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsIdentityUpdateIdentity**
> ResUpdateIdentity rsIdentityUpdateIdentity(reqUpdateIdentity)

Update identity data (name, avatar...)

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqUpdateIdentity = ReqUpdateIdentity(); // ReqUpdateIdentity | identityData: >              (RsGxsIdGroup)updated identiy data  

try { 
    var result = api_instance.rsIdentityUpdateIdentity(reqUpdateIdentity);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsIdentityUpdateIdentity: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqUpdateIdentity** | [**ReqUpdateIdentity**](ReqUpdateIdentity.md)| identityData: &gt;              (RsGxsIdGroup)updated identiy data   | [optional] 

### Return type

[**ResUpdateIdentity**](ResUpdateIdentity.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsJsonApiAskForStop**
> rsJsonApiAskForStop()

Request

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    api_instance.rsJsonApiAskForStop();
} catch (e) {
    print("Exception when calling DefaultApi->rsJsonApiAskForStop: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsJsonApiAuthorizeUser**
> ResAuthorizeUser rsJsonApiAuthorizeUser(reqAuthorizeUser)

null

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqAuthorizeUser = ReqAuthorizeUser(); // ReqAuthorizeUser | user: >              (string)user name to autorize, must not contain ':'          password: >              (string)password for the user  

try { 
    var result = api_instance.rsJsonApiAuthorizeUser(reqAuthorizeUser);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsJsonApiAuthorizeUser: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqAuthorizeUser** | [**ReqAuthorizeUser**](ReqAuthorizeUser.md)| user: &gt;              (string)user name to autorize, must not contain &#39;:&#39;          password: &gt;              (string)password for the user   | [optional] 

### Return type

[**ResAuthorizeUser**](ResAuthorizeUser.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsJsonApiGetAuthorizedTokens**
> ResGetAuthorizedTokens rsJsonApiGetAuthorizedTokens()

Get authorized tokens

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsJsonApiGetAuthorizedTokens();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsJsonApiGetAuthorizedTokens: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetAuthorizedTokens**](ResGetAuthorizedTokens.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsJsonApiGetBindingAddress**
> ResGetBindingAddress rsJsonApiGetBindingAddress()

null

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsJsonApiGetBindingAddress();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsJsonApiGetBindingAddress: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetBindingAddress**](ResGetBindingAddress.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsJsonApiIsAuthTokenValid**
> ResIsAuthTokenValid rsJsonApiIsAuthTokenValid(reqIsAuthTokenValid)

Check if given JSON API auth token is authorized

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var reqIsAuthTokenValid = ReqIsAuthTokenValid(); // ReqIsAuthTokenValid | token: >              (string)decoded  

try { 
    var result = api_instance.rsJsonApiIsAuthTokenValid(reqIsAuthTokenValid);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsJsonApiIsAuthTokenValid: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqIsAuthTokenValid** | [**ReqIsAuthTokenValid**](ReqIsAuthTokenValid.md)| token: &gt;              (string)decoded   | [optional] 

### Return type

[**ResIsAuthTokenValid**](ResIsAuthTokenValid.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsJsonApiListeningPort**
> ResListeningPort rsJsonApiListeningPort()

null

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsJsonApiListeningPort();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsJsonApiListeningPort: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResListeningPort**](ResListeningPort.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsJsonApiRequestNewTokenAutorization**
> ResRequestNewTokenAutorization rsJsonApiRequestNewTokenAutorization(reqRequestNewTokenAutorization)

This function should be used by JSON API clients that aren't authenticated yet, to ask their token to be authorized, the success or failure will depend on mNewAccessRequestCallback return value, and it will likely need human user interaction in the process.

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var reqRequestNewTokenAutorization = ReqRequestNewTokenAutorization(); // ReqRequestNewTokenAutorization | user: >              (string)user name to authorize          password: >              (string)password for the new user  

try { 
    var result = api_instance.rsJsonApiRequestNewTokenAutorization(reqRequestNewTokenAutorization);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsJsonApiRequestNewTokenAutorization: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRequestNewTokenAutorization** | [**ReqRequestNewTokenAutorization**](ReqRequestNewTokenAutorization.md)| user: &gt;              (string)user name to authorize          password: &gt;              (string)password for the new user   | [optional] 

### Return type

[**ResRequestNewTokenAutorization**](ResRequestNewTokenAutorization.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsJsonApiRestart**
> ResRestart rsJsonApiRestart()

Restart

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsJsonApiRestart();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsJsonApiRestart: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResRestart**](ResRestart.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsJsonApiRevokeAuthToken**
> ResRevokeAuthToken rsJsonApiRevokeAuthToken(reqRevokeAuthToken)

Revoke given auth token

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRevokeAuthToken = ReqRevokeAuthToken(); // ReqRevokeAuthToken | user: >              (string)user name of which to revoke authorization  

try { 
    var result = api_instance.rsJsonApiRevokeAuthToken(reqRevokeAuthToken);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsJsonApiRevokeAuthToken: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRevokeAuthToken** | [**ReqRevokeAuthToken**](ReqRevokeAuthToken.md)| user: &gt;              (string)user name of which to revoke authorization   | [optional] 

### Return type

[**ResRevokeAuthToken**](ResRevokeAuthToken.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsJsonApiSetBindingAddress**
> rsJsonApiSetBindingAddress(reqSetBindingAddress)

null

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetBindingAddress = ReqSetBindingAddress(); // ReqSetBindingAddress | address: >              (string)Binding address in IPv4 or IPv6 format.  

try { 
    api_instance.rsJsonApiSetBindingAddress(reqSetBindingAddress);
} catch (e) {
    print("Exception when calling DefaultApi->rsJsonApiSetBindingAddress: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetBindingAddress** | [**ReqSetBindingAddress**](ReqSetBindingAddress.md)| address: &gt;              (string)Binding address in IPv4 or IPv6 format.   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsJsonApiSetListeningPort**
> rsJsonApiSetListeningPort(reqSetListeningPort)

null

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetListeningPort = ReqSetListeningPort(); // ReqSetListeningPort | port: >              (integer)Must be available otherwise the binding will fail  

try { 
    api_instance.rsJsonApiSetListeningPort(reqSetListeningPort);
} catch (e) {
    print("Exception when calling DefaultApi->rsJsonApiSetListeningPort: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetListeningPort** | [**ReqSetListeningPort**](ReqSetListeningPort.md)| port: &gt;              (integer)Must be available otherwise the binding will fail   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsJsonApiVersion**
> ResVersion rsJsonApiVersion()

Write version information to given paramethers

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsJsonApiVersion();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsJsonApiVersion: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResVersion**](ResVersion.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsLoginHelperAttemptLogin**
> ResAttemptLogin rsLoginHelperAttemptLogin(reqAttemptLogin)

Normal way to attempt login

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqAttemptLogin = ReqAttemptLogin(); // ReqAttemptLogin | account: >              (RsPeerId)Id of the account to which attempt login          password: >              (string)Password for the given account  

try { 
    var result = api_instance.rsLoginHelperAttemptLogin(reqAttemptLogin);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsLoginHelperAttemptLogin: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqAttemptLogin** | [**ReqAttemptLogin**](ReqAttemptLogin.md)| account: &gt;              (RsPeerId)Id of the account to which attempt login          password: &gt;              (string)Password for the given account   | [optional] 

### Return type

[**ResAttemptLogin**](ResAttemptLogin.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsLoginHelperCollectEntropy**
> ResCollectEntropy rsLoginHelperCollectEntropy(reqCollectEntropy)

Feed extra entropy to the crypto libraries

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var reqCollectEntropy = ReqCollectEntropy(); // ReqCollectEntropy | bytes: >              (integer)number to feed to the entropy pool  

try { 
    var result = api_instance.rsLoginHelperCollectEntropy(reqCollectEntropy);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsLoginHelperCollectEntropy: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCollectEntropy** | [**ReqCollectEntropy**](ReqCollectEntropy.md)| bytes: &gt;              (integer)number to feed to the entropy pool   | [optional] 

### Return type

[**ResCollectEntropy**](ResCollectEntropy.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsLoginHelperCreateLocation**
> ResCreateLocation rsLoginHelperCreateLocation(reqCreateLocation)

Creates a new RetroShare location, and log in once is created

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateLocation = ReqCreateLocation(); // ReqCreateLocation | location: >              (RsLoginHelper_Location)provide input information to generate the location and storage to output the data of the generated location          password: >              (string)to protect and unlock the associated PGP key          makeHidden: >              (boolean)pass true to create an hidden location. UNTESTED!          makeAutoTor: >              (boolean)pass true to create an automatically configured Tor hidden location. UNTESTED!  

try { 
    var result = api_instance.rsLoginHelperCreateLocation(reqCreateLocation);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsLoginHelperCreateLocation: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateLocation** | [**ReqCreateLocation**](ReqCreateLocation.md)| location: &gt;              (RsLoginHelper_Location)provide input information to generate the location and storage to output the data of the generated location          password: &gt;              (string)to protect and unlock the associated PGP key          makeHidden: &gt;              (boolean)pass true to create an hidden location. UNTESTED!          makeAutoTor: &gt;              (boolean)pass true to create an automatically configured Tor hidden location. UNTESTED!   | [optional] 

### Return type

[**ResCreateLocation**](ResCreateLocation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsLoginHelperGetLocations**
> ResGetLocations rsLoginHelperGetLocations()

Get locations and associated information

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsLoginHelperGetLocations();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsLoginHelperGetLocations: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetLocations**](ResGetLocations.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsLoginHelperIsLoggedIn**
> ResIsLoggedIn rsLoginHelperIsLoggedIn()

Check if RetroShare is already logged in, this usually return true after a successfull

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsLoginHelperIsLoggedIn();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsLoginHelperIsLoggedIn: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResIsLoggedIn**](ResIsLoggedIn.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsAcceptLobbyInvite**
> ResAcceptLobbyInvite rsMsgsAcceptLobbyInvite(reqAcceptLobbyInvite)

acceptLobbyInvite accept a chat invite

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqAcceptLobbyInvite = ReqAcceptLobbyInvite(); // ReqAcceptLobbyInvite | id: >              (ChatLobbyId)chat lobby id you were invited into and you want to join          identity: >              (RsGxsId)chat identity to use  

try { 
    var result = api_instance.rsMsgsAcceptLobbyInvite(reqAcceptLobbyInvite);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsAcceptLobbyInvite: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqAcceptLobbyInvite** | [**ReqAcceptLobbyInvite**](ReqAcceptLobbyInvite.md)| id: &gt;              (ChatLobbyId)chat lobby id you were invited into and you want to join          identity: &gt;              (RsGxsId)chat identity to use   | [optional] 

### Return type

[**ResAcceptLobbyInvite**](ResAcceptLobbyInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsClearChatLobby**
> rsMsgsClearChatLobby(reqClearChatLobby)

clearChatLobby clear a chat lobby

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqClearChatLobby = ReqClearChatLobby(); // ReqClearChatLobby | id: >              (ChatId)chat lobby id to clear  

try { 
    api_instance.rsMsgsClearChatLobby(reqClearChatLobby);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsClearChatLobby: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqClearChatLobby** | [**ReqClearChatLobby**](ReqClearChatLobby.md)| id: &gt;              (ChatId)chat lobby id to clear   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsCloseDistantChatConnexion**
> ResCloseDistantChatConnexion rsMsgsCloseDistantChatConnexion(reqCloseDistantChatConnexion)

closeDistantChatConnexion

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCloseDistantChatConnexion = ReqCloseDistantChatConnexion(); // ReqCloseDistantChatConnexion | pid: >              (DistantChatPeerId)distant chat id to close the connection  

try { 
    var result = api_instance.rsMsgsCloseDistantChatConnexion(reqCloseDistantChatConnexion);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsCloseDistantChatConnexion: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCloseDistantChatConnexion** | [**ReqCloseDistantChatConnexion**](ReqCloseDistantChatConnexion.md)| pid: &gt;              (DistantChatPeerId)distant chat id to close the connection   | [optional] 

### Return type

[**ResCloseDistantChatConnexion**](ResCloseDistantChatConnexion.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsCreateChatLobby**
> ResCreateChatLobby rsMsgsCreateChatLobby(reqCreateChatLobby)

createChatLobby create a new chat lobby

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqCreateChatLobby = ReqCreateChatLobby(); // ReqCreateChatLobby | lobby_name: >              (string)lobby name          lobby_identity: >              (RsGxsId)chat id to use for new lobby          lobby_topic: >              (string)lobby toppic          invited_friends: >              (set<RsPeerId>)list of friends to invite          lobby_privacy_type: >              (ChatLobbyFlags)flag for new chat lobby  

try { 
    var result = api_instance.rsMsgsCreateChatLobby(reqCreateChatLobby);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsCreateChatLobby: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqCreateChatLobby** | [**ReqCreateChatLobby**](ReqCreateChatLobby.md)| lobby_name: &gt;              (string)lobby name          lobby_identity: &gt;              (RsGxsId)chat id to use for new lobby          lobby_topic: &gt;              (string)lobby toppic          invited_friends: &gt;              (set&lt;RsPeerId&gt;)list of friends to invite          lobby_privacy_type: &gt;              (ChatLobbyFlags)flag for new chat lobby   | [optional] 

### Return type

[**ResCreateChatLobby**](ResCreateChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsDenyLobbyInvite**
> rsMsgsDenyLobbyInvite(reqDenyLobbyInvite)

denyLobbyInvite deny a chat lobby invite

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqDenyLobbyInvite = ReqDenyLobbyInvite(); // ReqDenyLobbyInvite | id: >              (ChatLobbyId)chat lobby id you were invited into  

try { 
    api_instance.rsMsgsDenyLobbyInvite(reqDenyLobbyInvite);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsDenyLobbyInvite: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqDenyLobbyInvite** | [**ReqDenyLobbyInvite**](ReqDenyLobbyInvite.md)| id: &gt;              (ChatLobbyId)chat lobby id you were invited into   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetChatLobbyInfo**
> ResGetChatLobbyInfo rsMsgsGetChatLobbyInfo(reqGetChatLobbyInfo)

getChatLobbyInfo get lobby info of a subscribed chat lobby. Returns true if lobby id is valid.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetChatLobbyInfo = ReqGetChatLobbyInfo(); // ReqGetChatLobbyInfo | id: >              (ChatLobbyId)id to get infos from  

try { 
    var result = api_instance.rsMsgsGetChatLobbyInfo(reqGetChatLobbyInfo);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetChatLobbyInfo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetChatLobbyInfo** | [**ReqGetChatLobbyInfo**](ReqGetChatLobbyInfo.md)| id: &gt;              (ChatLobbyId)id to get infos from   | [optional] 

### Return type

[**ResGetChatLobbyInfo**](ResGetChatLobbyInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetChatLobbyList**
> ResGetChatLobbyList rsMsgsGetChatLobbyList()

getChatLobbyList get ids of subscribed lobbies

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsMsgsGetChatLobbyList();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetChatLobbyList: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetChatLobbyList**](ResGetChatLobbyList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetCustomStateString**
> ResGetCustomStateString rsMsgsGetCustomStateString(reqGetCustomStateString)

getCustomStateString get the custom status message from a peer

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetCustomStateString = ReqGetCustomStateString(); // ReqGetCustomStateString | peer_id: >              (RsPeerId)peer id to the peer you want to get the status message from  

try { 
    var result = api_instance.rsMsgsGetCustomStateString(reqGetCustomStateString);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetCustomStateString: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetCustomStateString** | [**ReqGetCustomStateString**](ReqGetCustomStateString.md)| peer_id: &gt;              (RsPeerId)peer id to the peer you want to get the status message from   | [optional] 

### Return type

[**ResGetCustomStateString**](ResGetCustomStateString.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetDefaultIdentityForChatLobby**
> ResGetDefaultIdentityForChatLobby rsMsgsGetDefaultIdentityForChatLobby()

getDefaultIdentityForChatLobby get the default identity used for chat lobbies

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsMsgsGetDefaultIdentityForChatLobby();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetDefaultIdentityForChatLobby: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetDefaultIdentityForChatLobby**](ResGetDefaultIdentityForChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetDistantChatStatus**
> ResGetDistantChatStatus rsMsgsGetDistantChatStatus(reqGetDistantChatStatus)

getDistantChatStatus receives distant chat info to a given distant chat id

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetDistantChatStatus = ReqGetDistantChatStatus(); // ReqGetDistantChatStatus | pid: >              (DistantChatPeerId)distant chat id  

try { 
    var result = api_instance.rsMsgsGetDistantChatStatus(reqGetDistantChatStatus);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetDistantChatStatus: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetDistantChatStatus** | [**ReqGetDistantChatStatus**](ReqGetDistantChatStatus.md)| pid: &gt;              (DistantChatPeerId)distant chat id   | [optional] 

### Return type

[**ResGetDistantChatStatus**](ResGetDistantChatStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetIdentityForChatLobby**
> ResGetIdentityForChatLobby rsMsgsGetIdentityForChatLobby(reqGetIdentityForChatLobby)

getIdentityForChatLobby

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetIdentityForChatLobby = ReqGetIdentityForChatLobby(); // ReqGetIdentityForChatLobby | lobby_id: >              (ChatLobbyId)lobby to get the chat id from  

try { 
    var result = api_instance.rsMsgsGetIdentityForChatLobby(reqGetIdentityForChatLobby);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetIdentityForChatLobby: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetIdentityForChatLobby** | [**ReqGetIdentityForChatLobby**](ReqGetIdentityForChatLobby.md)| lobby_id: &gt;              (ChatLobbyId)lobby to get the chat id from   | [optional] 

### Return type

[**ResGetIdentityForChatLobby**](ResGetIdentityForChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetListOfNearbyChatLobbies**
> ResGetListOfNearbyChatLobbies rsMsgsGetListOfNearbyChatLobbies()

getListOfNearbyChatLobbies get info about all lobbies, subscribed and unsubscribed

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsMsgsGetListOfNearbyChatLobbies();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetListOfNearbyChatLobbies: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetListOfNearbyChatLobbies**](ResGetListOfNearbyChatLobbies.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetLobbyAutoSubscribe**
> ResGetLobbyAutoSubscribe rsMsgsGetLobbyAutoSubscribe(reqGetLobbyAutoSubscribe)

getLobbyAutoSubscribe get current value of auto subscribe

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetLobbyAutoSubscribe = ReqGetLobbyAutoSubscribe(); // ReqGetLobbyAutoSubscribe | lobby_id: >              (ChatLobbyId)lobby to get value from  

try { 
    var result = api_instance.rsMsgsGetLobbyAutoSubscribe(reqGetLobbyAutoSubscribe);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetLobbyAutoSubscribe: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetLobbyAutoSubscribe** | [**ReqGetLobbyAutoSubscribe**](ReqGetLobbyAutoSubscribe.md)| lobby_id: &gt;              (ChatLobbyId)lobby to get value from   | [optional] 

### Return type

[**ResGetLobbyAutoSubscribe**](ResGetLobbyAutoSubscribe.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetMaxMessageSecuritySize**
> ResGetMaxMessageSecuritySize rsMsgsGetMaxMessageSecuritySize(reqGetMaxMessageSecuritySize)

getMaxMessageSecuritySize get the maximum size of a chta message

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetMaxMessageSecuritySize = ReqGetMaxMessageSecuritySize(); // ReqGetMaxMessageSecuritySize | type: >              (integer)chat type  

try { 
    var result = api_instance.rsMsgsGetMaxMessageSecuritySize(reqGetMaxMessageSecuritySize);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetMaxMessageSecuritySize: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetMaxMessageSecuritySize** | [**ReqGetMaxMessageSecuritySize**](ReqGetMaxMessageSecuritySize.md)| type: &gt;              (integer)chat type   | [optional] 

### Return type

[**ResGetMaxMessageSecuritySize**](ResGetMaxMessageSecuritySize.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetMessage**
> ResGetMessage rsMsgsGetMessage(reqGetMessage)

getMessage

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetMessage = ReqGetMessage(); // ReqGetMessage | msgId: >              (string)message ID to lookup  

try { 
    var result = api_instance.rsMsgsGetMessage(reqGetMessage);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetMessage: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetMessage** | [**ReqGetMessage**](ReqGetMessage.md)| msgId: &gt;              (string)message ID to lookup   | [optional] 

### Return type

[**ResGetMessage**](ResGetMessage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetMessageCount**
> ResGetMessageCount rsMsgsGetMessageCount()

getMessageCount

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsMsgsGetMessageCount();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetMessageCount: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetMessageCount**](ResGetMessageCount.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetMessageSummaries**
> ResGetMessageSummaries rsMsgsGetMessageSummaries()

getMessageSummaries

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsMsgsGetMessageSummaries();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetMessageSummaries: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetMessageSummaries**](ResGetMessageSummaries.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetMessageTag**
> ResGetMessageTag rsMsgsGetMessageTag(reqGetMessageTag)

getMessageTag

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetMessageTag = ReqGetMessageTag(); // ReqGetMessageTag | msgId: >              (string)None 

try { 
    var result = api_instance.rsMsgsGetMessageTag(reqGetMessageTag);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetMessageTag: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetMessageTag** | [**ReqGetMessageTag**](ReqGetMessageTag.md)| msgId: &gt;              (string)None  | [optional] 

### Return type

[**ResGetMessageTag**](ResGetMessageTag.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetMessageTagTypes**
> ResGetMessageTagTypes rsMsgsGetMessageTagTypes()

getMessageTagTypes

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsMsgsGetMessageTagTypes();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetMessageTagTypes: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetMessageTagTypes**](ResGetMessageTagTypes.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetMsgParentId**
> ResGetMsgParentId rsMsgsGetMsgParentId(reqGetMsgParentId)

getMsgParentId

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetMsgParentId = ReqGetMsgParentId(); // ReqGetMsgParentId | msgId: >              (string)None 

try { 
    var result = api_instance.rsMsgsGetMsgParentId(reqGetMsgParentId);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetMsgParentId: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetMsgParentId** | [**ReqGetMsgParentId**](ReqGetMsgParentId.md)| msgId: &gt;              (string)None  | [optional] 

### Return type

[**ResGetMsgParentId**](ResGetMsgParentId.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsGetPendingChatLobbyInvites**
> ResGetPendingChatLobbyInvites rsMsgsGetPendingChatLobbyInvites()

getPendingChatLobbyInvites get a list of all pending chat lobby invites

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsMsgsGetPendingChatLobbyInvites();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsGetPendingChatLobbyInvites: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetPendingChatLobbyInvites**](ResGetPendingChatLobbyInvites.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsInitiateDistantChatConnexion**
> ResInitiateDistantChatConnexion rsMsgsInitiateDistantChatConnexion(reqInitiateDistantChatConnexion)

initiateDistantChatConnexion initiate a connexion for a distant chat

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqInitiateDistantChatConnexion = ReqInitiateDistantChatConnexion(); // ReqInitiateDistantChatConnexion | to_pid: >              (RsGxsId)RsGxsId to start the connection          from_pid: >              (RsGxsId)owned RsGxsId who start the connection          notify: >              (boolean)notify remote that the connection is stablished  

try { 
    var result = api_instance.rsMsgsInitiateDistantChatConnexion(reqInitiateDistantChatConnexion);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsInitiateDistantChatConnexion: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqInitiateDistantChatConnexion** | [**ReqInitiateDistantChatConnexion**](ReqInitiateDistantChatConnexion.md)| to_pid: &gt;              (RsGxsId)RsGxsId to start the connection          from_pid: &gt;              (RsGxsId)owned RsGxsId who start the connection          notify: &gt;              (boolean)notify remote that the connection is stablished   | [optional] 

### Return type

[**ResInitiateDistantChatConnexion**](ResInitiateDistantChatConnexion.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsInvitePeerToLobby**
> rsMsgsInvitePeerToLobby(reqInvitePeerToLobby)

invitePeerToLobby invite a peer to join a lobby

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqInvitePeerToLobby = ReqInvitePeerToLobby(); // ReqInvitePeerToLobby | lobby_id: >              (ChatLobbyId)lobby it to invite into          peer_id: >              (RsPeerId)peer to invite  

try { 
    api_instance.rsMsgsInvitePeerToLobby(reqInvitePeerToLobby);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsInvitePeerToLobby: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqInvitePeerToLobby** | [**ReqInvitePeerToLobby**](ReqInvitePeerToLobby.md)| lobby_id: &gt;              (ChatLobbyId)lobby it to invite into          peer_id: &gt;              (RsPeerId)peer to invite   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsJoinVisibleChatLobby**
> ResJoinVisibleChatLobby rsMsgsJoinVisibleChatLobby(reqJoinVisibleChatLobby)

joinVisibleChatLobby join a lobby that is visible

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqJoinVisibleChatLobby = ReqJoinVisibleChatLobby(); // ReqJoinVisibleChatLobby | lobby_id: >              (ChatLobbyId)lobby to join to          own_id: >              (RsGxsId)chat id to use  

try { 
    var result = api_instance.rsMsgsJoinVisibleChatLobby(reqJoinVisibleChatLobby);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsJoinVisibleChatLobby: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqJoinVisibleChatLobby** | [**ReqJoinVisibleChatLobby**](ReqJoinVisibleChatLobby.md)| lobby_id: &gt;              (ChatLobbyId)lobby to join to          own_id: &gt;              (RsGxsId)chat id to use   | [optional] 

### Return type

[**ResJoinVisibleChatLobby**](ResJoinVisibleChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsMessageDelete**
> ResMessageDelete rsMsgsMessageDelete(reqMessageDelete)

MessageDelete

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqMessageDelete = ReqMessageDelete(); // ReqMessageDelete | msgId: >              (string)None 

try { 
    var result = api_instance.rsMsgsMessageDelete(reqMessageDelete);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsMessageDelete: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqMessageDelete** | [**ReqMessageDelete**](ReqMessageDelete.md)| msgId: &gt;              (string)None  | [optional] 

### Return type

[**ResMessageDelete**](ResMessageDelete.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsMessageForwarded**
> ResMessageForwarded rsMsgsMessageForwarded(reqMessageForwarded)

MessageForwarded

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqMessageForwarded = ReqMessageForwarded(); // ReqMessageForwarded | msgId: >              (string)None         forwarded: >              (boolean)None 

try { 
    var result = api_instance.rsMsgsMessageForwarded(reqMessageForwarded);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsMessageForwarded: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqMessageForwarded** | [**ReqMessageForwarded**](ReqMessageForwarded.md)| msgId: &gt;              (string)None         forwarded: &gt;              (boolean)None  | [optional] 

### Return type

[**ResMessageForwarded**](ResMessageForwarded.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsMessageJunk**
> ResMessageJunk rsMsgsMessageJunk(reqMessageJunk)

MessageJunk

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqMessageJunk = ReqMessageJunk(); // ReqMessageJunk | msgId: >              (string)None         mark: >              (boolean)None 

try { 
    var result = api_instance.rsMsgsMessageJunk(reqMessageJunk);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsMessageJunk: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqMessageJunk** | [**ReqMessageJunk**](ReqMessageJunk.md)| msgId: &gt;              (string)None         mark: &gt;              (boolean)None  | [optional] 

### Return type

[**ResMessageJunk**](ResMessageJunk.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsMessageLoadEmbeddedImages**
> ResMessageLoadEmbeddedImages rsMsgsMessageLoadEmbeddedImages(reqMessageLoadEmbeddedImages)

MessageLoadEmbeddedImages

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqMessageLoadEmbeddedImages = ReqMessageLoadEmbeddedImages(); // ReqMessageLoadEmbeddedImages | msgId: >              (string)None         load: >              (boolean)None 

try { 
    var result = api_instance.rsMsgsMessageLoadEmbeddedImages(reqMessageLoadEmbeddedImages);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsMessageLoadEmbeddedImages: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqMessageLoadEmbeddedImages** | [**ReqMessageLoadEmbeddedImages**](ReqMessageLoadEmbeddedImages.md)| msgId: &gt;              (string)None         load: &gt;              (boolean)None  | [optional] 

### Return type

[**ResMessageLoadEmbeddedImages**](ResMessageLoadEmbeddedImages.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsMessageRead**
> ResMessageRead rsMsgsMessageRead(reqMessageRead)

MessageRead

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqMessageRead = ReqMessageRead(); // ReqMessageRead | msgId: >              (string)None         unreadByUser: >              (boolean)None 

try { 
    var result = api_instance.rsMsgsMessageRead(reqMessageRead);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsMessageRead: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqMessageRead** | [**ReqMessageRead**](ReqMessageRead.md)| msgId: &gt;              (string)None         unreadByUser: &gt;              (boolean)None  | [optional] 

### Return type

[**ResMessageRead**](ResMessageRead.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsMessageReplied**
> ResMessageReplied rsMsgsMessageReplied(reqMessageReplied)

MessageReplied

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqMessageReplied = ReqMessageReplied(); // ReqMessageReplied | msgId: >              (string)None         replied: >              (boolean)None 

try { 
    var result = api_instance.rsMsgsMessageReplied(reqMessageReplied);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsMessageReplied: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqMessageReplied** | [**ReqMessageReplied**](ReqMessageReplied.md)| msgId: &gt;              (string)None         replied: &gt;              (boolean)None  | [optional] 

### Return type

[**ResMessageReplied**](ResMessageReplied.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsMessageSend**
> ResMessageSend rsMsgsMessageSend(reqMessageSend)

MessageSend

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqMessageSend = ReqMessageSend(); // ReqMessageSend | info: >              (Rs_Msgs_MessageInfo)None 

try { 
    var result = api_instance.rsMsgsMessageSend(reqMessageSend);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsMessageSend: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqMessageSend** | [**ReqMessageSend**](ReqMessageSend.md)| info: &gt;              (Rs_Msgs_MessageInfo)None  | [optional] 

### Return type

[**ResMessageSend**](ResMessageSend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsMessageStar**
> ResMessageStar rsMsgsMessageStar(reqMessageStar)

MessageStar

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqMessageStar = ReqMessageStar(); // ReqMessageStar | msgId: >              (string)None         mark: >              (boolean)None 

try { 
    var result = api_instance.rsMsgsMessageStar(reqMessageStar);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsMessageStar: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqMessageStar** | [**ReqMessageStar**](ReqMessageStar.md)| msgId: &gt;              (string)None         mark: &gt;              (boolean)None  | [optional] 

### Return type

[**ResMessageStar**](ResMessageStar.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsMessageToDraft**
> ResMessageToDraft rsMsgsMessageToDraft(reqMessageToDraft)

MessageToDraft

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqMessageToDraft = ReqMessageToDraft(); // ReqMessageToDraft | info: >              (Rs_Msgs_MessageInfo)None         msgParentId: >              (string)None 

try { 
    var result = api_instance.rsMsgsMessageToDraft(reqMessageToDraft);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsMessageToDraft: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqMessageToDraft** | [**ReqMessageToDraft**](ReqMessageToDraft.md)| info: &gt;              (Rs_Msgs_MessageInfo)None         msgParentId: &gt;              (string)None  | [optional] 

### Return type

[**ResMessageToDraft**](ResMessageToDraft.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsMessageToTrash**
> ResMessageToTrash rsMsgsMessageToTrash(reqMessageToTrash)

MessageToTrash

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqMessageToTrash = ReqMessageToTrash(); // ReqMessageToTrash | msgId: >              (string)Id of the message to mode to trash box          bTrash: >              (boolean)Move to trash if true, otherwise remove from trash  

try { 
    var result = api_instance.rsMsgsMessageToTrash(reqMessageToTrash);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsMessageToTrash: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqMessageToTrash** | [**ReqMessageToTrash**](ReqMessageToTrash.md)| msgId: &gt;              (string)Id of the message to mode to trash box          bTrash: &gt;              (boolean)Move to trash if true, otherwise remove from trash   | [optional] 

### Return type

[**ResMessageToTrash**](ResMessageToTrash.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsRemoveMessageTagType**
> ResRemoveMessageTagType rsMsgsRemoveMessageTagType(reqRemoveMessageTagType)

removeMessageTagType

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRemoveMessageTagType = ReqRemoveMessageTagType(); // ReqRemoveMessageTagType | tagId: >              (integer)None 

try { 
    var result = api_instance.rsMsgsRemoveMessageTagType(reqRemoveMessageTagType);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsRemoveMessageTagType: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRemoveMessageTagType** | [**ReqRemoveMessageTagType**](ReqRemoveMessageTagType.md)| tagId: &gt;              (integer)None  | [optional] 

### Return type

[**ResRemoveMessageTagType**](ResRemoveMessageTagType.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsResetMessageStandardTagTypes**
> ResResetMessageStandardTagTypes rsMsgsResetMessageStandardTagTypes()

resetMessageStandardTagTypes

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsMsgsResetMessageStandardTagTypes();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsResetMessageStandardTagTypes: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResResetMessageStandardTagTypes**](ResResetMessageStandardTagTypes.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsSendChat**
> ResSendChat rsMsgsSendChat(reqSendChat)

sendChat send a chat message to a given id

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSendChat = ReqSendChat(); // ReqSendChat | id: >              (ChatId)id to send the message          msg: >              (string)message to send  

try { 
    var result = api_instance.rsMsgsSendChat(reqSendChat);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsSendChat: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSendChat** | [**ReqSendChat**](ReqSendChat.md)| id: &gt;              (ChatId)id to send the message          msg: &gt;              (string)message to send   | [optional] 

### Return type

[**ResSendChat**](ResSendChat.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsSendLobbyStatusPeerLeaving**
> rsMsgsSendLobbyStatusPeerLeaving(reqSendLobbyStatusPeerLeaving)

sendLobbyStatusPeerLeaving notify friend nodes that we're leaving a subscribed lobby

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSendLobbyStatusPeerLeaving = ReqSendLobbyStatusPeerLeaving(); // ReqSendLobbyStatusPeerLeaving | lobby_id: >              (ChatLobbyId)lobby to leave  

try { 
    api_instance.rsMsgsSendLobbyStatusPeerLeaving(reqSendLobbyStatusPeerLeaving);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsSendLobbyStatusPeerLeaving: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSendLobbyStatusPeerLeaving** | [**ReqSendLobbyStatusPeerLeaving**](ReqSendLobbyStatusPeerLeaving.md)| lobby_id: &gt;              (ChatLobbyId)lobby to leave   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsSendMail**
> ResSendMail rsMsgsSendMail(reqSendMail)

sendMail

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSendMail = ReqSendMail(); // ReqSendMail | from: >              (RsGxsId)GXS id of the author          subject: >              (string)Mail subject          mailBody: >              (string)Mail body          to: >              (set<RsGxsId>)list of To: recipients          cc: >              (set<RsGxsId>)list of CC: recipients          bcc: >              (set<RsGxsId>)list of BCC: recipients          attachments: >              (vector<FileInfo>)list of suggested files  

try { 
    var result = api_instance.rsMsgsSendMail(reqSendMail);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsSendMail: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSendMail** | [**ReqSendMail**](ReqSendMail.md)| from: &gt;              (RsGxsId)GXS id of the author          subject: &gt;              (string)Mail subject          mailBody: &gt;              (string)Mail body          to: &gt;              (set&lt;RsGxsId&gt;)list of To: recipients          cc: &gt;              (set&lt;RsGxsId&gt;)list of CC: recipients          bcc: &gt;              (set&lt;RsGxsId&gt;)list of BCC: recipients          attachments: &gt;              (vector&lt;FileInfo&gt;)list of suggested files   | [optional] 

### Return type

[**ResSendMail**](ResSendMail.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsSendStatusString**
> rsMsgsSendStatusString(reqSendStatusString)

sendStatusString send a status string

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSendStatusString = ReqSendStatusString(); // ReqSendStatusString | id: >              (ChatId)chat id to send the status string to          status_string: >              (string)status string  

try { 
    api_instance.rsMsgsSendStatusString(reqSendStatusString);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsSendStatusString: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSendStatusString** | [**ReqSendStatusString**](ReqSendStatusString.md)| id: &gt;              (ChatId)chat id to send the status string to          status_string: &gt;              (string)status string   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsSetCustomStateString**
> rsMsgsSetCustomStateString(reqSetCustomStateString)

setCustomStateString set your custom status message

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetCustomStateString = ReqSetCustomStateString(); // ReqSetCustomStateString | status_string: >              (string)status message  

try { 
    api_instance.rsMsgsSetCustomStateString(reqSetCustomStateString);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsSetCustomStateString: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetCustomStateString** | [**ReqSetCustomStateString**](ReqSetCustomStateString.md)| status_string: &gt;              (string)status message   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsSetDefaultIdentityForChatLobby**
> ResSetDefaultIdentityForChatLobby rsMsgsSetDefaultIdentityForChatLobby(reqSetDefaultIdentityForChatLobby)

setDefaultIdentityForChatLobby set the default identity used for chat lobbies

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetDefaultIdentityForChatLobby = ReqSetDefaultIdentityForChatLobby(); // ReqSetDefaultIdentityForChatLobby | nick: >              (RsGxsId)chat identitiy to use  

try { 
    var result = api_instance.rsMsgsSetDefaultIdentityForChatLobby(reqSetDefaultIdentityForChatLobby);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsSetDefaultIdentityForChatLobby: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetDefaultIdentityForChatLobby** | [**ReqSetDefaultIdentityForChatLobby**](ReqSetDefaultIdentityForChatLobby.md)| nick: &gt;              (RsGxsId)chat identitiy to use   | [optional] 

### Return type

[**ResSetDefaultIdentityForChatLobby**](ResSetDefaultIdentityForChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsSetIdentityForChatLobby**
> ResSetIdentityForChatLobby rsMsgsSetIdentityForChatLobby(reqSetIdentityForChatLobby)

setIdentityForChatLobby set the chat identit

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetIdentityForChatLobby = ReqSetIdentityForChatLobby(); // ReqSetIdentityForChatLobby | lobby_id: >              (ChatLobbyId)lobby to change the chat idnetity for          nick: >              (RsGxsId)new chat identity  

try { 
    var result = api_instance.rsMsgsSetIdentityForChatLobby(reqSetIdentityForChatLobby);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsSetIdentityForChatLobby: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetIdentityForChatLobby** | [**ReqSetIdentityForChatLobby**](ReqSetIdentityForChatLobby.md)| lobby_id: &gt;              (ChatLobbyId)lobby to change the chat idnetity for          nick: &gt;              (RsGxsId)new chat identity   | [optional] 

### Return type

[**ResSetIdentityForChatLobby**](ResSetIdentityForChatLobby.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsSetLobbyAutoSubscribe**
> rsMsgsSetLobbyAutoSubscribe(reqSetLobbyAutoSubscribe)

setLobbyAutoSubscribe enable or disable auto subscribe for a chat lobby

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetLobbyAutoSubscribe = ReqSetLobbyAutoSubscribe(); // ReqSetLobbyAutoSubscribe | lobby_id: >              (ChatLobbyId)lobby to auto (un)subscribe          autoSubscribe: >              (boolean)set value for auto subscribe  

try { 
    api_instance.rsMsgsSetLobbyAutoSubscribe(reqSetLobbyAutoSubscribe);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsSetLobbyAutoSubscribe: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetLobbyAutoSubscribe** | [**ReqSetLobbyAutoSubscribe**](ReqSetLobbyAutoSubscribe.md)| lobby_id: &gt;              (ChatLobbyId)lobby to auto (un)subscribe          autoSubscribe: &gt;              (boolean)set value for auto subscribe   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsSetMessageTag**
> ResSetMessageTag rsMsgsSetMessageTag(reqSetMessageTag)

setMessageTag set == false && tagId == 0

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetMessageTag = ReqSetMessageTag(); // ReqSetMessageTag | msgId: >              (string)None         tagId: >              (integer)None         set: >              (boolean)None 

try { 
    var result = api_instance.rsMsgsSetMessageTag(reqSetMessageTag);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsSetMessageTag: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetMessageTag** | [**ReqSetMessageTag**](ReqSetMessageTag.md)| msgId: &gt;              (string)None         tagId: &gt;              (integer)None         set: &gt;              (boolean)None  | [optional] 

### Return type

[**ResSetMessageTag**](ResSetMessageTag.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsSetMessageTagType**
> ResSetMessageTagType rsMsgsSetMessageTagType(reqSetMessageTagType)

setMessageTagType

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetMessageTagType = ReqSetMessageTagType(); // ReqSetMessageTagType | tagId: >              (integer)None         text: >              (string)None         rgb_color: >              (integer)None 

try { 
    var result = api_instance.rsMsgsSetMessageTagType(reqSetMessageTagType);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsSetMessageTagType: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetMessageTagType** | [**ReqSetMessageTagType**](ReqSetMessageTagType.md)| tagId: &gt;              (integer)None         text: &gt;              (string)None         rgb_color: &gt;              (integer)None  | [optional] 

### Return type

[**ResSetMessageTagType**](ResSetMessageTagType.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsSystemMessage**
> ResSystemMessage rsMsgsSystemMessage(reqSystemMessage)

SystemMessage

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSystemMessage = ReqSystemMessage(); // ReqSystemMessage | title: >              (string)None         message: >              (string)None         systemFlag: >              (integer)None 

try { 
    var result = api_instance.rsMsgsSystemMessage(reqSystemMessage);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsSystemMessage: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSystemMessage** | [**ReqSystemMessage**](ReqSystemMessage.md)| title: &gt;              (string)None         message: &gt;              (string)None         systemFlag: &gt;              (integer)None  | [optional] 

### Return type

[**ResSystemMessage**](ResSystemMessage.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsMsgsUnsubscribeChatLobby**
> rsMsgsUnsubscribeChatLobby(reqUnsubscribeChatLobby)

unsubscribeChatLobby leave a chat lobby

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqUnsubscribeChatLobby = ReqUnsubscribeChatLobby(); // ReqUnsubscribeChatLobby | lobby_id: >              (ChatLobbyId)lobby to leave  

try { 
    api_instance.rsMsgsUnsubscribeChatLobby(reqUnsubscribeChatLobby);
} catch (e) {
    print("Exception when calling DefaultApi->rsMsgsUnsubscribeChatLobby: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqUnsubscribeChatLobby** | [**ReqUnsubscribeChatLobby**](ReqUnsubscribeChatLobby.md)| lobby_id: &gt;              (ChatLobbyId)lobby to leave   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersAcceptInvite**
> ResAcceptInvite rsPeersAcceptInvite(reqAcceptInvite)

Add trusted node from invite

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqAcceptInvite = ReqAcceptInvite(); // ReqAcceptInvite | invite: >              (string)invite string being it in cert or URL format          flags: >              (ServicePermissionFlags)service permissions flag  

try { 
    var result = api_instance.rsPeersAcceptInvite(reqAcceptInvite);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersAcceptInvite: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqAcceptInvite** | [**ReqAcceptInvite**](ReqAcceptInvite.md)| invite: &gt;              (string)invite string being it in cert or URL format          flags: &gt;              (ServicePermissionFlags)service permissions flag   | [optional] 

### Return type

[**ResAcceptInvite**](ResAcceptInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersAddFriend**
> ResAddFriend rsPeersAddFriend(reqAddFriend)

Add trusted node

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqAddFriend = ReqAddFriend(); // ReqAddFriend | sslId: >              (RsPeerId)SSL id of the node to add          gpgId: >              (RsPgpId)PGP id of the node to add          flags: >              (ServicePermissionFlags)service permissions flag  

try { 
    var result = api_instance.rsPeersAddFriend(reqAddFriend);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersAddFriend: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqAddFriend** | [**ReqAddFriend**](ReqAddFriend.md)| sslId: &gt;              (RsPeerId)SSL id of the node to add          gpgId: &gt;              (RsPgpId)PGP id of the node to add          flags: &gt;              (ServicePermissionFlags)service permissions flag   | [optional] 

### Return type

[**ResAddFriend**](ResAddFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersAddGroup**
> ResAddGroup rsPeersAddGroup(reqAddGroup)

addGroup create a new group

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqAddGroup = ReqAddGroup(); // ReqAddGroup | groupInfo: >              (RsGroupInfo)None 

try { 
    var result = api_instance.rsPeersAddGroup(reqAddGroup);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersAddGroup: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqAddGroup** | [**ReqAddGroup**](ReqAddGroup.md)| groupInfo: &gt;              (RsGroupInfo)None  | [optional] 

### Return type

[**ResAddGroup**](ResAddGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersAddPeerLocator**
> ResAddPeerLocator rsPeersAddPeerLocator(reqAddPeerLocator)

Add URL locator for given peer

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqAddPeerLocator = ReqAddPeerLocator(); // ReqAddPeerLocator | sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          locator: >              (RsUrl)peer url locator  

try { 
    var result = api_instance.rsPeersAddPeerLocator(reqAddPeerLocator);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersAddPeerLocator: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqAddPeerLocator** | [**ReqAddPeerLocator**](ReqAddPeerLocator.md)| sslId: &gt;              (RsPeerId)SSL id of the peer, own id is accepted too          locator: &gt;              (RsUrl)peer url locator   | [optional] 

### Return type

[**ResAddPeerLocator**](ResAddPeerLocator.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersAddSslOnlyFriend**
> ResAddSslOnlyFriend rsPeersAddSslOnlyFriend(reqAddSslOnlyFriend)

Add SSL-only trusted node When adding an SSL-only node, it is authorized to connect. Every time a connection is established the user is notified about the need to verify the PGP fingerprint, until she does, at that point the node become a full SSL+PGP friend.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqAddSslOnlyFriend = ReqAddSslOnlyFriend(); // ReqAddSslOnlyFriend | sslId: >              (RsPeerId)SSL id of the node to add          pgpId: >              (RsPgpId)PGP id of the node to add. Will be used for validation when the key is available.          details: >              (RsPeerDetails)Optional extra details known about the node to add  

try { 
    var result = api_instance.rsPeersAddSslOnlyFriend(reqAddSslOnlyFriend);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersAddSslOnlyFriend: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqAddSslOnlyFriend** | [**ReqAddSslOnlyFriend**](ReqAddSslOnlyFriend.md)| sslId: &gt;              (RsPeerId)SSL id of the node to add          pgpId: &gt;              (RsPgpId)PGP id of the node to add. Will be used for validation when the key is available.          details: &gt;              (RsPeerDetails)Optional extra details known about the node to add   | [optional] 

### Return type

[**ResAddSslOnlyFriend**](ResAddSslOnlyFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersAssignPeerToGroup**
> ResAssignPeerToGroup rsPeersAssignPeerToGroup(reqAssignPeerToGroup)

assignPeerToGroup add a peer to a group

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqAssignPeerToGroup = ReqAssignPeerToGroup(); // ReqAssignPeerToGroup | groupId: >              (RsNodeGroupId)None         peerId: >              (RsPgpId)None         assign: >              (boolean)true to assign a peer, false to remove a peer  

try { 
    var result = api_instance.rsPeersAssignPeerToGroup(reqAssignPeerToGroup);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersAssignPeerToGroup: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqAssignPeerToGroup** | [**ReqAssignPeerToGroup**](ReqAssignPeerToGroup.md)| groupId: &gt;              (RsNodeGroupId)None         peerId: &gt;              (RsPgpId)None         assign: &gt;              (boolean)true to assign a peer, false to remove a peer   | [optional] 

### Return type

[**ResAssignPeerToGroup**](ResAssignPeerToGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersAssignPeersToGroup**
> ResAssignPeersToGroup rsPeersAssignPeersToGroup(reqAssignPeersToGroup)

assignPeersToGroup add a list of peers to a group

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqAssignPeersToGroup = ReqAssignPeersToGroup(); // ReqAssignPeersToGroup | groupId: >              (RsNodeGroupId)None         peerIds: >              (list<RsPgpId>)None         assign: >              (boolean)true to assign a peer, false to remove a peer  

try { 
    var result = api_instance.rsPeersAssignPeersToGroup(reqAssignPeersToGroup);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersAssignPeersToGroup: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqAssignPeersToGroup** | [**ReqAssignPeersToGroup**](ReqAssignPeersToGroup.md)| groupId: &gt;              (RsNodeGroupId)None         peerIds: &gt;              (list&lt;RsPgpId&gt;)None         assign: &gt;              (boolean)true to assign a peer, false to remove a peer   | [optional] 

### Return type

[**ResAssignPeersToGroup**](ResAssignPeersToGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersConnectAttempt**
> ResConnectAttempt rsPeersConnectAttempt(reqConnectAttempt)

Trigger connection attempt to given node

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqConnectAttempt = ReqConnectAttempt(); // ReqConnectAttempt | sslId: >              (RsPeerId)SSL id of the node to connect  

try { 
    var result = api_instance.rsPeersConnectAttempt(reqConnectAttempt);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersConnectAttempt: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqConnectAttempt** | [**ReqConnectAttempt**](ReqConnectAttempt.md)| sslId: &gt;              (RsPeerId)SSL id of the node to connect   | [optional] 

### Return type

[**ResConnectAttempt**](ResConnectAttempt.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersEditGroup**
> ResEditGroup rsPeersEditGroup(reqEditGroup)

editGroup edit an existing group

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqEditGroup = ReqEditGroup(); // ReqEditGroup | groupId: >              (RsNodeGroupId)None         groupInfo: >              (RsGroupInfo)None 

try { 
    var result = api_instance.rsPeersEditGroup(reqEditGroup);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersEditGroup: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqEditGroup** | [**ReqEditGroup**](ReqEditGroup.md)| groupId: &gt;              (RsNodeGroupId)None         groupInfo: &gt;              (RsGroupInfo)None  | [optional] 

### Return type

[**ResEditGroup**](ResEditGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersGetFriendList**
> ResGetFriendList rsPeersGetFriendList()

Get trusted peers list

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsPeersGetFriendList();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersGetFriendList: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetFriendList**](ResGetFriendList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersGetGPGId**
> ResGetGPGId rsPeersGetGPGId(reqGetGPGId)

Get PGP id for the given peer

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetGPGId = ReqGetGPGId(); // ReqGetGPGId | sslId: >              (RsPeerId)SSL id of the peer  

try { 
    var result = api_instance.rsPeersGetGPGId(reqGetGPGId);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersGetGPGId: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetGPGId** | [**ReqGetGPGId**](ReqGetGPGId.md)| sslId: &gt;              (RsPeerId)SSL id of the peer   | [optional] 

### Return type

[**ResGetGPGId**](ResGetGPGId.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersGetGroupInfo**
> ResGetGroupInfo rsPeersGetGroupInfo(reqGetGroupInfo)

getGroupInfo get group information to one group

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetGroupInfo = ReqGetGroupInfo(); // ReqGetGroupInfo | groupId: >              (RsNodeGroupId)None 

try { 
    var result = api_instance.rsPeersGetGroupInfo(reqGetGroupInfo);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersGetGroupInfo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetGroupInfo** | [**ReqGetGroupInfo**](ReqGetGroupInfo.md)| groupId: &gt;              (RsNodeGroupId)None  | [optional] 

### Return type

[**ResGetGroupInfo**](ResGetGroupInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersGetGroupInfoByName**
> ResGetGroupInfoByName rsPeersGetGroupInfoByName(reqGetGroupInfoByName)

getGroupInfoByName get group information by group name

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetGroupInfoByName = ReqGetGroupInfoByName(); // ReqGetGroupInfoByName | groupName: >              (string)None 

try { 
    var result = api_instance.rsPeersGetGroupInfoByName(reqGetGroupInfoByName);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersGetGroupInfoByName: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetGroupInfoByName** | [**ReqGetGroupInfoByName**](ReqGetGroupInfoByName.md)| groupName: &gt;              (string)None  | [optional] 

### Return type

[**ResGetGroupInfoByName**](ResGetGroupInfoByName.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersGetGroupInfoList**
> ResGetGroupInfoList rsPeersGetGroupInfoList()

getGroupInfoList get list of all groups

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsPeersGetGroupInfoList();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersGetGroupInfoList: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetGroupInfoList**](ResGetGroupInfoList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersGetOnlineList**
> ResGetOnlineList rsPeersGetOnlineList()

Get connected peers list

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsPeersGetOnlineList();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersGetOnlineList: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetOnlineList**](ResGetOnlineList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersGetPeerDetails**
> ResGetPeerDetails rsPeersGetPeerDetails(reqGetPeerDetails)

Get details details of the given peer

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetPeerDetails = ReqGetPeerDetails(); // ReqGetPeerDetails | sslId: >              (RsPeerId)id of the peer  

try { 
    var result = api_instance.rsPeersGetPeerDetails(reqGetPeerDetails);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersGetPeerDetails: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetPeerDetails** | [**ReqGetPeerDetails**](ReqGetPeerDetails.md)| sslId: &gt;              (RsPeerId)id of the peer   | [optional] 

### Return type

[**ResGetPeerDetails**](ResGetPeerDetails.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersGetPeersCount**
> ResGetPeersCount rsPeersGetPeersCount(reqGetPeersCount)

Get peers count

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetPeersCount = ReqGetPeersCount(); // ReqGetPeersCount | countLocations: >              (boolean)true to count multiple locations of same owner  

try { 
    var result = api_instance.rsPeersGetPeersCount(reqGetPeersCount);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersGetPeersCount: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetPeersCount** | [**ReqGetPeersCount**](ReqGetPeersCount.md)| countLocations: &gt;              (boolean)true to count multiple locations of same owner   | [optional] 

### Return type

[**ResGetPeersCount**](ResGetPeersCount.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersGetPgpFriendList**
> ResGetPgpFriendList rsPeersGetPgpFriendList()

Get trusted PGP ids list

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsPeersGetPgpFriendList();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersGetPgpFriendList: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetPgpFriendList**](ResGetPgpFriendList.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersGetRetroshareInvite**
> ResGetRetroshareInvite rsPeersGetRetroshareInvite(reqGetRetroshareInvite)

Get RetroShare invite of the given peer

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetRetroshareInvite = ReqGetRetroshareInvite(); // ReqGetRetroshareInvite | sslId: >              (RsPeerId)Id of the peer of which we want to generate an invite, a null id (all 0) is passed, an invite for own node is returned.          includeSignatures: >              (boolean)true to add key signatures to the invite          includeExtraLocators: >              (boolean)false to avoid to add extra locators  

try { 
    var result = api_instance.rsPeersGetRetroshareInvite(reqGetRetroshareInvite);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersGetRetroshareInvite: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetRetroshareInvite** | [**ReqGetRetroshareInvite**](ReqGetRetroshareInvite.md)| sslId: &gt;              (RsPeerId)Id of the peer of which we want to generate an invite, a null id (all 0) is passed, an invite for own node is returned.          includeSignatures: &gt;              (boolean)true to add key signatures to the invite          includeExtraLocators: &gt;              (boolean)false to avoid to add extra locators   | [optional] 

### Return type

[**ResGetRetroshareInvite**](ResGetRetroshareInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersGetShortInvite**
> ResGetShortInvite rsPeersGetShortInvite(reqGetShortInvite)

Get RetroShare short invite of the given peer

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetShortInvite = ReqGetShortInvite(); // ReqGetShortInvite | sslId: >              (RsPeerId)Id of the peer of which we want to generate an invite, a null id (all 0) is passed, an invite for own node is returned.          formatRadix: >              (boolean)true to get in base64 format false to get URL.          bareBones: >              (boolean)true to get smallest invite, which miss also the information necessary to attempt an outgoing connection, but still enough to accept an incoming one.          baseUrl: >              (string)URL into which to sneak in the RetroShare invite radix, this is primarly useful to trick other applications into making the invite clickable, or to disguise the RetroShare invite into a \"normal\" looking web link. Used only if formatRadix is false.  

try { 
    var result = api_instance.rsPeersGetShortInvite(reqGetShortInvite);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersGetShortInvite: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetShortInvite** | [**ReqGetShortInvite**](ReqGetShortInvite.md)| sslId: &gt;              (RsPeerId)Id of the peer of which we want to generate an invite, a null id (all 0) is passed, an invite for own node is returned.          formatRadix: &gt;              (boolean)true to get in base64 format false to get URL.          bareBones: &gt;              (boolean)true to get smallest invite, which miss also the information necessary to attempt an outgoing connection, but still enough to accept an incoming one.          baseUrl: &gt;              (string)URL into which to sneak in the RetroShare invite radix, this is primarly useful to trick other applications into making the invite clickable, or to disguise the RetroShare invite into a \&quot;normal\&quot; looking web link. Used only if formatRadix is false.   | [optional] 

### Return type

[**ResGetShortInvite**](ResGetShortInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersIsFriend**
> ResIsFriend rsPeersIsFriend(reqIsFriend)

Check if given peer is a trusted node

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqIsFriend = ReqIsFriend(); // ReqIsFriend | sslId: >              (RsPeerId)id of the peer to check  

try { 
    var result = api_instance.rsPeersIsFriend(reqIsFriend);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersIsFriend: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqIsFriend** | [**ReqIsFriend**](ReqIsFriend.md)| sslId: &gt;              (RsPeerId)id of the peer to check   | [optional] 

### Return type

[**ResIsFriend**](ResIsFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersIsOnline**
> ResIsOnline rsPeersIsOnline(reqIsOnline)

Check if there is an established connection to the given peer

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqIsOnline = ReqIsOnline(); // ReqIsOnline | sslId: >              (RsPeerId)id of the peer to check  

try { 
    var result = api_instance.rsPeersIsOnline(reqIsOnline);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersIsOnline: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqIsOnline** | [**ReqIsOnline**](ReqIsOnline.md)| sslId: &gt;              (RsPeerId)id of the peer to check   | [optional] 

### Return type

[**ResIsOnline**](ResIsOnline.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersIsPgpFriend**
> ResIsPgpFriend rsPeersIsPgpFriend(reqIsPgpFriend)

Check if given PGP id is trusted

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqIsPgpFriend = ReqIsPgpFriend(); // ReqIsPgpFriend | pgpId: >              (RsPgpId)PGP id to check  

try { 
    var result = api_instance.rsPeersIsPgpFriend(reqIsPgpFriend);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersIsPgpFriend: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqIsPgpFriend** | [**ReqIsPgpFriend**](ReqIsPgpFriend.md)| pgpId: &gt;              (RsPgpId)PGP id to check   | [optional] 

### Return type

[**ResIsPgpFriend**](ResIsPgpFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersIsSslOnlyFriend**
> ResIsSslOnlyFriend rsPeersIsSslOnlyFriend(reqIsSslOnlyFriend)

Check if given peer is a trusted SSL node pending PGP approval Peers added through short invite remain in this state as long as their PGP key is not received and verified/approved by the user.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqIsSslOnlyFriend = ReqIsSslOnlyFriend(); // ReqIsSslOnlyFriend | sslId: >              (RsPeerId)id of the peer to check  

try { 
    var result = api_instance.rsPeersIsSslOnlyFriend(reqIsSslOnlyFriend);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersIsSslOnlyFriend: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqIsSslOnlyFriend** | [**ReqIsSslOnlyFriend**](ReqIsSslOnlyFriend.md)| sslId: &gt;              (RsPeerId)id of the peer to check   | [optional] 

### Return type

[**ResIsSslOnlyFriend**](ResIsSslOnlyFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersLoadCertificateFromString**
> ResLoadCertificateFromString rsPeersLoadCertificateFromString(reqLoadCertificateFromString)

Import certificate into the keyring

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqLoadCertificateFromString = ReqLoadCertificateFromString(); // ReqLoadCertificateFromString | cert: >              (string)string representation of the certificate  

try { 
    var result = api_instance.rsPeersLoadCertificateFromString(reqLoadCertificateFromString);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersLoadCertificateFromString: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqLoadCertificateFromString** | [**ReqLoadCertificateFromString**](ReqLoadCertificateFromString.md)| cert: &gt;              (string)string representation of the certificate   | [optional] 

### Return type

[**ResLoadCertificateFromString**](ResLoadCertificateFromString.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersLoadDetailsFromStringCert**
> ResLoadDetailsFromStringCert rsPeersLoadDetailsFromStringCert(reqLoadDetailsFromStringCert)

Examine certificate and get details without importing into the keyring

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqLoadDetailsFromStringCert = ReqLoadDetailsFromStringCert(); // ReqLoadDetailsFromStringCert | cert: >              (string)string representation of the certificate  

try { 
    var result = api_instance.rsPeersLoadDetailsFromStringCert(reqLoadDetailsFromStringCert);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersLoadDetailsFromStringCert: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqLoadDetailsFromStringCert** | [**ReqLoadDetailsFromStringCert**](ReqLoadDetailsFromStringCert.md)| cert: &gt;              (string)string representation of the certificate   | [optional] 

### Return type

[**ResLoadDetailsFromStringCert**](ResLoadDetailsFromStringCert.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersParseShortInvite**
> ResParseShortInvite rsPeersParseShortInvite(reqParseShortInvite)

Parse the give short invite to extract contained information

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqParseShortInvite = ReqParseShortInvite(); // ReqParseShortInvite | invite: >              (string)string containing the short invite to parse  

try { 
    var result = api_instance.rsPeersParseShortInvite(reqParseShortInvite);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersParseShortInvite: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqParseShortInvite** | [**ReqParseShortInvite**](ReqParseShortInvite.md)| invite: &gt;              (string)string containing the short invite to parse   | [optional] 

### Return type

[**ResParseShortInvite**](ResParseShortInvite.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersPgpIdFromFingerprint**
> ResPgpIdFromFingerprint rsPeersPgpIdFromFingerprint(reqPgpIdFromFingerprint)

Convert PGP fingerprint to PGP id

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqPgpIdFromFingerprint = ReqPgpIdFromFingerprint(); // ReqPgpIdFromFingerprint | fpr: >              (RsPgpFingerprint)PGP fingerprint to convert  

try { 
    var result = api_instance.rsPeersPgpIdFromFingerprint(reqPgpIdFromFingerprint);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersPgpIdFromFingerprint: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqPgpIdFromFingerprint** | [**ReqPgpIdFromFingerprint**](ReqPgpIdFromFingerprint.md)| fpr: &gt;              (RsPgpFingerprint)PGP fingerprint to convert   | [optional] 

### Return type

[**ResPgpIdFromFingerprint**](ResPgpIdFromFingerprint.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersRemoveFriend**
> ResRemoveFriend rsPeersRemoveFriend(reqRemoveFriend)

Revoke connection trust from to node

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRemoveFriend = ReqRemoveFriend(); // ReqRemoveFriend | pgpId: >              (RsPgpId)PGP id of the node  

try { 
    var result = api_instance.rsPeersRemoveFriend(reqRemoveFriend);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersRemoveFriend: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRemoveFriend** | [**ReqRemoveFriend**](ReqRemoveFriend.md)| pgpId: &gt;              (RsPgpId)PGP id of the node   | [optional] 

### Return type

[**ResRemoveFriend**](ResRemoveFriend.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersRemoveFriendLocation**
> ResRemoveFriendLocation rsPeersRemoveFriendLocation(reqRemoveFriendLocation)

Remove location of a trusted node, useful to prune old unused locations of a trusted peer without revoking trust

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRemoveFriendLocation = ReqRemoveFriendLocation(); // ReqRemoveFriendLocation | sslId: >              (RsPeerId)SSL id of the location to remove  

try { 
    var result = api_instance.rsPeersRemoveFriendLocation(reqRemoveFriendLocation);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersRemoveFriendLocation: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRemoveFriendLocation** | [**ReqRemoveFriendLocation**](ReqRemoveFriendLocation.md)| sslId: &gt;              (RsPeerId)SSL id of the location to remove   | [optional] 

### Return type

[**ResRemoveFriendLocation**](ResRemoveFriendLocation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersRemoveGroup**
> ResRemoveGroup rsPeersRemoveGroup(reqRemoveGroup)

removeGroup remove a group

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRemoveGroup = ReqRemoveGroup(); // ReqRemoveGroup | groupId: >              (RsNodeGroupId)None 

try { 
    var result = api_instance.rsPeersRemoveGroup(reqRemoveGroup);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersRemoveGroup: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRemoveGroup** | [**ReqRemoveGroup**](ReqRemoveGroup.md)| groupId: &gt;              (RsNodeGroupId)None  | [optional] 

### Return type

[**ResRemoveGroup**](ResRemoveGroup.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersSetDynDNS**
> ResSetDynDNS rsPeersSetDynDNS(reqSetDynDNS)

Set (dynamical) domain name associated to the given peer

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetDynDNS = ReqSetDynDNS(); // ReqSetDynDNS | sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          addr: >              (string)domain name string representation  

try { 
    var result = api_instance.rsPeersSetDynDNS(reqSetDynDNS);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersSetDynDNS: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetDynDNS** | [**ReqSetDynDNS**](ReqSetDynDNS.md)| sslId: &gt;              (RsPeerId)SSL id of the peer, own id is accepted too          addr: &gt;              (string)domain name string representation   | [optional] 

### Return type

[**ResSetDynDNS**](ResSetDynDNS.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersSetExtAddress**
> ResSetExtAddress rsPeersSetExtAddress(reqSetExtAddress)

Set external IPv4 address for given peer

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetExtAddress = ReqSetExtAddress(); // ReqSetExtAddress | sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          addr: >              (string)string representation of the external IPv4 address          port: >              (integer)external listening port  

try { 
    var result = api_instance.rsPeersSetExtAddress(reqSetExtAddress);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersSetExtAddress: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetExtAddress** | [**ReqSetExtAddress**](ReqSetExtAddress.md)| sslId: &gt;              (RsPeerId)SSL id of the peer, own id is accepted too          addr: &gt;              (string)string representation of the external IPv4 address          port: &gt;              (integer)external listening port   | [optional] 

### Return type

[**ResSetExtAddress**](ResSetExtAddress.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersSetLocalAddress**
> ResSetLocalAddress rsPeersSetLocalAddress(reqSetLocalAddress)

Set local IPv4 address for the given peer

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetLocalAddress = ReqSetLocalAddress(); // ReqSetLocalAddress | sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          addr: >              (string)string representation of the local IPv4 address          port: >              (integer)local listening port  

try { 
    var result = api_instance.rsPeersSetLocalAddress(reqSetLocalAddress);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersSetLocalAddress: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetLocalAddress** | [**ReqSetLocalAddress**](ReqSetLocalAddress.md)| sslId: &gt;              (RsPeerId)SSL id of the peer, own id is accepted too          addr: &gt;              (string)string representation of the local IPv4 address          port: &gt;              (integer)local listening port   | [optional] 

### Return type

[**ResSetLocalAddress**](ResSetLocalAddress.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersSetNetworkMode**
> ResSetNetworkMode rsPeersSetNetworkMode(reqSetNetworkMode)

Set network mode of the given peer

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetNetworkMode = ReqSetNetworkMode(); // ReqSetNetworkMode | sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          netMode: >              (integer)one of RS_NETMODE_*  

try { 
    var result = api_instance.rsPeersSetNetworkMode(reqSetNetworkMode);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersSetNetworkMode: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetNetworkMode** | [**ReqSetNetworkMode**](ReqSetNetworkMode.md)| sslId: &gt;              (RsPeerId)SSL id of the peer, own id is accepted too          netMode: &gt;              (integer)one of RS_NETMODE_*   | [optional] 

### Return type

[**ResSetNetworkMode**](ResSetNetworkMode.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPeersSetVisState**
> ResSetVisState rsPeersSetVisState(reqSetVisState)

set DHT and discovery modes

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetVisState = ReqSetVisState(); // ReqSetVisState | sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          vsDisc: >              (integer)one of RS_VS_DISC_*          vsDht: >              (integer)one of RS_VS_DHT_*  

try { 
    var result = api_instance.rsPeersSetVisState(reqSetVisState);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPeersSetVisState: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetVisState** | [**ReqSetVisState**](ReqSetVisState.md)| sslId: &gt;              (RsPeerId)SSL id of the peer, own id is accepted too          vsDisc: &gt;              (integer)one of RS_VS_DISC_*          vsDht: &gt;              (integer)one of RS_VS_DHT_*   | [optional] 

### Return type

[**ResSetVisState**](ResSetVisState.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsPostedRequestStatus**
> ResRequestStatus rsPostedRequestStatus(reqRequestStatus)

null

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqRequestStatus = ReqRequestStatus(); // ReqRequestStatus | token: >              (integer)None 

try { 
    var result = api_instance.rsPostedRequestStatus(reqRequestStatus);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsPostedRequestStatus: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqRequestStatus** | [**ReqRequestStatus**](ReqRequestStatus.md)| token: &gt;              (integer)None  | [optional] 

### Return type

[**ResRequestStatus**](ResRequestStatus.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsAutoPositiveOpinionForContacts**
> ResAutoPositiveOpinionForContacts rsReputationsAutoPositiveOpinionForContacts()

check if giving automatic positive opinion when flagging as contact is enbaled

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsReputationsAutoPositiveOpinionForContacts();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsAutoPositiveOpinionForContacts: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResAutoPositiveOpinionForContacts**](ResAutoPositiveOpinionForContacts.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsBanNode**
> rsReputationsBanNode(reqBanNode)

Enable automatic banning of all identities signed by the given node

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqBanNode = ReqBanNode(); // ReqBanNode | id: >              (RsPgpId)PGP id of the node          b: >              (boolean)true to enable, false to disable  

try { 
    api_instance.rsReputationsBanNode(reqBanNode);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsBanNode: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqBanNode** | [**ReqBanNode**](ReqBanNode.md)| id: &gt;              (RsPgpId)PGP id of the node          b: &gt;              (boolean)true to enable, false to disable   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsGetOwnOpinion**
> ResGetOwnOpinion rsReputationsGetOwnOpinion(reqGetOwnOpinion)

Get own opition about the given identity

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetOwnOpinion = ReqGetOwnOpinion(); // ReqGetOwnOpinion | id: >              (RsGxsId)Id of the identity  

try { 
    var result = api_instance.rsReputationsGetOwnOpinion(reqGetOwnOpinion);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsGetOwnOpinion: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetOwnOpinion** | [**ReqGetOwnOpinion**](ReqGetOwnOpinion.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**ResGetOwnOpinion**](ResGetOwnOpinion.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsGetReputationInfo**
> ResGetReputationInfo rsReputationsGetReputationInfo(reqGetReputationInfo)

Get reputation data of given identity

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetReputationInfo = ReqGetReputationInfo(); // ReqGetReputationInfo | id: >              (RsGxsId)Id of the identity          ownerNode: >              (RsPgpId)Optiona PGP id of the signed identity, accept a null (all zero/noninitialized) PGP id          stamp: >              (boolean)if true, timestamo the information  

try { 
    var result = api_instance.rsReputationsGetReputationInfo(reqGetReputationInfo);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsGetReputationInfo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetReputationInfo** | [**ReqGetReputationInfo**](ReqGetReputationInfo.md)| id: &gt;              (RsGxsId)Id of the identity          ownerNode: &gt;              (RsPgpId)Optiona PGP id of the signed identity, accept a null (all zero/noninitialized) PGP id          stamp: &gt;              (boolean)if true, timestamo the information   | [optional] 

### Return type

[**ResGetReputationInfo**](ResGetReputationInfo.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsIsIdentityBanned**
> ResIsIdentityBanned rsReputationsIsIdentityBanned(reqIsIdentityBanned)

This method allow fast checking if a GXS identity is banned.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqIsIdentityBanned = ReqIsIdentityBanned(); // ReqIsIdentityBanned | id: >              (RsGxsId)Id of the identity to check  

try { 
    var result = api_instance.rsReputationsIsIdentityBanned(reqIsIdentityBanned);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsIsIdentityBanned: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqIsIdentityBanned** | [**ReqIsIdentityBanned**](ReqIsIdentityBanned.md)| id: &gt;              (RsGxsId)Id of the identity to check   | [optional] 

### Return type

[**ResIsIdentityBanned**](ResIsIdentityBanned.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsIsNodeBanned**
> ResIsNodeBanned rsReputationsIsNodeBanned(reqIsNodeBanned)

Check if automatic banning of all identities signed by the given node is enabled

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqIsNodeBanned = ReqIsNodeBanned(); // ReqIsNodeBanned | id: >              (RsPgpId)PGP id of the node  

try { 
    var result = api_instance.rsReputationsIsNodeBanned(reqIsNodeBanned);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsIsNodeBanned: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqIsNodeBanned** | [**ReqIsNodeBanned**](ReqIsNodeBanned.md)| id: &gt;              (RsPgpId)PGP id of the node   | [optional] 

### Return type

[**ResIsNodeBanned**](ResIsNodeBanned.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsOverallReputationLevel**
> ResOverallReputationLevel rsReputationsOverallReputationLevel(reqOverallReputationLevel)

Get overall reputation level of given identity

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqOverallReputationLevel = ReqOverallReputationLevel(); // ReqOverallReputationLevel | id: >              (RsGxsId)Id of the identity  

try { 
    var result = api_instance.rsReputationsOverallReputationLevel(reqOverallReputationLevel);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsOverallReputationLevel: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqOverallReputationLevel** | [**ReqOverallReputationLevel**](ReqOverallReputationLevel.md)| id: &gt;              (RsGxsId)Id of the identity   | [optional] 

### Return type

[**ResOverallReputationLevel**](ResOverallReputationLevel.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsRememberBannedIdThreshold**
> ResRememberBannedIdThreshold rsReputationsRememberBannedIdThreshold()

Get number of days to wait before deleting a banned identity from local storage

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsReputationsRememberBannedIdThreshold();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsRememberBannedIdThreshold: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResRememberBannedIdThreshold**](ResRememberBannedIdThreshold.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsSetAutoPositiveOpinionForContacts**
> rsReputationsSetAutoPositiveOpinionForContacts(reqSetAutoPositiveOpinionForContacts)

Enable giving automatic positive opinion when flagging as contact

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetAutoPositiveOpinionForContacts = ReqSetAutoPositiveOpinionForContacts(); // ReqSetAutoPositiveOpinionForContacts | b: >              (boolean)true to enable, false to disable  

try { 
    api_instance.rsReputationsSetAutoPositiveOpinionForContacts(reqSetAutoPositiveOpinionForContacts);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsSetAutoPositiveOpinionForContacts: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetAutoPositiveOpinionForContacts** | [**ReqSetAutoPositiveOpinionForContacts**](ReqSetAutoPositiveOpinionForContacts.md)| b: &gt;              (boolean)true to enable, false to disable   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsSetOwnOpinion**
> ResSetOwnOpinion rsReputationsSetOwnOpinion(reqSetOwnOpinion)

Set own opinion about the given identity

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetOwnOpinion = ReqSetOwnOpinion(); // ReqSetOwnOpinion | id: >              (RsGxsId)Id of the identity          op: >              (RsOpinion)Own opinion  

try { 
    var result = api_instance.rsReputationsSetOwnOpinion(reqSetOwnOpinion);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsSetOwnOpinion: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetOwnOpinion** | [**ReqSetOwnOpinion**](ReqSetOwnOpinion.md)| id: &gt;              (RsGxsId)Id of the identity          op: &gt;              (RsOpinion)Own opinion   | [optional] 

### Return type

[**ResSetOwnOpinion**](ResSetOwnOpinion.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsSetRememberBannedIdThreshold**
> rsReputationsSetRememberBannedIdThreshold(reqSetRememberBannedIdThreshold)

Set number of days to wait before deleting a banned identity from local storage

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetRememberBannedIdThreshold = ReqSetRememberBannedIdThreshold(); // ReqSetRememberBannedIdThreshold | days: >              (integer)number of days to wait, 0 means never delete  

try { 
    api_instance.rsReputationsSetRememberBannedIdThreshold(reqSetRememberBannedIdThreshold);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsSetRememberBannedIdThreshold: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetRememberBannedIdThreshold** | [**ReqSetRememberBannedIdThreshold**](ReqSetRememberBannedIdThreshold.md)| days: &gt;              (integer)number of days to wait, 0 means never delete   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsSetThresholdForRemotelyNegativeReputation**
> rsReputationsSetThresholdForRemotelyNegativeReputation(reqSetThresholdForRemotelyNegativeReputation)

Set threshold on remote reputation to consider it remotely negative

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetThresholdForRemotelyNegativeReputation = ReqSetThresholdForRemotelyNegativeReputation(); // ReqSetThresholdForRemotelyNegativeReputation | thresh: >              (integer)Threshold value  

try { 
    api_instance.rsReputationsSetThresholdForRemotelyNegativeReputation(reqSetThresholdForRemotelyNegativeReputation);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsSetThresholdForRemotelyNegativeReputation: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetThresholdForRemotelyNegativeReputation** | [**ReqSetThresholdForRemotelyNegativeReputation**](ReqSetThresholdForRemotelyNegativeReputation.md)| thresh: &gt;              (integer)Threshold value   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsSetThresholdForRemotelyPositiveReputation**
> rsReputationsSetThresholdForRemotelyPositiveReputation(reqSetThresholdForRemotelyPositiveReputation)

Set threshold on remote reputation to consider it remotely positive

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqSetThresholdForRemotelyPositiveReputation = ReqSetThresholdForRemotelyPositiveReputation(); // ReqSetThresholdForRemotelyPositiveReputation | thresh: >              (integer)Threshold value  

try { 
    api_instance.rsReputationsSetThresholdForRemotelyPositiveReputation(reqSetThresholdForRemotelyPositiveReputation);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsSetThresholdForRemotelyPositiveReputation: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqSetThresholdForRemotelyPositiveReputation** | [**ReqSetThresholdForRemotelyPositiveReputation**](ReqSetThresholdForRemotelyPositiveReputation.md)| thresh: &gt;              (integer)Threshold value   | [optional] 

### Return type

void (empty response body)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsThresholdForRemotelyNegativeReputation**
> ResThresholdForRemotelyNegativeReputation rsReputationsThresholdForRemotelyNegativeReputation()

Get threshold on remote reputation to consider it remotely negative

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsReputationsThresholdForRemotelyNegativeReputation();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsThresholdForRemotelyNegativeReputation: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResThresholdForRemotelyNegativeReputation**](ResThresholdForRemotelyNegativeReputation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsReputationsThresholdForRemotelyPositiveReputation**
> ResThresholdForRemotelyPositiveReputation rsReputationsThresholdForRemotelyPositiveReputation()

Get threshold on remote reputation to consider it remotely negative

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsReputationsThresholdForRemotelyPositiveReputation();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsReputationsThresholdForRemotelyPositiveReputation: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResThresholdForRemotelyPositiveReputation**](ResThresholdForRemotelyPositiveReputation.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsServiceControlGetOwnServices**
> ResGetOwnServices rsServiceControlGetOwnServices()

get a map off all services.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();

try { 
    var result = api_instance.rsServiceControlGetOwnServices();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsServiceControlGetOwnServices: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResGetOwnServices**](ResGetOwnServices.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsServiceControlGetPeersConnected**
> ResGetPeersConnected rsServiceControlGetPeersConnected(reqGetPeersConnected)

getPeersConnected return peers using a service.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetPeersConnected = ReqGetPeersConnected(); // ReqGetPeersConnected | serviceId: >              (integer)service to look up.  

try { 
    var result = api_instance.rsServiceControlGetPeersConnected(reqGetPeersConnected);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsServiceControlGetPeersConnected: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetPeersConnected** | [**ReqGetPeersConnected**](ReqGetPeersConnected.md)| serviceId: &gt;              (integer)service to look up.   | [optional] 

### Return type

[**ResGetPeersConnected**](ResGetPeersConnected.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsServiceControlGetServiceItemNames**
> ResGetServiceItemNames rsServiceControlGetServiceItemNames(reqGetServiceItemNames)

getServiceItemNames return a map of service item names.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetServiceItemNames = ReqGetServiceItemNames(); // ReqGetServiceItemNames | serviceId: >              (integer)service to look up  

try { 
    var result = api_instance.rsServiceControlGetServiceItemNames(reqGetServiceItemNames);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsServiceControlGetServiceItemNames: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetServiceItemNames** | [**ReqGetServiceItemNames**](ReqGetServiceItemNames.md)| serviceId: &gt;              (integer)service to look up   | [optional] 

### Return type

[**ResGetServiceItemNames**](ResGetServiceItemNames.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsServiceControlGetServiceName**
> ResGetServiceName rsServiceControlGetServiceName(reqGetServiceName)

getServiceName lookup the name of a service.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetServiceName = ReqGetServiceName(); // ReqGetServiceName | serviceId: >              (integer)service to look up  

try { 
    var result = api_instance.rsServiceControlGetServiceName(reqGetServiceName);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsServiceControlGetServiceName: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetServiceName** | [**ReqGetServiceName**](ReqGetServiceName.md)| serviceId: &gt;              (integer)service to look up   | [optional] 

### Return type

[**ResGetServiceName**](ResGetServiceName.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsServiceControlGetServicePermissions**
> ResGetServicePermissions rsServiceControlGetServicePermissions(reqGetServicePermissions)

getServicePermissions return permissions of one service.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetServicePermissions = ReqGetServicePermissions(); // ReqGetServicePermissions | serviceId: >              (integer)service id to look up  

try { 
    var result = api_instance.rsServiceControlGetServicePermissions(reqGetServicePermissions);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsServiceControlGetServicePermissions: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetServicePermissions** | [**ReqGetServicePermissions**](ReqGetServicePermissions.md)| serviceId: &gt;              (integer)service id to look up   | [optional] 

### Return type

[**ResGetServicePermissions**](ResGetServicePermissions.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsServiceControlGetServicesAllowed**
> ResGetServicesAllowed rsServiceControlGetServicesAllowed(reqGetServicesAllowed)

getServicesAllowed return a mpa with allowed service information.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetServicesAllowed = ReqGetServicesAllowed(); // ReqGetServicesAllowed | peerId: >              (RsPeerId)peer to look up  

try { 
    var result = api_instance.rsServiceControlGetServicesAllowed(reqGetServicesAllowed);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsServiceControlGetServicesAllowed: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetServicesAllowed** | [**ReqGetServicesAllowed**](ReqGetServicesAllowed.md)| peerId: &gt;              (RsPeerId)peer to look up   | [optional] 

### Return type

[**ResGetServicesAllowed**](ResGetServicesAllowed.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsServiceControlGetServicesProvided**
> ResGetServicesProvided rsServiceControlGetServicesProvided(reqGetServicesProvided)

getServicesProvided return services provided by a peer.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqGetServicesProvided = ReqGetServicesProvided(); // ReqGetServicesProvided | peerId: >              (RsPeerId)peer to look up  

try { 
    var result = api_instance.rsServiceControlGetServicesProvided(reqGetServicesProvided);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsServiceControlGetServicesProvided: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqGetServicesProvided** | [**ReqGetServicesProvided**](ReqGetServicesProvided.md)| peerId: &gt;              (RsPeerId)peer to look up   | [optional] 

### Return type

[**ResGetServicesProvided**](ResGetServicesProvided.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rsServiceControlUpdateServicePermissions**
> ResUpdateServicePermissions rsServiceControlUpdateServicePermissions(reqUpdateServicePermissions)

updateServicePermissions update service permissions of one service.

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure HTTP basic authorization: BasicAuth
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'YOUR_USERNAME'
//defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'YOUR_PASSWORD';

var api_instance = DefaultApi();
var reqUpdateServicePermissions = ReqUpdateServicePermissions(); // ReqUpdateServicePermissions | serviceId: >              (integer)service to update          permissions: >              (RsServicePermissions)new permissions  

try { 
    var result = api_instance.rsServiceControlUpdateServicePermissions(reqUpdateServicePermissions);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->rsServiceControlUpdateServicePermissions: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reqUpdateServicePermissions** | [**ReqUpdateServicePermissions**](ReqUpdateServicePermissions.md)| serviceId: &gt;              (integer)service to update          permissions: &gt;              (RsServicePermissions)new permissions   | [optional] 

### Return type

[**ResUpdateServicePermissions**](ResUpdateServicePermissions.md)

### Authorization

[BasicAuth](../README.md#BasicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

