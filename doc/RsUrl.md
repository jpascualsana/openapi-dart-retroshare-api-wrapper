# openapi.model.RsUrl

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**schemeSeparator** | **String** |  | [optional] [default to null]
**ipv6WrapOpen** | **String** |  | [optional] [default to null]
**ipv6Separator** | **String** |  | [optional] [default to null]
**ipv6WrapClose** | **String** |  | [optional] [default to null]
**portSeparator** | **String** |  | [optional] [default to null]
**pathSeparator** | **String** |  | [optional] [default to null]
**querySeparator** | **String** |  | [optional] [default to null]
**queryAssign** | **String** |  | [optional] [default to null]
**queryFieldSep** | **String** |  | [optional] [default to null]
**fragmentSeparator** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


