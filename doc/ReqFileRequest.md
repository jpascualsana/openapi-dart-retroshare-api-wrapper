# openapi.model.ReqFileRequest

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileName** | **String** |  | [optional] [default to null]
**hash** | **String** |  | [optional] [default to null]
**size** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**destPath** | **String** |  | [optional] [default to null]
**flags** | **int** |  | [optional] [default to null]
**srcIds** | **List&lt;String&gt;** |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


