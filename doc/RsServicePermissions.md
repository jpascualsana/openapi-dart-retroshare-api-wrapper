# openapi.model.RsServicePermissions

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mServiceId** | **int** |  | [optional] [default to null]
**mServiceName** | **String** |  | [optional] [default to null]
**mDefaultAllowed** | **bool** |  | [optional] [default to null]
**mPeersAllowed** | **List&lt;String&gt;** |  | [optional] [default to []]
**mPeersDenied** | **List&lt;String&gt;** |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


