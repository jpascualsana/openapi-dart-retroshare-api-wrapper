# openapi.model.ResGetChannelAllContent

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **bool** |  | [optional] [default to null]
**posts** | [**List&lt;RsGxsChannelPost&gt;**](RsGxsChannelPost.md) |  | [optional] [default to []]
**comments** | [**List&lt;RsGxsComment&gt;**](RsGxsComment.md) |  | [optional] [default to []]
**votes** | [**List&lt;RsGxsVote&gt;**](RsGxsVote.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


