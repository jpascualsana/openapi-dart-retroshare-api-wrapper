# openapi.model.ResGetTrafficInfo

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**retval** | **int** |  | [optional] [default to null]
**outLst** | [**List&lt;RSTrafficClue&gt;**](RSTrafficClue.md) |  | [optional] [default to []]
**inLst** | [**List&lt;RSTrafficClue&gt;**](RSTrafficClue.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


