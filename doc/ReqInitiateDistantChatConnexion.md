# openapi.model.ReqInitiateDistantChatConnexion

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**toPid** | **String** |  | [optional] [default to null]
**fromPid** | **String** |  | [optional] [default to null]
**notify** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


