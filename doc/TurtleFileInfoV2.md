# openapi.model.TurtleFileInfoV2

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fSize** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**fHash** | **String** |  | [optional] [default to null]
**fName** | **String** |  | [optional] [default to null]
**fWeight** | **num** |  | [optional] [default to null]
**fSnippet** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


