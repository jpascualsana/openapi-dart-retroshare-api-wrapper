# openapi.model.RsMsgsMsgTagTypeTypes

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **int** |  | [optional] [default to null]
**value** | [**RsMsgsMsgTagTypeValue**](RsMsgsMsgTagTypeValue.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


