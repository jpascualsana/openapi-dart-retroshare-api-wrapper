# openapi.model.RsPeerServiceInfo

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mPeerId** | **String** |  | [optional] [default to null]
**mServiceList** | [**List&lt;RsPeerServiceInfoMServiceList&gt;**](RsPeerServiceInfoMServiceList.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


