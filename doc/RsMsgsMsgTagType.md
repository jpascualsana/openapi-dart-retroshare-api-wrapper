# openapi.model.RsMsgsMsgTagType

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**types** | [**List&lt;RsMsgsMsgTagTypeTypes&gt;**](RsMsgsMsgTagTypeTypes.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


