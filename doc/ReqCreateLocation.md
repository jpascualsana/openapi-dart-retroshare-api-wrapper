# openapi.model.ReqCreateLocation

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location** | [**RsLoginHelperLocation**](RsLoginHelperLocation.md) |  | [optional] [default to null]
**password** | **String** |  | [optional] [default to null]
**makeHidden** | **bool** |  | [optional] [default to null]
**makeAutoTor** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


