# openapi.model.RsMsgMetaData

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mGroupId** | **String** |  | [optional] [default to null]
**mMsgId** | **String** |  | [optional] [default to null]
**mThreadId** | **String** |  | [optional] [default to null]
**mParentId** | **String** |  | [optional] [default to null]
**mOrigMsgId** | **String** |  | [optional] [default to null]
**mAuthorId** | **String** |  | [optional] [default to null]
**mMsgName** | **String** |  | [optional] [default to null]
**mPublishTs** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**mMsgFlags** | **int** |  | [optional] [default to null]
**mMsgStatus** | **int** |  | [optional] [default to null]
**mChildTs** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**mServiceString** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


