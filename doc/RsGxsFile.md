# openapi.model.RsGxsFile

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mName** | **String** |  | [optional] [default to null]
**mHash** | **String** |  | [optional] [default to null]
**mSize** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


