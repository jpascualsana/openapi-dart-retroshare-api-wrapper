# openapi.model.ReqRequestDirDetails

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**handle** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**flags** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


