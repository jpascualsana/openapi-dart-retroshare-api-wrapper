# openapi.model.RsGxsChannelGroup

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mDescription** | **String** |  | [optional] [default to null]
**mImage** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] [default to null]
**mAutoDownload** | **bool** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


