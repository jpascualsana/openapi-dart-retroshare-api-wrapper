# openapi.model.RsFileTree

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mFiles** | [**List&lt;RsFileTreeFileData&gt;**](RsFileTreeFileData.md) |  | [optional] [default to []]
**mDirs** | [**List&lt;RsFileTreeDirData&gt;**](RsFileTreeDirData.md) |  | [optional] [default to []]
**mTotalFiles** | **int** |  | [optional] [default to null]
**mTotalSize** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


