# openapi.model.ResGetMessageCount

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nInbox** | **int** |  | [optional] [default to null]
**nInboxNew** | **int** |  | [optional] [default to null]
**nOutbox** | **int** |  | [optional] [default to null]
**nDraftbox** | **int** |  | [optional] [default to null]
**nSentbox** | **int** |  | [optional] [default to null]
**nTrashbox** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


