# openapi.model.GxsServiceStatistic

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mNumMsgs** | **int** |  | [optional] [default to null]
**mNumGrps** | **int** |  | [optional] [default to null]
**mSizeOfMsgs** | **int** |  | [optional] [default to null]
**mSizeOfGrps** | **int** |  | [optional] [default to null]
**mNumGrpsSubscribed** | **int** |  | [optional] [default to null]
**mNumThreadMsgsNew** | **int** |  | [optional] [default to null]
**mNumThreadMsgsUnread** | **int** |  | [optional] [default to null]
**mNumChildMsgsNew** | **int** |  | [optional] [default to null]
**mNumChildMsgsUnread** | **int** |  | [optional] [default to null]
**mSizeStore** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


