# openapi.model.RsFileTreeDirData

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] [default to null]
**subdirs** | [**List&lt;ReqBanFileFileSize&gt;**](ReqBanFileFileSize.md) |  | [optional] [default to []]
**subfiles** | [**List&lt;ReqBanFileFileSize&gt;**](ReqBanFileFileSize.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


