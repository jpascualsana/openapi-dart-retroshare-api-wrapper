# openapi.model.ResExportCollectionLinkRetval

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorNumber** | **int** |  | [optional] [default to null]
**errorCategory** | **String** |  | [optional] [default to null]
**errorMessage** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


