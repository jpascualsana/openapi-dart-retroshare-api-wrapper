# openapi.model.ReqCreateIdentity

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] [default to null]
**avatar** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] [default to null]
**pseudonimous** | **bool** |  | [optional] [default to null]
**pgpPassword** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


