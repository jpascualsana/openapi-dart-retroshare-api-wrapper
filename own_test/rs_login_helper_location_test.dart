import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for RespRsLoginHelperAttemptLogin
void main() {

  group('test ReqRsLoginHelperAttemptLogin', () {
    // RsInitLoadCertificateStatus retval (default value: null)
    test('to test attempt login', () async {
      try { 
        var instance = ReqAttemptLogin();
        instance.password = "prueba";
        instance.account = "814228577bc0c5da968c79272adcbfce";
        print(instance.account.runtimeType);
        var api_instance = DefaultApi();
        api_instance.rsLoginHelperAttemptLogin(reqAttemptLogin : instance)
          .then((onValue){
            print(onValue.retval.value);
          });
      } catch (e) {
        print("Exception when calling DefaultApi->rsLoginHelperAttemptLogin: $e\n");
      }

    });


  });

}
