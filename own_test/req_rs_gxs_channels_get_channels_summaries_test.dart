import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for rsGxsChannelsGetChannelsSummaries
void main() {

  group('test rsGxsChannelsGetChannelsSummaries', () {
    // RsInitLoadCertificateStatus retval (default value: null)
    test('to test rsGxsChannelsGetChannelsSummaries', () async {
      try { 

        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'test';
        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'test';

        var api_instance = DefaultApi();
        api_instance.rsGxsChannelsGetChannelsSummaries().then((onValue){
          print(onValue);
        });
      } catch (e) {
        print("Exception when calling DefaultApi->rsGxsChannelsGetChannelsSummaries: $e\n");
      }

    });


  });

}
