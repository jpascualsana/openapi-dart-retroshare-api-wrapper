import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for rsGxsChannelsCreateChannelV2
void main() {

  group('test rsGxsChannelsCreateChannelV2', () {
    // RsInitLoadCertificateStatus retval (default value: null)
    test('to test rsGxsChannelsCreateChannelV2', () async {
      try { 

        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'test';
        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'test';

        var reqRsGxsChannelsCreateChannelV2 = ReqCreateChannelV2(); // ReqRsGxsChannelsCreateChannelV2 | name: >              (string)Name of the channel          description: >              (string)Description of the channel          thumbnail: >              (RsGxsImage)Optional image to show as channel thumbnail.          authorId: >              (RsGxsId)Optional id of the author. Leave empty for an anonymous channel.          circleType: >              (RsGxsCircleType)Optional visibility rule, default public.          circleId: >              (RsGxsCircleId)If the channel is not public specify the id of the circle who can see the channel. Depending on the value you pass for circleType this should be be an external circle if EXTERNAL is passed, a local friend group id if NODES_GROUP is passed, empty otherwise.
        reqRsGxsChannelsCreateChannelV2.name = "Dart test channel v2";
        reqRsGxsChannelsCreateChannelV2.description = "Dart test channel v2 description";
        // reqRsGxsChannelsCreateChannelV2.thumbnail = RsGxsImage
        // reqRsGxsChannelsCreateChannelV2.authorId = RsGxsId
        // reqRsGxsChannelsCreateChannelV2.circleType = RsGxsCircleType
        // reqRsGxsChannelsCreateChannelV2.circleId = RsGxsCircleId
        var api_instance = DefaultApi();
        api_instance.rsGxsChannelsCreateChannelV2(reqCreateChannelV2 : reqRsGxsChannelsCreateChannelV2)
          .then((onValue){
            print(onValue);
        });
      } catch (e) {
        print("Exception when calling DefaultApi->rsGxsChannelsCreateChannelV2: $e\n");
      }

    });


  });

}
