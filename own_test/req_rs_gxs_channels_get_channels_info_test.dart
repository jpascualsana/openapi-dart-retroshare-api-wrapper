import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for reqRsGxsChannelsGetChannelsInfo
void main() {

  group('test reqRsGxsChannelsGetChannelsInfo', () {
    // RsInitLoadCertificateStatus retval (default value: null)
    test('to test reqRsGxsChannelsGetChannelsInfo', () async {
      try { 

        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'test';
        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'test';

        List<String> chanIds = ["1c168611e3a364b8dae8879abf628d07"];

        var reqRsGxsChannelsGetChannelsInfo = ReqGetChannelsInfo();
        reqRsGxsChannelsGetChannelsInfo.chanIds = chanIds;

        var api_instance = DefaultApi();
        api_instance.rsGxsChannelsGetChannelsInfo(
            reqGetChannelsInfo: reqRsGxsChannelsGetChannelsInfo)
          .then((onValue){
            print(onValue.channelsInfo[0].mDescription);
        });
      } catch (e) {
        print("Exception when calling DefaultApi->reqRsGxsChannelsGetChannelsInfo: $e\n");
      }

    });


  });

}
