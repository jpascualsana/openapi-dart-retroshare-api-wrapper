import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for ReqRsIdentityUpdateIdentity
void main() {

  group('test ReqRsIdentityUpdateIdentity', () {
    // RsInitLoadCertificateStatus retval (default value: null)
    test('to test ReqRsIdentityUpdateIdentity', () async {
      try { 

        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'test';
        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'test';
        var api_instance = DefaultApi();

        // For debug
        // First get all the identites
        // var ids = await api_instance.rsIdentityGetOwnSignedIds();
        // Or 
        // var ids = await api_instance.rsIdentityGetIdentitiesSummaries();
        // for (var item in ids.ids) {
        //   print(item);
        // }

        // Get the list of Identities info you want to edit
        String id = "175598f57711851b2258b0be8325c0a5";
        var reqRsIdentityGetIdentitiesInfo = ReqGetIdentitiesInfo();
        reqRsIdentityGetIdentitiesInfo.ids = [id];
        var idList
          = await api_instance.rsIdentityGetIdentitiesInfo(reqGetIdentitiesInfo: reqRsIdentityGetIdentitiesInfo);

        RsGxsIdGroup reqIdInfo;
        for (var idInfo in idList.idsInfo) {
          // Get the info of the Id that we want, if we check for one will be in position [0]
          if(idInfo.mMeta.mGroupId == id){
            reqIdInfo = idInfo;
          }
        }

        // Modify the id info
        print("Change: " + reqIdInfo.mMeta.mGroupName);
        reqIdInfo.mMeta.mGroupName = "DartIdentity modified";
        print("To: " +reqIdInfo.mMeta.mGroupName);

        // And update it against the backend
        ReqUpdateIdentity reqRsIdentityUpdateIdentity = ReqUpdateIdentity();
        reqRsIdentityUpdateIdentity.identityData = reqIdInfo;

        api_instance.rsIdentityUpdateIdentity(reqUpdateIdentity: reqRsIdentityUpdateIdentity)
          .then((onValue){
            print(onValue);
        });

      } catch (e) {
        print("Exception when calling DefaultApi->ReqRsIdentityUpdateIdentity: $e\n");
      }

    });


  });

}
